/**
 * This is the base class for a controller class.
 * 
 * This controller class should be extended by a class for each controller in the backend.
 * 
 * The function names of those controllers should be mapped with the function names they
 * are given in the back-end as well in order to easily find what we're looking for here.
 * 
 * @type T the entity type in the database (use `any` when the controller is not mapped to an entity.)
 * 
 * @author Stan Hurks
 */
export default abstract class Controller {

    /**
     * The api base url for the controller, e.g.: '/project'
     */
    public abstract apiBaseURL: string
}