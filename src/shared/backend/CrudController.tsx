import Controller from './Controller'

/**
 * This is the base class for a CRUD controller class.
 * 
 * This controller class should be extended by a class for each controller in the backend that is mapped
 * directly to an entity (e.g.: ProjectController).
 * 
 * Additional to the base class this class will contain basic CRUD operations for entity controllers.
 * 
 * The function names of those controllers should be mapped with the function names they
 * are given in the back-end as well in order to easily find what we're looking for here.
 * 
 * @type ENTITY_TYPE the entity type in the database
 * 
 * @author Stan Hurks
 */
export default abstract class CrudController<ENTITY_TYPE> extends Controller {

}