/**
 * An HTTP response.
 * 
 * @author Stan Hurks
 */
export default interface HttpResponse<T> {
    /**
     * The HTTP status code
     */
    status: number
    
    /**
     * The data
     */
    data: T

    /**
     * The response headers
     */
    headers: {[key: string]: string}
}