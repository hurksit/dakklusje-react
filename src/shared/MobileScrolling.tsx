import Device from './Device'

/**
 * The mobile scrolling manager, will give the user a better experience
 * when scrolling on mobile devices.
 * 
 * @author Stan Hurks
 */
export default class MobileScrolling {
    /**
     * The main instance
     */
    public static instance: MobileScrolling

    /**
     * Whether the scroll hack was disabled externally
     */
    public disabledExternally: boolean = false

    /**
     * Start position of the touch event
     */
    private startPosition: {x: number, y: number} = {
        x: 0,
        y: 0
    }

    /**
     * Whether or not the browser has support for the passive event listening option
     */
    private supportsPassiveOption: boolean = false

    /**
     * Whether or not the scroll hack is enabled
     */
    private _enabled: boolean = false

    public get enabled(): boolean {
        return this._enabled
    }

    constructor() {
        this.checkPassiveOptionSupport()
    }

    /**
     * Enable the hack
     */
    public enable = (): void => {
        window.addEventListener('touchstart', this.onTouchstart as any, this.supportsPassiveOption 
            ? { passive : false } : false)
        window.addEventListener('touchmove', this.onTouchmove as any, this.supportsPassiveOption 
            ? { passive : false } : false)
        this._enabled = true
    }

    /**
     * Disable the hack
     */
    public disable = (): void => {
        window.removeEventListener('touchstart', this.onTouchstart as any, false)
        window.removeEventListener('touchmove', this.onTouchmove as any, false)
        this._enabled = false
    }

    /**
     * Checks if the browser has support for the passive option
     */
    public checkPassiveOptionSupport = (): void => {
        const testEventListener = (): void => {}
        try {
            const opts = Object.defineProperty({}, 'passive', {
                // eslint-disable-next-line
                get: () => {
                    this.supportsPassiveOption = true
                }
            })
            window.addEventListener('test', testEventListener, opts)
        } catch (ex) {
            console.error(ex)
            window.removeEventListener('test', testEventListener)
        }
        window.removeEventListener('test', testEventListener)
    }

    /**
     * When the touch move starts
     * @param {Event} event the event
     */
    public onTouchmove = (event: TouchEvent & MouseEvent): void => {
        try {
            let element: HTMLElement = event.target as HTMLElement
            if (element === null) {
                return
            }

            // Check all parent elements for scrollability
            while (element !== document.body) {
                const style = window.getComputedStyle(element)
                if (!style) {
                    break
                }
    
                // Ignore range input element
                if (element.nodeName === 'INPUT' && element.getAttribute('type') === 'range') {
                    return
                }
    
                const webkitOverflowScrolling = style.getPropertyValue('-webkit-overflow-scrolling')
                const overflowX = style.getPropertyValue('overflow-x')
                const overflowY = style.getPropertyValue('overflow-y')
                const height = parseInt(style.getPropertyValue('height'), 10)
                const width = parseInt(style.getPropertyValue('width'), 10)
    
                // Determine if the element should scroll
                const isScrollable = 
                    (
                        (Device.isIPhone && webkitOverflowScrolling === 'touch')
                        ||
                        (!Device.isIPhone)
                    )
                    &&
                    (
                        ['auto', 'scroll'].indexOf(overflowY) !== -1
                        || ['auto', 'scroll'].indexOf(overflowX) !== -1
                    )
                    &&
                    (' ' + element.className + ' ').indexOf('scroll-hack') !== -1

                const canScrollVertically = element.scrollHeight > element.offsetHeight
                const canScrollHorizontally = element.scrollWidth > element.offsetWidth
    
                if (isScrollable && (canScrollHorizontally || canScrollVertically)) {
                    // Get the current X, Y position of the touch
                    const currentPosition = {
                        x: event.touches ? event.touches[0].screenX : event.screenX,
                        y: event.touches ? event.touches[0].screenY : event.screenY
                    }
    
                    // Determine if the user is trying to scroll past the top, bottom, left or right
                    const isAtTop = (this.startPosition.y <= currentPosition.y && element.scrollTop <= 0)
                    const isAtBottom = (this.startPosition.y >= currentPosition.y 
                        && element.scrollHeight - element.scrollTop === height)
                    const isAtLeft = (this.startPosition.x <= currentPosition.x && element.scrollLeft <= 0)
                    const isAtRight = (this.startPosition.x >= currentPosition.x 
                        && element.scrollWidth - element.scrollLeft === width)
                    
                    // Check if the scroll is vertical
                    const isVertical = (Math.abs(currentPosition.x - this.startPosition.x) 
                        < Math.abs(currentPosition.y - this.startPosition.y))
    
                    // Stop a bounce bug when scrolled past its boundaries
                    if ((canScrollVertically && isVertical && (isAtTop || isAtBottom)) 
                        || (canScrollHorizontally && !isVertical && (isAtLeft || isAtRight))) {
                        event.preventDefault()
                    }
    
                    // No need to continue up the DOM
                    return
                }
    
                // Test the next parent
                if (element.parentElement) {
                    element = element.parentElement
                } else {
                    break
                }
            }
    
            // Stop the bouncing -- no parents are scrollable
            event.preventDefault()
        } catch (exception) {
            console.error('Error in touch move event: ', exception)
        }
    }

    /**
     * When the user starts the touch event
     * @param event the event
     */
    public onTouchstart = (event: MouseEvent & TouchEvent): void => {
        // Make sure the user can not zoom
        if (event.touches.length !== 1) {
            event.preventDefault()
            return
        }
        this.startPosition = {
            x: event.touches ? event.touches[0].screenX : event.screenX,
            y: event.touches ? event.touches[0].screenY : event.screenY
        }
    }
}
