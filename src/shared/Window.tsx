/**
 * All cross browser / device window functionality.
 * 
 * @author Stan Hurks
 */
export default class Window {
    
    /**
     * Scroll the window
     */
    public static readonly scrollTo = (options: ScrollToOptions, callback?: () => void) => {
        const onScroll = () => {
            const scrollLeft = window.scrollX || window.pageXOffset
            const scrollTop = window.scrollY || window.pageYOffset
            const deltaX = options.left === undefined ? 0 : scrollLeft - options.left
            const deltaY = options.top === undefined ? 0 : scrollTop - options.top

            if (Math.abs(deltaX) < 1 && Math.abs(deltaY) < 1) {
                window.removeEventListener('scroll', onScroll)
                if (callback) {
                    callback()
                }
            }
        }
        window.addEventListener('scroll', onScroll)
        onScroll()
        
        try {
            window.scrollTo(options)
        } catch (e) {
            console.error('Could not scroll window with options, trying older variant.', e)
            window.scrollTo(options.left || 0, options.top || 0)
        }
    }
}