import { Subscription } from 'rxjs'
import Events from './Events'

/**
 * Manage all CSS variables in here.
 * 
 * @author Stan Hurks
 */
export default class CSSVariables {
    /**
     * The subscription for the window.resize event.
     */
    private static subscriptionWindowResize: Subscription = 
        Events.window.resize.subscribe(CSSVariables.onWindowResize)

    /**
     * Whenever the window resizes
     */
    private static onWindowResize(): void {
        CSSVariables.processViewportUnits()
    }

    /**
     * Process the viewport units
     */
    private static processViewportUnits(): void {
        // Update the viewport units
        const vh = window.innerHeight * 0.01
        const vw = window.innerWidth * 0.01
        document.documentElement.style.setProperty('--vh', `${vh}px`);
        document.documentElement.style.setProperty('--vw', `${vw}px`);
    }

    public static initialize(): void {
        CSSVariables.processViewportUnits()
    }

    public static cleanUp(): void {
        CSSVariables.subscriptionWindowResize.unsubscribe()
    }
}