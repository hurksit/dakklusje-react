import { RegisterAsRoofSpecialistCompanyDetailsState } from '../../../pages/non-authorized-pages/register-as-roof-specialist/company-details/RegisterAsRoofSpecialistCompanyDetails'
import { RegisterAsRoofSpecialistCocState } from '../../../pages/non-authorized-pages/register-as-roof-specialist/coc/RegisterAsRoofSpecialistCoc'
import { RegisterAsRoofSpecialistContactPersonState } from '../../../pages/non-authorized-pages/register-as-roof-specialist/contact-person/RegisterAsRoofSpecialistContactPerson';
import { RegisterAsRoofSpecialistLoginDetailsState } from '../../../pages/non-authorized-pages/register-as-roof-specialist/login-details/RegisterAsRoofSpecialistLoginDetails';

/**
 * The data in local storage for the RegisterAsRoofSpecialistPage
 * 
 * @author Stan Hurks
 */
export default class StoragePageRegisterAsRoofSpecialistDataModel {

    /**
     * The data for the company details tab
     */
    public companyDetails!: RegisterAsRoofSpecialistCompanyDetailsState

    /**
     * The data for the coc tab
     */
    public coc!: RegisterAsRoofSpecialistCocState

    /**
     * The contact person data
     */
    public contactPerson!: RegisterAsRoofSpecialistContactPersonState

    /**
     * The login details data
     */
    public loginDetails!: RegisterAsRoofSpecialistLoginDetailsState

    constructor() {
        this.companyDetails = {
            branche: null,

            companyDescription: null
        }

        this.coc = {
            cocNumber: null,

            cocCompany: null
        }

        this.contactPerson = {
            fullName: null,

            phoneNumber: null
        }

        this.loginDetails = {
            emailAddress: null,
            password: null,
            passwordRepeat: null,
            acceptedTermsAndConditions: false
        }
    }
}