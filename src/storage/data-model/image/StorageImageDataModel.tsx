import Storage from '../../Storage'
import ImageResponse from '../../../backend/response/entity/ImageResponse'

/**
 * The data model in storage for an image.
 * 
 * @author Stan Hurks
 */
export default class StorageImageDataModel {

    /**
     * The image url
     */
    private _imageURL!: string

    public get imageURL(): string {
        return this._imageURL
    }

    public set imageURL(imageURL: string) {
        this._imageURL = imageURL
        Storage.update.next()
    }

    /**
     * The background position
     */
    private _backgroundPosition!: string

    public get backgroundPosition(): string {
        return this._backgroundPosition
    }

    public set backgroundPosition(backgroundPosition: string) {
        this._backgroundPosition = backgroundPosition
        Storage.update.next()
    }

    /**
     * The zoom
     */
    private _zoom!: number

    public get zoom(): number {
        return this._zoom
    }

    public set zoom(zoom: number) {
        this._zoom = zoom
        Storage.update.next()
    }

    constructor(image: ImageResponse) {
        this.imageURL = image.file.content
        this.backgroundPosition = image.backgroundPosition
        this.zoom = image.zoom
    }
}