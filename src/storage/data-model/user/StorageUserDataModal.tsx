import Storage from '../../Storage'
import StorageImageDataModel from '../image/StorageImageDataModel'
import StorageConsumerDataModel from '../consumer/StorageConsumerDataModel'
import StorageRoofSpecialistDataModel from '../roof-specialist/StorageRoofSpecialistDataModel'
import AuthorizedUserResponse from '../../../backend/response/entity/AuthorizedUserResponse'

/**
 * The data model for the user in the storage.
 * 
 * @author Stan Hurks
 */
export default class StorageUserDataModal {
    /**
     * The user id
     */
    private _id?: string | null

    public get id(): string | null | undefined {
        return this._id
    }

    public set id(id: string | null | undefined) {
        this._id = id
        Storage.update.next()
    }

    /**
     * The name of the user (consumer full name or roof specialist company name)
     */
    private _name?: string | null

    public get name(): string | null | undefined {
        return this._name
    }

    public set name(name: string | null | undefined) {
        this._name = name
        Storage.update.next()
    }

    /**
     * The postal code
     */
    private _postalCode?: string

    public get postalCode(): string | undefined {
        return this._postalCode
    }

    public set postalCode(postalCode: string | undefined) {
        this._postalCode = postalCode
        Storage.update.next()
    }

    /**
     * The postal code latitude coordinate
     */
    private _postalCodeLatitude?: number

    public get postalCodeLatitude(): number | undefined {
        return this._postalCodeLatitude
    }

    public set postalCodeLatitude(postalCodeLatitude: number | undefined) {
        this._postalCodeLatitude = postalCodeLatitude
        Storage.update.next()
    }

    /**
     * The postal code longitude coordinate
     */
    private _postalCodeLongitude?: number

    public get postalCodeLongitude(): number | undefined {
        return this._postalCodeLongitude
    }

    public set postalCodeLongitude(postalCodeLongitude: number | undefined) {
        this._postalCodeLongitude = postalCodeLongitude
        Storage.update.next()
    }

    /**
     * The email address
     */
    private _email?: string

    public get email(): string | undefined {
        return this._email
    }

    public set email(email: string | undefined) {
        this._email = email
        Storage.update.next()
    }

    /**
     * The language of the user.
     */
    private _locale?: string

    public get locale(): string | undefined {
        return this._locale
    }

    public set locale(locale: string | undefined) {
        this._locale = locale
        Storage.update.next()
    }

    /**
     * The roof specialist data
     */
    public roofSpecialist?: StorageRoofSpecialistDataModel

    /**
     * The consumer data
     */
    public consumer?: StorageConsumerDataModel

    /**
     * The profile picture data
     */
    public profilePicture?: StorageImageDataModel

    constructor(user?: AuthorizedUserResponse) {
        if (user) {
            this.id = user.id
            this.email = user.emailAddress
            this.locale = user.locale
    
            if (user.profilePicture) {
                this.profilePicture = new StorageImageDataModel(user.profilePicture)
            }
            if (user.consumer) {
                this.name = user.consumer.personalDetailsFullName
                this.consumer = new StorageConsumerDataModel(user.consumer)
                this.postalCode = user.consumer.personalDetailsPostalCode.postalCode
                this.postalCodeLatitude = user.consumer.personalDetailsPostalCode.latitude
                this.postalCodeLongitude = user.consumer.personalDetailsPostalCode.longitude
            }
            if (user.roofSpecialist) {
                this.name = user.roofSpecialist.contactPersonName
                this.roofSpecialist = new StorageRoofSpecialistDataModel(user.roofSpecialist)
                this.postalCode = user.roofSpecialist.cocCompany.postalCode.postalCode
                this.postalCodeLatitude = user.roofSpecialist.cocCompany.postalCode.latitude
                this.postalCodeLongitude = user.roofSpecialist.cocCompany.postalCode.longitude
            }
        }
    }
}