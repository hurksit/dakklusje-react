import StorageSessionDataModel from './session/StorageSessionDataModel'
import StoragePageAddJobDataModel from './page/StoragePageAddJobDataModel'
import StoragePageRegisterAsRoofSpecialistDataModel from './register-as-roof-specialist/StoragePageRegisterAsRoofSpecialistDataModel'
import Storage from '../Storage'

/**
 * The data structure for the local storage.
 */
export default class StorageDataModel {

    /**
     * All session data.
     * 
     * Although this is definitely typed, it will not always be filled.
     * 
     * Only use this property in authorized 
     */
    public session: StorageSessionDataModel = new StorageSessionDataModel()

    /**
     * All page data
     */
    public page: {
        addJob: StoragePageAddJobDataModel

        registerAsRoofSpecialist: StoragePageRegisterAsRoofSpecialistDataModel
    } = {
        addJob: new StoragePageAddJobDataModel(),

        registerAsRoofSpecialist: new StoragePageRegisterAsRoofSpecialistDataModel()
    }

    /**
     * Whether or not the browser notification has been sent in regards
     * to the best user experience.
     */
    private _browserNotificationSent: boolean = false

    public get browserNotificationSent(): boolean {
        return this._browserNotificationSent
    }

    public set browserNotificationSent(browserNotificationSent: boolean) {
        this._browserNotificationSent = browserNotificationSent
        Storage.update.next()
    }

    /**
     * The token for firebase
     */
    private _firebaseToken: string|null = null

    public get firebaseToken(): string|null {
        return this._firebaseToken
    }

    public set firebaseToken(firebaseToken: string|null) {
        this._firebaseToken = firebaseToken
        Storage.update.next()
    }
}