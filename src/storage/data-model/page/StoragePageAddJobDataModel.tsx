import { AddJobJobState } from '../../../pages/conditional-authorized-pages/add-job/content/job/AddJobJob'
import { AddJobPersonalDetailsState } from '../../../pages/conditional-authorized-pages/add-job/content/personal-details/AddJobPersonalDetails'
import { AddJobPicturesState } from '../../../pages/conditional-authorized-pages/add-job/content/pictures/AddJobPictures'
import { AddJobLoginCredentialsState } from '../../../pages/conditional-authorized-pages/add-job/content/login-credentials/AddJobLoginCredentials'

/**
 * The data model for the page `AddJob` in Storage.
 * 
 * @author Stan Hurks
 */
export default class StoragePageAddJobDataModel {
    /**
     * The data for the job tab in the add job page.
     */
    public job!: AddJobJobState

    /**
     * The personal details 
     */
    public personalDetails!: AddJobPersonalDetailsState

    /**
     * The pictures
     */
    public pictures!: AddJobPicturesState

    /**
     * The login details.
     * 
     * No worries, no passwords are stored in here.
     */
    public loginDetails!: AddJobLoginCredentialsState

    constructor() {
        this.job = {
            title: null,
            branche: null,
            roofType: null,
            jobCategory: null,
            houseType: null,
            roofMaterialType: null,
            roofIsolationType: null,
            description: null,
            surfaceMeasurements: null,
            measurementUnit: null
        }

        this.personalDetails = {
            fullName: null,
            postalCode: null
        }

        this.pictures = {
            tempImages: [],
            jobImages: []
        }

        this.loginDetails = {
            emailAddress: null,
            password: null,
            passwordRepeat: null,
            acceptedTermsAndConditions: false
        }
    }
}