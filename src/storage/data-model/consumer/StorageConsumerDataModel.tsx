import Storage from '../../Storage'
import AuthorizedConsumerResponse from '../../../backend/response/entity/AuthorizedConsumerResponse'

/**
 * The data model for the consumer in a Storage.
 * 
 * @author Stan Hurks
 */
export default class StorageConsumerDataModel {

    /**
     * The ID of the consumer
     */
    private _id?: string

    public get id(): string | undefined {
        return this._id
    }

    public set id(id: string | undefined) {
        this._id = id
        Storage.update.next()
    }

    constructor(consumer: AuthorizedConsumerResponse) {
        this.id = consumer.id
    }
}