import Storage from '../../Storage'
import StorageUserDataModal from '../user/StorageUserDataModal'
import AuthorizedSessionResponse from '../../../backend/response/entity/AuthorizedSessionResponse'

/**
 * The session data model for storage.
 * 
 * @author Stan Hurks
 */
export default class StorageSessionDataModel {

    /**
     * The ID of the session
     */
    private _uuid?: string

    public get uuid(): string | undefined {
        return this._uuid
    }

    public set uuid(uuid: string | undefined) {
        this._uuid = uuid
        Storage.update.next()
    }

    /**
     * The user data
     */
    public user!: StorageUserDataModal

    constructor(session?: AuthorizedSessionResponse) {
        if (session) {
            this.uuid = session.uuid
            this.user = new StorageUserDataModal(session.user)
        } else {
            this.user = new StorageUserDataModal()
        }
    }
}