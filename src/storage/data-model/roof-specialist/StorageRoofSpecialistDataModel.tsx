import Storage from '../../Storage'
import AuthorizedRoofSpecialistResponse from '../../../backend/response/entity/AuthorizedRoofSpecialistResponse'
import RoofSpecialistFilterResponse from '../../../backend/response/entity/RoofSpecialistFilterResponse';

/**
 * The data model for the roof specialist in a Storage.
 * 
 * @author Stan Hurks
 */
export default class StorageRoofSpecialistDataModel {

    /**
     * The ID of the roof specialist
     */
    private _id?: string

    public get id(): string | undefined {
        return this._id
    }

    public set id(id: string | undefined) {
        this._id = id
        Storage.update.next()
    }

    /**
     * Whether the profile is 100% complete
     */
    private _profileComplete?: boolean

    public get profileComplete(): boolean | undefined {
        return this._profileComplete
    }

    public set profileComplete(profileComplete: boolean | undefined) {
        this._profileComplete = profileComplete
        Storage.update.next()
    }

    /**
     * The roof specialist filters
     */
    public filters: RoofSpecialistFilterResponse[] = []

    constructor(roofSpecialist: AuthorizedRoofSpecialistResponse) {
        this.id = roofSpecialist.id
        this.filters = roofSpecialist.filters
        this.profileComplete = roofSpecialist.portfolioImages.length > 0
            && roofSpecialist.paymentType !== null
    }
}