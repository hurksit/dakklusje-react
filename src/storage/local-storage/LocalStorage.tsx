import StorageDataModel from '../data-model/StorageDataModel'
import { Subject, Subscription } from 'rxjs'
import Storage from '../Storage'

/**
 * The local storage manager class.
 * 
 * @author Stan Hurks
 */
export default abstract class LocalStorage {

    /**
     * The name of the object in the local storage
     */
    public static readonly storageName: string = 'data'

    /**
     * Update the local storage.
     * 
     * This needs to be called within every setter in the data model.
     */
    public static readonly update: Subject<void> = new Subject()

    /**
     * The default local storage settings can be set in this object.
     * 
     * During runtime this member will be deeply watched for changes.
     * If anything changes the local storage object will be updated
     * automatically.
     */
    public static data: StorageDataModel
    
    /**
     * The subscription to the update subject
     */
    protected static subscriptionUpdate: Subscription

    /**
     * Initialize the component.
     */
    public static initialize = () => {

        // Initialize the data member
        LocalStorage.data = new StorageDataModel()

        // Parse the existing data from local storage.
        const rawData = localStorage.getItem(LocalStorage.storageName)
        const data = rawData === null ? null : JSON.parse(rawData)

        // Merge the data out of local storage in the `LocalStorage.data` member.
        if (data !== null) {
            try {
                // Merge the data from storage into `LocalStorage.data`
                Storage.recurseDataPropertiesAndMergeInDataMember(data, LocalStorage.data)
            } catch (ex) {
                console.error(ex, 'Resetting local storage data, the data stored was not valid.')
            }
        }

        // Subscribe to the update function so the data gets updated and saved automatically in local storage.
        // When a key changes in the data model!
        if (!LocalStorage.subscriptionUpdate) {
            LocalStorage.subscriptionUpdate = LocalStorage.update.subscribe(LocalStorage.onUpdate)
        }
    }

    /**
     * Clean up the local storage class
     */
    public static cleanUp() {
        LocalStorage.subscriptionUpdate.unsubscribe()
    }

    /**
     * Whenever the update subject is triggered.
     * 
     * Save the object in the local storage.
     */
    public static onUpdate() {
        // After the set operation has been done (current Thread loop doing that operation)..
        // ..save the updated data model object in the local storage.
        setTimeout(() => {
            localStorage.setItem(LocalStorage.storageName, JSON.stringify(LocalStorage.data))
        })
    }

    /**
     * Clear the local storage
     */
    public static clear() {
        localStorage.removeItem(LocalStorage.storageName)
        LocalStorage.subscriptionUpdate.unsubscribe()
        LocalStorage.initialize()
    }
}