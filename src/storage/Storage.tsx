import StorageDataModel from './data-model/StorageDataModel'
import Objects from '../shared/Objects'
import SessionStorage from './session-storage/SessionStorage'
import LocalStorage from './local-storage/LocalStorage'
import { Subject, Subscription } from 'rxjs'

/**
 * The storage base class.
 * 
 * This class contains shared pieces of functionality between storages (e.g.: LocalStorage, SessionStorage).
 * 
 * @author Stan Hurks
 */
export default abstract class Storage {

    /**
     * Update the local storage.
     * 
     * This needs to be called within every setter in the data model.
     */
    public static readonly update: Subject<void> = new Subject()

    /**
     * This returns the static `data` member on the automatically
     * determined subtype of Storage.
     */
    public static get data(): StorageDataModel {
        return SessionStorage.data.session.uuid
            ? SessionStorage.data
            : LocalStorage.data
    }
    
    /**
     * The subscription to the update subject
     */
    protected static subscriptionUpdate: Subscription

    /**
     * Initialize the storages
     */
    public static initialize = () => {

        if (!Storage.subscriptionUpdate) {
            // Subscribe to the update function so the data gets updated and saved automatically in all storages.
            // When a key changes in the data model!
            Storage.subscriptionUpdate = Storage.update.subscribe(Storage.onUpdate)
        }

        // Initialize all the subtype storage classes
        LocalStorage.initialize()
        SessionStorage.initialize()
    }

    /**
     * Clean up the storage classes
     */
    public static cleanUp() {
        Storage.subscriptionUpdate.unsubscribe()

        LocalStorage.cleanUp()
        SessionStorage.cleanUp()
    }

    /**
     * Clear the storages
     */
    public static clear() {
        LocalStorage.clear()
        SessionStorage.clear()
    }

    /**
     * Recurse through all the data properties found in the object stored in the storage.
     * If the property name is also found in the data model structure then the following happens:
     * 
     * If it's an object the function recurses.
     * If it's a primitive value / array the function sets the value in the member of the storage object.
     * 
     * @param data The data object from the storage.
     * @param referenceStorageData a reference to the data object stored in the sub type.
     */
    public static recurseDataPropertiesAndMergeInDataMember(data: any, referenceStorageData: any) {
        for (const propertyName of Object.keys(data)) {
            const propertyValue = data[propertyName]

            // The property name is found in storage data
            if (Objects.isObject(propertyValue)) {

                // If it is an object in session storage, recurse again and replace primitive values..
                referenceStorageData[propertyName] = Storage.recurseDataPropertiesAndMergeInDataMember(data[propertyName], referenceStorageData[propertyName] || {})
            } else {

                // ..Otherwise just replace the value
                referenceStorageData[propertyName] = propertyValue
            }
        }
        return referenceStorageData
    }

    /**
     * Whenever the update subject is triggered.
     * 
     * Save the data for all subtypes of Storage.
     */
    protected static onUpdate() {
        LocalStorage.update.next()
        SessionStorage.update.next()
    }
}