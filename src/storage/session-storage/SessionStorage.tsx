import StorageDataModel from '../data-model/StorageDataModel'
import { Subject, Subscription } from 'rxjs'
import Storage from '../Storage'

 /**
 * The session storage manager class.
 * 
 * @author Stan Hurks
 */
export default abstract class SessionStorage {

    /**
     * The name of the object in the session storage
     */
    public static readonly storageName: string = 'data'

    /**
     * Update the session storage.
     * 
     * This needs to be called within every setter in the data model.
     */
    public static readonly update: Subject<void> = new Subject()
    
    /**
     * The default session storage settings can be set in this object.
     * 
     * During runtime this member will be deeply watched for changes.
     * If anything changes the session storage object will be updated
     * automatically.
     */
    public static data: StorageDataModel

    /**
     * The subscription to the update subject
     */
    protected static subscriptionUpdate: Subscription

    /**
     * Initialize the component.
     */
    public static initialize() {

        // Initialize the data member
        SessionStorage.data = new StorageDataModel()

        // Parse the existing data from session storage.
        const rawData = sessionStorage.getItem(SessionStorage.storageName)
        const data = rawData === null ? null : JSON.parse(rawData)

        // Merge the data out of session storage in the `SessionStorage.data` member.
        if (data !== null) {
            try {
                // Merge the data from storage into `SessionStorage.data`
                Storage.recurseDataPropertiesAndMergeInDataMember(data, SessionStorage.data)
            } catch (ex) {
                console.error(ex, 'Resetting session storage data, the data stored was not valid.')
            }
        }

        if (!SessionStorage.subscriptionUpdate) {
            // Subscribe to the update function so the data gets updated and saved automatically in session storage.
            // When a key changes in the data model!
            SessionStorage.subscriptionUpdate = SessionStorage.update.subscribe(SessionStorage.onUpdate)
        }
    }

    /**
     * Clean up the session storage class
     */
    public static cleanUp() {
        SessionStorage.subscriptionUpdate.unsubscribe()
    }

    /**
     * Whenever the update subject is triggered.
     * 
     * Save the object in the session storage.
     */
    public static onUpdate() {
        // After the set operation has been done (current Thread loop doing that operation)..
        // ..save the updated data model object in the session storage.
        setTimeout(() => {
            sessionStorage.setItem(SessionStorage.storageName, JSON.stringify(SessionStorage.data))
        })
    }

    /**
     * Clear the session storage
     */
    public static clear() {
        sessionStorage.removeItem(SessionStorage.storageName)
        SessionStorage.subscriptionUpdate.unsubscribe()
        SessionStorage.initialize()
    }
}