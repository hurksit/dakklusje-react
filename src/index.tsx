import React from 'react'
import ReactDOM from 'react-dom'
import App from './core/app/App'
import './shared/index.scss'
import './index.scss'
import messaging from './firebase-messaging'
import registerServiceWorker, { unregister } from './registerServiceWorker'
import Device from './shared/Device'

// Mount the React DOM
ReactDOM.render(<App />, document.getElementById('root'))

// Register the service worker
if (Device.isMobile) {
    registerServiceWorker()
} else {
    unregister()
}

// Initialize push messages
if ('serviceWorker' in navigator && messaging !== null) {
    navigator.serviceWorker
        .register('sw.js')
        .then((registration) => {
            if (messaging !== null) {
                messaging.useServiceWorker(registration)
                console.info('Registration successful, scope is:', registration.scope)
            }
        })
        .catch((error) => {
            console.error('Firebases registration failed:', error)
        })
}