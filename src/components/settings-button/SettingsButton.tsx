import React from 'react'
import classNames from 'classnames'
import { Settings } from '@material-ui/icons'
import './settings-button.scss'

/**
 * The props
 */
interface SettingsButtonProps {

    /**
     * Whether the button is disabled
     */
    disabled?: boolean

    /**
     * The onclick callback
     */
    onClick?: () => void
}

/**
 * The state
 */
interface SettingsButtonState {

    /**
     * Whether the button is clicked
     */
    clicked: boolean
}

/**
 * The settings button.
 * 
 * @author Stan Hurks
 */
export default class SettingsButton extends React.Component<SettingsButtonProps, SettingsButtonState> {

    constructor(props: any) {
        super(props)

        this.state = {
            clicked: false
        }
    }

    public render = () => {
        return (
            <div className="settings-button">
                <button 
                    className={classNames({
                        'settings-button-button': true,
                        'clicked': this.state.clicked
                    })}
                    disabled={this.props.disabled === true}
                    onClick={(e) => {
                        if (this.props.disabled || !this.props.onClick) {
                            e.preventDefault()
                            return
                        }

                        this.props.onClick()
                    }}
                    onMouseDown={() => {
                        this.setState({
                            clicked: true
                        })
                    }} onMouseUp={() => {
                        this.setState({
                            clicked: false
                        })
                    }}>
                    <div className="settings-button-button-content">
                        <Settings />
                    </div>
                </button>
            </div>
        )
    }
}