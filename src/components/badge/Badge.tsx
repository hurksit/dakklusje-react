import React from 'react'
import './badge.scss'
import { Check } from '@material-ui/icons'
import classNames from 'classnames'

/**
 * The props
 */
interface BadgeProps {

    /**
     * The value to display in the badge
     */
    value: 'done'|number

    /**
     * Which color the badge should be. Default: orange
     */
    color?: 'orange'|'grey'

    /**
     * Whether to show the full number rather than `9+`
     */
    showFullNumber?: boolean
}

/**
 * A badge with a number
 * 
 * @author Stan Hurks
 */
export default class Badge extends React.Component<BadgeProps> {

    public render = () => {
        return (
            <div
                className={classNames({
                    'badge': true,
                    ['color-' + (this.props.color || 'orange')]: true
                })}
                style={{
                    width: this.props.value === 'done' ? 22 : 15 + 7 * String(this.getParsedValue()).length
                }}
                >
                {
                    this.getParsedValue()
                }
            </div>
        )
    }

    /**
     * Get the parsed value
     */
    private getParsedValue = () => {
        if (this.props.value === 'done') {
            return (
                <Check />
            )
        } else if (this.props.value > 9 && !this.props.showFullNumber) {
            return '9+'
        } else {
            return this.props.value.toFixed(0)
        }
    }
}