import React from 'react'
import './collapse.scss'
import Button from '../button/Button'
import Translations from '../../translations/Translations'
import { Subscription } from 'rxjs'
import Events from '../../shared/Events'

/**
 * The props
 */
interface CollapseProps {

    /**
     * The title
     */
    title: string

    /**
     * The description
     */
    description: string

    /**
     * Whether or not the collapse is opened
     */
    opened?: boolean
}

/**
 * The state
 */
interface CollapseState {

    /**
     * Whether or not the collapse is opened
     */
    opened: boolean

    /**
     * The height of the child elements inside of the menu.
     */
    menuHeight: number
}

/**
 * The collapse.
 * 
 * @author Stan Hurks
 */
export default class Collapse extends React.Component<CollapseProps, CollapseState> {

    /**
     * The pixels per second the menu transition should take to open the menu
     * more fluently.
     */
    public static readonly MENU_TRANSITION_PIXELS_PER_SECOND = 1200

    /**
     * The reference to the menu element
     */
    private menuContentRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The subscription for the window resize event
     */
    private subscriptionWindowResize!: Subscription

    constructor(props: any) {
        super(props)
        
        this.state = {
            opened: this.props.opened || false,

            menuHeight: 0
        }
    }

    public componentDidMount = () => {
        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
        this.onWindowResize()
    }

    public componentWillUnmount = () => {
        this.subscriptionWindowResize.unsubscribe()
    }

    public componentWillReceiveProps = () => {
        if (this.props.opened !== undefined) {
            this.setState({
                opened: this.props.opened
            })
        }
        this.onWindowResize()
    }

    public render = () => {
        return (
            <div className="collapse">
                <div className="collapse-top">
                    <div className="collapse-top-left">
                        <h1 className="collapse-top-left-title">
                            {
                                this.props.title
                            }
                        </h1><div className="collapse-top-left-description">
                            {
                                this.props.description
                            }
                        </div>
                    </div><div className="collapse-top-right">
                        <Button color={
                            this.state.opened
                            ? "orange"
                            : "white"
                        } size="small" onClick={() => {
                            this.setState({
                                opened: !this.state.opened
                            })
                        }}>
                            {
                                this.state.opened
                                ? Translations.translations.modals.defaultButtons.close
                                : Translations.translations.modals.defaultButtons.open
                            }
                        </Button>
                    </div>
                </div><div className="collapse-menu" style={{
                    ...this.state.opened
                    ? {
                        height: this.state.menuHeight
                    }
                    : {
                        height: 0
                    },

                    transition: `all ${this.state.menuHeight / Collapse.MENU_TRANSITION_PIXELS_PER_SECOND}s ease-in-out`,
                    MozTransition: `all ${this.state.menuHeight / Collapse.MENU_TRANSITION_PIXELS_PER_SECOND}s ease-in-out`,
                    OTransition: `all ${this.state.menuHeight / Collapse.MENU_TRANSITION_PIXELS_PER_SECOND}s ease-in-out`,
                    WebkitTransition: `all ${this.state.menuHeight / Collapse.MENU_TRANSITION_PIXELS_PER_SECOND}s ease-in-out`
                }}>
                    <div className="collapse-menu-content" ref={this.menuContentRef}>
                        <div className="collapse-menu-content-hr"></div>
                        {
                            this.props.children
                        }
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the window changes dimensions.
     */
    private onWindowResize = () => {
        setTimeout(() => {
            if (!this.menuContentRef.current) {
                return
            }
            this.setState({
                menuHeight: this.menuContentRef.current.offsetHeight
            })
        })
    }
}