import React from 'react'
import classNames from 'classnames'
import './navigation-button.scss'

/**
 * The props
 */
interface NavigationButtonProps {

    /**
     * Whether or not the button is selected
     */
    selected: boolean

    /**
     * The route to navigate to on click
     */
    href: string
}

/**
 * A button in the navigation
 * 
 * @author Stan Hurks
 */
export default class NavigationButton extends React.Component<NavigationButtonProps> {

    public render = () => {
        return (
            <a className={classNames({
                'navigation-button': true,
                'selected': this.props.selected
            })} href={this.props.href}>
                {
                    this.props.children
                }
            </a>
        )
    }
}