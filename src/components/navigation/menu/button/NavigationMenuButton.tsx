import React from 'react'
import './navigation-menu-button.scss'
import Button from '../../../button/Button'
import classNames from 'classnames'
import Navigation from '../../Navigation'
import App from '../../../../core/app/App'
import { Subscription } from 'rxjs'
import WebSocket from '../../../../backend/WebSocket'

/**
 * The props
 */
interface NavigationMenuButtonProps {
    
    /**
     * Whether or not the navigation menu is opened
     */
    isMenuOpen: boolean

    /**
     * Whether or not the navigation menu is closed AND has been opened after mounting.
     */
    isMenuClosed: boolean
}

/**
 * The state
 */
interface NavigationMenuButtonState {

    /**
     * The inbox badge count
     */
    inboxBadgeCount: number
}

/**
 * The button for the navigation menu.
 * 
 * @author Stan Hurks
 */
export default class NavigationMenuButton extends React.Component<NavigationMenuButtonProps, NavigationMenuButtonState> {

    /**
     * The subscription to the inbox badge count
     */
    private subscriptionInboxBadgeCount!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            inboxBadgeCount: App.inboxBadgeCount
        }
    }

    public componentDidMount = () => {
        this.subscriptionInboxBadgeCount = WebSocket.userInboxBadgeCount.subscribe(this.onUserInboxBadgeCount)
    }

    public componentWillUnmount = () => {
        this.subscriptionInboxBadgeCount.unsubscribe()
    }

    public render = () => {
        return (
            <div className="navigation-menu-button">
                <Button color={this.props.isMenuOpen ? "orange" : "white"} onClick={() => {
                    if (this.props.isMenuOpen) {
                        this.closeNavigationMenu()
                    } else {
                        this.openNavigationMenu()
                    }
                }} badge={this.state.inboxBadgeCount === 0 ? undefined : this.state.inboxBadgeCount}>
                    <div className={classNames({
                        'navigation-menu-button-bars': true,
                        'is-menu-open': this.props.isMenuOpen,
                        'is-menu-closed': this.props.isMenuClosed
                    })}>
                        <div className="navigation-menu-button-bars-bar"></div>
                        <div className="navigation-menu-button-bars-bar"></div>
                        <div className="navigation-menu-button-bars-bar"></div>
                    </div>
                </Button>
            </div>
        )
    }

    /**
     * Opens the navigation menu
     */
    private openNavigationMenu = () => {
        Navigation.open.next()
    }

    /**
     * Closes the navigation me\nu
     */
    private closeNavigationMenu = () => {
        Navigation.close.next()
    }

    /**
     * Whenever the badge count changes
     */
    private onUserInboxBadgeCount = (badgeCount: number) => {
        this.setState({
            inboxBadgeCount: badgeCount
        })
    }
}
