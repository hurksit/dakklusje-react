import React from 'react'
import './navigation-menu-mobile.scss'
import classNames from 'classnames'
import Button from '../../../button/Button'
import Navigation from '../../Navigation'
import { Subject, Subscription } from 'rxjs'
import Translations from '../../../../translations/Translations'
import NavigationMenuModal from '../../../modal/navigation/menu/NavigationMenuModal'
import Authorization from '../../../../core/Authorization'
import App from '../../../../core/app/App';
import WebSocket from '../../../../backend/WebSocket';

/**
 * The props
 */
interface NavigationMenuMobileProps {
    
    /**
     * Whether or not the navigation menu is opened
     */
    isMenuOpen: boolean

    /**
     * Whether or not the navigation menu is closed AND has been opened after mounting.
     */
    isMenuClosed: boolean
}

/**
 * The state
 */
interface NavigationMenuMobileState {
    /**
     * Cube transition properties for switching between the
     * app and navigation main menu
     */
    cubeTransition: {
        index: number

        toIndex: number|null

        fromIndex: number|null
    }

    /**
     * The inbox badge count
     */
    inboxBadgeCount: number
}

/**
 * The navigation menu for mobile.
 * 
 * @author Stan Hurks
 */
export default class NavigationMenuMobile extends React.Component<NavigationMenuMobileProps, NavigationMenuMobileState> {

    /**
     * The subject to open the navigation
     */
    public static readonly open: Subject<void> = new Subject()

    /**
     * The subject to close the navigation
     */
    public static readonly close: Subject<void> = new Subject()

    /**
     * The subscription to the inbox badge count
     */
    private subscriptionInboxBadgeCount!: Subscription

    constructor(props: any) {
        super(props)
        
        this.state = {
            cubeTransition: {
                index: 0,

                toIndex: null,

                fromIndex: null
            },

            inboxBadgeCount: App.inboxBadgeCount
        }
    }

    public componentDidMount = () => {
        this.subscriptionInboxBadgeCount = WebSocket.userInboxBadgeCount.subscribe(this.onUserInboxBadgeCount)
    }

    public componentWillUnmount = () => {
        this.subscriptionInboxBadgeCount.unsubscribe()
    }

    public render = () => {
        return (
            <div className={classNames({
                'navigation-menu-mobile': true,
                'is-menu-open': this.props.isMenuOpen,
                'is-menu-closed': this.props.isMenuClosed,
                'animation-cube': true,
                'animation-cube-left-to-right': this.state.cubeTransition.toIndex !== null && this.state.cubeTransition.fromIndex !== null
                    && this.state.cubeTransition.toIndex < this.state.cubeTransition.fromIndex,
                'animation-cube-right-to-left': this.state.cubeTransition.toIndex !== null && this.state.cubeTransition.fromIndex !== null
                    && this.state.cubeTransition.toIndex > this.state.cubeTransition.fromIndex
            })}>
                <div className="cube">
                    <div className={classNames({
                        'navigation-menu-mobile-main-menu': true,
                        'active': this.state.cubeTransition.index === 0
                    })}>
                        <div className="navigation-menu-mobile-main-menu-top">
                            <Button color="white" onClick={() => {
                                this.goToMenu()
                            }} badge={this.state.inboxBadgeCount === 0 ? undefined : this.state.inboxBadgeCount}>
                                {
                                    Authorization.isAuthorized()
                                    ?
                                    Translations.translations.components.navigation.buttons.menu
                                    :
                                    Translations.translations.components.navigation.buttons.login
                                }
                            </Button>
                        </div><div className="navigation-menu-mobile-main-menu-hr">
                        </div><div className="navigation-menu-mobile-main-menu-buttons">
                            {
                                this.props.children
                            }
                        </div>
                    </div><div className={classNames({
                        'navigation-menu-mobile-menu': true,
                        'active': this.state.cubeTransition.index === 1
                    })}>
                        <div className="navigation-menu-mobile-menu-top">
                            <Button color="purple" onClick={() => {
                                this.goToMainMenu()
                            }}>
                                {
                                    Translations.translations.components.navigation.menu.mobile.buttons.backToMainMenu
                                }
                            </Button>
                        </div><div className="navigation-menu-mobile-menu-hr">
                        </div><div className="navigation-menu-mobile-menu-modal-content modal-content non-absolute">
                            <NavigationMenuModal />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Go to the menu
     */
    private goToMenu = () => {
        NavigationMenuMobile.open.next()
        this.setState({
            cubeTransition: {
                ...this.state.cubeTransition,

                toIndex: 1,

                fromIndex: 0
            }
        }, () => {
            setTimeout(() => {
                this.setState({
                    cubeTransition: {
                        index: 1,

                        toIndex: null,

                        fromIndex: null
                    }
                })
            }, Navigation.animationTimeMs)
        })
    }

    /**
     * Go to the main menu
     */
    private goToMainMenu = () => {
        NavigationMenuMobile.close.next()
        this.setState({
            cubeTransition: {
                ...this.state.cubeTransition,

                toIndex: 0,

                fromIndex: 1
            }
        }, () => {
            setTimeout(() => {
                this.setState({
                    cubeTransition: {
                        index: 0,

                        toIndex: null,

                        fromIndex: null
                    }
                })
            }, Navigation.animationTimeMs)
        })
    }

    /**
     * Whenever the badge count changes
     */
    private onUserInboxBadgeCount = (badgeCount: number) => {
        this.setState({
            inboxBadgeCount: badgeCount
        })
    }
}