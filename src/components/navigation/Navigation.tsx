import React from 'react'
import './navigation.scss'
import NavigationButton from './button/NavigationButton'
import Logo from '../logo/Logo'
import Button from '../button/Button'
import NavigationMenuButton from './menu/button/NavigationMenuButton'
import { Subject, Subscription } from 'rxjs'
import NavigationMenuMobile from './menu/mobile/NavigationMenuMobile'
import classNames from 'classnames'
import Translations from '../../translations/Translations'
import { Route } from '../../enumeration/Route'
import App from '../../core/app/App'
import Modal from '../modal/Modal'
import NavigationMenuModal from '../modal/navigation/menu/NavigationMenuModal'
import Authorization from '../../core/Authorization'
import Badge from '../badge/Badge'
import Events from '../../shared/Events'
import WebSocket from '../../backend/WebSocket'

/** 
 * The props
 */
interface NavigationProps {

    /**
     * The variant to render
     */
    variant: 'default' | 'inbox'

    /**
     * The active tab
     */
    activeTab?: Route
}

/**
 * The state
 */
interface NavigationState {
    
    /**
     * Whether or not the navigation menu is opened
     */
    isMenuOpen: boolean

    /**
     * Whether or not the navigation menu is closed AND has been opened after mounting.
     */
    isMenuClosed: boolean

    /**
     * Whether or not the menu in the NavigationMenuMobile component is opened
     */
    isMenuMobileMenuOpen: boolean

    /**
     * Whether or not the navigation is hidden
     */
    hidden: boolean

    /**
     * The window dimensions
     */
    windowDimensions: {
        width: number

        height: number
    }

    /**
     * The inbox badge count
     */
    inboxBadgeCount: number
}

/**
 * The navigation
 * 
 * @author Stan Hurks
 */
export default class Navigation extends React.Component<NavigationProps, NavigationState> {

    /**
     * The amount of ms the animation takes
     */
    public static readonly animationTimeMs = 500

    /**
     * The subject to open the navigation
     */
    public static readonly open: Subject<void> = new Subject()

    /**
     * The subject to close the navigation
     */
    public static readonly close: Subject<void> = new Subject()

    /**
     * Whether or not to hide the navigation bar.
     * 
     * This is used to create a full page inbox connection page.
     */
    public static readonly hide: Subject<boolean> = new Subject()

    /**
     * Whether or not the navigation menu is opened
     */
    public static isMenuOpen: boolean = false

    /**
     * Whether or not the navigation menu is closed AND has been opened after mounting.
     */
    public static isMenuClosed: boolean = false

    /**
     * The subscription to when the navigation menu opens
     */
    private subscriptionNavigationMenuOpen!: Subscription

    /**
     * The subscription to when the navigation menu closes
     */
    private subscriptionNavigationMenuClose!: Subscription

    /**
     * The subscription to when the navigation menu opens in NavigationMenuMobile
     */
    private subscriptionNavigationMenuMobileMenuOpen!: Subscription

    /**
     * The subscription to when the navigation menu closes in NavigationMenuMobile
     */
    private subscriptionNavigationMenuMobileMenuClose!: Subscription

    /**
     * The subscription to hide/show the navigation
     */
    private subscriptionNavigationHide!: Subscription

    /**
     * The subscription to the window resize event
     */
    private subscriptionWindowResize!: Subscription

    /**
     * The login button ref
     */
    private menuButtonRef: React.RefObject<any> = React.createRef()

    /**
     * The subscription to the inbox badge count
     */
    private subscriptionInboxBadgeCount!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            isMenuOpen: Navigation.isMenuOpen,

            isMenuClosed: Navigation.isMenuClosed,

            isMenuMobileMenuOpen: false,

            hidden: false,

            windowDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            },

            inboxBadgeCount: App.inboxBadgeCount
        }
    }

    public componentDidMount = () => {
        this.subscriptionNavigationMenuOpen = Navigation.open.subscribe(this.onNavigationMenuOpen)
        this.subscriptionNavigationMenuClose = Navigation.close.subscribe(this.onNavigationMenuClose)
        this.subscriptionNavigationMenuMobileMenuOpen = NavigationMenuMobile.open.subscribe(this.onNavigationMenuMobileMenuOpen)
        this.subscriptionNavigationMenuMobileMenuClose = NavigationMenuMobile.close.subscribe(this.onNavigationMenuMobileMenuClose)
        this.subscriptionNavigationHide = Navigation.hide.subscribe(this.onNavigationHide)
        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
        this.subscriptionInboxBadgeCount = WebSocket.userInboxBadgeCount.subscribe(this.onUserInboxBadgeCount)
    }

    public componentWillUnmount = () => {
        this.subscriptionNavigationMenuOpen.unsubscribe()
        this.subscriptionNavigationMenuClose.unsubscribe()
        this.subscriptionNavigationMenuMobileMenuOpen.unsubscribe()
        this.subscriptionNavigationMenuMobileMenuClose.unsubscribe()
        this.subscriptionNavigationHide.unsubscribe()
        this.subscriptionWindowResize.unsubscribe()
        this.subscriptionInboxBadgeCount.unsubscribe()
    }

    public render = () => {
        return (
            <div className={classNames({
                'navigation-container': true,
                'hidden': this.state.hidden
            })}>
                {
                    this.renderVariantDefault()
                }
                {
                    this.renderVariantInbox()
                }
            </div>
        )
    }

    /**
     * Render the default variant of the navigation
     */
    private renderVariantDefault = () => {
        return (
            <nav id="navigation-default" style={{
                ...this.props.variant === 'default' ? {} : {
                    display: 'none'
                }
            }}>
                <div id="navigation-default-top" className={classNames({
                    open: this.state.isMenuMobileMenuOpen && this.state.isMenuOpen
                })}>
                    <div id="navigation-default-top-content">
                        <div id="navigation-default-top-content-left">
                            <Logo labelColor='purple' />
                        </div><div id="navigation-default-top-content-right-pc">
                            <div className="div-ref" ref={this.menuButtonRef}>
                                <Button color='white' onClick={() => {
                                    if (!this.menuButtonRef.current) {
                                        return
                                    }
                                    Modal.mount.next({
                                        element: (
                                            <NavigationMenuModal />
                                        ),
                                        dismissable: true,
                                        target: this.menuButtonRef.current,
                                        position: 'bottom-right',
                                        pcOnly: true,
                                        offsetY: 50
                                    })
                                }} badge={this.state.inboxBadgeCount === 0 ? undefined : this.state.inboxBadgeCount}>
                                    {
                                        Authorization.isAuthorized()
                                        ?
                                        Translations.translations.components.navigation.buttons.menu
                                        :
                                        Translations.translations.components.navigation.buttons.login
                                    }
                                </Button>
                            </div>
                        </div><div id="navigation-default-top-content-right-mobile">
                            <NavigationMenuButton isMenuOpen={this.state.isMenuOpen} isMenuClosed={this.state.isMenuClosed} />
                        </div>
                    </div>
                </div><div id="navigation-default-bottom">
                    <div id="navigation-default-bottom-content">
                        {
                            this.renderDefaultNavigationButtons()
                        }
                    </div>
                </div><NavigationMenuMobile isMenuOpen={this.state.isMenuOpen} isMenuClosed={this.state.isMenuClosed} >
                    {
                        this.renderDefaultNavigationButtons()
                    }
                </NavigationMenuMobile>
            </nav>
        )
    }

    /**
     * Render the navigation buttons for the default variant
     */
    private renderDefaultNavigationButtons = () => {
        return (
            <div className="default-navigation-buttons">
                <NavigationButton href={Route.HOME} selected={this.props.activeTab === Route.HOME}>
                    {
                        Translations.translations.components.navigation.buttons.home
                    }
                </NavigationButton><NavigationButton href={Route.CONSUMER_PLACE_YOUR_JOB} 
                    selected={typeof this.props.activeTab === 'string' && this.props.activeTab.startsWith(Route.CONSUMER_PLACE_YOUR_JOB) && !this.props.activeTab.endsWith(Route.CONSUMER_JOBS)}>
                    {
                        Translations.translations.components.navigation.buttons.addAJob
                    }
                </NavigationButton><NavigationButton href={Route.HOW_DOES_IT_WORK} selected={this.props.activeTab !== undefined && (this.props.activeTab as string).startsWith(Route.HOW_DOES_IT_WORK)}>
                    {
                        Translations.translations.components.navigation.buttons.howDoesItWork
                    }
                </NavigationButton>
            </div>
        )
    }

    /**
     * Render the inbox variant of the navigation
     */
    private renderVariantInbox = () => {
        return (
            <nav id="navigation-inbox" style={{
                ...this.props.variant === 'inbox' ? {} : {
                    display: 'none'
                }
            }}>
                <div id="navigation-inbox-left" className={classNames({
                    'badge-hidden': this.state.inboxBadgeCount === 0
                })}>
                    <div id="navigation-inbox-left-label">
                        {
                            Translations.translations.components.navigation.inbox.left.label
                        }
                        {
                            this.state.inboxBadgeCount > 0
                            &&
                            <Badge value={this.state.inboxBadgeCount} />
                        }
                    </div>
                </div><div id="navigation-inbox-center">
                    <Logo labelColor="white" />
                </div><div id="navigation-inbox-right">
                    <Button color="purple" href={Route.HOME}>
                        {
                            Translations.translations.components.navigation.inbox.right.buttons.backToWebsite
                        }
                    </Button>
                </div>
            </nav>
        )
    }

    /**
     * Whenever the window changes size
     */
    private onWindowResize = () => {
        this.setState({
            windowDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        })
    }

    /**
     * Whether or not to hide the navigation
     */
    private onNavigationHide = (hide: boolean) => {
        this.setState({
            hidden: hide
        })
    }

    /**
     * Whenever the navigation menu is opened
     */
    private onNavigationMenuOpen = () => {
        Navigation.isMenuOpen = true
        App.bodyScroll.next(false)
        document.body.classList.add('navigation-menu-open')

        this.setState({
            isMenuOpen: true,
            isMenuClosed: false
        })
    }

    /**
     * Whenever the navigation menu is closed
     */
    private onNavigationMenuClose = () => {
        Navigation.isMenuClosed = true
        Navigation.isMenuOpen = false
        App.bodyScroll.next(true)
        document.body.classList.add('navigation-menu-closed')

        this.setState({
            isMenuClosed: true,
            isMenuOpen: false
        })
    }

    /**
     * Whenever the navigation menu is opened in the NavigationMenuMobile component
     */
    private onNavigationMenuMobileMenuOpen = () => {
        document.body.classList.add('navigation-menu-mobile-menu-open')
        document.body.classList.remove('navigation-menu-mobile-menu-closed')

        this.setState({
            isMenuMobileMenuOpen: true
        })
    }

    /**
     * Whenever the navigation menu is closed in the NavigationMenuMobile component
     */
    private onNavigationMenuMobileMenuClose = () => {
        document.body.classList.add('navigation-menu-mobile-menu-closed')
        document.body.classList.remove('navigation-menu-mobile-menu-open')

        this.setState({
            isMenuMobileMenuOpen: false
        })
    }

    /**
     * Whenever the badge count changes
     */
    private onUserInboxBadgeCount = (badgeCount: number) => {
        this.setState({
            inboxBadgeCount: badgeCount
        })
    }
}