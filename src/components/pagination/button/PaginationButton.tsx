import React from 'react'
import classNames from 'classnames'
import './pagination-button.scss'

/**
 * The props
 */
interface PaginationButtonProps {

    /**
     * Whether the button is active.
     */
    active?: boolean

    /**
     * Whether the button is disabled
     */
    disabled?: boolean

    /**
     * The onclick callback
     */
    onClick?: () => void
}

/**
 * The state
 */
interface PaginationButtonState {

    /**
     * Whether the button is clicked
     */
    clicked: boolean
}

/**
 * The button in the pagination bar
 * 
 * @author Stan Hurks
 */
export default class PaginationButton extends React.Component<PaginationButtonProps, PaginationButtonState> {

    constructor(props: any) {
        super(props)

        this.state = {
            clicked: false
        }
    }

    public render = () => {
        return (
            <div className="pagination-button">
                <button
                    className={classNames({
                        'pagination-button-button': true,
                        'active': this.props.active,
                        'clicked': this.state.clicked
                    })}
                    disabled={this.props.disabled === true}
                    onClick={(e) => {
                        if (this.props.disabled || !this.props.onClick) {
                            e.preventDefault()
                            return
                        }

                        this.props.onClick()
                    }}
                    onMouseDown={() => {
                        this.setState({
                            clicked: true
                        })
                    }} onMouseUp={() => {
                        this.setState({
                            clicked: false
                        })
                    }}>
                    <div className="pagination-button-button-content">
                        {
                            this.props.children
                        }
                    </div>
                </button>
            </div>
        )
    }
}