import React from 'react'
import './pagination-bar.scss'
import Pagination from '../../../backend/pagination/Pagination'
import PaginationButton from '../button/PaginationButton'
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons'
import { Subscription } from 'rxjs'
import Events from '../../../shared/Events'

/**
 * The props
 */
interface PaginationBarProps {

    /**
     * The pagination object
     */
    pagination: Pagination<any>

    /**
     * Whenever the page changes
     */
    onChangePage: (pageNumber: number) => void
}

/**
 * The state
 */
interface PaginationBarState {
    
    /**
     * The dimensions of the window
     */
    windowDimensions: {
        width: number
        height: number
    }
}

/**
 * The bar used to navigate a paginated page with.
 * 
 * @author Stan Hurks
 */
export default class PaginationBar extends React.Component<PaginationBarProps, PaginationBarState> {

    /**
     * The subscription to when the window changes size
     */
    private subscriptionWindowResize!: Subscription

    constructor(props: any) {
        super(props)
        
        this.state = {
            windowDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        }
    }

    public componentDidMount = () => {
        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
    }

    public componentWillUnmount = () => {
        this.subscriptionWindowResize.unsubscribe()
    }

    public render = () => {
        return (
            <div className="pagination-bar">
                <div className="pagination-bar-left">
                    <PaginationButton onClick={() => {
                        this.props.onChangePage(this.props.pagination.currentPage - 1)
                    }} disabled={this.props.pagination.currentPage === 1}>
                        <KeyboardArrowLeft />
                    </PaginationButton>
                </div><div className="pagination-bar-center">
                    {
                        (
                            this.props.pagination.currentPage > 2
                            || (
                                this.state.windowDimensions.width < 430
                                && this.props.pagination.currentPage > 1
                            )
                        )
                        &&
                        <div className="pagination-bar-center-first">
                            <PaginationButton onClick={() => {
                                this.props.onChangePage(1)
                            }}>
                                1
                            </PaginationButton>
                        </div>
                    }
                    {
                        this.props.pagination.currentPage > 2
                        &&
                        <div className="pagination-bar-center-ellipsis">
                            ...
                        </div>
                    }
                    <div className="pagination-bar-center-buttons">
                        {
                            this.props.pagination.currentPage !== 1
                            &&
                            this.state.windowDimensions.width >= 430
                            &&
                            <PaginationButton onClick={() => {
                                this.props.onChangePage(this.props.pagination.currentPage - 1)
                            }}>
                                {
                                    this.props.pagination.currentPage - 1
                                }
                            </PaginationButton>
                        }
                        <PaginationButton
                            active={true}>
                            {
                                this.props.pagination.currentPage
                            }
                        </PaginationButton>
                        {
                            this.props.pagination.currentPage < this.props.pagination.lastPage
                            &&
                            this.state.windowDimensions.width >= 430
                            &&
                            <PaginationButton onClick={() => {
                                this.props.onChangePage(this.props.pagination.currentPage + 1)
                            }}>
                                {
                                    this.props.pagination.currentPage + 1
                                }
                            </PaginationButton>
                        }
                    </div>
                    {
                        (
                            this.props.pagination.currentPage + 1 < this.props.pagination.lastPage
                            ||
                            (this.props.pagination.currentPage < this.props.pagination.lastPage && this.state.windowDimensions.width < 430)
                        )
                        &&
                        <div className="pagination-bar-center-ellipsis">
                            ...
                        </div>
                    }
                    {
                        (
                            this.props.pagination.currentPage + 1 < this.props.pagination.lastPage
                            ||
                            (this.props.pagination.currentPage < this.props.pagination.lastPage && this.state.windowDimensions.width < 430)
                        )
                        &&
                        <div className="pagination-bar-center-last">
                            <PaginationButton onClick={() => {
                                this.props.onChangePage(this.props.pagination.lastPage)
                            }}>
                                {
                                    this.props.pagination.lastPage
                                }
                            </PaginationButton>
                        </div>
                    }
                </div><div className="pagination-bar-right">
                    <PaginationButton onClick={() => {
                        this.props.onChangePage(this.props.pagination.currentPage + 1)
                    }} disabled={this.props.pagination.currentPage >= this.props.pagination.lastPage}>
                        <KeyboardArrowRight />
                    </PaginationButton>
                </div>
            </div>
        )
    }

    /**
     * Whenever the window changes size
     */
    private onWindowResize = () => {
        this.setState({
            windowDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            }
        })
    }
}