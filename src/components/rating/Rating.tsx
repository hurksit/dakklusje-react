import React from 'react'
import './rating.scss'
import StarEmpty from '../icon/star/IconStarEmpty'
import StarHalf from '../icon/star/IconStarHalf'
import StarFull from '../icon/star/IconStarFull'
import classNames from 'classnames'

/**
 * The props
 */
interface RatingProps {

    /**
     * The rating value in stars (1-5)
     */
    value: number

    /**
     * The callback on select
     */
    onSelect?: (value: number) => void
}

/**
 * The state
 */
interface RatingState {
    
    /**
     * The value on hover
     */
    hoverValue: number|null
}

/**
 * A rating
 * 
 * @author Stan Hurks
 */
export default class Rating extends React.Component<RatingProps, RatingState> {

    constructor(props: any) {
        super(props)

        this.state = {
            hoverValue: null
        }
    }

    public render = () => {
        return (
            <div className={classNames({
                'rating': true,
                'selectable': !!this.props.onSelect
            })}>
                {
                    [1, 2, 3, 4, 5].map((value, index) => 
                        <div className="rating-star" key={index} onClick={() => {
                            if (this.props.onSelect) {
                                this.props.onSelect(value)
                            }
                        }} onMouseOver={() => {
                            if (this.props.onSelect) {
                                this.setState({
                                    hoverValue: value
                                })
                            }
                        }} onMouseOut={() => {
                            if (this.props.onSelect) {
                                this.setState({
                                    hoverValue: null
                                })
                            }
                        }}>
                            {
                                this.state.hoverValue === null
                                ? (
                                    this.props.value >= value
                                    ?
                                    <StarFull />
                                    :
                                    (
                                        this.props.value >= value - 0.5
                                        ?
                                        <StarHalf />
                                        :
                                        <StarEmpty />
                                    )
                                )
                                : (
                                    this.state.hoverValue >= value
                                    ? <StarFull />
                                    : <StarEmpty />
                                )
                            }
                        </div>
                    )
                }
            </div>
        )
    }
}