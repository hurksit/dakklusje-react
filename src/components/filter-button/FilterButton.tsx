import React from 'react'
import { FilterList } from '@material-ui/icons'
import './filter-button.scss'
import Badge from '../badge/Badge';
import classNames from 'classnames';

/**
 * The props
 */
interface FilterButtonProps {

    /**
     * The count to display in the badge, if any.
     */
    badge?: number

    /**
     * When the user clicks the button
     */
    onClick: () => void
}

/**
 * The state
 */
interface FilterButtonState {

    /**
     * Whether or not the button is clicked
     */
    clicked: boolean
}

/**
 * A button with a filter icon.
 * 
 * @author Stan Hurks
 */
export default class FilterButton extends React.Component<FilterButtonProps, FilterButtonState> {

    constructor(props: any) {
        super(props)

        this.state = {
            clicked: false
        }
    }

    public render = () => {
        return (
            <div className="filter-button">
                <button
                    className={classNames({
                        'filter-button-button': true,
                        'clicked': this.state.clicked
                    })}
                    onClick={() => {
                        this.props.onClick()
                    }}
                    onMouseDown={() => {
                        this.setState({
                            clicked: true
                        })
                    }} onMouseUp={() => {
                        this.setState({
                            clicked: false
                        })
                    }}
                    >
                    <FilterList />

                    {
                        this.props.badge !== undefined
                        &&
                        <div className="filter-button-button-badge">
                            <Badge value={this.props.badge} />
                        </div>
                    }
                </button>
            </div>
        )
    }
}