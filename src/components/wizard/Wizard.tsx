import React from 'react'
import './wizard.scss'

/**
 * The wizard component
 * 
 * @author Stan Hurks
 */
export default class Wizard extends React.Component {

    public render = () => {
        return (
            <div className="wizard">
                <div className="wizard-container">
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
}