import React from 'react'
import './wizard-tabs.scss'

/**
 * The tabs in a wizard component.
 * 
 * @author Stan Hurks
 */
export default class WizardTabs extends React.Component {

    public render = () => {
        return (
            <div className="wizard-tabs">
                {
                    this.props.children
                }
            </div>
        )
    }
}