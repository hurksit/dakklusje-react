import React from 'react'
import './wizard-tab.scss'
import Badge from '../../../badge/Badge'
import classNames from 'classnames'

/**
 * The props
 */
interface WizardTabProps {

    /**
     * What to display inside of the badge
     */
    badge: 'done'|1|2|3|4

    /**
     * Whether the tab is active
     */
    active: boolean

    /**
     * Callback on click
     */
    onClick: () => void

    /**
     * Whether or not the tab is disabled
     */
    disabled?: boolean
}

/**
 * A tab in the WizardTabs component.
 * 
 * @author Stan Hurks
 */
export default class WizardTab extends React.Component<WizardTabProps> {

    public render = () => {
        return (
            <div className={classNames({
                'wizard-tab': true,
                'active': this.props.active,
                'disabled': this.props.disabled
            })} onClick={() => {
                this.props.onClick()
            }}>
                <div className="wizard-tab-badge">
                    <Badge value={this.props.badge} color={this.props.active ? 'orange' : 'grey'} />
                </div><div className="wizard-tab-label">
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
}