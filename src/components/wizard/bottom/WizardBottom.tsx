import React from 'react'
import './wizard-bottom.scss'
import Breadcrumbs, { BreadcrumbsProps } from '../../breadcrumbs/Breadcrumbs'

/**
 * The bottom in the Wizard component.
 * 
 * @author Stan Hurks
 */
export default class WizardBottom extends React.Component<BreadcrumbsProps> {

    public render = () => {
        return (
            <div className="wizard-bottom">
                <div className="wizard-bottom-main">
                    <div className="wizard-bottom-main-right">
                        {
                            this.props.children
                        }
                    </div>
                </div><div className="wizard-bottom-breadcrumbs">
                    <Breadcrumbs path={this.props.path} params={this.props.params} />
                </div>
            </div>
        )
    }
}