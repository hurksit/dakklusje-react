import React from 'react'
import './wizard-top.scss'
import RadialProgress from '../../radial-progress/RadialProgress'
import classNames from 'classnames';

/**
 * The props
 */
interface WizardTopProps {

    /**
     * The title
     */
    title?: string

    /**
     * The percentage to display in the radial progress
     */
    percentage?: number

    /**
     * The overlay color to use
     */
    overlayColor?: string
}

/**
 * The top part of a wizard.
 * 
 * @author Stan Hurks
 */
export default class WizardTop extends React.Component<WizardTopProps> {

    public render = () => {
        return (
            <div className="wizard-top">
                {
                    this.props.title
                    &&
                    <div className="wizard-top-left">
                        <h1 className="wizard-top-left-title">
                            {
                                this.props.title
                            }
                        </h1>
                    </div>
                }

                {
                    this.props.percentage !== undefined
                    &&
                    <div className="wizard-top-right">
                        <RadialProgress percentage={this.props.percentage} />
                    </div>
                }

                {
                    this.props.children
                    &&
                    <div className="wizard-top-content">
                        {
                            this.props.children
                        }
                    </div>
                }

                <div className={classNames({
                    'wizard-top-overlay': true,
                    'visible': this.props.overlayColor
                })} style={{
                    ...this.props.overlayColor ? {
                        backgroundColor: this.props.overlayColor
                    } : {}
                }} />
            </div>
        )
    }
}