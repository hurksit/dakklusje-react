import React from 'react'
import './wizard-content-outro.scss'

/**
 * The props
 */
interface WizardContentOutroProps {

    /**
     * The title
     */
    title: string
}

/**
 * The outro in the WizardContent
 * 
 * @author Stan Hurks
 */
export default class WizardContentOutro extends React.Component<WizardContentOutroProps> {

    public render = () => {
        return (
            <div className="wizard-content-outro">
                <div className="wizard-content-outro-hr">
                </div><div className="wizard-content-outro-title">
                    {
                        this.props.title
                    }
                </div><div className="wizard-content-outro-description">
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
}