import React from 'react'
import './wizard-content-intro.scss'
import Badge from '../../../badge/Badge'

/**
 * The props
 */
interface WizardContentIntroProps {

    /**
     * The title
     */
    title: string

    /**
     * A number to display in the badge
     */
    badge?: number
}

/**
 * The intro in the WizardContent
 * 
 * @author Stan Hurks
 */
export default class WizardContentIntro extends React.Component<WizardContentIntroProps> {

    public render = () => {
        return (
            <div className="wizard-content-intro">
                <div className="wizard-content-intro-title">
                    {
                        this.props.title
                    }
                    {
                        this.props.badge !== undefined
                        &&
                        <Badge value={this.props.badge} />
                    }
                </div><div className="wizard-content-intro-description">
                    {
                        this.props.children
                    }
                </div><div className="wizard-content-intro-hr"></div>
            </div>
        )
    }
}