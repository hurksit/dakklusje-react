import React from 'react'
import './wizard-content.scss'

/**
 * The content of a wizard
 * 
 * @author Stan Hurks
 */
export default class WizardContent extends React.Component {

    public render = () => {
        return (
            <div className="wizard-content">
                {
                    this.props.children
                }
            </div>
        )
    }
}