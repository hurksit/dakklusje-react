import React from 'react'
import { Route } from '../../enumeration/Route'
import { ArrowForward } from '@material-ui/icons'
import './breadcrumbs.scss'
import Translations from '../../translations/Translations'

/**
 * The props
 */
export interface BreadcrumbsProps {

    /**
     * The path of the breadcrumbs
     */
    path: Route[]

    /**
     * The replacement for route params
     */
    params?: {
        [param: string]: string
    }
}

/**
 * The breadcrumbs with structured data for SEO.
 * 
 * @author Stan Hurks
 */
export default class Breadcrumbs extends React.Component<BreadcrumbsProps> {

    public render = () => {
        return (
            <ol className="breadcrumbs" vocab="https://schema.org/" typeof="BreadcrumbList">
                {
                    this.props.path.map((route, index) =>
                        <li className="breadcrumbs-path" property="itemListElement" typeof="ListItem" key={index}>
                            <a href={this.getHref(route)} className="breadcrumbs-path-label" property="item" typeof="WebPage">
                                <span property="name">
                                    {
                                        this.getLabel(route)
                                    }
                                </span>
                            </a>
                            <meta property="position" content={`${index + 1}`} />
                            {
                                index !== this.props.path.length - 1
                                &&
                                <div className="breadcrumbs-path-arrow">
                                    <ArrowForward />
                                </div>
                            }
                        </li>
                    )
                }
            </ol>
        )
    }

    /**
     * Get the label
     */
    private getLabel = (route: Route): string => {
        let label: string = Translations.translations.breadcrumbs(route)
        if (this.props.params) {
            for (const param of Object.keys(this.props.params)) {
                label = label.replace(':' + param, this.props.params[param])
            }
        }
        return label
    }

    /**
     * Get the HREF
     */
    private getHref = (route: Route): string => {
        let href: string = route
        if (this.props.params) {
            for (const param of Object.keys(this.props.params)) {
                href = href.replace(':' + param, this.props.params[param])
            }
        }
        return href
    }
}