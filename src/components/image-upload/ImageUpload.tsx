import React, { ChangeEvent } from 'react'
import { Subject, Subscription } from 'rxjs'
import EXIF from 'exif-js'

/**
 * This component will contain a form input of type file that will be hidden from rendering in the DOM
 * and can be used with RxJS.
 * 
 * @author Stan Hurks
 */
export default class ImageUpload extends React.Component {

    /**
     * Prompt to upload a file
     */
    public static readonly prompt: Subject<void> = new Subject()

    /**
     * Whenever a new base64 image is available
     */
    public static readonly newImageBase64: Subject<string> = new Subject()

    /**
     * Whenever an upload fails
     */
    public static readonly uploadFailed: Subject<void> = new Subject()

    /**
     * The internal ref
     */
    private ref: React.RefObject<HTMLInputElement> = React.createRef()

    /**
     * The internal subscription to the prompt subject
     */
    private subscriptionPrompt!: Subscription

    public componentDidMount = () => {
        this.subscriptionPrompt = ImageUpload.prompt.subscribe(this.onPrompt)
    }

    public componentWillUnmount = () => {
        this.subscriptionPrompt.unsubscribe()
    }

    public render = () => {
        return (
            <input type="file" ref={this.ref} accept="image/*" style={{
                position: 'fixed',
                left: '-1000px',
                zIndex: -1
            }} onChange={this.onChange} />
        )
    }

    /**
     * Whenever the image upload is prompted
     */
    private onPrompt = () => {
        if (this.ref.current) {
            this.ref.current.click()
        }
    }

    /**
     * Whenever the input changes
     */
    private onChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files.length === 1) {
            const file = event.target.files[0]
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onload = () => {
                const canvas = document.createElement('canvas')
                const context = canvas.getContext('2d')
                if (context === null) {
                    ImageUpload.uploadFailed.next()
                    return
                }

                const image = new Image()
                image.setAttribute('crossOrigin', 'anonymous')
                image.src = reader.result as string
                image.onload = () => {

                    // Shrink the file size if too large
                    const maxDimension = 1024
                    let width = image.width
                    let height = image.height
                    if (width > height && width > maxDimension) {
                        width = maxDimension
                        height = maxDimension * (image.height / image.width)
                    }
                    else if (height > width && height > maxDimension) {
                        height = maxDimension
                        width = maxDimension * (image.width / image.height)
                    }
                    
                    // The orientation extracted by the EXIF library
                    let exifOrientation: any

                    // Check orientation in EXIF metadatas
                    EXIF.getData(image as any, function() {
                        const allMetaData = EXIF.getAllTags(image as any)
                        exifOrientation = allMetaData.Orientation
                    })
            
                    // Set proper canvas dimensions before transform & export
                    if ([5, 6, 7, 8].indexOf(exifOrientation) !== -1) {
                        canvas.width = height;
                        canvas.height = width;
                    } else {
                        canvas.width = width;
                        canvas.height = height;
                    }
            
                    // transform context before drawing image
                    switch (exifOrientation) {
                        case 2:
                            context.transform(-1, 0, 0, 1, width, 0);
                            break;
                        case 3:
                            context.transform(-1, 0, 0, -1, width, height);
                            break;
                        case 4:
                            context.transform(1, 0, 0, -1, 0, height);
                            break;
                        case 5:
                            context.transform(0, 1, 1, 0, 0, 0);
                            break;
                        case 6:
                            context.transform(0, 1, -1, 0, height, 0);
                            break;
                        case 7:
                            context.transform(0, -1, -1, 0, height, width);
                            break;
                        case 8:
                            context.transform(0, -1, 1, 0, 0, width);
                            break;
                        default:
                            context.transform(1, 0, 0, 1, 0, 0);
                    }

                    // Draw the image to the canvas
                    context.drawImage(image, 0, 0, width, height)

                    ImageUpload.newImageBase64.next(canvas.toDataURL().replace('data:image/png;base64,', ''))
                }
                image.onerror = (error) => {
                    ImageUpload.uploadFailed.next()    
                    console.error(error)
                }
            }
            reader.onerror = (error) => {
                ImageUpload.uploadFailed.next()
                console.error(error)
            }
        }
    }
}