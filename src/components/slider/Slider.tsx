import React from 'react'
import './slider.scss'
import Slide from './Slide'
import Badge from '../badge/Badge'
import { ArrowForward } from '@material-ui/icons'
import classNames from 'classnames'

/**
 * The slider properties
 */
interface SliderProps {

    /**
     * The slides to display
     */
    slides: Slide[]
}

/**
 * The state
 */
interface SliderState {

    /**
     * The slide index
     */
    slideIndex: number
}

/**
 * A slider, which can display a title and an image.
 * 
 * @author Stan Hurks
 */
export default class Slider extends React.Component<SliderProps, SliderState> {

    constructor(props: any) {
        super(props)

        this.state = {
            slideIndex: 0
        }
    }

    public render = () => {
        return (
            <div className="slider">
                <div className="slider-content">
                    {
                        this.props.slides.map((slide, index) =>
                            <div className="slider-content-slide" key={index} style={{
                                transform: `translateX(${100 * (index - this.state.slideIndex)}%)`,
                                WebkitTransform: `translateX(${100 * (index - this.state.slideIndex)}%)`,
                                msTransform: `translateX(${100 * (index - this.state.slideIndex)}%)`,
                                OTransform: `translateX(${100 * (index - this.state.slideIndex)}%)`
                            }}>
                                <div className="slider-content-slide-content">
                                    <div className="slider-content-slide-content-badge">
                                        <Badge value={index + 1} />
                                    </div><div className="slider-content-slide-content-title">
                                        {
                                            typeof slide.title === 'string'
                                            ? slide.title
                                            : slide.title.map((part, partIndex) =>
                                                <div className="div" key={partIndex}>
                                                    {
                                                        part
                                                    }
                                                </div>
                                            )
                                        }
                                    </div><div className="slider-content-slide-content-picture" style={{
                                        backgroundImage: `url(${slide.imageURL})`
                                    }}>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </div><div className={classNames({
                    'slider-prev': true,
                    'disabled': this.state.slideIndex === 0
                })} onClick={() => {
                    if (this.state.slideIndex === 0) {
                        return
                    }
                    this.setState({
                        slideIndex: this.state.slideIndex - 1
                    })
                }}>
                    <ArrowForward />
                </div><div className={classNames({
                    'slider-next': true,
                    'disabled': this.state.slideIndex === this.props.slides.length - 1
                })} onClick={() => {
                    if (this.state.slideIndex === this.props.slides.length - 1) {
                        return
                    }
                    this.setState({
                        slideIndex: this.state.slideIndex + 1
                    })
                }}>
                    <ArrowForward />
                </div><div className="slider-dots">
                    {
                        this.props.slides.map((slide, index) =>
                            <div className={classNames({
                                'slider-dots-dot': true,
                                'active': this.state.slideIndex === index
                            })} key={index} onClick={() => {
                                this.setState({
                                    slideIndex: index
                                })
                            }}>
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }   
}