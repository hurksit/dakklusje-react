/**
 * A slide in the slider
 * @author Stan Hurks
 */
export default interface Slide {

    /**
     * The title of the slide
     */
    title: string|string[]

    /**
     * The url for the image (can be base64)
     */
    imageURL: string
}