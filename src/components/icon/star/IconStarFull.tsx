import React from 'react'

/**
 * A filled star icon
 * 
 * @author Stan Hurks
 */
export default class IconStarFull extends React.Component {

    public render = () => {
        return (
            <svg className="icon" width="21px" height="20px" viewBox="0 0 21 20" version="1.1">
                <defs>
                    <polygon id="path-1" points="10.5 0 7.2905 6.63103478 -3.55271368e-15 7.63953386 5.306 12.7459914 4.011 20 10.5 16.5241391 16.989 19.9991238 15.694 12.7451152 21 7.63953386 13.7095 6.63191098"></polygon>
                </defs>
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="star-full">
                        <mask id="mask-2" fill="white">
                            <use xlinkHref="#path-1"></use>
                        </mask>
                        <use id="Shape" fill="#FFFFFF" fillRule="nonzero" xlinkHref="#path-1"></use>
                    </g>
                </g>
            </svg>
        )
    }
}