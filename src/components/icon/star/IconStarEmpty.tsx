import React from 'react'

/**
 * An empty star icon
 * 
 * @author Stan Hurks
 */
export default class IconStarEmpty extends React.Component {

    public render = () => {
        return (
            <svg className="icon" width="21px" height="20px" viewBox="0 0 21 20" version="1.1">
                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g fill="#FFFFFF" fillRule="nonzero">
                        <path d="M10.5,4 L12.4865838,8.30977288 L17,8.96469374 L13.7151178,12.284033 L14.5165576,16.9991053 L10.5,14.7408809 L6.48344241,17 L7.2848822,12.2849277 L4,8.96558844 L8.51341623,8.30977288 L10.5,4 L10.5,4 Z M10.5,0 L7.2905,6.63103478 L0,7.63953386 L5.306,12.7459914 L4.011,20 L10.5,16.5241391 L16.989,19.9991238 L15.694,12.7451152 L21,7.63953386 L13.7095,6.63191098 L10.5,0 Z"></path>
                    </g>
                </g>
            </svg>
        )
    }
}