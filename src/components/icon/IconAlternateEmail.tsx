import React from 'react'

/**
 * The alternate email icon
 * 
 * @author Stan Hurks
 */
export default class IconAlternateEmail extends React.Component {

    public render = () => {
        return (
            <svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink">
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" fillOpacity="0.9">
                    <g id="Attachment-alternative-icon" transform="translate(-3.000000, -3.000000)" fill="#777777" fillRule="nonzero">
                        <g id="Group">
                            <g>
                                <path d="M20,3.33333333 C10.8,3.33333333 3.33333333,10.8 3.33333333,20 C3.33333333,29.2 10.8,36.6666667 20,36.6666667 L28.3333333,36.6666667 L28.3333333,33.3333333 L20,33.3333333 C12.7666667,33.3333333 6.66666667,27.2333333 6.66666667,20 C6.66666667,12.7666667 12.7666667,6.66666667 20,6.66666667 C27.2333333,6.66666667 33.3333333,12.7666667 33.3333333,20 L33.3333333,22.3833333 C33.3333333,23.7 32.15,25 30.8333333,25 C29.5166667,25 28.3333333,23.7 28.3333333,22.3833333 L28.3333333,20 C28.3333333,15.4 24.6,11.6666667 20,11.6666667 C15.4,11.6666667 11.6666667,15.4 11.6666667,20 C11.6666667,24.6 15.4,28.3333333 20,28.3333333 C22.3,28.3333333 24.4,27.4 25.9,25.8833333 C26.9833333,27.3666667 28.85,28.3333333 30.8333333,28.3333333 C34.1166667,28.3333333 36.6666667,25.6666667 36.6666667,22.3833333 L36.6666667,20 C36.6666667,10.8 29.2,3.33333333 20,3.33333333 Z M20,25 C17.2333333,25 15,22.7666667 15,20 C15,17.2333333 17.2333333,15 20,15 C22.7666667,15 25,17.2333333 25,20 C25,22.7666667 22.7666667,25 20,25 Z" id="Shape"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        )
    }
}