import React from 'react'

/**
 * The location icon
 * 
 * @author Stan Hurks
 */
export default class IconLocation extends React.Component {

    public render = () => {
        return (
            <svg className="icon" width="14px" height="20px" viewBox="0 0 14 20" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink">
                <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="Klus---consument---hover" transform="translate(-432.000000, -14.000000)" fill="#F13C20" fillRule="nonzero">
                        <g id="Klus">
                            <g id="SVG-Layer" transform="translate(427.000000, 12.000000)">
                                <g id="Group">
                                    <path d="M12,2 C8.13,2 5,5.13 5,9 C5,14.25 12,22 12,22 C12,22 19,14.25 19,9 C19,5.13 15.87,2 12,2 Z M12,11.5 C10.62,11.5 9.5,10.38 9.5,9 C9.5,7.62 10.62,6.5 12,6.5 C13.38,6.5 14.5,7.62 14.5,9 C14.5,10.38 13.38,11.5 12,11.5 Z" id="Shape"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        )
    }
}