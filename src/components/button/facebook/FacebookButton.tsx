import React from 'react'
import Button from '../Button'
import './facebook-button.scss'
import Modal from '../../modal/Modal'
import Translations from '../../../translations/Translations'
import Loader from '../../loader/Loader'

/**
 * The props
 */
interface FacebookButtonProps {
    
    /**
     * The button size
     */
    size: 'small' | 'regular'

     /**
     * Callback for when the user is authorized using the facebook button
     */
    onAuthorized: (facebookAccessToken: string) => void
}

/**
 * The button to login with facebook.
 * 
 * @author Stan Hurks
 */
export default class FacebookButton extends React.Component<FacebookButtonProps> {

    public render = () => {
        return (
            <div className="facebook-button">
                <Button size={this.props.size} color="white" onClick={() => {
                    Loader.set.next(Translations.translations.integrations.facebook.login.loader)

                    const FB: fb.FacebookStatic = window['FB']
                    FB.login((response) => {
                        if (response.status === 'connected') {
                            this.props.onAuthorized(response.authResponse.accessToken)
                        } else {
                            Loader.set.next(null)
                            Modal.mountError.next(Translations.translations.integrations.facebook.login.error)
                        }
                    })
                }}>
                    <svg width="11px" height="20px" viewBox="0 0 11 20" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <g transform="translate(-28.000000, -24.000000)" fill="#FFFFFF" fillRule="nonzero">
                                <g>
                                    <g transform="translate(28.000000, 24.000000)">
                                        <g>
                                            <path d="M7.50997177,0.215973379 C4.89655093,0.215973379 3.10678099,1.78953444 3.10678099,4.68033557 L3.10678099,7.17169565 L0.150236412,7.17169565 L0.150236412,10.550498 L3.10678099,10.550498 L3.10678099,19.2159734 L6.64088619,19.2159734 L6.64088619,10.550498 L9.58917785,10.550498 L10.0319736,7.17169565 L6.64088619,7.17169565 L6.64088619,5.01414766 C6.64088619,4.03618877 6.91663696,3.36952598 8.3378041,3.36952598 L10.1502364,3.36952598 L10.1502364,0.348955797 C9.83674661,0.307791812 8.76180802,0.215973379 7.50997177,0.215973379 L7.50997177,0.215973379 Z"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </Button>
            </div>
        )
    }
}