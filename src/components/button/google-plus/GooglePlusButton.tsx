import React from 'react'
import Button from '../Button'
import './google-plus-button.scss'
import Loader from '../../loader/Loader'
import Translations from '../../../translations/Translations'
import Modal from '../../modal/Modal'

/**
 * The props
 */
interface GooglePlusButtonProps {
    
    /**
     * The size of the button
     */
    size?: 'small' | 'regular'

    /**
     * Callback for when the user is authorized using the google plus button
     */
    onAuthorized: (googleIdToken: string) => void
}

/**
 * The google plus social media button.
 * 
 * @author Stan Hurks
 */
export default class GooglePlusButton extends React.Component<GooglePlusButtonProps> {

    public render = () => {
        return (
            <div className="google-plus-button">
                <Button size={this.props.size} color="white" onClick={() => {
                    Loader.set.next(Translations.translations.integrations.googlePlus.login.loader)

                    window['auth2'].signIn().then(() => {
                        this.props.onAuthorized(window['auth2'].currentUser.get().getAuthResponse().id_token)
                    }).catch((err: any) => {
                        Loader.set.next(null)
                        Modal.mountError.next(Translations.translations.integrations.googlePlus.login.error)
                    })
                }}>
                    <svg width="35px" height="21px" viewBox="0 0 35 21" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <g transform="translate(-16.000000, -23.000000)" fill="#FFFFFF" fillRule="nonzero">
                                <g>
                                    <g transform="translate(16.000000, 23.000000)">
                                        <path d="M10.1043092,8.99589983 L10.1043092,12.4244713 L15.7757377,12.4244713 C15.5471663,13.8944713 14.061452,16.738757 10.1043092,16.738757 C6.69002344,16.738757 3.90430916,13.9116141 3.90430916,10.4244713 C3.90430916,6.93875697 6.69002344,4.11018554 10.1043092,4.11018554 C12.0471663,4.11018554 13.3471663,4.93875697 14.0900234,5.65304269 L16.8043092,3.03875697 C15.061452,1.41018554 12.8043092,0.424471259 10.1043092,0.424471259 C4.57573773,0.424471259 0.104309157,4.89589983 0.104309157,10.4244713 C0.104309157,15.9530427 4.57573773,20.4244713 10.1043092,20.4244713 C15.8757377,20.4244713 19.7057377,16.3673284 19.7057377,10.6530427 C19.7057377,9.99589983 19.6328806,9.49589983 19.5471663,8.99589983 L10.1043092,8.99589983 Z M10.1043092,8.99589983 L34.3900234,11.8530427 L30.1043092,11.8530427 L30.1043092,16.138757 L27.2471663,16.138757 L27.2471663,11.8530427 L22.961452,11.8530427 L22.961452,8.99589983 L27.2471663,8.99589983 L27.2471663,4.71018554 L30.1043092,4.71018554 L30.1043092,8.99589983 L34.3900234,8.99589983 L34.3900234,11.8530427 L10.1043092,8.99589983 Z"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </Button>
            </div>
        )
    }
}