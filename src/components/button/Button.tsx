import React from 'react'
import classNames from 'classnames'
import './button.scss'
import Badge from '../badge/Badge';

/**
 * The props
 */
export interface ButtonProps {

    /**
     * The color of the button
     */
    color: 'white' | 'purple' | 'light-purple' | 'orange'

    /**
     * Whether or not the button is disabled
     */
    disabled?: boolean

    /**
     * The on click call back.
     */
    onClick?: (...any: any) => any

    /**
     * An href attribute if the button goes to a URL/route
     */
    href?: string

    /**
     * The size of the button
     */
    size?: 'small' | 'regular'

    /**
     * Whether or not to make the button the full width of the container element
     */
    fullWidth?: boolean

    /**
     * The type of button
     */
    type?: 'submit'
    
    /**
     * The badge to display at the top right of the button
     */
    badge?: number
}

/**
 * The state
 */
interface ButtonState {
    clicked: boolean

    /**
     * The badge count to display
     */
    badge?: number
}

/**
 * A button
 * 
 * @author Stan Hurks
 */
export default class Button extends React.Component<ButtonProps, ButtonState> {

    constructor(props: any) {
        super(props)

        this.state = {
            clicked: false,

            badge: this.props.badge
        }
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.setState({
                badge: this.props.badge
            })
        })
    }

    public render = () => {
        if (this.props.href) {
            return (
                // eslint-disable-next-line
                <a className={classNames({
                    'button': true,
                    'clicked': this.state.clicked,
                    'disabled': this.props.disabled,
                    ['color-' + this.props.color]: true,
                    ['size-' + (this.props.size || 'regular')]: true,
                    'full-width': this.props.fullWidth
                })} href={this.props.disabled ? '#' : this.props.href} onMouseDown={() => {
                    this.setState({
                        clicked: true
                    })
                }} onMouseUp={() => {
                    this.setState({
                        clicked: false
                    })
                }} onClick={(event) => {
                    if (this.props.disabled) {
                        event.preventDefault()
                        return
                    }
                }}>
                    {
                        this.props.children
                    }
                    {
                        this.state.badge !== undefined
                        &&
                        <Badge value={this.state.badge} />
                    }
                </a>
            )
        }
        return (
            <button className={classNames({
                'button': true,
                'clicked': this.state.clicked,
                ['color-' + this.props.color]: true,
                ['size-' + (this.props.size || 'regular')]: true,
                'full-width': this.props.fullWidth
            })} disabled={this.props.disabled} onMouseDown={() => {
                this.setState({
                    clicked: true
                })
            }} onMouseUp={() => {
                this.setState({
                    clicked: false
                })
            }} onClick={() => {
                if (this.props.disabled) {
                    return
                }
                if (this.props.onClick) {
                    this.props.onClick()
                }
            }} type={this.props.type || 'button'}>
                {
                    this.props.children
                }
                {
                    this.state.badge !== undefined
                    &&
                    <Badge value={this.state.badge} />
                }
            </button>
        )
    }
}