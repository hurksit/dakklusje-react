import React from 'react'
import './page-section-content.scss'

/**
 * The content of a page section.
 * 
 * @author Stan Hurks
 */
export default class PageSectionContent extends React.Component {

    public render = () => {
        return (
            <div className="page-section-content">
                {
                    this.props.children
                }
            </div>
        )
    }
}