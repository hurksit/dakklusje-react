import React from 'react'
import './page-section.scss'

/**
 * The props
 */
interface PageSectionProps {
    
    /**
     * The id of the element
     */
    id?: string

    /**
     * The color of the page section
     */
    color: 'light-grey' | 'grey'
}

/**
 * A section of a page.
 * 
 * @author Stan Hurks
 */
export default class PageSection extends React.Component<PageSectionProps> {

    public render = () => {
        return (
            <section className="page-section" style={{
                backgroundColor: this.getHEXColor(1),
                boxShadow: `0 2px 20px ${this.getHEXColor(0.5)}`,
                MozBoxShadow: `0 2px 20px ${this.getHEXColor(0.5)}`,
                WebkitBoxShadow: `0 2px 20px ${this.getHEXColor(0.5)}`
            }} id={this.props.id}>
                <div className="page-section-grid">
                    {
                        this.props.children
                    }
                </div>
            </section>
        )
    }

    /**
     * Get the HEX color based on the color prop
     */
    private getHEXColor = (opacity: number) => {
        switch (this.props.color) {
            case 'light-grey':
                return `rgba(247, 247, 247, ${opacity})`
            case 'grey':
                return `rgba(231, 231, 231, ${opacity})`
        }
    }
}