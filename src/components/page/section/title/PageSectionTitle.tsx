import React from 'react'
import './page-section-title.scss'

/**
 * The title of a page section.
 * 
 * @author Stan Hurks
 */
export default class PageSectionTitle extends React.Component {

    public render = () => {
        return (
            <h1 className="page-section-title">
                {
                    this.props.children
                }
            </h1>
        )
    }
}