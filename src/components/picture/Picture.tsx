import React from 'react'
import ImageResponse from '../../backend/response/entity/ImageResponse'
import { CameraAlt } from '@material-ui/icons'
import './picture.scss'
import Translations from '../../translations/Translations'

/**
 * The props
 */
interface PictureProps {

    /**
     * The image
     */
    image: ImageResponse|null

    /**
     * The size of the element (pixels or css string)
     */
    size: number
}

/**
 * An image based on an image from the database.
 * 
 * This component has functionality to control the background position, zoom.
 * 
 * @author Stan Hurks
 */
export default class Picture extends React.Component<PictureProps> {

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public render = () => {
        if (this.props.image && this.props.image.file) {
            return this.renderWithPicture()
        }
        return this.renderWithoutPicture()
    }

    /**
     * Render the component with an image
     */
    public renderWithPicture = () => {
        return (
            <div className="picture">
                <div className="picture-inner" style={{
                    height: this.props.size,
                    width: this.props.size
                }}>
                    <img src={this.getImgSrc()} alt={(this.props.image as any).file.title} className="picture-inner-image" draggable={false} height={this.getImgHeight()} style={{
                        transform: `translate(${this.getTranslation()})`,
                        OTransform: `translate(${this.getTranslation()})`,
                        WebkitTransform: `translate(${this.getTranslation()}`
                    }} />
                </div>
            </div>
        )
    }

    /**
     * Render the component without an image
     */
    private renderWithoutPicture = () => {
        return (
            <div className="picture">
                <div className="picture-inner" style={{
                    height: this.props.size,
                    width: this.props.size
                }}>
                    <div className="picture-inner-no-image">
                        <div className="picture-inner-no-image-icon">
                            <CameraAlt />
                        </div><div className="picture-inner-no-image-label">
                            {
                                Translations.translations.components.picture.noPhoto
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Get the translation
     */
    private getTranslation = () => {
        if (!this.props.image || !this.props.image.backgroundPosition) {
            return 'none'
        }
        let backgroundPosition = this.props.image.backgroundPosition.split(' ').map((x) => Number(x.replace('%', '').trim()))
        return `${-backgroundPosition[0]}%, ${-backgroundPosition[1]}%`
    }

    /**
     * Get the height of the img element
     */
    private getImgHeight = () => {
        const zoom = (this.props.image && this.props.image.zoom) || 1
        return Math.max(
            0.5,
            Math.min(
                5,
                zoom
            )
        ) * this.props.size
    }

    /**
     * Get the img src
     */
    private getImgSrc = () => {
        return (this.props.image as any).file.content
    }
}