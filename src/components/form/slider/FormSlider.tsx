import React from 'react'
import './form-slider.scss'
import { Subscription } from 'rxjs'
import Events from '../../../shared/Events'

/**
 * The props
 */
interface FormSliderProps {

    /**
     * The label to display
     */
    label: string
    
    /**
     * The value to slide with
     */
    value: number

    /**
     * Whenever the value changes
     */
    onChange: (value: number) => void

    /**
     * The minimum amount
     */
    minValue: number

    /**
     * The maximum amount
     */
    maxValue: number

    /**
     * The amount of steps to get from the minimum amount to the maximum amount
     */
    steps: number
}

/**
 * The state
 */
interface FormSliderState {
    
    /**
     * All properties for the drag and drop functionality.
     */
    dragAndDrop?: {
        startX: number

        currentX: number
    }
}

/**
 * A slider in a form.
 * 
 * @author Stan Hurks
 */
export default class FormSlider extends React.Component<FormSliderProps, FormSliderState> {

    /**
     * The ref for the handle element
     */
    private handleRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The ref for the bar element
     */
    private barRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The ref for the progress element
     */
    private progressRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The subscription for the mouseup event on the document
     */
    private subscriptionDocumentMouseup!: Subscription

    /**
     * The subscription for the mousemove event on the document
     */
    private subscriptionDocumentMousemove!: Subscription

    /**
     * The subscription for the mousedown event on the document
     */
    private subscriptionDocumentMousedown!: Subscription

    /**
     * The subscription for the touchstart event on the document
     */
    private subscriptionDocumentTouchstart!: Subscription

    /**
     * The subscription for the touchmove event on the document
     */
    private subscriptionDocumentTouchmove!: Subscription

    /**
     * The subscription for the touchend event on the document
     */
    private subscriptionDocumentTouchend!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            dragAndDrop: undefined
        }
    }

    public componentDidMount = () => {
        this.subscriptionDocumentMouseup = Events.document.mouseup.subscribe(this.onDocumentMouseup as any)
        this.subscriptionDocumentMousemove = Events.document.mousemove.subscribe(this.onDocumentMousemove as any)
        this.subscriptionDocumentMousedown = Events.document.mousedown.subscribe(this.onDocumentMousedown as any)
        this.subscriptionDocumentTouchstart = Events.document.touchstart.subscribe(this.onDocumentTouchstart as any)
        this.subscriptionDocumentTouchmove = Events.document.touchmove.subscribe(this.onDocumentTouchmove as any)
        this.subscriptionDocumentTouchend = Events.document.touchend.subscribe(this.onDocumentTouchend as any)
    }

    public componentWillUnmount = () => {
        this.subscriptionDocumentMouseup.unsubscribe()
        this.subscriptionDocumentMousemove.unsubscribe()
        this.subscriptionDocumentMousedown.unsubscribe()
        this.subscriptionDocumentTouchstart.unsubscribe()
        this.subscriptionDocumentTouchmove.unsubscribe()
        this.subscriptionDocumentTouchend.unsubscribe()
    }

    public render = () => {
        return (
            <div className="form-slider">
                <div className="form-slider-top">
                    <div className="form-slider-top-label">
                        {
                            this.props.label
                        }
                    </div><div className="form-slider-top-value">
                        {
                            this.getValueFromProgressPercentage().toFixed(1)
                        }
                    </div>
                </div><div className="form-slider-slider">
                    <div className="form-slider-slider-bar" onClick={(event) => {
                        if (!this.barRef.current) {
                            return
                        }

                        if ([this.progressRef.current, this.barRef.current].indexOf(event.target as HTMLDivElement) !== -1) {
                            const offsetX = event.clientX - this.barRef.current.getBoundingClientRect().left
                            const width = this.barRef.current.getBoundingClientRect().width
                            const percentage = Math.round((offsetX / width) * this.props.steps) / this.props.steps
                            const value = percentage * (this.props.maxValue - this.props.minValue) + this.props.minValue
                            this.props.onChange(value)
                        }
                    }} ref={this.barRef}>
                        <div className="form-slider-slider-bar-progress" style={{
                            width: `${this.getProgressPercentage()}%`
                        }} ref={this.progressRef}></div>

                        <div className="form-slider-slider-bar-handle" style={{
                            left: `${this.getProgressPercentage()}%`
                        }} ref={this.handleRef} ></div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the mousedown event is triggered on the document
     */
    private onDocumentMousedown = (event: MouseEvent) => {
        if (event.target !== this.handleRef.current) {
            return
        }
        this.setState({
            dragAndDrop: {
                startX: event.clientX,
                currentX: event.clientX
            }
        })
    }

    /**
     * Whenever the mousemove event is triggered on the document
     */
    private onDocumentMousemove = (event: MouseEvent) => {
        if (!this.state.dragAndDrop || !this.barRef.current) {
            return
        }
        const minX = this.barRef.current.getBoundingClientRect().left
        const maxX = minX + this.barRef.current.getBoundingClientRect().width
        this.setState({
            dragAndDrop: {
                ...this.state.dragAndDrop,
                currentX: Math.max(minX, Math.min(maxX, event.clientX))
            }
        }, () => {
            this.props.onChange(this.getValueFromProgressPercentage())
            this.setState({
                dragAndDrop: {
                    currentX: Math.max(minX, Math.min(maxX, event.clientX)),
                    startX: Math.max(minX, Math.min(maxX, event.clientX))
                }
            })
        })
    }

    /**
     * Whenever the mouseup event is triggered on the document
     */
    private onDocumentMouseup = (event: MouseEvent) => {
        if (!this.state.dragAndDrop) {
            return
        }
        this.props.onChange(this.getValueFromProgressPercentage())
        this.setState({
            dragAndDrop: undefined
        })
    }

    /**
     * Whenever the touchstart event is triggered on the document
     */
    private onDocumentTouchstart = (event: TouchEvent) => {
        if (event.touches.length !== 1 || this.handleRef.current === null) {
            return
        }

        const touch = event.touches.item(0) as Touch
        const handlePosition = this.handleRef.current.getBoundingClientRect()
        const deltaPosition = {
            x: touch.clientX - (handlePosition.left + (handlePosition.width / 2)),
            y: touch.clientY - (handlePosition.top + (handlePosition.height / 2)),
        }
        const distance = Math.sqrt(Math.pow(deltaPosition.x, 2) + Math.pow(deltaPosition.y, 2))
        if (distance > 40) {
            return
        }

        this.setState({
            dragAndDrop: {
                startX: touch.clientX,
                currentX: touch.clientX
            }
        })
    }

    /**
     * Whenever the touchmove event is triggered on the document
     */
    private onDocumentTouchmove = (event: TouchEvent) => {
        if (!this.state.dragAndDrop || !this.barRef.current) {
            return
        }
        const touch = event.touches.item(0)
        if (!touch) {
            return
        }
        const minX = this.barRef.current.getBoundingClientRect().left
        const maxX = minX + this.barRef.current.getBoundingClientRect().width
        this.setState({
            dragAndDrop: {
                ...this.state.dragAndDrop,
                currentX: Math.max(minX, Math.min(maxX, touch.clientX))
            }
        }, () => {
            this.props.onChange(this.getValueFromProgressPercentage())
            this.setState({
                dragAndDrop: {
                    currentX: Math.max(minX, Math.min(maxX, touch.clientX)),
                    startX: Math.max(minX, Math.min(maxX, touch.clientX))
                }
            })
        })
    }

    /**
     * Whenever the touchend event is triggered on the document
     */
    private onDocumentTouchend = (event: TouchEvent) => {
        if (!this.state.dragAndDrop) {
            return
        }
        this.props.onChange(this.getValueFromProgressPercentage())
        this.setState({
            dragAndDrop: undefined
        })
    }

    /**
     * Get the percentage of the value clamped between the min and max values
     */
    private getProgressPercentage = () => {
        let decimal = (this.props.value - this.props.minValue) / (this.props.maxValue - this.props.minValue)
        if (this.state.dragAndDrop && this.barRef.current) {
            decimal += (this.state.dragAndDrop.currentX - this.state.dragAndDrop.startX) / this.barRef.current.getBoundingClientRect().width
        }
        return Math.min(1, Math.max(0, decimal)) * 100
    }

    /**
     * Calculates the value based on the progress percentage
     */
    private getValueFromProgressPercentage = (): number => {
        const decimal = this.getProgressPercentage() / 100
        return (decimal * (this.props.maxValue - this.props.minValue)) + this.props.minValue
    }
}