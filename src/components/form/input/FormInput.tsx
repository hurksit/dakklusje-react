import React from 'react'
import './form-input.scss'
import classNames from 'classnames'
import { KeyCode } from '../../../shared/KeyCodes'
import { Search } from '@material-ui/icons'
import { Subscription } from 'rxjs'
import Events from '../../../shared/Events'

/**
 * The props
 */
interface FormInputProps<T> {

    /**
     * The input type
     */
    type: 'text' | 'multiline' | 'number' | 'password' | 'email' | 'filter' | 'euro'

    /**
     * The value
     */
    value: T

    /**
     * The placeholder when there is no value
     */
    placeholder?: string

    /**
     * The callback on change of value
     */
    onChange?: (value: T) => void

    /**
     * The callback on blur
     */
    onBlur?: () => void

    /**
     * Whether or not the input field is disabled
     */
    disabled?: boolean

    /**
     * Whether or not to autocomplete the value of form
     */
    autoComplete?: boolean|string

    /**
     * Optional: when the user presses enter perform a callback.
     */
    onEnter?: (inCombinationWithControlOrCommandKey: boolean) => void

    /**
     * When the value changes for type 'filter' and:
     * 
     * - the input has been blurred
     * - enter has been pressed
     * - the icon has been pressed
     * 
     * This will also cause the icon to be clickable
     */
    onChangeFilter?: () => void

    /**
     * The minimum amount of columns for a textarea
     */
    minColumns?: number

    /**
     * The maximum amount of columns for a textarea
     */
    maxColumns?: number
}

/**
 * The state
 */
interface FormInputState {
    
    /**
     * Whether or not the input is focused
     */
    isFocused: boolean

    /**
     * The amount of cols in the textarea
     */
    cols: number

    /**
     * The line height
     */
    lineHeight: number

    /**
     * The textarea base height
     */
    textAreaBaseHeight: number
}

/**
 * An input in a form (text + multiline, numbers)
 * 
 * @type T the datatype of the value property
 * 
 * @author Stan Hurks
 */
export default class FormInput<T> extends React.Component<FormInputProps<T>, FormInputState> {

    /**
     * The subscription for when a key has been pressed in the document
     */
    private subscriptionDocumentKeyDown!: Subscription

    /**
     * The subscription to the window resize event
     */
    private subscriptionWindowResize!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            isFocused: false,

            cols: 1,

            lineHeight: 0,

            textAreaBaseHeight: 0
        }
    }

    public componentDidMount = () => {
        this.subscriptionDocumentKeyDown = Events.document.keydown.subscribe(this.onDocumentKeyDown)
        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
        this.onWindowResize()
    }

    public componentWillUnmount = () => {
        this.subscriptionDocumentKeyDown.unsubscribe()
        this.subscriptionWindowResize.unsubscribe()
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.updateTextAreaHeight()
        })
    }

    public render = () => {
        return (
            <div className={classNames({
                'form-input': true,
                'with-value': this.hasValue(),
                'disabled': this.props.disabled,
                'focused': this.state.isFocused,
                ['type-' + this.props.type]: true,
                'filter-icon-hover': !!this.props.onChangeFilter
            })}>
                {
                    this.props.type === 'multiline'
                    ?
                    <textarea
                        style={{
                            ...this.props.minColumns !== undefined && this.props.maxColumns !== undefined
                            ? {
                                height: this.state.textAreaBaseHeight + (Math.max(0, this.state.cols - 1)) * this.state.lineHeight,
                                maxHeight: this.state.textAreaBaseHeight + (Math.max(0, this.state.cols - 1)) * this.state.lineHeight,
                                minHeight: this.state.textAreaBaseHeight + (Math.max(0, this.state.cols - 1)) * this.state.lineHeight
                            } : {}
                        }}
                        value={typeof this.props.value === 'number' && this.props.value === 0 ? '0' : (this.props.value as any || '')}
                        placeholder={this.props.placeholder} 
                        onChange={(event) => {     
                            if (!this.props.onChange) {
                                return
                            }
                            setTimeout(() => {
                                this.updateTextAreaHeight()
                            })
                            this.props.onChange(event.target.value as any)
                        }} 
                        disabled={this.props.disabled}
                        onFocus={() => {
                            this.setState({
                                isFocused: true
                            })
                        }}
                        onBlur={() => {
                            this.setState({
                                isFocused: false
                            })

                            if (this.props.onBlur) {
                                this.props.onBlur()
                            }
                        }}
                        ></textarea>
                    :
                    <input
                        type={this.getNativeInputType()}
                        placeholder={this.props.placeholder}
                        onChange={(event) => {        
                            if (!this.props.onChange) {
                                return
                            }            
                            switch (this.props.type) {
                                case 'text':
                                    this.props.onChange(event.target.value as any)
                                    break
                                case 'filter':
                                    this.props.onChange(event.target.value as any)
                                    break
                                case 'multiline':
                                    this.props.onChange(event.target.value as any)
                                    break
                                case 'email':
                                    this.props.onChange(event.target.value as any)
                                    break
                                case 'password':
                                    this.props.onChange(event.target.value as any)
                                    break
                                case 'euro':
                                    this.props.onChange(Number(event.target.valueAsNumber.toFixed(2)) as any)
                                    break
                                case 'number':
                                    this.props.onChange(event.target.valueAsNumber as any)
                                    break
                            }
                        }}
                        disabled={this.props.disabled}
                        onFocus={() => {
                            this.setState({
                                isFocused: true
                            })
                        }}
                        onBlur={() => {
                            this.setState({
                                isFocused: false
                            })
                            if (this.props.onChangeFilter) {
                                this.props.onChangeFilter()
                            }
                            if (this.props.onBlur) {
                                this.props.onBlur()
                            }
                        }}
                        value={this.props.value as any || ''} />
                }
                { 
                    this.props.type === 'filter'
                    &&
                    <div className="form-input-search-icon" onClick={() => {
                        if (!this.props.onChangeFilter) {
                            return
                        }
                        this.props.onChangeFilter()
                    }}>
                        <Search />
                    </div>
                }
            </div>
        )        
    }

    /**
     * Whenever the window resizes
     */
    private onWindowResize = () => {
        this.setState({
            lineHeight: window.innerWidth < 1000
                ? 0.9 * 18 * 2
                : 0.6 * 18 * 2,
            textAreaBaseHeight: window.innerWidth < 1000
                ? 39
                : 45
        })
    }

    /**
     * Whenever the user presses a key in the document
     */
    private onDocumentKeyDown = (event: KeyboardEvent) => {
        if (!this.state.isFocused) {
            return
        }

        if (event.keyCode === KeyCode.RETURN) {
            if (this.props.onEnter) {
                this.props.onEnter(event.metaKey)
            }

            if (this.props.onChangeFilter) {
                this.props.onChangeFilter()
            }
        }
    }

    /**
     * Updates the text area height
     */
    private updateTextAreaHeight = () => {
        if (((this.props.value || '') as string).length > 0) {
            let cols = ((this.props.value || '') as string).split('\n').length
            if (this.props.maxColumns !== undefined && cols > this.props.maxColumns) {
                cols = this.props.maxColumns
            }
            if (this.props.minColumns !== undefined && cols < this.props.minColumns) {
                cols = this.props.minColumns
            }
            this.setState({
                cols
            })
        } else {
            this.setState({
                cols: 1
            })
        }
    }

    /**
     * Get the native input type
     */
    private getNativeInputType = (): string => {
        switch (this.props.type) {
            case 'multiline':
                return 'text'
            case 'text':
                return 'text'
            case 'euro':
                return 'number'
            case 'filter':
                return 'text'
            case 'email':
                return 'email'
            case 'password':
                return 'password'
            case 'number':
                return 'number'
        }
    }

    /**
     * Checks whether or not the input has a value
     */
    private hasValue = (): boolean => {
        switch (this.props.type) {
            case 'multiline':
                return this.props.value !== null && typeof this.props.value === 'string' && this.props.value.length > 0
            case 'text':
                return this.props.value !== null && typeof this.props.value === 'string' && this.props.value.length > 0
            case 'euro':
                return this.props.value !== null && typeof this.props.value === 'number'
            case 'filter':
                return this.props.value !== null && typeof this.props.value === 'string' && this.props.value.length > 0
            case 'password':
                return this.props.value !== null && typeof this.props.value === 'string' && this.props.value.length > 0
            case 'email':
                return this.props.value !== null && typeof this.props.value === 'string' && this.props.value.length > 0
            case 'number':
                return this.props.value !== null && typeof this.props.value === 'number'
        }
    }
}