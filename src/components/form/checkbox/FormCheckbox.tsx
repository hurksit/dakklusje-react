import React from 'react'
import './form-checkbox.scss'
import { Check } from '@material-ui/icons'
import classNames from 'classnames'

/**
 * The props
 */
interface FormCheckboxProps {
    
    /**
     * The value of the checkbox
     */
    value: boolean

    /**
     * The label to display next to the checkbox
     */
    label: string

    /**
     * The on change callback for the component.
     */
    onChange: (value: boolean) => void

    /**
     * The size of the checkbox label
     */
    size?: 'small' | 'regular'
}

/**
 * A checkbox in a form.
 * 
 * @author Stan Hurks
 */
export default class FormCheckbox extends React.Component<FormCheckboxProps> {
    
    public render = () => {
        return (
            <div className={classNames({
                'form-checkbox': true,
                [`size-${this.props.size || 'regular'}`]: true,
                'active': this.props.value
            })} onClick={() => {
                this.props.onChange(!this.props.value)
            }}>
                <div className="form-checkbox-box">
                    <Check />
                </div><div className="form-checkbox-label">
                    {
                        this.props.label
                    }
                </div>
            </div>
        )
    }
}