import React from 'react'
import './form-group.scss'
import FormValidation, { FormValidationFieldInvalidated, FormValidationOptionsAsFormGroupProperty } from '../../../core/FormValidation'
import { FormInvalidation } from '../../../enumeration/FormInvalidation'
import { Subscription, Subject } from 'rxjs'
import Translations from '../../../translations/Translations'
import classNames from 'classnames'
import IconError from '../../icon/IconError'

/**
 * The props
 */
interface FormGroupProps {

    /**
     * A brief description of the form element in the form group.
     * 
     * Note that this MUST be unique for the FormValidation functionality.
     */
    label: string

    /**
     * A text to help the user understand the form element more
     */
    helperText: string|JSX.Element

    /**
     * The validation rule to use, if any
     */
    validation?: FormValidationOptionsAsFormGroupProperty
}

/**
 * The state
 */
interface FormGroupState {

    /**
     * The form invalidation (if any)
     */
    invalidation: FormInvalidation | null

    /**
     * Whether or not the error icon is hovered
     */
    hoverErrorIcon: boolean
}

/**
 * A container for form elements with a label, validation and helpertext.
 * 
 * @author Stan Hurks
 */
export default class FormGroup extends React.Component<FormGroupProps, FormGroupState> {

    /**
     * Whenever the form validation changes
     */
    public static readonly formValidationChange: Subject<void> = new Subject()

    /**
     * The subscription to when a field has been validated in the FormValidation class
     */
    private subscriptionFormValidationFieldValidated!: Subscription

    /**
     * The subscription to when a field has been invalidated in the FormValidation class
     */
    private subscriptionFormValidationFieldInvalidated!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            invalidation: null,

            hoverErrorIcon: false
        }
    }

    public componentDidMount = () => {
        if (this.props.validation) {
            this.subscriptionFormValidationFieldInvalidated = FormValidation.fieldInvalidated.subscribe(this.onFormValidationFieldInvalidated)
            this.subscriptionFormValidationFieldValidated = FormValidation.fieldValidated.subscribe(this.onFormValidationFieldValidated)

            FormValidation.addField({
                ...this.props.validation,
                label: this.props.label
            })
        }
    }

    public componentWillUnmount = () => {
        if (this.props.validation) {
            this.subscriptionFormValidationFieldInvalidated.unsubscribe()
            this.subscriptionFormValidationFieldValidated.unsubscribe()

            FormValidation.removeField(this.props.label)
        }
    }

    public componentWillReceiveProps = () => {
        if (this.props.validation) {
            FormValidation.removeField(this.props.label)
            FormValidation.addField({
                ...this.props.validation,
                label: this.props.label
            })
        }
    }

    public render = () => {
        return (
            <div className="form-group">
                <div className="form-group-label">
                    <span className={classNames({
                        'form-group-label-label': true,
                        'error': this.state.invalidation !== null
                    })}>
                        {
                            this.props.label
                        }
                    </span>
                    {
                        this.state.invalidation !== null
                        &&
                        <div className="form-group-label-error" onMouseOver={() => {
                            if (window.innerWidth < 1000) {
                                return
                            }
                            this.setState({
                                hoverErrorIcon: true
                            })
                        }} onMouseLeave={() => {
                            if (window.innerWidth < 1000) {
                                return
                            }
                            this.setState({
                                hoverErrorIcon: false
                            })
                        }} onClick={() => {
                            if (window.innerWidth >= 1000) {
                                return
                            }
                            this.setState({
                                hoverErrorIcon: !this.state.hoverErrorIcon
                            })
                        }}>
                            <IconError />
                        </div>
                    }
                </div>
                {
                    this.state.hoverErrorIcon
                    &&
                    this.state.invalidation !== null
                    ?
                    <div className="form-group-helper-text error">
                        {
                            Translations.translations.enumerations.formInvalidation(this.state.invalidation, this.props.validation)
                        }
                    </div>
                    :
                    <div className="form-group-helper-text">
                        {
                            this.props.helperText
                        }
                    </div>
                }
                <div className="form-group-element">
                    {
                        this.props.children
                    }
                </div>
                {
                    this.props.validation
                    &&
                    this.props.validation.string
                    &&
                    this.props.validation.string.maxLength
                    &&
                    <div className={classNames({
                        'form-group-string-length-indicator': true,
                        'error': this.isStringTooLong() || this.isStringTooShort()
                    })}>
                        {
                            this.getStringLength()
                        } / {
                            this.getStringMinLengthOrMaxLength()
                        }
                    </div>
                }
            </div>
        )
    }

    /**
     * Whenever a field has been invalidated in the FormValidation class.
     */
    private onFormValidationFieldInvalidated = (payload: FormValidationFieldInvalidated) => {
        if (!this.props.validation) {
            this.setState({
                invalidation: null
            })
            return
        }
        if (payload.label !== this.props.label) {
            return
        }
        this.setState({
            invalidation: payload.invalidation
        }, () => {
            FormGroup.formValidationChange.next()
        })
    }

    /**
     * Whenever a field has been validated in the FormValidation class.
     */
    private onFormValidationFieldValidated = (label: string) => {
        if (!this.props.validation) {
            this.setState({
                invalidation: null
            })
            return
        }
        if (label !== this.props.label) {
            return
        }
        this.setState({
            invalidation: null
        }, () => {
            FormGroup.formValidationChange.next()
        })
    }

    /**
     * Checks whether the value is a string and if its too short
     */
    private isStringTooShort = (): boolean => {
        if (!this.props.validation || !this.props.validation.string || !this.props.validation.string.minLength) {
            return false
        }

        const value = this.props.validation.evaluateValue()
        if (value === undefined || value === null || typeof value !== 'string') {
            return true
        }

        return value.length < this.props.validation.string.minLength
    }

    /**
     * Checks whether the value is a string and if its too long
     */
    private isStringTooLong = (): boolean => {
        if (!this.props.validation || !this.props.validation.string || !this.props.validation.string.maxLength) {
            return false
        }

        const value = this.props.validation.evaluateValue()
        if (value === undefined || value === null || typeof value !== 'string') {
            return true
        }

        return value.length > this.props.validation.string.maxLength
    }

    /**
     * Get the length of the evaluated value as a string
     */
    private getStringLength = (): number => {
        if (this.props.validation) {
            const evaluatedValue = this.props.validation.evaluateValue()
            if (typeof evaluatedValue === 'string') {
                return evaluatedValue.length
            }
        }
        return 0
    }

    /**
     * Get the min length required if one is provided in the props and the length of the evaluated string is shorter than that.
     * 
     * Otherwise return the max length.
     */
    private getStringMinLengthOrMaxLength = (): number => {
        if (this.props.validation && this.props.validation.string && this.props.validation.string.maxLength) {
            const evaluatedValue = this.props.validation.evaluateValue()
            const minLength = this.props.validation.string.minLength
            if (typeof evaluatedValue === 'string') {
                if (minLength && evaluatedValue.length < minLength) {
                    return minLength
                }
                return this.props.validation.string.maxLength
            } else if (minLength) {
                return minLength
            } else {
                return this.props.validation.string.maxLength
            }
        }
        return 0
    }
}
