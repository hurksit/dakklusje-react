import React from 'react'
import './form-select.scss'
import { KeyboardArrowDown } from '@material-ui/icons'
import { Subject, Subscription } from 'rxjs'
import classNames from 'classnames'
import Modal from '../../modal/Modal'
import FormSelectMenu from '../../modal/form-select-menu/FormSelectMenu'

/**
 * An option in the form select
 */
export interface FormSelectOption {

    /**
     * The key
     */
    key: string

    /**
     * The value
     */
    value: string
}

/**
 * The props
 */
interface FormSelectProps {

    /**
     * The key of the value to display
     */
    value: string[]|string|null
    
    /**
     * The available options
     */
    options: FormSelectOption[]

    /**
     * The buttons to display next to the search input
     */
    buttons?: Array<{
        /**
         * The label of the button
         */
        label: string

        /**
         * The color of the button
         */
        color: 'white' | 'purple' | 'light-purple' | 'orange'

        /**
         * The callback on click
         */
        onClick: () => void
    }>

    /**
     * The placeholder
     */
    placeholder?: string

    /**
     * The callback for when the selection has changed.
     */
    onChange?: (value: any) => void

    /**
     * Whether or not the element is disabled
     */
    disabled?: boolean
}

/**
 * The state
 */
interface FormSelectState {

    /**
     * Whether the select is open at the moment.
     */
    active: boolean
}

/**
 * A form select.
 * 
 * @author Stan Hurks
 */
export default class FormSelect extends React.Component<FormSelectProps, FormSelectState> {

    /**
     * The subject for closing the select.
     * 
     * This closes all FormSelect instances.
     */
    public static readonly close: Subject<void> = new Subject()

    /**
     * The reference to this element
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The subscription for the close subject
     */
    private subscriptionClose!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            active: false
        }
    }

    public componentDidMount = () => {
        this.subscriptionClose = FormSelect.close.subscribe(this.onClose)
    }

    public componentWillUnmount = () => {
        this.subscriptionClose.unsubscribe()
    }

    public render = () => {
        return (
            <div className={classNames({
                'form-select': true,
                'active': this.state.active,
                'disabled': this.props.disabled
            })} ref={this.ref} onClick={() => {
                if (!this.ref.current || this.props.disabled || !this.props.onChange) {
                    return
                }
                this.setState({
                    active: true
                })
                Modal.mount.next({
                    element: (
                        <FormSelectMenu
                            buttons={this.props.buttons}
                            options={this.props.options}
                            value={this.props.value}
                            onChange={this.props.onChange} />
                    ),
                    target: this.ref.current,
                    position: 'bottom-left',
                    offsetY: 120,
                    dismissable: true,
                    history: true
                })
            }}>
                <div className="form-select-table">
                    <div className="form-select-table-left">
                        {
                            this.evaluateValue()
                        }
                    </div><div className="form-select-table-right">
                        <KeyboardArrowDown />
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the form select menu gets closed
     */
    private onClose = () => {
        this.setState({
            active: false
        })
    }

    /**
     * Evaluates the value
     */
    private evaluateValue = (): string => {
        if (!this.props.placeholder) {
            return ''
        }
        if (this.props.value === null) {
            return this.props.placeholder
        }
        else if (typeof this.props.value === 'string') {
            const option = this.props.options.filter((v) => v.key === this.props.value)
            return option[0]
                ? option[0].value
                : this.props.placeholder
        }
        else if (typeof this.props.value === 'object' && this.props.value.length > 0) {
            let value = ''
            for (const key of this.props.value) {
                const option = this.props.options.filter((v) => v.key === key)
                value += option[0] ? option[0].value + ', ' : this.props.placeholder + ', '
            }
            return value.substring(0, value.length - 2)
        }
        return this.props.placeholder
    }
}