import React from 'react'
import { Subject, Subscription } from 'rxjs'
import './loader.scss'
import classNames from 'classnames'
import Translations from '../../translations/Translations'
import AppInfo from '../../core/AppInfo'
import ProgressSpinner from '../progress-spinner/ProgressSpinner'

/**
 * The state
 */
interface LoaderState {
    /**
     * The content to display in the loader.
     * Null when the loader should be hidden.
     */
    content: string|null
}

/**
 * The loader component.
 * 
 * This is used to give the user feedback on
 * what is happening in the back-end during API calls while they wait for the response.
 * 
 * @author Stan Hurks
 */
export default class Loader extends React.Component<any, LoaderState> {

    /**
     * Whether or not the loader is active
     */
    public static isActive: boolean = false

    /**
     * Display a message in the loader or hide it when the value given is `null`
     */
    public static set: Subject<string|null> = new Subject()
    
    /**
     * The internal subscription on the set subject.
     */
    private subscriptionSet!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            content: null
        }
    }

    public componentDidMount() {
        this.subscriptionSet = Loader.set.subscribe(this.onSet)
    }

    public componentWillUnmount() {
        this.subscriptionSet.unsubscribe()
    }

    public render() {
        return (
            <aside className={classNames({
                'loader': true,
                'show': this.state.content !== null
            })}>
                <div className="loader-content">
                    <div className="loader-content-top">
                        <ProgressSpinner useLogo />
                    </div><h1 className="loader-content-bottom">
                        {
                            this.state.content
                        }
                    </h1>
                </div>

                <div className="loader-bottom-left">
                    <div className="loader-bottom-left-copyright">
                        &copy; {
                            new Date().getFullYear() > 2019
                            ? '2019 - ' + new Date().getFullYear()
                            : '2019'
                        } {
                            AppInfo.designedAndBuildBy.companyName
                        }
                    </div><div className="loader-bottom-left-all-rights-reserved">
                        {
                            Translations.translations.components.loader.allRightsReserved
                        }
                    </div>
                </div>
            </aside>
        )
    }

    /**
     * Whenever the set subject has been called
     * @param content the content
     */
    private onSet = (content: string|null) => {
        this.setState({
            content
        }, () => {
            if (this.state.content === null) {
                Loader.isActive = false
            } else {
                Loader.isActive = true
            }
        })
    }
}