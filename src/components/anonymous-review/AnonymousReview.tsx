import React from 'react'
import './anonymous-review.scss'
import AnonymousReviewResponse from '../../backend/response/entity/AnonymousReviewResponse'
import Picture from '../picture/Picture'
import Rating from '../rating/Rating'
import Translations from '../../translations/Translations'

interface AnonymousReviewProps {
    value: AnonymousReviewResponse
}

/**
 * An anonymous review for the experience from others component.
 * 
 * @author Stan Hurks
 */
export default class AnonymousReview extends React.Component<AnonymousReviewProps> {

    public render = () => {
        return (
            <div className="anonymous-review">
                <div className="anonymous-review-content">
                    <div className="anonymous-review-content-title">
                        {
                            this.props.value.title
                        }
                    </div><div className="anonymous-review-content-rating-category">
                        <div className="anonymous-review-content-rating-category-rating">
                            <Rating value={this.props.value.rating} />
                        </div><div className="anonymous-review-content-rating-category-category">
                            {
                                Translations.translations.backend.enumerations.jobCategory(this.props.value.jobCategory)
                            }
                        </div>
                    </div><div className="anonymous-review-content-picture-description">
                        <div className="anonymous-review-content-picture-description-picture-container">
                            <Picture image={this.props.value.image} size={150} />
                        </div><div className="anonymous-review-content-picture-description-description">
                            {
                                this.props.value.description
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}