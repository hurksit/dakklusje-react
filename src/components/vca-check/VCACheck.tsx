import React from 'react'
import './vca-check.scss'
import { Check } from '@material-ui/icons'

/**
 * The component to show that the roofspecialist has a VCA diploma.
 * 
 * @author Stan Hurks
 */
export default class VCACheck extends React.Component {

    public render = () => {
        return (
            <div className="vca-check">
                <Check />
            </div>
        )
    }
}