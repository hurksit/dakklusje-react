import React from 'react'
import './profile-picture-change-modal.scss'
import Button from '../../../button/Button'
import Modal from '../../Modal'
import ImageCropper from '../../../image-cropper/ImageCropper'
import Storage from '../../../../storage/Storage'
import Translations from '../../../../translations/Translations'
import Loader from '../../../loader/Loader'
import Backend from '../../../../backend/Backend'
import SuccessModal from '../../generic/success/SuccessModal'

/**
 * The state
 */
interface ProfilePictureChangeModalState {
    
    /**
     * The image
     */
    image?: {

        /**
         * The image data URL
         */
        url: string

        /**
         * The background position of image
         */
        backgroundPosition: string

        /**
         * The zoom applied to image
         */
        zoom: number
    }
}

/**
 * The modal to change the profile picture with an image cropper.
 * 
 * @author Stan Hurks
 */
export default class ProfilePictureChangeModal extends React.Component<any, ProfilePictureChangeModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            image: undefined
        }
    }

    public componentDidMount = () => {
        if (Storage.data.session.user.profilePicture) {
            this.setState({
                image: {
                    url: Storage.data.session.user.profilePicture.imageURL,
                    backgroundPosition: Storage.data.session.user.profilePicture.backgroundPosition || '50% 50%',
                    zoom: Storage.data.session.user.profilePicture.zoom || 1
                }
            })
        } else {
            Modal.dismiss.next()
        }
    }

    public render = () => {
        return (
            <div className="profile-picture-change-modal no-padding">
                <div className="profile-picture-change-modal-image-cropper">
                    {
                        this.state.image
                        &&
                        <ImageCropper image={this.state.image} onChange={(data) => {
                            if (!this.state.image) {
                                return
                            }
                            this.setState({
                                image: {
                                    ...this.state.image,
                                    backgroundPosition: data.backgroundPosition,
                                    zoom: data.zoom
                                }
                            })
                        }} />
                    }
                </div><div className="modal-content-content padding">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            if (!this.state.image) {
                                return
                            }
                            Loader.set.next(Translations.translations.backend.controllers.profile.setProfilePictureBackgroundStyle.loader)
                            Backend.controllers.profile.setProfilePictureBackgroundStyle({
                                backgroundPosition: this.state.image.backgroundPosition,
                                zoom: this.state.image.zoom
                            }).then(() => {
                                Modal.mount.next({
                                    element: (
                                        <SuccessModal title={Translations.translations.backend.controllers.profile.setProfilePictureBackgroundStyle.responses[200].title} buttons={(
                                            <div className="modal-content-content-center">
                                                <Button color="orange" size="small" onClick={() => {
                                                    Modal.dismiss.next()
                                                }}>
                                                    {
                                                        Translations.translations.modals.defaultButtons.close
                                                    }
                                                </Button>
                                            </div>
                                        )}>
                                            {
                                                Translations.translations.backend.controllers.profile.setProfilePictureBackgroundStyle.responses[200].content
                                            }
                                        </SuccessModal>
                                    ),
                                    history: true,
                                    dismissLevels: 3
                                })
                            })
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.save
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}