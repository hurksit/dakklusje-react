import React from 'react'
import './profile-picture-settings-modal.scss'
import ProfilePicture from '../../../profile-picture/ProfilePicture'
import { Edit } from '@material-ui/icons'
import Button from '../../../button/Button'
import Modal from '../../Modal'
import Storage from '../../../../storage/Storage'
import Loader from '../../../loader/Loader'
import Backend from '../../../../backend/Backend'
import App from '../../../../core/app/App'
import ImageUpload from '../../../image-upload/ImageUpload'
import { Subscription } from 'rxjs'
import Translations from '../../../../translations/Translations'
import ProfilePictureChangeModal from '../change/ProfilePictureChangeModal'

/**
 * The modal for the profile picture settings.
 * 
 * @author Stan Hurks
 */
export default class ProfilePictureSettingsModal extends React.Component {

    /**
     * The subscription for when the app has been updated
     */
    private subscriptionAppUpdate!: Subscription

    public componentDidMount = () => {
        this.subscriptionAppUpdate = ImageUpload.newImageBase64.subscribe(this.onAppUpdate)
    }

    public componentWillUnmount = () => {
        this.subscriptionAppUpdate.unsubscribe()
    }

    public render = () => {
        return (
            <div className="profile-picture-settings-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.profilePicture.settings.title
                    }
                </div><div className="modal-content-content">
                    <ProfilePicture hoverIcon={(<Edit />)} onClick={() => {
                        App.imageUploadContext = 'profile-picture'
                        ImageUpload.prompt.next()
                    }} />
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div>
                {
                    Storage.data.session.user.profilePicture !== undefined
                    &&
                    <div>
                        <div className="modal-content-content">
                            <Button color="white" size="small" fullWidth onClick={() => {
                                Modal.mount.next({
                                    element: (
                                        <ProfilePictureChangeModal />
                                    ),
                                    pcOnly: window.innerWidth >= 1000,
                                    history: window.innerWidth >= 1000,
                                    dismissable: false
                                })
                            }}>
                                {
                                    Translations.translations.modals.profilePicture.settings.reposition
                                }
                            </Button>
                        </div><div className="modal-content-content">
                            <Button color="orange" size="small" fullWidth onClick={() => {
                                this.deleteProfilePicture()
                            }}>
                                {
                                    Translations.translations.modals.profilePicture.settings.delete
                                }
                            </Button>
                        </div><div className="modal-content-content">
                            <div className="modal-content-hr center"></div>
                        </div>
                    </div>
                }
                <div className="modal-content-content">
                    <div className="modal-content-content-center">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the app is updated
     */
    private onAppUpdate = () => {
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    /**
     * Deletes the profile picture
     */
    private deleteProfilePicture = () => {
        Loader.set.next(Translations.translations.backend.controllers.profile.deleteProfilePicture.loader)

        Backend.controllers.profile.deleteProfilePicture().then(() => {
            Storage.data.session.user.profilePicture = undefined
            setTimeout(() => {
                Modal.dismiss.next()
                App.update.next()
            })
        }).catch((response) => {
            switch (response.status) {
                case 503:
                    Modal.mountErrorHistory.next(Translations.translations.backend.generic.responses[503].amazonWebServicesS3)
                    break
            }
        })
    }
}