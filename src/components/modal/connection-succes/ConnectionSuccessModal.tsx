import React from 'react'
import './connection-success-modal.scss'
import { Check } from '@material-ui/icons'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import RoofSpecialistJobResponse from '../../../backend/controller/roofspecialist/job/response/RoofSpecialistJobResponse'
import JobCosts from '../../../core/JobCosts'

/**
 * The props
 */
interface ConnectionSuccessModalProps {

    /**
     * The job
     */
    job: RoofSpecialistJobResponse
}

/**
 * The modal when there is a new connection for the user.
 * 
 * @author Stan Hurks
 */
export default class ConnectionSuccessModal extends React.Component<ConnectionSuccessModalProps> {

    public render = () => {
        return (
            <div className="connection-success-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.connectionSuccess.title
                    }
                </div><div className="modal-content-content">
                    <Check />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.connectionSuccess.content(JobCosts.calculateCosts(this.props.job))
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}