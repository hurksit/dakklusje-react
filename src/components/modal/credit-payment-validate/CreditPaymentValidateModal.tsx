import React from 'react'
import Button from '../../button/Button'
import Backend from '../../../backend/Backend'
import Loader from '../../loader/Loader'
import Modal from '../Modal'
import Translations from '../../../translations/Translations'
import './credit-payment-validate-modal.scss'
import Device from '../../../shared/Device'

/**
 * The props
 */
interface CreditPaymentValidateModalProps {

    /**
     * The id of the credit request
     */
    id: string

    /**
     * The link to checkout for apple devices PWA
     */
    checkoutLink: string
}

/**
 * The modal to validate a payment in mollie for a credit request.
 * 
 * @author Stan Hurks
 */
export default class CreditPaymentValidateModal extends React.Component<CreditPaymentValidateModalProps> {

    public render = () => {
        return (
            <div className="credit-payment-validate-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.creditPaymentValidate.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w50"></div>
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.creditPaymentValidate.description[Device.isPWAStandalone ? 'pwa' : 'nonPwa']
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    {
                        Device.isPWAStandalone
                        &&
                        <Button color="white" size="small" href={this.props.checkoutLink}>
                            {
                                Translations.translations.modals.defaultButtons.toCheckout
                            }
                        </Button>
                    }
                    <Button color="orange" size="small" onClick={() => {
                        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.validateCreditRequest.loader)
                        Backend.controllers.roofSpecialist.validateCreditRequest(this.props.id).then(() => {
                            Modal.mountSuccess.next(Translations.translations.backend.controllers.roofSpecialist.validateCreditRequest.responses[200])
                        }).catch((response) => {
                            const translation = Translations.translations.backend.controllers.roofSpecialist.validateCreditRequest.responses[response.status]
                            if (translation !== undefined) {
                                Modal.mountError.next(translation)
                            }
                        })
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.validateCosts
                        }
                    </Button>
                </div>
            </div>
        )
    }
}