import React from 'react'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import TempImageResponse from '../../../backend/response/entity/TempImageResponse'
import './edit-temp-image-modal.scss'
import ImageUpload from '../../image-upload/ImageUpload'
import Modal from '../Modal'
import ChangeImageBackgroundStyleModal from '../image/background-style/ChangeImageBackgroundStyleModal'
import Backend from '../../../backend/Backend'
import Loader from '../../loader/Loader'

/**
 * The props
 */
interface EditTempImageModalProps {

    /**
     * The temp image
     */
    tempImage: TempImageResponse

    /**
     * Whenever there is a new picture requested through `ImageUpload`.
     */
    onNewImageRequest: () => void

    /**
     * Whenever the values in the image cropper change
     */
    onChangeTempImage: () => void
}

/**
 * The modal to edit a temp image.
 * 
 * @author Stan Hurks
 */
export default class EditTempImageModal extends React.Component<EditTempImageModalProps> {

    public render = () => {
        return (
            <div className="edit-temp-image-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.editTempImage.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        Modal.mount.next({
                            element: (
                                <ChangeImageBackgroundStyleModal
                                    onChange={(imageCropperImage) => {
                                        Loader.set.next(Translations.translations.backend.controllers.consumer.job.register.editTempImageBackgroundStyle.loader)
                                        Backend.controllers.consumerJobRegister.editTempImageBackgroundStyle(this.props.tempImage.id, imageCropperImage).then((response) => {
                                            this.props.onChangeTempImage()

                                            Modal.dismiss.next('all')
                                        }).catch((response) => {
                                            const translation = Translations.translations.backend.controllers.consumer.job.register.editTempImageBackgroundStyle.responses[response.status]
                                            if (translation !== undefined) {
                                                Modal.mountErrorHistory.next(translation)
                                            }
                                        })
                                    }}
                                    image={this.props.tempImage.image}/>
                            ),
                            history: true,
                            dismissable: false
                        })
                    }}>
                        {
                            Translations.translations.modals.editTempImage.buttons.reposition
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        ImageUpload.prompt.next()
                        this.props.onNewImageRequest()
                    }}>
                        {
                            Translations.translations.modals.editTempImage.buttons.newPicture
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.consumer.job.register.deleteTempImage.loader)
                            Backend.controllers.consumerJobRegister.deleteTempImage(this.props.tempImage.id).then(() => {
                                this.props.onChangeTempImage()
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.consumer.job.register.deleteTempImage.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.delete
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}