import React from 'react'
import './success-modal.scss'
import { Check } from '@material-ui/icons'

/**
 * The props
 */
interface SuccessModalProps {

    /**
     * The title
     */
    title: string

    /**
     * The buttons
     */
    buttons: JSX.Element
}

/**
 * The success modal.
 * 
 * @author Stan Hurks
 */
export default class SuccessModal extends React.Component<SuccessModalProps> {

    public render = () => {
        return (
            <div className="success-modal">
                <div className="modal-content-title">
                    {
                        this.props.title
                    }
                </div><div className="modal-content-content">
                    <Check />
                </div><div className="modal-content-content">
                    {
                        this.props.children
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    {
                        this.props.buttons
                    }
                </div>
            </div>
        )
    }
}