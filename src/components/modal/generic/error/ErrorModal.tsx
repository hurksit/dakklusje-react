import React from 'react'
import './error-modal.scss'
import IconError from '../../../icon/IconError'

/**
 * The props
 */
interface ErrorModalProps {

    /**
     * The title
     */
    title: string

    /**
     * The buttons
     */
    buttons: JSX.Element
}

/**
 * The error modal.
 * 
 * @author Stan Hurks
 */
export default class ErrorModal extends React.Component<ErrorModalProps> {

    public render = () => {
        return (
            <div className="error-modal">
                <div className="modal-content-title">
                    {
                        this.props.title
                    }
                </div><div className="modal-content-content">
                    <IconError />
                </div><div className="modal-content-content">
                    {
                        this.props.children
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    {
                        this.props.buttons
                    }
                </div>
            </div>
        )
    }
}