import React from 'react'
import './form-select-menu.scss'
import { FormSelectOption } from '../../form/select/FormSelect'
import FormInput from '../../form/input/FormInput'
import Translations from '../../../translations/Translations'
import FormCheckbox from '../../form/checkbox/FormCheckbox'
import Button from '../../button/Button'
import Modal from '../Modal'

/**
 * The props
 */
interface FormSelectMenuProps {

    /**
     * The key of the value to display
     */
    value: string[]|string|null

    /**
     * The available options
     */
    options: FormSelectOption[]

    /**
     * The callback for when the selection has changed.
     */
    onChange: (value: any) => void

    /**
     * The buttons to display next to the search input
     */
    buttons?: Array<{
        /**
         * The label of the button
         */
        label: string

        /**
         * The color of the button
         */
        color: 'white' | 'purple' | 'light-purple' | 'orange'

        /**
         * The callback on click
         */
        onClick: () => void
    }>
}

/**
 * The state
 */
interface FormSelectMenuState {
    
    /**
     * The string to filter with
     */
    filter: string

    /**
     * The key of the value to display
     */
    value: string[]|string|null
}

/**
 * The menu for the form select component.
 * 
 * @author Stan Hurks
 */
export default class FormSelectMenu extends React.Component<FormSelectMenuProps, FormSelectMenuState> {

    constructor(props: any) {
        super(props)

        this.state = {
            filter: '',

            value: this.props.value
        }
    }

    public render = () => {
        return (
            <div className="form-select-menu">
                <div className="modal-content-content">
                    <div className="form-select-menu-top">
                        <div className="form-select-menu-top-left">
                            <FormInput 
                                value={this.state.filter}
                                placeholder={Translations.translations.components.form.input.filter.placeholder}
                                type="filter"
                                onChange={(filter) => {
                                    this.setState({
                                        filter
                                    })
                                }} />
                        </div>
                        {
                            this.props.buttons
                            ?
                            <div className="form-select-menu-top-right">
                                {
                                    this.props.buttons.map((button, buttonIndex) => 
                                        <Button color={button.color} onClick={button.onClick} size="small" disabled={this.state.value === null} key={buttonIndex}>
                                            {
                                                button.label
                                            }
                                        </Button>
                                    )
                                }
                            </div>
                            :
                            <div className="form-select-menu-top-right">
                                {
                                    this.props.value !== null
                                    && typeof this.props.value !== 'string'
                                    &&
                                    <Button color="purple" size="small" onClick={() => {
                                        this.setState({
                                            value: []
                                        }, () => {
                                            this.props.onChange(this.state.value)
                                        })
                                    }}>
                                        {
                                            Translations.translations.modals.defaultButtons.reset
                                        }
                                    </Button>
                                }

                                <Button color="orange" size="small" onClick={() => {
                                    Modal.dismiss.next()
                                }}>
                                    {
                                        Translations.translations.modals.defaultButtons.ok
                                    }
                                </Button>
                            </div>
                        }
                    </div>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr w100"></div>
                </div><div className="form-select-menu-content modal-content-content">
                    <div className="form-select-menu-content-options">
                        {
                            this.props.options
                                .filter((option) => this.state.filter.length === 0 || option.value.toLowerCase().indexOf(this.state.filter.toLowerCase()) !== -1)
                                .map((option, optionIndex) =>
                                    <FormCheckbox
                                        label={option.value}
                                        value={this.isCheckboxActive(option)}
                                        onChange={(value) => {
                                            if (this.state.value === null) {
                                                this.onChange(option.key)
                                            } else if (typeof this.state.value === 'string') {
                                                this.onChange(option.key === this.state.value ? null : option.key)
                                            } else {
                                                if (this.state.value.indexOf(option.key) === -1) {
                                                    this.onChange(this.state.value.filter((v) => v !== option.key).concat([option.key]))
                                                } else {
                                                    this.onChange(this.state.value.filter((v) => v !== option.key))
                                                }
                                            }
                                        }}
                                        key={optionIndex} />
                                )
                        }
                    </div>
                    <div className="form-select-menu-content-gradient-top"></div>
                    <div className="form-select-menu-content-gradient-bottom"></div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the value changes internally
     */
    private onChange = (value: string|string[]|null) => {
        this.setState({
            value
        })
        this.props.onChange(value)
    }

    /**
     * Checks whether the checkbox is active
     */
    private isCheckboxActive = (option: FormSelectOption): boolean => {
        if (this.state.value === null) {
            return false
        } else if (typeof this.state.value === 'string') {
            return this.state.value === option.key
        } else {
            return this.state.value.indexOf(option.key) !== -1
        }
    }
}