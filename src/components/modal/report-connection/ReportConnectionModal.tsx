import React from 'react'
import './report-connection-modal.scss'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import ConnectionResponse from '../../../backend/response/entity/ConnectionResponse'
import Storage from '../../../storage/Storage'

/**
 * The props
 */
interface ReportConnectionModalProps {

    /**
     * The connection to report
     */
    connection: ConnectionResponse
}

/**
 * The state
 */
interface ReportConnectionModalState {

    /**
     * Whether or not the form is validated
     */
    validated: boolean

    /**
     * The description
     */
    description: string|null
}

/**
 * The modal to report a connection.
 */
export default class ReportConnectionModal extends React.Component<ReportConnectionModalProps, ReportConnectionModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            validated: false,

            description: null
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public render = () => {
        return (
            <div className="report-connection-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.reportConnection.title
                    }
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportConnection.form.user.label}
                        helperText={Translations.translations.modals.reportConnection.form.user.helperText}
                        >

                        <FormInput
                            type="text"
                            value={
                                Storage.data.session.user.roofSpecialist !== undefined
                                ? (
                                    this.props.connection.consumer && this.props.connection.consumer.personalDetailsFullName
                                    ? this.props.connection.consumer.personalDetailsFullName
                                    : Translations.translations.pages.inbox.contacts.content.connection.name.anonymous
                                )
                                : this.props.connection.roofSpecialist.cocCompany.companyName
                            }
                            disabled={true}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportConnection.form.description.label}
                        helperText={Translations.translations.modals.reportConnection.form.description.helperText}
                        validation={{
                            ...FormValidation.entity.report.description,
                            evaluateValue: () => {
                                return this.state.description
                            }
                        }}
                        >

                        <FormInput 
                            type="multiline"
                            value={this.state.description}
                            placeholder={Translations.translations.modals.reportConnection.form.description.placeholder}
                            onChange={(description) => {
                                this.setState({
                                    description
                                }, () => {
                                    this.setState({
                                        validated: FormValidation.validate()
                                    })
                                })
                            }}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.cancel
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" disabled={!this.state.validated} onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.reportConnection.loader)
                            Backend.controllers.inboxConnection.reportConnection({
                                description: this.state.description as string,
                                connectionId: this.props.connection.id
                            }).then(() => {
                                Modal.mountSuccess.next(Translations.translations.backend.controllers.inbox.connection.reportConnection.responses[200])
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.inbox.connection.reportConnection.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.reportConnection.form.buttons.report
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}