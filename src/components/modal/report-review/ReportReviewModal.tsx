import React from 'react'
import './report-review-modal.scss'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import ReviewResponse from '../../../backend/response/entity/ReviewResponse'

/**
 * The props
 */
interface ReportReviewModalProps {

    /**
     * The review to report
     */
    review: ReviewResponse
}

/**
 * The state
 */
interface ReportReviewModalState {

    /**
     * Whether or not the form is validated
     */
    validated: boolean

    /**
     * The description
     */
    description: string|null
}

/**
 * The modal to report a review.
 */
export default class ReportReviewModal extends React.Component<ReportReviewModalProps, ReportReviewModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            validated: false,

            description: null
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public render = () => {
        return (
            <div className="report-review-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.reportReview.title
                    }
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportReview.form.user.label}
                        helperText={Translations.translations.modals.reportReview.form.user.helperText}
                        >

                        <FormInput
                            type="text"
                            value={
                                this.props.review.consumerPersonalDetailsFullName || Translations.translations.pages.inbox.contacts.content.connection.name.anonymous
                            }
                            disabled={true}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportReview.form.description.label}
                        helperText={Translations.translations.modals.reportReview.form.description.helperText}
                        validation={{
                            ...FormValidation.entity.report.description,
                            evaluateValue: () => {
                                return this.state.description
                            }
                        }}
                        >

                        <FormInput 
                            type="multiline"
                            value={this.state.description}
                            placeholder={Translations.translations.modals.reportReview.form.description.placeholder}
                            onChange={(description) => {
                                this.setState({
                                    description
                                }, () => {
                                    this.setState({
                                        validated: FormValidation.validate()
                                    })
                                })
                            }}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.cancel
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" disabled={!this.state.validated} onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.review.reportReview.loader)
                            Backend.controllers.review.reportReview({
                                description: this.state.description as string,
                                reviewId: this.props.review.id
                            }).then(() => {
                                Modal.mountSuccess.next(Translations.translations.backend.controllers.review.reportReview.responses[200])
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.review.reportReview.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.reportReview.form.buttons.report
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}