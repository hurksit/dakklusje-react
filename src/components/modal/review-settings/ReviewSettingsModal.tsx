import React from 'react'
import './review-settings-modal.scss'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import ReviewResponse from '../../../backend/response/entity/ReviewResponse'
import Modal from '../Modal'
import './review-settings-modal.scss'
import ReportReviewModal from '../report-review/ReportReviewModal'

/**
 * The props
 */
interface ReviewSettingsModalProps {

    /**
     * The review
     */
    review: ReviewResponse
}

/**
 * The review settings modal.
 * 
 * @author Stan Hurks
 */
export default class ReviewSettingsModal extends React.Component<ReviewSettingsModalProps> {

    public render = () => {
        return (
            <div className="review-settings-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.reviewSettings.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" fullWidth onClick={() => {
                        Modal.mount.next({
                            element: (
                                <ReportReviewModal review={this.props.review} />
                            ),
                            history: true
                        })
                    }}>
                        {
                            Translations.translations.modals.reviewSettings.buttons.report
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}