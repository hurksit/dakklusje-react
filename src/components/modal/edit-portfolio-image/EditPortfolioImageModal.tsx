import React from 'react'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import ChangeImageBackgroundStyleModal from '../image/background-style/ChangeImageBackgroundStyleModal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import PortfolioImageResponse from '../../../backend/response/entity/PortfolioImageResponse'
import ImageUpload from '../../image-upload/ImageUpload'
import './edit-portfolio-image-modal.scss'

/**
 * The props
 */
interface EditPortfolioImageModalProps {

    /**
     * The portfolio image
     */
    portfolioImage: PortfolioImageResponse

    /**
     * Whenever there is a new picture requested through `ImageUpload`
     */
    onNewImageRequest: () => void

    /**
     * Whenever the values in the image cropper change
     */
    onChangePortfolioImage: () => void
}

/**
 * The modal 
 */
export default class EditPortfolioImageModal extends React.Component<EditPortfolioImageModalProps> {

    public render = () => {
        return (
            <div className="edit-portfolio-image-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.editPortfolioImage.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        Modal.mount.next({
                            element: (
                                <ChangeImageBackgroundStyleModal
                                    onChange={(imageCropperImage) => {
                                        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.portfolio.editPortfolioImageBackgroundStyle.loader)
                                        Backend.controllers.roofSpecialistPortfolio.editPortfolioImageBackgroundStyle(this.props.portfolioImage.id, imageCropperImage).then((response) => {
                                            this.props.onChangePortfolioImage()

                                            Modal.dismiss.next('all')
                                        }).catch((response) => {
                                            const translation = Translations.translations.backend.controllers.roofSpecialist.portfolio.editPortfolioImageContent.responses[response.status]
                                            if (translation !== undefined) {
                                                Modal.mountErrorHistory.next(translation)
                                            }
                                        })
                                    }}
                                    image={this.props.portfolioImage.image}
                                    />
                            ),
                            history: true,
                            dismissable: false
                        })
                    }}>
                        {
                            Translations.translations.modals.editPortfolioImage.buttons.reposition
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        ImageUpload.prompt.next()
                        this.props.onNewImageRequest()
                    }}>
                        {
                            Translations.translations.modals.editPortfolioImage.buttons.newPicture
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.portfolio.deletePortfolioImage.loader)
                            Backend.controllers.roofSpecialistPortfolio.deletePortfolioImage(this.props.portfolioImage.id).then((response) => {
                                this.props.onChangePortfolioImage()
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.roofSpecialist.portfolio.deletePortfolioImage.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.delete
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}