import React from 'react'
import './calculate-costs-modal.scss'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import { JobCategory } from '../../../backend/enumeration/JobCategory'
import FormGroup from '../../form/group/FormGroup'
import FormSelect from '../../form/select/FormSelect'

/**
 * The state
 */
interface CalculateCostsModalState {

    /**
     * The selected job category
     */
    jobCategory: JobCategory|null
}

/**
 * The modal to calculate the average costs for a job.
 * 
 * @author Stan Hurks
 */
export default class CalculateCostsModal extends React.Component<any, CalculateCostsModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            jobCategory: null
        }
    }

    public render = () => {
        return (
            <div className="calculate-costs-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.calculateCosts.title
                    }
                </div>
                <div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.calculateCosts.jobCategory.label}
                        helperText={Translations.translations.modals.calculateCosts.jobCategory.helperText}>

                        <FormSelect
                            placeholder={Translations.translations.modals.calculateCosts.jobCategory.placeholder}
                            value={this.state.jobCategory}
                            options={Object.keys(JobCategory).map((key) => ({
                                key,
                                value: Translations.translations.backend.enumerations.jobCategory(key as any)
                            }))}
                            onChange={(jobCategory) => {
                                this.setState({
                                    jobCategory
                                })
                            }}
                            />
                    </FormGroup>
                </div>
                {
                    this.state.jobCategory !== null
                    &&
                    (
                        <div className="modal-content-content">
                            {
                                Translations.translations.averageCostsForJob(this.state.jobCategory)
                            }
                        </div>
                    )
                }
                <div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}