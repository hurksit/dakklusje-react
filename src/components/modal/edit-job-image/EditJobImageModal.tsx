import React from 'react'
import './edit-job-image-modal.scss'
import Translations from '../../../translations/Translations'
import Modal from '../Modal'
import Button from '../../button/Button'
import ChangeImageBackgroundStyleModal from '../image/background-style/ChangeImageBackgroundStyleModal'
import Backend from '../../../backend/Backend'
import Loader from '../../loader/Loader'
import JobImageResponse from '../../../backend/response/entity/JobImageResponse'
import ImageUpload from '../../image-upload/ImageUpload'

/**
 * The props
 */
interface EditJobImageModalProps { 

    /**
     * The job image
     */
    jobImage: JobImageResponse

    /**
     * Whenever there is a new picture requested through `ImageUpload`.
     */
    onNewImageRequest: () => void

    /**
     * Whenever the values in the image cropper change
     */
    onChangeJobImage: () => void
}

/**
 * The modal to edit a job image.
 * 
 * @author Stan Hurks
 */
export default class EditJobImageModal extends React.Component<EditJobImageModalProps> {

    public render = () => {
        return (
            <div className="edit-job-image-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.editJobImage.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        Modal.mount.next({
                            element: (
                                <ChangeImageBackgroundStyleModal
                                    onChange={(imageCropperImage) => {
                                        Loader.set.next(Translations.translations.backend.controllers.consumer.job.editJobImageBackgroundStyle.loader)
                                        Backend.controllers.consumerJob.editJobImageBackgroundStyle(this.props.jobImage.id, imageCropperImage).then((response) => {
                                            this.props.onChangeJobImage()

                                            Modal.dismiss.next('all')
                                        }).catch((response) => {
                                            const translation = Translations.translations.backend.controllers.consumer.job.editJobImageBackgroundStyle.responses[response.status]
                                            if (translation !== undefined) {
                                                Modal.mountErrorHistory.next(translation)
                                            }
                                        })
                                    }}
                                    image={this.props.jobImage.image}/>
                            ),
                            history: true,
                            dismissable: false
                        })
                    }}>
                        {
                            Translations.translations.modals.editJobImage.buttons.reposition
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        ImageUpload.prompt.next()
                        this.props.onNewImageRequest()
                    }}>
                        {
                            Translations.translations.modals.editJobImage.buttons.newPicture
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.consumer.job.deleteJobImage.loader)
                            Backend.controllers.consumerJob.deleteJobImage(this.props.jobImage.id).then(() => {
                                this.props.onChangeJobImage()
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.consumer.job.deleteJobImage.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.delete
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}