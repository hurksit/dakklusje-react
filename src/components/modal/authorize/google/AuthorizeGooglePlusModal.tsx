import React from 'react'
import './authorize-google-plus-modal.scss'
import Translations from '../../../../translations/Translations'
import Button from '../../../button/Button'
import Modal from '../../Modal'
import FormInput from '../../../form/input/FormInput'
import Expressions from '../../../../core/Expressions'
import Loader from '../../../loader/Loader'
import Backend from '../../../../backend/Backend'
import AuthorizedSessionResponse from '../../../../backend/response/entity/AuthorizedSessionResponse'

/**
 * The props
 */
interface AuthorizeGooglePlusModalProps {
    
    /**
     * The google id token
     */
    googleIdToken: string

    /**
     * Whenever the google+ login has been authorized.
     */
    onAuthorized: (response: AuthorizedSessionResponse) => void
}

/**
 * The state
 */
interface AuthorizeGooglePlusModalState {
    emailAddress: string|null

    password: string|null
}

/**
 * The modal to connect google+ with an account.
 * 
 * @author Stan Hurks
 */
export default class AuthorizeGooglePlusModal extends React.Component<AuthorizeGooglePlusModalProps, AuthorizeGooglePlusModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            emailAddress: null,

            password: null
        }
    }

    public render = () => {
        return (
            <form className="authorize-google-plus-modal" onSubmit={() => {
                this.connectWithGooglePlus()
            }}>
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.authorize.googlePlus.title
                    }
                </div><div className="modal-content-content">
                    <FormInput type="email" placeholder={Translations.translations.backend.entity.user.emailAddress.placeholder} autoComplete value={this.state.emailAddress} onChange={(value) => {
                        this.setState({
                            emailAddress: value
                        })
                    }} />
                    <FormInput type="password" placeholder={Translations.translations.backend.entity.user.password.placeholder} autoComplete="current-password" value={this.state.password} onChange={(value) => {
                        this.setState({
                            password: value
                        })
                    }} onEnter={() => {
                        this.connectWithGooglePlus()
                    }} />
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" fullWidth onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" type="submit" fullWidth disabled={!this.validate()}>
                            {
                                Translations.translations.modals.navigationMenuModal.unauthorized.form.login
                            }
                        </Button>
                    </div>
                </div>
            </form>
        )
    }

    /**
     * Connect with google-plus
     */
    private connectWithGooglePlus = () => {
        if (!this.validate()) {
            return
        }
        Loader.set.next(Translations.translations.backend.controllers.socialMedia.connectWithGooglePlus.loader)
        Backend.controllers.socialMedia.connectWithGooglePlus({
            googleIdToken: this.props.googleIdToken,
            emailAddress: this.state.emailAddress as string,
            password: this.state.password as string
        }).then((response) => {
            this.props.onAuthorized(response.data)
            Modal.dismiss.next()
        }).catch((response) => {
            const translation: string|undefined = Translations.translations.backend.controllers.socialMedia.connectWithGooglePlus.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }

    /**
     * Validates the form
     */
    private validate = (): boolean => {
        return Boolean(
            this.state.emailAddress
            && this.state.password
            && this.state.emailAddress.length > 0
            && this.state.password.length > 0
            && Expressions.email.test(this.state.emailAddress)
        )
    }
}