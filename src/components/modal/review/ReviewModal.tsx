import React from 'react'
import InboxConnectionResponse from '../../../backend/controller/inbox/response/InboxConnectionResponse'
import Rating from '../../rating/Rating'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import Modal from '../Modal'
import ReviewResponse from '../../../backend/response/entity/ReviewResponse'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import ReviewSuccesModal from '../review-success/ReviewSuccesModal'
import ReviewDeletedModal from '../review-deleted/ReviewDeletedModal'
import './review-modal.scss'

/**
 * The props
 */
interface ReviewModalProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse

    /**
     * The rating
     */
    rating: number

    /**
     * Whenever the review has been updated
     */
    onUpdate: (review: ReviewResponse|null) => void
}

/**
 * The state
 */
interface ReviewModalState {

    /**
     * The rating
     */
    rating: number

    /**
     * The review description
     */
    description: string|null

    /**
     * Whether the form is validated
     */
    validated: boolean

    /**
     * The review
     */
    review: ReviewResponse|null
}

/**
 * The modal to make a review.
 * 
 * @author Stan Hurks
 */
export default class ReviewModal extends React.Component<ReviewModalProps, ReviewModalState> {
    
    constructor(props: any) {
        super(props)

        this.state = {
            rating: this.props.rating,

            description: null,

            validated: false,
            
            review: null
        }

        FormValidation.reset()
    }

    public componentDidMount = () => {
        setTimeout(() => {
            this.setState({
                validated: FormValidation.validate()
            })
        })
        this.updateReview()
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.setState({
                rating: this.props.rating
            })
        })
    }

    public render = () => {
        return (
            <div className="review-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.review.title
                    }
                </div><div className="review-modal-rating">
                    <Rating value={this.state.rating} onSelect={(rating) => {
                        this.setState({
                            rating
                        })
                    }} />
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.review.form.description.label}
                        helperText={Translations.translations.modals.review.form.description.helperText}
                        validation={{
                            ...FormValidation.entity.review.description,
                            evaluateValue: () => {
                                return this.state.description
                            }
                        }}>

                        <FormInput
                            type="multiline"
                            value={this.state.description}
                            placeholder={Translations.translations.modals.review.form.description.placeholder(this.props.connection)}
                            onChange={(description) => {
                                this.setState({
                                    description
                                }, () => {
                                    this.setState({
                                        validated: FormValidation.validate()
                                    })
                                })
                            }}
                            />
                    </FormGroup>
                </div>
                {
                    this.state.review !== null
                    &&
                    <div className="modal-content-content right">
                        <Button color="orange" size="small" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.deleteReview.loader)
                            Backend.controllers.inboxConnectionConsumer.deleteReview(this.props.connection.id).then(() => {
                                Modal.mount.next({
                                    element: (
                                        <ReviewDeletedModal />
                                    ),
                                    history: true
                                })
                                this.props.onUpdate(null)
                            })
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.delete
                            }
                        </Button>
                    </div>
                }
                <div className="modal-content-content">
                    <div className="modal-content-hr right w25"></div>
                </div>
                <div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" fullWidth onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" fullWidth disabled={
                            !this.state.validated && (
                                this.state.review === null
                                || (
                                    this.state.review.description === this.state.description
                                    && this.state.review.rating === this.state.rating
                                )
                            )
                        } onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.addReviewRevision[this.state.review === null ? 'newReview' : 'existingReview'].loader)
                            Backend.controllers.inboxConnectionConsumer.addReviewRevision(this.props.connection.id, {
                                rating: this.state.rating,
                                content: this.state.description as string
                            }).then((response) => {
                                Modal.mount.next({
                                    element: (
                                        <ReviewSuccesModal existingReview={this.state.review !== null} />
                                    ),
                                    history: true
                                })
                                this.props.onUpdate(response.data)
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.inbox.connection.consumer.addReviewRevision[this.state.review === null ? 'newReview' : 'existingReview'].responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.review.buttons.place
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Update the review
     */
    private updateReview = () => {
        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.getReviewForConnection.loader)
        Backend.controllers.inboxConnectionConsumer.getReviewForConnection(this.props.connection.id).then((response) => {
            if (response.status === 200) {
                this.setState({
                    review: response.data,
                    description: response.data.description
                }, () => {
                    this.setState({
                        validated: FormValidation.validate()
                    })
                })
            }
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.inbox.connection.consumer.getReviewForConnection.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}