import React from 'react'
import Translations from '../../../../translations/Translations'
import Button from '../../../button/Button'
import RoofSpecialistJobResponse from '../../../../backend/controller/roofspecialist/job/response/RoofSpecialistJobResponse'
import Modal from '../../Modal'
import './job-settings-roof-specialist-modal.scss'
import ReportJobModal from '../../report-job/ReportJobModal'

/**
 * The props
 */
interface JobSettingsRoofSpecialistModalProps {

    /**
     * The job
     */
    job: RoofSpecialistJobResponse
}

/**
 * The job settings modal for roof specialists.
 * 
 * @author Stan Hurks
 */
export default class JobSettingsRoofSpecialistModal extends React.Component<JobSettingsRoofSpecialistModalProps> {

    public render = () => {
        return (
            <div className="job-settings-roof-specialist-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.jobSettings.roofSpecialist.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w50"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.mount.next({
                            element: (
                                <ReportJobModal job={this.props.job} />
                            ),
                            history: true,
                            dismissable: false
                        })
                    }} fullWidth>
                        {
                            Translations.translations.modals.jobSettings.roofSpecialist.buttons.reportJob
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w75"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>
                </div>
            </div>
        )
    }
}