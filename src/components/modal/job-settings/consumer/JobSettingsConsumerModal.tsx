import React from 'react'
import Translations from '../../../../translations/Translations'
import Button from '../../../button/Button'
import './job-settings-consumer-modal.scss'
import Routes from '../../../../routes/Routes'
import { Route } from '../../../../enumeration/Route'
import JobResponse from '../../../../backend/response/entity/JobResponse'
import Modal from '../../Modal'
import DestructiveActionModal from '../../destructive-action/DestructiveActionModal'
import Loader from '../../../loader/Loader'
import Backend from '../../../../backend/Backend'

/**
 * The props
 */
interface JobSettingsModalProps {

    /**
     * The job
     */
    job: JobResponse

    /**
     * Whenever the job is deleted
     */
    onDeleteJob: () => void
}

/**
 * The job settings modal for consumers.
 * 
 * @author Stan Hurks
 */
export default class JobSettingsConsumerModal extends React.Component<JobSettingsModalProps> {

    public render = () => {
        return (
            <div className="job-settings-consumer-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.jobSettings.consumer.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w25"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" onClick={() => {
                        Routes.redirectURL.next(Route.CONSUMER_EDIT_JOB.replace(':jobId', String(this.props.job.id)))
                        Modal.dismiss.next('all')
                    }} fullWidth>
                        {
                            Translations.translations.modals.jobSettings.consumer.buttons.editJob
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.mount.next({
                            element: (
                                <DestructiveActionModal onContinue={() => {
                                    Loader.set.next(Translations.translations.backend.controllers.consumer.job.deleteJob.loader)
                                    Backend.controllers.consumerJob.deleteJob(this.props.job.id).then(() => {
                                        this.props.onDeleteJob()
                                    }).catch((response) => {
                                        const translation = Translations.translations.backend.controllers.consumer.job.deleteJob.responses[response.status]
                                        if (translation !== undefined) {
                                            Modal.mountErrorHistory.next(translation)
                                        }
                                    })
                                }}>
                                    {
                                        Translations.translations.modals.jobSettings.consumer.destructiveAction.content
                                    }
                                </DestructiveActionModal>
                            ),
                            history: true
                        })
                    }} fullWidth>
                        {
                            Translations.translations.modals.jobSettings.consumer.buttons.deleteJob
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>
                </div>
            </div>
        )
    }
}