import React from 'react'
import './registration-complete-consumer-modal.scss'
import { MailOutline } from '@material-ui/icons'
import Button from '../../../button/Button'
import Translations from '../../../../translations/Translations'
import Modal from '../../Modal'

/**
 * The modal for when the registration is complete for the consumer.
 * 
 * @author Stan Hurks
 */
export default class RegistrationCompleteConsumerModal extends React.Component {

    public render = () => {
        return (
            <div className="registration-complete-consumer-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.registrationComplete.consumer.title
                    }
                </div><div className="modal-content-content">
                    <MailOutline />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.registrationComplete.consumer.content.map((part, partIndex) => 
                            <div key={partIndex}>
                                {
                                    part === ''
                                    ? (
                                        <br />
                                    )
                                    : part
                                }
                            </div>
                        )
                    }    
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}