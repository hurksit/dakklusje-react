import React from 'react'
import Translations from '../../../../translations/Translations'
import { MailOutline } from '@material-ui/icons'
import Button from '../../../button/Button'
import Modal from '../../Modal'
import './registration-complete-roof-specialist-modal.scss'

/**
 * The modal for when the registration is complete for the roof specialist.
 * 
 * @author Stan Hurks
 */
export default class RegistrationCompleteRoofSpecialistModal extends React.Component {

    public render = () => {
        return (
            <div className="registration-complete-roof-specialist-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.registrationComplete.roofSpecialist.title
                    }
                </div><div className="modal-content-content">
                    <MailOutline />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.registrationComplete.roofSpecialist.content.map((part, partIndex) => 
                            <div key={partIndex} style={{
                                marginTop: 10
                            }}>
                                {
                                    part
                                }
                            </div>
                        )
                    }    
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}