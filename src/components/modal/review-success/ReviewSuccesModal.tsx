import React from 'react'
import './review-success-modal.scss'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import { Check } from '@material-ui/icons'

/**
 * The props
 */
interface ReviewSuccesModalProps {
    
    /**
     * Whether or not the review already existed and is edited
     */
    existingReview: boolean
}

/**
 * The modal for when a review has been added/edited successfully.
 * 
 * @author Stan Hurks
 */
export default class ReviewSuccesModal extends React.Component<ReviewSuccesModalProps> {

    public render = () => {
        return (
            <div className="review-success-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.backend.controllers.inbox.connection.consumer.addReviewRevision[
                            this.props.existingReview
                            ? 'existingReview'
                            : 'newReview'
                        ].responses[200].title
                    }
                </div><div className="modal-content-content">
                    <Check />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.backend.controllers.inbox.connection.consumer.addReviewRevision[
                            this.props.existingReview
                            ? 'existingReview'
                            : 'newReview'
                        ].responses[200].content
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next('all')
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}