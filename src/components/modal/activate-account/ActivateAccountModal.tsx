import React from 'react'
import Translations from '../../../translations/Translations'
import IconError from '../../icon/IconError'
import Button from '../../button/Button'
import './activate-account-modal.scss'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import SuccessModal from '../generic/success/SuccessModal'

/**
 * The props
 */
interface ActivateAccountModalProps {

    /**
     * The e-mailaddress
     */
    emailAddress: string
}

/**
 * The modal to send a new activation mail to activate the account.
 * 
 * @author Stan Hurks
 */
export default class ActivateAccountModal extends React.Component<ActivateAccountModalProps> {

    public render = () => {
        return (
            <div className="activate-account-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.activateAccount.title
                    }
                </div><div className="modal-content-content">
                    <IconError />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.activateAccount.content
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.profile.resendActivationLink.loader)
                            Backend.controllers.profile.resendActivationLink({
                                emailAddress: this.props.emailAddress
                            }).then(() => {
                                Modal.mount.next({
                                    element: (
                                        <SuccessModal title={Translations.translations.backend.controllers.profile.resendActivationLink.responses[200].title} buttons={(
                                            <div className="modal-content-content-center">
                                                <Button color="orange" size="small" onClick={() => {
                                                    Modal.dismiss.next()
                                                }}>
                                                    {
                                                        Translations.translations.modals.defaultButtons.close
                                                    }
                                                </Button>
                                            </div>
                                        )}>
                                            {
                                                Translations.translations.backend.controllers.profile.resendActivationLink.responses[200].content
                                            }
                                        </SuccessModal>
                                    ),
                                    history: true,
                                    dismissLevels: 2
                                })
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.profile.resendActivationLink.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.activateAccount.buttons.sendNewActivationMail
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}