import React from 'react'
import './change-image-background-style-modal.scss'
import ImageResponse from '../../../../backend/response/entity/ImageResponse'
import ImageCropper, { ImageCropperImage } from '../../../image-cropper/ImageCropper'
import Translations from '../../../../translations/Translations'
import Button from '../../../button/Button'
import Modal from '../../Modal'

/**
 * The props
 */
interface ChangeImageBackgroundStyleModalProps {

    /**
     * The image to adjust.
     */
    image: ImageResponse

    /**
     * Whenever the values in the image cropper change
     */
    onChange: (imageCropperImage: ImageCropperImage) => void
}

/**
 * The state
 */
interface ChangeImageBackgroundStyleModalState extends ImageCropperImage {
}

/**
 * The modal to change the background style for an image.
 * 
 * @author Stan Hurks
 */
export default class ChangeImageBackgroundStyleModal extends React.Component<ChangeImageBackgroundStyleModalProps, ChangeImageBackgroundStyleModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            backgroundPosition: this.props.image.backgroundPosition,
            zoom: this.props.image.zoom
        }
    }

    public componentWillReceiveProps = () => {
        this.setState({
            backgroundPosition: this.props.image.backgroundPosition,
            zoom: this.props.image.zoom
        })
    }

    public render = () => {
        return (
            <div className="change-image-background-style-modal">
                <ImageCropper
                    image={{
                        backgroundPosition: this.state.backgroundPosition || '50% 50%',
                        zoom: this.state.zoom || 1,
                        url: this.props.image.file.content
                    }}
                    onChange={(imageCropperImage: ImageCropperImage) => {
                        this.setState({
                            ...imageCropperImage
                        })
                    }}
                    >
                </ImageCropper>
                <div className="modal-content-content padding">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            this.props.onChange({
                                backgroundPosition: this.state.backgroundPosition,
                                zoom: this.state.zoom
                            })
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.save
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}