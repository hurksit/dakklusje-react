import React from 'react'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import RoofSpecialistFilterResponse from '../../../backend/response/entity/RoofSpecialistFilterResponse'
import { RoofType } from '../../../backend/enumeration/RoofType'
import { JobCategory } from '../../../backend/enumeration/JobCategory'
import { HouseType } from '../../../backend/enumeration/HouseType'
import { RoofMaterialType } from '../../../backend/enumeration/RoofMaterialType'
import { RoofIsolationType } from '../../../backend/enumeration/RoofIsolationType'
import { RoofSpecialistFilterType } from '../../../backend/enumeration/RoofSpecialistFilterType'
import './roof-specialist-filters-modal.scss'
import RoofSpecialistFilterModalFilter from './filter/RoofSpecialistFilterModalFilter'
import FormSelect from '../../form/select/FormSelect'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'

/**
 * The props
 */
interface RoofSpecialistFiltersModalProps {

    /**
     * The filters of roof specialist
     */
    filters: RoofSpecialistFilterResponse[]

    /**
     * A callback for when the filters have changed
     */
    onChangeFilters: (filters: RoofSpecialistFilterResponse[]) => void
}

/**
 * The state
 */
interface RoofSpecialistFiltersModalState {

    /**
     * The surface measurements
     */
    surfaceMeasurements: string|null

    /**
     * The roof types
     */
    roofTypes: RoofType[]

    /**
     * The job categories
     */
    jobCategories: JobCategory[]

    /**
     * The house types
     */
    houseTypes: HouseType[]

    /**
     * The roof material types
     */
    roofMaterialTypes: RoofMaterialType[]

    /**
     * The roof isolation types
     */
    roofIsolationTypes: RoofIsolationType[]

    /**
     * The max travel distance
     */
    distance: string|null
}

/**
 * The modal to set the filters for the roof specialist.
 * 
 * @author Stan Hurks
 */
export default class RoofSpecialistFiltersModal extends React.Component<RoofSpecialistFiltersModalProps, RoofSpecialistFiltersModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            surfaceMeasurements: null,
            roofTypes: [],
            jobCategories: [],
            houseTypes: [],
            roofMaterialTypes: [],
            roofIsolationTypes: [],
            distance: null
        }
    }

    public componentDidMount = () => {
        this.updateState()
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.updateState()
        })
    }

    public render = () => {
        return (
            <div className="roof-specialist-filters-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.roofSpecialistFilters.title   
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w25"></div>
                </div><div className="modal-content-content">

                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.surfaceMeasurements.label}>
                        <FormSelect
                            value={this.state.surfaceMeasurements}
                            options={Translations.translations.modals.roofSpecialistFilters.form.surfaceMeasurements.options}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.surfaceMeasurements.placeholder}
                            onChange={(surfaceMeasurements) => {
                                this.setState({
                                    surfaceMeasurements
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>
                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.distance.label}>
                        <FormSelect
                            value={this.state.distance}
                            options={Translations.translations.modals.roofSpecialistFilters.form.distance.options}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.distance.placeholder}
                            onChange={(distance) => {
                                this.setState({
                                    distance
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>
                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.roofType.label(this.state.roofTypes.length)}>
                        <FormSelect
                            value={this.state.roofTypes}
                            options={Object.keys(RoofType).map((roofType) => ({
                                key: roofType,
                                value: Translations.translations.backend.enumerations.roofType(roofType as any)
                            }))}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.roofType.placeholder}
                            onChange={(roofTypes) => {
                                this.setState({
                                    roofTypes
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>
                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.jobCategory.label(this.state.jobCategories.length)}>
                        <FormSelect
                            value={this.state.jobCategories}
                            options={Object.keys(JobCategory).map((jobCategory) => ({
                                key: jobCategory,
                                value: Translations.translations.backend.enumerations.jobCategory(jobCategory as any)
                            }))}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.jobCategory.placeholder}
                            onChange={(jobCategories) => {
                                this.setState({
                                    jobCategories
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>
                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.houseType.label(this.state.houseTypes.length)}>
                        <FormSelect
                            value={this.state.houseTypes}
                            options={Object.keys(HouseType).map((houseType) => ({
                                key: houseType,
                                value: Translations.translations.backend.enumerations.houseType(houseType as any)
                            }))}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.houseType.placeholder}
                            onChange={(houseTypes) => {
                                this.setState({
                                    houseTypes
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>
                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.roofMaterialType.label(this.state.roofMaterialTypes.length)}>
                        <FormSelect
                            value={this.state.roofMaterialTypes}
                            options={Object.keys(RoofMaterialType).map((roofMaterialType) => ({
                                key: roofMaterialType,
                                value: Translations.translations.backend.enumerations.roofMaterialType(roofMaterialType as any)
                            }))}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.roofMaterialType.placeholder}
                            onChange={(roofMaterialTypes) => {
                                this.setState({
                                    roofMaterialTypes
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>
                    <RoofSpecialistFilterModalFilter label={Translations.translations.modals.roofSpecialistFilters.form.roofIsolationType.label(this.state.roofIsolationTypes.length)}>
                        <FormSelect
                            value={this.state.roofIsolationTypes}
                            options={Object.keys(RoofIsolationType).map((roofIsolationType) => ({
                                key: roofIsolationType,
                                value: Translations.translations.backend.enumerations.roofIsolationType(roofIsolationType as any)
                            }))}
                            placeholder={Translations.translations.modals.roofSpecialistFilters.form.roofIsolationType.placeholder}
                            onChange={(roofIsolationTypes) => {
                                this.setState({
                                    roofIsolationTypes
                                })
                            }}
                            />
                    </RoofSpecialistFilterModalFilter>

                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w25"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="purple" size="small" onClick={() => {
                            this.setState({
                                surfaceMeasurements: null,
                                roofTypes: [],
                                jobCategories: [],
                                houseTypes: [],
                                roofMaterialTypes: [],
                                roofIsolationTypes: [],
                                distance: null
                            })
                        }}>
                            {
                                Translations.translations.modals.roofSpecialistFilters.buttons.reset
                            }
                        </Button>
                        <Button color="orange" size="small" onClick={() => {
                            const filters = this.generateFilters()
                            Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.job.setFilters.loader)
                            Backend.controllers.roofSpecialistJob.setFilters({
                                filters
                            }).then(() => {
                                Modal.mountSuccess.next(Translations.translations.backend.controllers.roofSpecialist.job.setFilters.responses[200])
                                this.props.onChangeFilters(filters)
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.roofSpecialist.job.setFilters.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.roofSpecialistFilters.buttons.set
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Update the state
     */
    private updateState = () => {
        const state: RoofSpecialistFiltersModalState = {
            surfaceMeasurements: null,
            roofTypes: [],
            jobCategories: [],
            houseTypes: [],
            roofMaterialTypes: [],
            roofIsolationTypes: [],
            distance: null
        }

        for (const filter of this.props.filters) {
            switch (filter.filterType) {
                case RoofSpecialistFilterType.SURFACE_MEASUREMENTS:
                    state.surfaceMeasurements = filter.value
                    break
                case RoofSpecialistFilterType.ROOF_TYPE:
                    state.roofTypes.push(filter.value as any)
                    break
                case RoofSpecialistFilterType.JOB_CATEGORY:
                    state.jobCategories.push(filter.value as any)
                    break
                case RoofSpecialistFilterType.HOUSE_TYPE:
                    state.houseTypes.push(filter.value as any)
                    break
                case RoofSpecialistFilterType.ROOF_MATERIAL_TYPE:
                    state.roofMaterialTypes.push(filter.value as any)
                    break
                case RoofSpecialistFilterType.ROOF_ISOLATION_TYPE:
                    state.roofIsolationTypes.push(filter.value as any)
                    break
                case RoofSpecialistFilterType.DISTANCE:
                    state.distance = filter.value
                    break
            }
        }

        this.setState(state)
    }

    /**
     * Generate the filters based on the state
     */
    private generateFilters = (): RoofSpecialistFilterResponse[] => {
        const filters: RoofSpecialistFilterResponse[] = []
        
        if (this.state.surfaceMeasurements !== null) {
            filters.push({
                filterType: RoofSpecialistFilterType.SURFACE_MEASUREMENTS,
                value: this.state.surfaceMeasurements
            })
        }
        if (this.state.distance !== null) {
            filters.push({
                filterType: RoofSpecialistFilterType.DISTANCE,
                value: this.state.distance
            })
        }
        if (this.state.roofTypes.length > 0) {
            for (const roofType of this.state.roofTypes) {
                filters.push({
                    filterType: RoofSpecialistFilterType.ROOF_TYPE,
                    value: roofType
                })
            }
        }
        if (this.state.jobCategories.length > 0) {
            for (const jobCategory of this.state.jobCategories) {
                filters.push({
                    filterType: RoofSpecialistFilterType.JOB_CATEGORY,
                    value: jobCategory
                })
            }
        }
        if (this.state.houseTypes.length > 0) {
            for (const houseType of this.state.houseTypes) {
                filters.push({
                    filterType: RoofSpecialistFilterType.HOUSE_TYPE,
                    value: houseType
                })
            }
        }
        if (this.state.roofMaterialTypes.length > 0) {
            for (const roofMaterialType of this.state.roofMaterialTypes) {
                filters.push({
                    filterType: RoofSpecialistFilterType.ROOF_MATERIAL_TYPE,
                    value: roofMaterialType
                })
            }
        }
        if (this.state.roofIsolationTypes.length > 0) {
            for (const roofIsolationType of this.state.roofIsolationTypes) {
                filters.push({
                    filterType: RoofSpecialistFilterType.ROOF_ISOLATION_TYPE,
                    value: roofIsolationType
                })
            }
        }
        return filters
    }
}