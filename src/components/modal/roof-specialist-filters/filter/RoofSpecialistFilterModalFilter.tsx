import React from 'react'
import './roof-specialist-filters-modal-filter.scss'

/**
 * The props
 */
interface RoofSpecialistFilterModalFilterProps {

    /**
     * The label to display
     */
    label: string
}

/**
 * A filter in the roof specialist filter modal.
 * 
 * @author Stan Hurks
 */
export default class RoofSpecialistFilterModalFilter extends React.Component<RoofSpecialistFilterModalFilterProps> {

    public render = () => {
        return (
            <div className="roof-specialist-filters-modal-filter">
                <div className="roof-specialist-filters-modal-filter-label">
                    {
                        this.props.label
                    }
                </div><div className="roof-specialist-filters-modal-filter-content">
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
}