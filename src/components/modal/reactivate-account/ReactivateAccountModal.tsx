import React from 'react'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import './reactivate-account-modal.scss'
import Backend from '../../../backend/Backend'
import LoginRequest from '../../../backend/controller/authentication/request/LoginRequest'
import Modal from '../Modal'
import SuccessModal from '../generic/success/SuccessModal'
import LocalStorage from '../../../storage/local-storage/LocalStorage'
import SessionStorage from '../../../storage/session-storage/SessionStorage'
import App from '../../../core/app/App'

/**
 * The props
 */
interface ReactivateAccountModalProps {

    /**
     * The login request from the NavigationMenuModal
     */
    loginRequest: LoginRequest

    /**
     * Whether or not the users session should be stored in the SessionStorage or LocalStorage.
     */
    remainLoggedIn: boolean
}

/**
 * The state
 */
interface ReactivateAccountModalState {

    /**
     * The consumers full name
     */
    fullName: string|null
}

/**
 * The modal to reactivate the account for the consumer.
 * 
 * @author Stan Hurks
 */
export default class ReactivateAccountModal extends React.Component<ReactivateAccountModalProps, ReactivateAccountModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            fullName: null 
        }

        FormValidation.reset()
        
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public render = () => {
        return (
            <form className="reactivate-account-modal" onSubmit={() => {
                this.reactivateAccount()
            }}>
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.reactivateAccount.title
                    }
                </div><div className="modal-content-content">
                    <FormGroup 
                        label={Translations.translations.modals.reactivateAccount.form.fullName.label}
                        helperText={Translations.translations.modals.reactivateAccount.form.fullName.helperText}
                        validation={{
                            ...FormValidation.entity.user.fullName,
                            evaluateValue: () => {
                                return this.state.fullName
                            }
                        }}
                        >
                        <FormInput
                            type="text"
                            value={this.state.fullName}
                            placeholder={Translations.translations.modals.reactivateAccount.form.fullName.placeholder}
                            onChange={(fullName) => {
                                this.setState({
                                    fullName: fullName as string
                                })
                            }}
                            onEnter={() => {
                                this.reactivateAccount()
                            }}
                            />
                    </FormGroup><div className="modal-content-content">
                        <div className="modal-content-hr center"></div>
                    </div><div className="modal-content-content">
                        <div className="modal-content-content-left">
                            <Button color="white" size="small">
                                {
                                    Translations.translations.modals.defaultButtons.cancel
                                }
                            </Button>
                        </div><div className="modal-content-content-right">
                            <Button color="orange" size="small" disabled={!this.validate()} type="submit">
                                {
                                    Translations.translations.modals.reactivateAccount.buttons.reactivate
                                }
                            </Button>
                        </div>
                    </div>
                </div>
            </form>
        )
    }

    /**
     * Validate the form
     */
    private validate = () => {
        return FormValidation.validate()
    }

    /**
     * Reactivates the account
     */
    private reactivateAccount = () => {
        if (!this.validate()) {
            return
        }
        Backend.controllers.consumer.reactivateAccount({
            loginRequest: this.props.loginRequest,
            changePersonalDetailsRequest: {
                fullName: this.state.fullName as string,
                postalCode: null,
                emailAddress: null,
                password: null,
                currentPassword: null
            }
        }).then((response) => {
            const storage = this.props.remainLoggedIn ? LocalStorage : SessionStorage
            storage.data.session.uuid = response.data.uuid

            Modal.mount.next({
                element: (
                    <SuccessModal title={Translations.translations.backend.controllers.consumer.reactivateAccount.responses[200].title} buttons={(
                        <div className="modal-content-content-center">
                            <Button color="orange" size="small" onClick={() => {
                                Modal.dismiss.next('all')
                            }}>
                                {
                                    Translations.translations.modals.defaultButtons.close
                                }
                            </Button>
                        </div>
                    )}>
                        {
                            Translations.translations.backend.controllers.consumer.reactivateAccount.responses[200].content
                        }
                    </SuccessModal>
                ),
                history: true,
                dismissLevels: 2
            })

            App.login.next(response.data)
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.consumer.reactivateAccount.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}