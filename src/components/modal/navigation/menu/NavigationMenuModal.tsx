import React from 'react'
import './navigation-menu-modal.scss'
import FormInput from '../../../form/input/FormInput'
import FormCheckbox from '../../../form/checkbox/FormCheckbox'
import Button from '../../../button/Button'
import FacebookButton from '../../../button/facebook/FacebookButton'
import GooglePlusButton from '../../../button/google-plus/GooglePlusButton'
import Loader from '../../../loader/Loader'
import Backend from '../../../../backend/Backend'
import App from '../../../../core/app/App'
import LocalStorage from '../../../../storage/local-storage/LocalStorage'
import SessionStorage from '../../../../storage/session-storage/SessionStorage'
import Authorization from '../../../../core/Authorization'
import Storage from '../../../../storage/Storage'
import Translations from '../../../../translations/Translations'
import ProfilePicture from '../../../profile-picture/ProfilePicture'
import Modal from '../../Modal'
import { Settings, Edit } from '@material-ui/icons'
import ProfilePictureSettingsModal from '../../profile-picture/settings/ProfilePictureSettingsModal'
import ImageUpload from '../../../image-upload/ImageUpload'
import { Subscription } from 'rxjs'
import ForgotPasswordModal from '../../forgot-password/ForgotPasswordModal'
import AuthorizeFacebookModal from '../../authorize/facebook/AuthorizeFacebookModal'
import AuthorizedSessionResponse from '../../../../backend/response/entity/AuthorizedSessionResponse'
import AuthorizeGooglePlusModal from '../../authorize/google/AuthorizeGooglePlusModal'
import { Route } from '../../../../enumeration/Route'
import ActivateAccountModal from '../../activate-account/ActivateAccountModal'
import LoginRequest from '../../../../backend/controller/authentication/request/LoginRequest'
import ReactivateAccountModal from '../../reactivate-account/ReactivateAccountModal'
import WebSocket from '../../../../backend/WebSocket'

/**
 * The props
 */
interface NavigationMenuModalProps {

    /**
     * When the authorization has been completed.
     */
    onAuthorized?: (session: AuthorizedSessionResponse) => void
}

/**
 * The state
 */
interface NavigationMenuModalState {

    /**
     * The authorized properties
     */
    authorized: {

        /**
         * Whether or not the profile picture change has been prompted from this modal.
         */
        promptedProfilePictureChange: boolean
    }

    /**
     * When the user is not authorized
     */
    unauthorized: {
        /**
         * The emailaddress
         */
        emailAddress: string|null
    
        /**
         * The password
         */
        password: string|null

        /**
         * The FB.authResponse.accessToken
         */
        facebookAccessToken: string|null

        /**
         * The google plus id
         */
        googleIdToken: string|null
    
        /**
         * Whether or not the users session should be stored in the SessionStorage or LocalStorage.
         */
        remainLoggedIn: boolean
    }

    /**
     * The amount of unread messages in the inbox
     */
    inboxMessageCount: number
}

/**
 * The login modal.
 * 
 * @author Stan Hurks
 */
export default class NavigationMenuModal extends React.Component<NavigationMenuModalProps, NavigationMenuModalState> {

    /**
     * The subscription for when the app updates
     */
    private subscriptionAppUpdate!: Subscription

    /**
     * The subscription for when a new profile picture has been set by app.
     */
    private subscriptionAppProfilePictureChange!: Subscription

    /**
     * The subscription for when an upload fails in the ImageUpload component.
     */
    private subscriptionImageUploadUploadFailed!: Subscription

    /**
     * The subscription to the inbox badge count
     */
    private subscriptionInboxBadgeCount!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            authorized: {
                promptedProfilePictureChange: false
            },

            unauthorized: {
                emailAddress: null,
    
                password: null,

                facebookAccessToken: null,
                
                googleIdToken: null,
    
                remainLoggedIn: true
            },

            inboxMessageCount: App.inboxBadgeCount
        }
    }

    public componentDidMount = () => {
        this.subscriptionAppUpdate = App.update.subscribe(this.onAppUpdate)
        this.subscriptionAppProfilePictureChange = App.profilePictureChange.subscribe(this.onAppProfilePictureChange)
        this.subscriptionImageUploadUploadFailed = ImageUpload.uploadFailed.subscribe(this.onImageUploadUploadFailed)
        this.subscriptionInboxBadgeCount = WebSocket.userInboxBadgeCount.subscribe(this.onUserInboxBadgeCount)
    }

    public componentWillUnmount = () => {
        this.subscriptionAppUpdate.unsubscribe()
        this.subscriptionAppProfilePictureChange.unsubscribe()
        this.subscriptionImageUploadUploadFailed.unsubscribe()
        this.subscriptionInboxBadgeCount.unsubscribe()
    }

    public render = () => {
        if (Authorization.isAuthorized()) {
            return this.renderAuthorized()
        }
        return this.renderUnauthorized()
    }

    /**
     * Render the authorized modal
     */
    private renderAuthorized = () => {
        return (
            <div className="navigation-menu-modal navigation-menu-modal-authorized">
                <div className="modal-content-title">
                    {
                        new Date().getHours() >= 12 && new Date().getHours() <= 17
                        ? Translations.translations.modals.navigationMenuModal.authorized.greeting.afternoon
                        : (
                            new Date().getHours() > 17
                            ? Translations.translations.modals.navigationMenuModal.authorized.greeting.evening
                            : (
                                new Date().getHours() >= 5
                                ? Translations.translations.modals.navigationMenuModal.authorized.greeting.morning
                                : Translations.translations.modals.navigationMenuModal.authorized.greeting.night
                            )
                        )
                    }, {
                        Storage.data.session.user.name
                    }!
                </div>
                <div className="modal-content-content" style={{
                    height: 124
                }}>
                    <div className="modal-content-content-left" style={{
                        width: '100%',
                        paddingRight: 140
                    }}>
                        <div className="button-container">
                            <Button color="white" size="small" fullWidth href={Route.INBOX} badge={this.state.inboxMessageCount === 0 ? undefined : this.state.inboxMessageCount}>
                                {
                                    Translations.translations.modals.navigationMenuModal.authorized.buttons.inbox
                                }
                            </Button>
                        </div><div className="button-container" style={{
                            marginTop: '1.6rem'
                        }}>
                            <Button color="white" size="small" fullWidth href={Storage.data.session.user.consumer !== null ? Route.CONSUMER_JOBS : Route.ROOF_SPECIALIST_JOBS}>
                                {
                                    Translations.translations.modals.navigationMenuModal.authorized.buttons.jobs
                                }
                            </Button>
                        </div>
                    </div><div className="modal-content-content-right" style={{
                        position: 'absolute',
                        right: 0,
                        width: 124,
                        paddingLeft: 0
                    }}>
                        {
                            !Storage.data.session.user.profilePicture
                            ?
                            (
                                <ProfilePicture onClick={() => {
                                    App.imageUploadContext = 'profile-picture'
                                    ImageUpload.prompt.next()

                                    this.setState({
                                        authorized: {
                                            ...this.state.authorized,
                                            promptedProfilePictureChange: true
                                        }
                                    })
                                }} hoverIcon={(<Edit />)} />
                            )
                            :
                            (
                                <ProfilePicture onClick={() => {
                                    Modal.mount.next({
                                        element: (
                                            <ProfilePictureSettingsModal />
                                        ),
                                        pcOnly: window.innerWidth >= 1000,
                                        history: window.innerWidth >= 1000,
                                        dismissable: false
                                    })
                                }} hoverIcon={(<Settings /> )} />
                            )
                        }
                    </div>
                </div>
                <div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div>
                <div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="purple" size="small" fullWidth href={Route.PROFILE}>
                            {
                                Translations.translations.modals.navigationMenuModal.authorized.buttons.profile
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" fullWidth onClick={() => {
                            this.logout()
                        }}>
                            {
                                Translations.translations.modals.navigationMenuModal.authorized.buttons.logout
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Render the unauthorized menu
     */
    private renderUnauthorized = () => {
        return (
            <form className="navigation-menu-modal navigation-menu-modal-unauthorized" onSubmit={() => {
                this.attemptLogin()
            }}>
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.navigationMenuModal.unauthorized.title
                    }
                </div><div className="modal-content-content">
                    <FormInput type="email" placeholder={Translations.translations.backend.entity.user.emailAddress.placeholder} autoComplete value={this.state.unauthorized.emailAddress} onChange={(value) => {
                        this.setState({
                            unauthorized: {
                                ...this.state.unauthorized,
                                emailAddress: value
                            }
                        })
                    }} />
                    <FormInput type="password" placeholder={Translations.translations.backend.entity.user.password.placeholder} autoComplete="current-password" value={this.state.unauthorized.password} onChange={(value) => {
                        this.setState({
                            unauthorized: {
                                ...this.state.unauthorized,
                                password: value
                            }
                        })
                    }} />
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <FormCheckbox value={this.state.unauthorized.remainLoggedIn} label={Translations.translations.modals.navigationMenuModal.unauthorized.form.remainLoggedIn} size="small" onChange={(value) => {
                            this.setState({
                                unauthorized: {
                                    ...this.state.unauthorized,
                                    remainLoggedIn: value
                                }
                            })
                        }} />
                    </div><div className="modal-content-content-right">
                        <button type="button" onClick={() => {
                            Modal.mount.next({
                                element: (
                                    <ForgotPasswordModal />
                                ),
                                pcOnly: window.innerWidth >= 1000,
                                history: window.innerWidth >= 1000,
                                dismissable: false
                            })
                        }}>
                            {
                                Translations.translations.modals.navigationMenuModal.unauthorized.form.forgotPassword
                            }
                        </button>
                    </div>
                </div><div className="modal-content-content buttons">
                    <div className="modal-content-content-left modal-content-content-inline">
                        <Button color="orange" size="small" type="submit" fullWidth>
                            {
                                Translations.translations.modals.navigationMenuModal.unauthorized.form.login
                            }
                        </Button>
                    </div><div className="modal-content-content-right modal-content-content-inline">
                        <div className="modal-content-vr"></div>
                        <span className="navigation-menu-modal-login-with">
                            {
                                Translations.translations.modals.navigationMenuModal.unauthorized.form.orLoginWith
                            }
                        </span>
                        <FacebookButton size="small" onAuthorized={(facebookAccessToken) => {
                            this.setState({
                                unauthorized: {
                                    ...this.state.unauthorized,
                                    facebookAccessToken,
                                    googleIdToken: null
                                }
                            }, () => {
                                this.attemptLogin()
                            })
                        }} />
                        <GooglePlusButton size="small" onAuthorized={(googleIdToken) => {
                            this.setState({
                                unauthorized: {
                                    ...this.state.unauthorized,
                                    facebookAccessToken: null,
                                    googleIdToken
                                }
                            }, () => {
                                this.attemptLogin()
                            })
                        }} />
                    </div>
                </div>
            </form>
        )
    }

    /**
     * Whenever the app is updated
     */
    private onAppUpdate = () => {
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    /**
     * Whenever a new profile picture has been set by App.
     */
    private onAppProfilePictureChange = () => {
        if (this.state.authorized.promptedProfilePictureChange) {
            this.setState({
                authorized: {
                    ...this.state.authorized,
                    promptedProfilePictureChange: false
                }
            })

            setTimeout(() => {
                Modal.mount.next({
                    element: (
                        <ProfilePictureSettingsModal />
                    ),
                    pcOnly: window.innerWidth >= 1000,
                    history: window.innerWidth >= 1000,
                    dismissable: false
                })
            })
        }
    }

    /**
     * Whenever an upload fails in the ImageUpload component
     */
    private onImageUploadUploadFailed = () => {
        if (this.state.authorized.promptedProfilePictureChange) {
            this.setState({
                authorized: {
                    ...this.state.authorized,
                    promptedProfilePictureChange: false
                }
            })
        }
    }

    /**
     * Whenever the badge count changes
     */
    private onUserInboxBadgeCount = (badgeCount: number) => {
        this.setState({
            inboxMessageCount: badgeCount
        })
    }

    /**
     * Attempts to log the user in
     */
    private attemptLogin = () => {
        const authorize = (session: AuthorizedSessionResponse) => {
            const storage = this.state.unauthorized.remainLoggedIn ? LocalStorage : SessionStorage
            storage.data.session.uuid = session.uuid

            App.login.next(session)

            setTimeout(() => {
                this.forceUpdate()

                if (this.props.onAuthorized) {
                    setTimeout(() => {
                        if (!this.props.onAuthorized) {
                            return
                        }
                        this.props.onAuthorized(session)
                    })
                }
            })
        }

        Loader.set.next(Translations.translations.backend.controllers.authentication.login.loader)
        const loginRequest: LoginRequest = {
            emailAddress: this.state.unauthorized.facebookAccessToken === null && this.state.unauthorized.googleIdToken === null ? this.state.unauthorized.emailAddress : null,
            password: this.state.unauthorized.facebookAccessToken === null && this.state.unauthorized.googleIdToken === null ? this.state.unauthorized.password : null,
            facebookAccessToken: this.state.unauthorized.facebookAccessToken,
            googleIdToken: this.state.unauthorized.googleIdToken
        }
        Backend.controllers.authentication.login(loginRequest).then((response) => {
            authorize(response.data)
        }).catch((response) => {
            this.setState({
                unauthorized: {
                    ...this.state.unauthorized,
                    facebookAccessToken: null,
                    googleIdToken: null
                }
            })
            switch (response.status) {
                case 403:
                    if (!this.state.unauthorized.emailAddress) {
                        // Note: this is an open scenario, though in orde to connect facebook/g+ an account has to
                        // be activated in the first place, so this scenario will never happen naturally.
                        return
                    }
                    Modal.mount.next({
                        element: (
                            <ActivateAccountModal emailAddress={this.state.unauthorized.emailAddress} />
                        ),
                        history: true,
                        pcOnly: window.innerWidth >= 1000,
                        dismissable: false
                    })
                    break
                case 409:
                    Modal.mount.next({
                        element: (
                            <ReactivateAccountModal loginRequest={loginRequest} remainLoggedIn={this.state.unauthorized.remainLoggedIn} />
                        ),
                        history: true,
                        pcOnly: window.innerWidth >= 1000,
                        dismissable: false
                    })
                    break
                default:
                    if (response.status === 404 && (this.state.unauthorized.facebookAccessToken || this.state.unauthorized.googleIdToken)) {
                        if (this.state.unauthorized.facebookAccessToken) {
                            Modal.mount.next({
                                element: (
                                    <AuthorizeFacebookModal facebookAccessToken={this.state.unauthorized.facebookAccessToken as string} onAuthorized={(session) => {
                                        authorize(session)
                                    }} />
                                ),
                                pcOnly: window.innerWidth >= 1000,
                                history: window.innerWidth >= 1000,
                                dismissable: false
                            })
                        } else if (this.state.unauthorized.googleIdToken) {
                            Modal.mount.next({
                                element: (
                                    <AuthorizeGooglePlusModal googleIdToken={this.state.unauthorized.googleIdToken as string} onAuthorized={(session) => {
                                        authorize(session)
                                    }} />
                                ),
                                pcOnly: window.innerWidth >= 1000,
                                history: window.innerWidth >= 1000,
                                dismissable: false
                            })
                        }
                    } else {
                        const translation: string|undefined = Translations.translations.backend.controllers.authentication.login.responses[response.status]
                        if (translation) {
                            Modal.mountErrorHistory.next(translation)
                        }
                    }
                break
            }
        })
    }

    /**
     * Log the user out
     */
    private logout = () => {
        Loader.set.next(Translations.translations.backend.controllers.authentication.logout.loader)

        const finish = () => {
            App.logout.next()
            
            setTimeout(() => {
                this.forceUpdate()
            })
        }

        Backend.controllers.authentication.logout()
            .then(finish)
            .catch(finish)
    }
}