import React from 'react'
import './report-job-modal.scss'
import RoofSpecialistJobResponse from '../../../backend/controller/roofspecialist/job/response/RoofSpecialistJobResponse'
import FormValidation from '../../../core/FormValidation'
import Translations from '../../../translations/Translations'
import FormInput from '../../form/input/FormInput'
import FormGroup from '../../form/group/FormGroup'
import Button from '../../button/Button'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'

/**
 * The props
 */
interface ReportJobModalProps {

    /**
     * The referenced job
     */
    job: RoofSpecialistJobResponse
}

/**
 * The state
 */
interface ReportJobModalState {

    /**
     * Whether or not the form is validated
     */
    validated: boolean

    /**
     * The description
     */
    description: string|null
}


/**
 * The modal to report a job.
 * 
 * @author Stan Hurks
 */
export default class ReportJobModal extends React.Component<ReportJobModalProps, ReportJobModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            validated: false,

            description: null
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public render = () => {
        return (
            <div className="report-job-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.reportJob.title
                    }
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportJob.form.job.label}
                        helperText={Translations.translations.modals.reportJob.form.job.helperText}
                        >

                        <FormInput
                            type="text"
                            value={this.props.job.title}
                            disabled={true}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportJob.form.description.label}
                        helperText={Translations.translations.modals.reportJob.form.description.helperText}
                        validation={{
                            ...FormValidation.entity.report.description,
                            evaluateValue: () => {
                                return this.state.description
                            }
                        }}
                        >

                        <FormInput 
                            type="multiline"
                            value={this.state.description}
                            placeholder={Translations.translations.modals.reportJob.form.description.placeholder}
                            onChange={(description) => {
                                this.setState({
                                    description
                                }, () => {
                                    this.setState({
                                        validated: FormValidation.validate()
                                    })
                                })
                            }}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.cancel
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" disabled={!this.state.validated} onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.job.reportJob.loader)
                            Backend.controllers.roofSpecialistJob.reportJob(this.props.job.id, {
                                description: this.state.description as string
                            }).then(() => {
                                Modal.mountSuccess.next(Translations.translations.backend.controllers.roofSpecialist.job.reportJob.responses[200])
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.roofSpecialist.job.reportJob.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.reportJob.form.buttons.report
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}