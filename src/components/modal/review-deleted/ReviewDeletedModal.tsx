import React from 'react'
import './review-deleted-modal.scss'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import { Check } from '@material-ui/icons'

/**
 * The modal for when the review has been deleted successfully.
 * 
 * @author Stan Hurks
 */
export default class ReviewDeletedModal extends React.Component {

    public render = () => {
        return (
            <div className="review-deleted-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.backend.controllers.inbox.connection.consumer.deleteReview.responses[200].title
                    }
                </div><div className="modal-content-content">
                    <Check />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.backend.controllers.inbox.connection.consumer.deleteReview.responses[200].content
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next('all')
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}