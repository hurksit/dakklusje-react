import React from 'react'
import './forgot-password-modal.scss'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import SuccessModal from '../generic/success/SuccessModal'

/**
 * The state
 */
interface ForgotPasswordModalState {

    /**
     * The email address
     */
    emailAddress: string|null
}

/**
 * The forgot password modal.
 * 
 * @author Stan Hurks
 */
export default class ForgotPasswordModal extends React.Component<any, ForgotPasswordModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            emailAddress: null
        }

        FormValidation.reset()

        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public render = () => {
        return (
            <form className="forgot-password-modal" onSubmit={() => {
                this.resetPassword()
            }}>
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.forgotPassword.title
                    }
                </div><div className="modal-content-content">

                    <FormGroup 
                        label={Translations.translations.modals.forgotPassword.form.emailAddress.label}
                        helperText={Translations.translations.modals.forgotPassword.form.emailAddress.helperText}
                        validation={{
                            ...FormValidation.entity.user.email,
                            evaluateValue: () => {
                                return this.state.emailAddress
                            }
                        }}>

                        <FormInput
                            type="email"
                            value={this.state.emailAddress}
                            placeholder={Translations.translations.modals.forgotPassword.form.emailAddress.label}
                            onChange={(value) => {
                                this.setState({
                                    emailAddress: value
                                })
                            }}
                            onEnter={() => {
                                this.resetPassword()
                            }} />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }} fullWidth>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" type="submit" disabled={!this.validate()} fullWidth>
                            {
                                Translations.translations.modals.forgotPassword.buttons.restorePassword
                            }
                        </Button>
                    </div>
                </div>
            </form>
        )
    }

    /**
     * Validate the form
     */
    private validate = () => {
        return FormValidation.validate()
    }

    /**
     * Reset the password
     */
    private resetPassword = () => {
        if (!this.validate()) {
            return
        }
        Loader.set.next(Translations.translations.backend.controllers.forgotPassword.requestPasswordReset.loader)
        Backend.controllers.forgotPassword.requestPasswordReset({
            emailAddress: this.state.emailAddress as string
        }).then(() => {
            Modal.mount.next({
                element: (
                    <SuccessModal title={Translations.translations.backend.controllers.forgotPassword.requestPasswordReset.responses[200].title} buttons={(
                        <div className="modal-content-content-center">
                            <Button color="orange" size="small" onClick={() => {
                                Modal.dismiss.next()
                                Modal.dismiss.next()
                            }}>
                                {
                                    Translations.translations.modals.defaultButtons.close
                                }
                            </Button>
                        </div>
                    )}>
                        {
                            Translations.translations.backend.controllers.forgotPassword.requestPasswordReset.responses[200].content
                        }
                    </SuccessModal>
                ),
                history: true,
                dismissLevels: 2
            })
        }).catch((response) => {
            const translation: string|undefined = Translations.translations.backend.controllers.forgotPassword.requestPasswordReset.responses[response.status]
            if (translation) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}