import React from 'react'
import './restore-password-modal.scss'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import Button from '../../button/Button'
import FormValidation from '../../../core/FormValidation'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import Modal from '../Modal'

/**
 * The props
 */
interface RestorePasswordModalProps {

    /**
     * The UUID
     */
    uuid: string
}

/**
 * The state
 */
interface RestorePasswordModalState {
    password: string|null

    passwordRepeat: string|null
}

/**
 * The modal to restore the password.
 * 
 * @author Stan Hurks
 */
export default class RestorePasswordModal extends React.Component<RestorePasswordModalProps, RestorePasswordModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            password: null,

            passwordRepeat: null
        }

        FormValidation.reset()
        
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public render = () => {
        return (
            <form className="restore-password-modal" onSubmit={() => {
                this.restorePassword()
            }}>
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.restorePassword.title
                    }
                </div><div className="modal-content-content">
                    <FormGroup label={Translations.translations.modals.restorePassword.form.password.label}
                        helperText={Translations.translations.modals.restorePassword.form.password.helperText}
                        validation={{
                            ...FormValidation.entity.user.password,
                            evaluateValue: () => {
                                return this.state.password
                            }
                        }}>
                        <FormInput type="password" value={this.state.password} placeholder={Translations.translations.modals.restorePassword.form.password.label} onChange={(password) => {
                            this.setState({
                                password
                            })
                        }} />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <FormGroup label={Translations.translations.modals.restorePassword.form.passwordRepeat.label}
                        helperText={Translations.translations.modals.restorePassword.form.passwordRepeat.helperText}
                        validation={{
                            ...FormValidation.entity.user.password,
                            evaluateValue: () => {
                                return this.state.passwordRepeat
                            },
                            string: {
                                ...FormValidation.entity.user.password.string,
                                matchEvaluatedValue: () => {
                                    return this.state.password || ''
                                }
                            }
                        }}>
                        <FormInput type="password" value={this.state.passwordRepeat} placeholder={Translations.translations.modals.restorePassword.form.passwordRepeat.label} onChange={(passwordRepeat) => {
                            this.setState({
                                passwordRepeat
                            })
                        }} onEnter={() => {
                            this.restorePassword()
                        }} />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-center">
                        <Button color="orange" size="small" type="submit" disabled={!this.validate()}>
                            {
                                Translations.translations.modals.restorePassword.buttons.restorePassword
                            }
                        </Button>
                    </div>
                </div>
            </form>
        )
    }

    /**
     * Validate the form
     */
    private validate = () => {
        return FormValidation.validate()
    }

    /**
     * Restore the password
     */
    private restorePassword = () => {
        if (!this.validate()) {
            return
        }
        Loader.set.next(Translations.translations.backend.controllers.profile.restorePassword.loader)
        Backend.controllers.profile.restorePassword(this.props.uuid, {
            password: this.state.password as string
        }).then(() => {
            Modal.mountSuccess.next(Translations.translations.backend.controllers.profile.restorePassword.responses[200])
        }).catch((response) => {
            Modal.dismiss.next()
            const translation: string|undefined = Translations.translations.backend.controllers.profile.restorePassword.responses[response.status]
            if (translation) {
                Modal.mountError.next(translation)
            }
        })
    }
}