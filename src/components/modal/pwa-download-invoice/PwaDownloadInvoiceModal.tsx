import React from 'react'
import './pwa-download-invoice-modal.scss'
import Button from '../../button/Button'
import Modal from '../Modal'
import Translations from '../../../translations/Translations'
import Loader from '../../loader/Loader'

/**
 * The props
 */
interface PwaDownloadInvoiceModalProps {

    /**
     * The href
     */
    href: string
}

/**
 * The modal to download an invoice for PWA.
 * 
 * @author Stan Hurks
 */
export default class PwaDownloadInvoiceModal extends React.Component<PwaDownloadInvoiceModalProps> {

    public componentDidMount = () => {
        Loader.set.next(null)
    }

    public render = () => {
        return (
            <div className="pwa-download-invoice-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.pwaDownloadInvoice.title
                    }
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.pwaDownloadInvoice.description
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" href={this.props.href}>
                            {
                                Translations.translations.modals.pwaDownloadInvoice.buttons.download
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}