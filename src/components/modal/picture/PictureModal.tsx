import React from 'react'
import './picture-modal.scss'
import Button from '../../button/Button'
import Translations from '../../../translations/Translations'
import Modal from '../Modal'

/**
 * The props
 */
interface PictureModalProps {
    
    /**
     * The image src
     */
    imgSrc: string
}

/**
 * The modal for displaying a picture.
 * 
 * @author Stan Hurks
 */
export default class PictureModal extends React.Component<PictureModalProps> {

    public render = () => {
        return (
            <div className="picture-modal">
                <div className="picture-modal-picture">
                    <img src={this.props.imgSrc} alt="modal background" draggable={false} />
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w25"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }
}