import React from 'react'
import './job-payment-validate-modal.scss'
import Button from '../../button/Button'
import Translations from '../../../translations/Translations'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import InboxConnectionResponse from '../../../backend/controller/inbox/response/InboxConnectionResponse'
import Device from '../../../shared/Device'

/**
 * The props
 */
interface JobPaymentValidateModalProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse

    /**
     * On success callback
     */
    onSuccess: () => void

    /**
     * On error callback
     */
    onError: () => void

    /**
     * The href to checkout for PWA apple devices
     */
    checkoutHref: string
}

/**
 * The modal to validate a payment in mollie for a job.
 * 
 * @author Stan Hurks
 */
export default class JobPaymentValidateModal extends React.Component<JobPaymentValidateModalProps> {

    public render = () => {
        return (
            <div className="job-payment-validate-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.jobPaymentValidate.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right w50"></div>
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.jobPaymentValidate.description[Device.isPWAStandalone ? 'pwa' : 'nonPwa']
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    {
                        Device.isPWAStandalone
                        &&
                        <Button color="white" size="small" href={this.props.checkoutHref}>
                            {
                                Translations.translations.modals.defaultButtons.toCheckout
                            }
                        </Button>
                    }
                    <Button color="orange" size="small" onClick={() => {
                        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.validateCosts.loader)
                        Backend.controllers.inboxConnectionRoofSpecialist.validateCosts(this.props.connection.id).then((response) => {
                            Modal.mountSuccess.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.validateCosts.responses[200])
                        }).catch((response) => {
                            const translation = Translations.translations.backend.controllers.inbox.connection.roofSpecialist.validateCosts.responses[response.status]
                            if (translation !== undefined) {
                                Modal.mountError.next(translation)
                            }
                        })
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.validateCosts
                        }
                    </Button>
                </div>
            </div>
        )
    }
}