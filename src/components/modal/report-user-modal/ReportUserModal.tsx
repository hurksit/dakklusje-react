import React from 'react'
import './report-user-modal.scss'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'

/**
 * The props
 */
interface ReportUserModalProps {

    /**
     * The full name of the user to report
     */
    fullName: string

    /**
     * The user id
     */
    userId: string
}

/**
 * The state
 */
interface ReportUserModalState {

    /**
     * Whether or not the form is validated
     */
    validated: boolean

    /**
     * The description
     */
    description: string|null
}

/**
 * The modal to report a user.
 */
export default class ReportUserModal extends React.Component<ReportUserModalProps, ReportUserModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            validated: false,

            description: null
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public render = () => {
        return (
            <div className="report-user-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.reportUser.title
                    }
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportUser.form.user.label}
                        helperText={Translations.translations.modals.reportUser.form.user.helperText}
                        >

                        <FormInput
                            type="text"
                            value={this.props.fullName}
                            disabled={true}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.reportUser.form.description.label}
                        helperText={Translations.translations.modals.reportUser.form.description.helperText}
                        validation={{
                            ...FormValidation.entity.report.description,
                            evaluateValue: () => {
                                return this.state.description
                            }
                        }}
                        >

                        <FormInput 
                            type="multiline"
                            value={this.state.description}
                            placeholder={Translations.translations.modals.reportUser.form.description.placeholder}
                            onChange={(description) => {
                                this.setState({
                                    description
                                }, () => {
                                    this.setState({
                                        validated: FormValidation.validate()
                                    })
                                })
                            }}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.cancel
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" disabled={!this.state.validated} onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.report.reportUser.loader)
                            Backend.controllers.report.reportUser({
                                description: this.state.description as string,
                                userId: this.props.userId
                            }).then(() => {
                                Modal.mountSuccess.next(Translations.translations.backend.controllers.report.reportUser.responses[200])
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.report.reportUser.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.reportUser.form.buttons.report
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}