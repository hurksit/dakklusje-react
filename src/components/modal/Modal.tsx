import React from 'react'
import './modal.scss'
import { Subject, Subscription } from 'rxjs'
import classNames from 'classnames'
import App from '../../core/app/App'
import Events from '../../shared/Events'
import { KeyCode } from '../../shared/KeyCodes'
import Loader from '../loader/Loader'
import ErrorModal from './generic/error/ErrorModal'
import Button from '../button/Button'
import Navigation from '../navigation/Navigation'
import Translations from '../../translations/Translations'
import SuccessModal from './generic/success/SuccessModal'
import Window from '../../shared/Window'
import FormSelect from '../form/select/FormSelect'
import { Route } from '../../enumeration/Route'
import MobileScrolling from '../../shared/MobileScrolling';

/**
 * The mount options
 */
export interface MountOptions {

    /**
     * The element
     */
    element: JSX.Element,

    /**
     * If this is true the modal will be added to history.
     * 
     * Otherwise it will clear the history and mount the new modal.
     */
    history?: boolean

    /**
     * Whether or not the element is dismissable
     */
    dismissable?: boolean

    /**
     * The amount of levels that will be deleted from history when dismissing.
     * 
     * By default this will be 1.
     */
    dismissLevels?: number

    /**
     * The target element
     */
    target?: Element

    /**
     * The position the modal should stick at relative to the target
     */
    position?: 'left' | 'right' | 'bottom-left' | 'bottom-right'

    /**
     * Whether or not the modal is only visible on PC
     */
    pcOnly?: boolean

    /**
     * Whether or not to have an additional y offset when scrolling to the target.
     */
    offsetY?: number
}

/**
 * The state
 */
interface ModalState {
    
    /**
     * Whether or not the modal is locked from mounting
     */
    locked: boolean

    /**
     * Whether or not the modal is visible
     */
    visible: boolean

    /**
     * The history of mount options.
     */
    history: MountOptions[]
}

/**
 * A modal
 * 
 * @author Stan Hurks
 */
export default class Modal extends React.Component<any, ModalState> {

    /**
     * The time it takes in MS for the modal to appear/disappear
     */
    public static animationTimeMS: number = 300

    /**
     * Mount a non dismissable modal
     */
    public static mount: Subject<MountOptions> = new Subject()

    /**
     * Mount an error as a model
     */
    public static mountError: Subject<string> = new Subject()

    /**
     * Mount a success as a model
     */
    public static mountSuccess: Subject<string> = new Subject()

    /**
     * Mount an error as a model
     */
    public static mountErrorHistory: Subject<string> = new Subject()

    /**
     * Mount an error as a model
     */
    public static mountSuccessHistory: Subject<string> = new Subject()

    /**
     * Dismisses the modal
     */
    public static dismiss: Subject<'all'> = new Subject()

    /**
     * The internal subscription for mounting a non dismissable modal
     */
    private subscriptionMount!: Subscription

    /**
     * The internal subscription for mounting an ErrorModal
     */
    private subscriptionMountError!: Subscription

    /**
     * The internal subscription for mounting an SuccessModal
     */
    private subscriptionMountSuccess!: Subscription

    /**
     * The internal subscription for mounting an ErrorModal and adding the modal to history
     */
    private subscriptionMountErrorHistory!: Subscription

    /**
     * The internal subscription for mounting an SuccessModal and adding then modal to history
     */
    private subscriptionMountSuccessHistory!: Subscription

    /**
     * The internal subscription for dismissing a modal
     */
    private subscriptionDismiss!: Subscription

    /**
     * The internal subscription for when the window resizes
     */
    private subscriptionWindowResize!: Subscription

    /**
     * The internal subscription for when the user releases a key in the document.
     */
    private subscriptionDocumentKeyUp!: Subscription

    /**
     * The reference to the modal content
     */
    private modalContentRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            locked: false,

            visible: false,

            history: []
        }
    }

    public componentDidMount = () => {
        this.subscriptionMount = Modal.mount.subscribe(this.onMount)
        this.subscriptionMountError = Modal.mountError.subscribe(this.onMountError)
        this.subscriptionMountSuccess = Modal.mountSuccess.subscribe(this.onMountSuccess)
        this.subscriptionMountErrorHistory = Modal.mountErrorHistory.subscribe(this.onMountErrorHistory)
        this.subscriptionMountSuccessHistory = Modal.mountSuccessHistory.subscribe(this.onMountSuccessHistory)
        this.subscriptionDismiss = Modal.dismiss.subscribe(this.onDismiss)
        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
        this.subscriptionDocumentKeyUp = Events.document.keyup.subscribe(this.onDocumentKeyUp as any)
    }

    public componentWillUnmount = () => {
        this.subscriptionMount.unsubscribe()
        this.subscriptionMountError.unsubscribe()
        this.subscriptionMountSuccess.unsubscribe()
        this.subscriptionMountErrorHistory.unsubscribe()
        this.subscriptionMountSuccessHistory.unsubscribe()
        this.subscriptionDismiss.unsubscribe()
        this.subscriptionWindowResize.unsubscribe()
        this.subscriptionDocumentKeyUp.unsubscribe()
    }

    public render = () => {
        return (
            <aside className={classNames({
                'modal': true,
                'visible': this.state.visible,
                'dismissable': this.isCurrentModalDismissable(),
                'pc-only': this.isCurrentModalPcOnly()
            })}>
                <div className="modal-background-overlay" onClick={() => {
                    if (this.isCurrentModalDismissable()) {
                        Modal.dismiss.next()
                    }
                }}></div>

                <div className="modal-content" ref={this.modalContentRef}>
                    {
                        this.state.history.map((mountOptions, index) =>
                            <div style={{
                                ...index < this.state.history.length - 1
                                ? {
                                    display: 'none'
                                }: {}
                            }} key={index} className={classNames({
                                'active': index === this.state.history.length - 1
                            })}>
                                {
                                    mountOptions.element
                                }
                            </div>
                        )
                    }
                </div>
            </aside>
        )
    }

    /**
     * Whenever the modal is mounted
     */
    private onMount = (mountOptions: MountOptions) => {
        if (this.state.locked) {
            console.info('Modal is locked due to animation transition time, please wait.')
            return
        }

        const alterBodyScroll: boolean = !window.location.pathname.startsWith(Route.INBOX)
        const performMount = () => {
            if (alterBodyScroll) {
                App.bodyScroll.next(false)
            } else {
                if (MobileScrolling.instance.enabled) {
                    MobileScrolling.instance.disable()
                    MobileScrolling.instance.disabledExternally = true
                }
            }
        
            // Focus on the first input (if found)
            const focusOnFirstInputFound = () => {
                if (this.modalContentRef.current) {
                    const firstInput = this.modalContentRef.current.querySelector('.active input, .active .button') as HTMLInputElement
                    if (firstInput !== null) {
                        firstInput.focus()
                    } else {
                        const focusedElement = document.querySelector(':focus') as HTMLElement
                        if (focusedElement) {
                            focusedElement.blur()
                        }
                    }
                }
            }

            if (mountOptions.history) {
                this.setState({
                    visible: false
                }, () => {
                    setTimeout(() => {
                        const parentModal = this.state.history[this.state.history.length - 1]
                        this.setState({
                            visible: true,
                            history: [
                                ...this.state.history,
                                ...parentModal ? [{
                                    ...mountOptions,
                                    target: parentModal.target,
                                    position: parentModal.position,
                                    pcOnly: parentModal.pcOnly || false
                                }] : [mountOptions]
                            ]
                        }, () => {
                            this.updatePosition()
                            setTimeout(() => {
                                focusOnFirstInputFound()
                            })
                        })
                    }, window.innerWidth < 1000 ? 0 : Modal.animationTimeMS)
                })
            } else {
                this.setState({    
                    visible: true,
                    history: [
                        mountOptions
                    ]
                }, () => {
                    this.updatePosition()
                    setTimeout(() => {
                        focusOnFirstInputFound()
                    })
                })
            }
        }

        if (this.state.history.length === 0 && mountOptions.target && mountOptions.position) {
            if (alterBodyScroll) {
                Window.scrollTo({
                    top: Math.max(0, window.scrollY + mountOptions.target.getBoundingClientRect().top - (mountOptions.offsetY || 0)),
                    behavior: 'smooth'
                }, () => {
                    performMount()
                })
            } else {
                performMount()
            }
        } else {
            performMount()
        }
    }

    /**
     * Mounts an error modal with some content
     */
    private onMountError = (content: string) => {
        Modal.mount.next({
            element: (
                <ErrorModal title={Translations.translations.modals.generic.error.title} buttons={(
                    <div className="modal-content-content-center">
                        <Button color="orange" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div>
                )}>
                    {
                        content
                    }
                </ErrorModal>
            )
        })
    }

    /**
     * Mounts an error modal with some content
     */
    private onMountSuccess = (content: string) => {
        Modal.mount.next({
            element: (
                <SuccessModal title={Translations.translations.modals.generic.success.title} buttons={(
                    <div className="modal-content-content-center">
                        <Button color="orange" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div>
                )}>
                    {
                        content
                    }
                </SuccessModal>
            )
        })
    }

    /**
     * Mounts an error modal with some content and adds the modal to history.
     */
    private onMountErrorHistory = (content: string) => {
        Modal.mount.next({
            element: (
                <ErrorModal title={Translations.translations.modals.generic.error.title} buttons={(
                    <div className="modal-content-content-center">
                        <Button color="orange" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div>
                )}>
                    {
                        content
                    }
                </ErrorModal>
            ),
            history: true
        })
    }

    /**
     * Mounts an error modal with some content and adds the modal to history.
     */
    private onMountSuccessHistory = (content: string) => {
        Modal.mount.next({
            element: (
                <SuccessModal title={Translations.translations.modals.generic.success.title} buttons={(
                    <div className="modal-content-content-center">
                        <Button color="orange" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div>
                )}>
                    {
                        content
                    }
                </SuccessModal>
            ),
            history: true
        })
    }

    /**
     * Whenever the modal should be dismissed.
     */
    private onDismiss = (options?: 'all') => {
        FormSelect.close.next()
        setTimeout(() => {
            this.setState({
                locked: true,
                visible: false
            }, () => {
                setTimeout(() => {               
                    if (this.modalContentRef.current) {
                        this.modalContentRef.current.classList.remove('right')
                        this.modalContentRef.current.classList.remove('centered')
                        this.modalContentRef.current.style.top = null
                        this.modalContentRef.current.style.left = null
                    }

                    const amountOfLevels = (this.state.history.length > 0 && this.state.history[this.state.history.length - 1].dismissLevels) || 1
                    
                    this.setState({
                        history: options === 'all' ? [] : this.state.history.filter((v, i, a) => i < a.length - amountOfLevels),
                        locked: false
                    }, () => {
                        if (this.state.history.length > 0) {
                            this.setState({
                                visible: true
                            }, () => {
                                this.updatePosition()
                            })
                        } else if (!Navigation.isMenuOpen) {
                            const alterBodyScroll: boolean = !window.location.pathname.startsWith(Route.INBOX)
                            if (alterBodyScroll) {
                                App.bodyScroll.next(true)
                            } else {
                                if (MobileScrolling.instance.disabledExternally && !MobileScrolling.instance.enabled) {
                                    MobileScrolling.instance.enable()
                                    MobileScrolling.instance.disabledExternally = false
                                }
                            }

                            this.setState({
                                visible: false
                            })
                        }
                    })
                }, Modal.animationTimeMS)
            })

            document.body.focus()
        })
    }

    /**
     * Whenever the user releases a key in the document
     */
    private onDocumentKeyUp = (event: KeyboardEvent) => {
        if (Loader.isActive) {
            // The modal should not be closable when higher level components are active on top of it.
            return
        }

        if (event.keyCode === KeyCode.ESCAPE && this.isCurrentModalDismissable()) {
            Modal.dismiss.next()
        }
    }

    /**
     * Whenever the window changes size
     */
    private onWindowResize = () => {
        this.updatePosition()
    }

    /**
     * Updates the position of the modal content
     */
    private updatePosition = () => {
        if (this.modalContentRef.current) {
            const currentTarget = this.getCurrentTarget()
            const currentPosition = this.getCurrentPosition()
            if (currentTarget && currentPosition) {
                // The offset to target in pixels
                const offset = 10
    
                switch (currentPosition) {
                    case 'left':
                        this.modalContentRef.current.style.top = `${currentTarget.getBoundingClientRect().top}px`
                        this.modalContentRef.current.style.left = `${currentTarget.getBoundingClientRect().left - offset}px`
                        this.modalContentRef.current.classList.add('right')
                    break
                    case 'right':
                        this.modalContentRef.current.style.top = `${currentTarget.getBoundingClientRect().top}px`
                        this.modalContentRef.current.style.left = `${currentTarget.getBoundingClientRect().right + offset}px`
                    break
                    case 'bottom-left':
                        this.modalContentRef.current.style.top = `${currentTarget.getBoundingClientRect().top + currentTarget.getBoundingClientRect().height + offset}px`
                        this.modalContentRef.current.style.left = `${currentTarget.getBoundingClientRect().left}px`
                    break
                    case 'bottom-right':
                        this.modalContentRef.current.style.top = `${currentTarget.getBoundingClientRect().top + currentTarget.getBoundingClientRect().height + offset}px`
                        this.modalContentRef.current.style.left = `${currentTarget.getBoundingClientRect().right}px`
                        this.modalContentRef.current.classList.add('right')
                    break
                }
            } else if (this.state.history.length > 0) {
                this.modalContentRef.current.classList.add('centered')
            }
        }
    }

    /**
     * Get the target of the last mounted modal or null
     */
    private getCurrentTarget = () => {
        return this.state.history.length > 0
            ? this.state.history[this.state.history.length - 1].target || null
            : null
    }

    /**
     * Get the position of the last mounted modal or null
     */
    private getCurrentPosition = () => {
        return this.state.history.length > 0
            ? this.state.history[this.state.history.length - 1].position || null
            : null
    }

    /**
     * Check if the current modal is dismissable
     */
    private isCurrentModalDismissable = () => {
        return this.state.history.length > 0
            ? this.state.history[this.state.history.length - 1].dismissable || false
            : false
    }

    /**
     * Check if the current modal is pc only
     */
    private isCurrentModalPcOnly = () => {
        return this.state.history.length > 0
            ? this.state.history[this.state.history.length - 1].pcOnly || false
            : false
    }
}