import React from 'react'
import Button from '../../button/Button'
import Translations from '../../../translations/Translations'
import Storage from '../../../storage/Storage'
import './non-modern-browser-modal.scss'
import Modal from '../Modal'

/**
 * The modal to display a message for using a better browser for a better user experience.
 * 
 * @author Stan Hurks
 */
export default class NonModernBrowserModal extends React.Component {

    public render = () => {
        return (
            <div className="non-modern-browser-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.nonModernBrowser.title
                    }
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.nonModernBrowser.content
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr w50 center"></div>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" onClick={() => {
                        Storage.data.browserNotificationSent = true
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.ok
                        }
                    </Button>
                </div>
            </div>
        )
    }
}