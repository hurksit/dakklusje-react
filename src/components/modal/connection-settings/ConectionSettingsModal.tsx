import React from 'react'
import './connection-settings-modal.scss'
import Translations from '../../../translations/Translations'
import Button from '../../button/Button'
import Modal from '../Modal'
import InboxConnectionResponse from '../../../backend/controller/inbox/response/InboxConnectionResponse'
import Storage from '../../../storage/Storage'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import InboxPage from '../../../pages/authorized-pages/inbox/InboxPage'
import ReportConnectionModal from '../report-connection/ReportConnectionModal'
import DestructiveActionModal from '../destructive-action/DestructiveActionModal';

/**
 * The props
 */
interface ConnectionSettingsModalProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The settings for a connection in the inbox.
 * 
 * @author Stan Hurks
 */
export default class ConnectionSettingsModal extends React.Component<ConnectionSettingsModalProps> {

    public render = () => {
        return (
            <div className="connection-settings-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.connectionSettings.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr w25 right"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" fullWidth onClick={() => {
                        Modal.mount.next({
                            element: (
                                (
                                    <ReportConnectionModal connection={this.props.connection} />
                                )
                            ),
                            history: true
                        })
                    }}>
                        {
                            Translations.translations.modals.connectionSettings.buttons.report
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <Button color="orange" size="small" fullWidth onClick={() => {
                        Modal.mount.next({
                            element: (
                                <DestructiveActionModal onContinue={() => {
                                    if (Storage.data.session.user.roofSpecialist !== undefined) {
                                        this.deleteAsRoofSpecialist()
                                    } else {
                                        this.deleteAsConsumer()
                                    }
                                }}>
                                    {
                                        Storage.data.session.user.roofSpecialist !== undefined
                                        ? Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.destructiveAction.removeContact
                                        : Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.destructiveActionDeleteContact
                                    }
                                </DestructiveActionModal>
                            ),
                            history: true
                        })
                    }}>
                        {
                            Translations.translations.modals.connectionSettings.buttons.deleteContact
                        }
                    </Button>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center w50"></div>
                </div><div className="modal-content-content">
                    <Button color="white" size="small" onClick={() => {
                        Modal.dismiss.next()
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.close
                        }
                    </Button>
                </div>
            </div>
        )
    }

    /**
     * Delete the connection as a consumer
     */
    private deleteAsConsumer = () => {
        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.loader.delete)
        Backend.controllers.inboxConnectionConsumer.declineConnection(this.props.connection.id).then(() => {
            Modal.dismiss.next('all')
            InboxPage.setSelectedConnection.next(null)
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.responses.delete[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }

    /**
     * Delete the connection as a roof specialist
     */
    private deleteAsRoofSpecialist = () => {
        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.loader.delete)
        Backend.controllers.inboxConnectionRoofSpecialist.declineCosts(this.props.connection.id).then(() => {
            Modal.dismiss.next('all')
            InboxPage.setSelectedConnection.next(null)
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.responses.delete[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}