import React from 'react'
import Translations from '../../../translations/Translations'
import FormGroup from '../../form/group/FormGroup'
import FormInput from '../../form/input/FormInput'
import './add-credits-modal.scss'
import FormValidation from '../../../core/FormValidation'
import Button from '../../button/Button'
import Modal from '../Modal'
import Loader from '../../loader/Loader'
import Backend from '../../../backend/Backend'
import CreditPaymentValidateModal from '../credit-payment-validate/CreditPaymentValidateModal'

/**
 * The state
 */
interface AddCreditsModalState {

    /**
     * The amount
     */
    amount: number|null

    /**
     * Whether or not the form is validated
     */
    validated: boolean
}

/**
 * The modal to add credits.
 * 
 * @author Stan Hurks
 */
export default class AddCreditsModal extends React.Component<any, AddCreditsModalState> {

    constructor(props: any) {
        super(props)

        this.state = {
            amount: 30,

            validated: false
        }
    }

    public componentDidMount = () => {
        FormValidation.stash()
    
        setTimeout(() => {
            this.setState({
                validated: FormValidation.validate()
            })
        })
    }

    public componentWillUnmount = () => {
        FormValidation.unstash()
    }

    public render = () => {
        return (
            <div className="add-credits-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.addCredits.title
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr right"></div>
                </div><div className="modal-content-content">
                    <FormGroup
                        label={Translations.translations.modals.addCredits.amount.label}
                        helperText={Translations.translations.modals.addCredits.amount.helperText}
                        validation={{
                            number: {
                                minValue: 30,
                                maxValue: 50000
                            },
                            evaluateValue: () => {
                                return this.state.amount
                            }
                        }}>

                        <FormInput
                            placeholder={Translations.translations.modals.addCredits.amount.placeholder}
                            type="euro"
                            value={this.state.amount}
                            onChange={(amount) => {
                                this.setState({
                                    amount
                                }, () => {
                                    this.setState({
                                        validated: FormValidation.validate()
                                    })
                                })
                            }}
                            />
                    </FormGroup>
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.cancel
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            if (!this.state.validated) {
                                return
                            }
                            Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.addCredits.loader)
                            Backend.controllers.roofSpecialist.addCredits({
                                amount: this.state.amount as number
                            }).then((response) => {
                                
                                // Mount the modal
                                Modal.mount.next({
                                    element: (
                                        <CreditPaymentValidateModal id={response.data.creditRequestId} checkoutLink={response.data.checkoutHref} />
                                    ),
                                    history: true
                                })

                                // Go to checkout
                                window.open(response.data.checkoutHref)
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.roofSpecialist.addCredits.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            {
                                Translations.translations.modals.addCredits.button.add
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}