import React from 'react'
import IconError from '../../icon/IconError'
import Button from '../../button/Button'
import Translations from '../../../translations/Translations'
import Modal from '../Modal'
import './destructive-action-modal.scss'

/**
 * The props
 */
interface DestructiveActionModalProps {

    /**
     * Whenever the user decides to perform the destructive action.
     */
    onContinue: () => void
}

/**
 * The modal for when the user is about to perform a destructive action.
 * 
 * @author Stan Hurks
 */
export default class DestructiveActionModal extends React.Component<DestructiveActionModalProps> {

    public render = () => {
        return (
            <div className="destructive-action-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.destructiveAction.title   
                    }
                </div><div className="modal-content-content">
                    <IconError />
                </div><div className="modal-content-content">
                    {
                        this.props.children
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.cancel
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" onClick={() => {
                            this.props.onContinue()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.continue
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}