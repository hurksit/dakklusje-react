import React from 'react'
import './pending-payment-modal.scss'
import Translations from '../../../translations/Translations'
import IconError from '../../icon/IconError'
import { PaymentType } from '../../../backend/enumeration/PaymentType'
import Button from '../../button/Button'
import Modal from '../Modal'

/**
 * The props
 */
interface PendingPaymentModalProps {

    /**
     * The amount of the payment (including VAT)
     */
    amount: number

    /**
     * The link in mollie
     */
    mollieLink: string
}

/**
 * The state
 */
interface PendingPaymentModalState {
    
    /**
     * The selected payment method
     */
    paymentMethod: PaymentType|null
}

/**
 * The modal to pay a pending payment.
 * 
 * @author Stan Hurks
 */
export default class PendingPaymentModal extends React.Component<PendingPaymentModalProps, PendingPaymentModalState> {

    public render = () => {
        return (
            <div className="pending-payment-modal">
                <div className="modal-content-title">
                    {
                        Translations.translations.modals.pendingPayment.title
                    }
                </div><div className="modal-content-content">
                    <IconError />
                </div><div className="modal-content-content">
                    {
                        Translations.translations.modals.pendingPayment.content(this.props.amount)
                    }
                </div><div className="modal-content-content">
                    <div className="modal-content-hr center"></div>
                </div><div className="modal-content-content">
                    <div className="modal-content-content-left">
                        <Button color="white" size="small" onClick={() => {
                            Modal.dismiss.next()
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.close
                            }
                        </Button>
                    </div><div className="modal-content-content-right">
                        <Button color="orange" size="small" href={this.props.mollieLink}>
                            {
                                Translations.translations.modals.pendingPayment.buttons.toPaymentPage
                            }
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}