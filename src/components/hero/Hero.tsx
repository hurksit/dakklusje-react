import React from 'react'
import './hero.scss'
import background from './background.png'

/**
 * The props
 */
interface HeroProps {

    /**
     * The id of the element
     */
    id?: string

    /**
     * The section height
     */
    height?: number

    /**
     * Whether or not to render the hero as absolute positioned.
     */
    absolute?: boolean
}

/**
 * The hero of a page (top-section).
 * 
 * @author Stan Hurks
 */
export default class Hero extends React.Component<HeroProps> {

    public render = () => {
        return (
            <div className="hero-container" style={{
                ...this.props.height ? {
                    height: this.props.height,
                    maxHeight: this.props.height,
                    overflowY: 'hidden'
                } : {},

                ...this.props.absolute ? {
                    position: 'absolute',
                    left: 0,
                    width: '100vw'
                } : {}
            }}>
                <section id={this.props.id} style={{
                    backgroundImage: `url(${background})`
                }} className="hero">
                    <div className="hero-content">
                        <div className="hero-content-grid">
                            {
                                this.props.children
                            }
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}