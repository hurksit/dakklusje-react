import React from 'react'
import { Check } from '@material-ui/icons'
import './quality-checks.scss'
import Translations from '../../../translations/Translations'

/**
 * The quality checks.
 * 
 * @author Stan Hurks
 */
export default class QualityChecks extends React.Component {

    public render = () => {
        return (
            <div className="quality-checks">
                <div className="quality-checks-grid">
                    <div className="quality-checks-grid-quality-check">
                        <div className="quality-checks-grid-quality-check-checkmark">
                            <Check width={24} />
                        </div><div className="quality-checks-grid-quality-check-label">
                            {
                                Translations.translations.components.qualityChecks[1]
                            }
                        </div>
                    </div><div className="quality-checks-grid-quality-check">
                        <div className="quality-checks-grid-quality-check-checkmark">
                            <Check width={24} />
                        </div><div className="quality-checks-grid-quality-check-label">
                            {
                                Translations.translations.components.qualityChecks[2]
                            }
                        </div>
                    </div><div className="quality-checks-grid-quality-check">
                        <div className="quality-checks-grid-quality-check-checkmark">
                            <Check width={24} />
                        </div><div className="quality-checks-grid-quality-check-label">
                            {
                                Translations.translations.components.qualityChecks[3]
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}