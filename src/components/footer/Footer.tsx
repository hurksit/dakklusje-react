import React from 'react'
import QualityChecks from './quality-checks/QualityChecks'
import AppInfo from '../../core/AppInfo'
import Translations from '../../translations/Translations'
import Button from '../button/Button'
import './footer.scss'
import { Route } from '../../enumeration/Route'

/**
 * The footer
 * 
 * @author Stan Hurks
 */
export default class Footer extends React.Component {

    public render = () => {
        return (
            <footer className="footer">
                <QualityChecks />
                <aside className="footer-top">
                    <div className="footer-top-grid">
                        <div className="footer-top-grid-top">
                            <Button color="white" href={Route.CONSUMER_PLACE_YOUR_JOB}>
                                {
                                    Translations.translations.components.footer.top.addJob
                                }
                            </Button>
                            <span>
                                {
                                    Translations.translations.components.footer.top.text
                                }
                            </span>
                        </div><div className="footer-top-grid-hr"></div>
                    </div>
                </aside><div className="footer-main">
                    <div className="footer-main-grid">
                        <div className="footer-main-grid-column">
                            <div className="footer-main-grid-column-title">
                                {
                                    Translations.translations.components.footer.bottom.about.title
                                }
                            </div><div className="footer-main-grid-column-hr">
                            </div><div className="footer-main-grid-column-content">
                                <a href="/over-ons">
                                    {
                                        Translations.translations.components.footer.bottom.about.content.aboutUs
                                    }
                                </a>
                                <a href={AppInfo.designedAndBuildBy.termsAndAgreementsLink}>
                                    {
                                        Translations.translations.components.footer.bottom.about.content.terms
                                    }
                                </a>
                                <a href="/inschrijven-als-dakspecialist">
                                    {
                                        Translations.translations.components.footer.bottom.about.content.registerAsRoofSpecialist
                                    }
                                </a>
                            </div><div className="footer-main-grid-column-hr">
                            </div><div className="footer-main-grid-column-legal">
                                &copy; {
                                    new Date().getFullYear() > 2019
                                    ? '2019 - ' + new Date().getFullYear()
                                    : '2019'
                                } {
                                    AppInfo.designedAndBuildBy.companyName
                                }
                                <br />
                                {
                                    Translations.translations.components.loader.allRightsReserved
                                }
                            </div>
                        </div><div className="footer-main-grid-column">
                            <div className="footer-main-grid-column-title">
                                {
                                    AppInfo.designedAndBuildBy.companyName
                                }
                            </div><div className="footer-main-grid-column-hr">
                            </div><div className="footer-main-grid-column-content">
                                <div className="footer-main-grid-column-content-title">
                                    {
                                        Translations.translations.components.footer.bottom.coc.title
                                    }
                                </div><div className="footer-main-grid-column-content-content">
                                    {
                                        AppInfo.designedAndBuildBy.cocNumber
                                    }
                                </div><div className="footer-main-grid-column-content-title">
                                    {
                                        Translations.translations.components.footer.bottom.contact.title
                                    }
                                </div><div className="footer-main-grid-column-content-content">
                                    {
                                        Translations.translations.components.footer.bottom.contact.content.email
                                    }: <a href={`mailto:${AppInfo.designedAndBuildBy.emailAddress}`}>{AppInfo.designedAndBuildBy.emailAddress}</a><br/>
                                </div>
                            </div>
                        </div><div className="footer-main-grid-column">
                            <div className="footer-main-grid-column-title">
                                {
                                    Translations.translations.components.footer.bottom.followUs.title
                                }
                            </div><div className="footer-main-grid-column-hr">
                            </div><div className="footer-main-grid-column-content">
                                <a href={AppInfo.designedAndBuildBy.linkedIn}>LinkedIn</a>
                                <a href={AppInfo.designedAndBuildBy.facebook}>Facebook</a>
                                <a href={AppInfo.designedAndBuildBy.instagram}>Instagram</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}