import React from 'react'
import './step.scss'
import Badge from '../badge/Badge'

/**
 * The props
 */
interface StepProps {

    /**
     * The index of the step
     */
    index?: number

    /**
     * The title to display
     */
    title: string
}

/**
 * A step in the home page.
 * 
 * @author Stan Hurks
 */
export default class Step extends React.Component<StepProps> {

    public render = () => {
        return (
            <div className="step">
                <div className="step-table">
                    {
                        this.props.index
                        &&
                        <div className="step-table-left">
                            <Badge value={this.props.index} />
                        </div>
                    }
                    <div className="step-table-right">
                        {
                            this.props.title
                        }
                    </div>
                </div><div className="step-content" style={{
                    ...this.props.index !== undefined ? {} : {
                        paddingLeft: 0
                    }
                }}>
                    {
                        this.props.children
                    }
                </div>
            </div>
        )
    }
} 