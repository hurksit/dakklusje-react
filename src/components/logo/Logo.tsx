import React from 'react'
import './logo.scss'
import logo from './logo.png'

/**
 * The props
 */
interface LogoProps {

    /**
     * Whether or not to hide the label
     */
    hideLabel?: boolean

    /**
     * The color of the label
     */
    labelColor: 'white' | 'purple'
}

/**
 * The logo component
 * 
 * @author Stan Hurks
 */
export default class Logo extends React.Component<LogoProps> {

    public render = () => {
        return (
            <div className="logo">
                <div className="logo-image">
                    <img src={logo} alt="Logo" draggable={false} />
                </div>
                {
                    !this.props.hideLabel
                    &&
                    <div className="logo-label" style={{
                        color: this.props.labelColor === 'white'
                            ? 'white'
                            : '#4056A1'
                    }}>
                        akklusje.nl
                    </div>
                }
            </div>
        )
    }
}