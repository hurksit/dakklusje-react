import React from 'react'
import PageSectionTitle from '../page/section/title/PageSectionTitle'
import PageSection from '../page/section/PageSection'
import AnonymousReviewResponse from '../../backend/response/entity/AnonymousReviewResponse'
import Backend from '../../backend/Backend'
import PageSectionContent from '../page/section/content/PageSectionContent'
import ProgressSpinner from '../progress-spinner/ProgressSpinner'
import AnonymousReview from '../anonymous-review/AnonymousReview'
import Translations from '../../translations/Translations'

/**
 * The state
 */
interface ExperienceFromOthersState {

    /**
     * The anonymous reviews
     */
    anonymousReviews: AnonymousReviewResponse[]|null
}

/**
 * The experience from others section.
 * 
 * @author Stan Hurks
 */
export default class ExperienceFromOthers extends React.Component<any, ExperienceFromOthersState> {

    constructor(props: any) {
        super(props)

        this.state = {
            anonymousReviews: null
        }
    }

    public componentDidMount = () => {
        this.updateData()
    }

    public render = () => {
        return (
            <div className="experience-from-others">
                {
                    this.state.anonymousReviews === null || this.state.anonymousReviews.length >= 2
                    ?
                    <PageSection color='grey'>
                        <PageSectionTitle>
                            {
                                Translations.translations.components.experienceFromOthers.title
                            }
                        </PageSectionTitle><PageSectionContent>
                            {
                                this.state.anonymousReviews !== null
                                ?
                                <aside className="experience-from-others-container">
                                    {
                                        this.state.anonymousReviews.map((review, index) => <AnonymousReview value={review} key={index} />)
                                    }
                                </aside>
                                :
                                <ProgressSpinner />
                            }
                        </PageSectionContent>
                    </PageSection>
                    :
                    undefined
                }
            </div>
        )        
    }

    /**
     * Updates the data
     */
    private updateData = () => {
        Backend.controllers.review.listReviewsExperienceFromOthers()
            .then((response) => {
                this.setState({
                    anonymousReviews: response.data
                })
            })
            .catch((response) => {
                console.error(response)
            })
    }
}