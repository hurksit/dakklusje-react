import React from 'react'
import Picture from '../picture/Picture'
import Storage from '../../storage/Storage'
import Translations from '../../translations/Translations'
import { Person } from '@material-ui/icons'
import './profile-picture.scss'
import { Subscription } from 'rxjs'
import App from '../../core/app/App'
import ImageResponse from '../../backend/response/entity/ImageResponse'
import classNames from 'classnames'

/**
 * The props
 */
interface ProfilePictureProps {

    /**
     * The icon to display when hovering
     */
    hoverIcon?: JSX.Element,

    /**
     * The action to perform on click
     */
    onClick?: () => void

    /**
     * The image (when this is not the personal profile picture)
     */
    image?: ImageResponse|null

    /**
     * The size (default: 124)
     */
    size?: number

}

/**
 * The state
 */
interface ProfilePictureState {

    /**
     * The size (default: 124)
     */
    size?: number
}

/**
 * The profile picture container
 */
export default class ProfilePicture extends React.Component<ProfilePictureProps, ProfilePictureState> {

    /**
     * The subscription for when the app updates
     */
    private subscriptionAppUpdate!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            size: this.props.size
        }
    }

    public componentDidMount = () => {
        this.subscriptionAppUpdate = App.update.subscribe(this.onAppUpdate)
    }

    public componentWillUnmount = () => {
        this.subscriptionAppUpdate.unsubscribe()
    }

    public componentWillReceiveProps = () => {
        this.setState({
            size: this.props.size
        })
    }
    
    public render = () => {
        return (
            <div className={classNames({
                'profile-picture': true,
                'hoverable': !!this.props.onClick
            })} onClick={() => {
                if (this.props.onClick) {
                    this.props.onClick()
                }
            }} style={{
                width: this.props.size || 124,
                height: this.props.size || 124
            }}>
                {
                    this.props.image !== undefined
                    ? (
                        this.props.image !== null
                        ?
                        <Picture image={this.props.image} size={this.props.size || 124} />
                        :
                        <div className="profile-picture-no-picture">
                            <Person />
                        </div>
                    )
                    : (
                        Storage.data.session.user.profilePicture !== undefined
                        ?
                        <Picture image={this.getImage()} size={this.props.size || 124} />
                        :
                        <div className="profile-picture-no-picture">
                            <Person />
                        </div>
                    )
                }

                {
                    this.props.onClick
                    &&
                    <div className="profile-picture-hover">
                        {
                            this.props.hoverIcon
                        }
                    </div>
                }
            </div>
        )
    }

    /**
     * Whenever the app updates
     */
    private onAppUpdate = () => {
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    /**
     * Get the image
     */
    private getImage = (): ImageResponse|null => {
        if (!Storage.data.session.user.profilePicture) {
            return null
        }
        return {
            id: '',
            backgroundPosition: Storage.data.session.user.profilePicture.backgroundPosition,
            zoom: Storage.data.session.user.profilePicture.zoom,
            file: {
                id: '',
                content: Storage.data.session.user.profilePicture.imageURL,
                createdAt: 0,
                title: Translations.translations.components.profilePicture.alt
            }
        }
    }
}