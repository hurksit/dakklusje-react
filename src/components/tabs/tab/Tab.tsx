import React from 'react'
import './tab.scss'
import classNames from 'classnames'

/**
 * The props
 */
interface TabProps {

    /**
     * The callback on hover
     */
    onHover: (isHovered: boolean) => void

    /**
     * The on click callback
     */
    onClick: () => void

    /**
     * The color to use
     * default: `blue`
     */
    color?: 'white' | 'blue'
}

/**
 * A tab in the tabs component.
 * 
 * @author Stan Hurks
 */
export default class Tab extends React.Component<TabProps> {

    public render = () => {
        return (
            <div className={classNames({
                'tab': true,
                ['color-' + (this.props.color || 'blue')]: true
            })} onMouseOver={() => {
                this.props.onHover(true)
            }} onMouseOut={() => {
                this.props.onHover(false)
            }} onClick={() => {
                this.props.onClick()
            }}>
                {
                    this.props.children
                }
            </div>
        )
    }
}