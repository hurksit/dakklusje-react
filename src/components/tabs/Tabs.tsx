import React from 'react'
import './tabs.scss'

/**
 * The props
 */
interface TabsProps {

    /**
     * The index of the tab that is hovered
     */
    hoverIndex: number|null

    /**
     * The tab index
     */
    tabIndex: number
}

/**
 * The state
 */
interface TabsState {

    /**
     * The amount of tabs in the component
     */
    tabsCount: number
}

/**
 * The tabs component.
 * 
 * @author Stan Hurks
 */
export default class Tabs extends React.Component<TabsProps, TabsState> {

    /**
     * The reference to the tabs element
     */
    private tabsRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            tabsCount: 0
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            this.updateTabsCount()
        })
    }

    public componentDidUpdate = () => {
        setTimeout(() => {
            this.updateTabsCount()
        })
    }

    public render = () => {
        return (
            <div className="tabs">
                <div className="tabs-table">
                    <div className="tabs-table-row" ref={this.tabsRef}>
                        {
                            this.props.children
                        }
                    </div>
                </div><div className="tabs-border-bottom" style={{
                    ...this.props.hoverIndex !== null ? {
                        left: `${100 / this.state.tabsCount * this.props.hoverIndex}%`
                    } : {
                        left: `${100 / this.state.tabsCount * this.props.tabIndex}%`
                    },
                    width: `${(100 / Math.max(this.state.tabsCount, 1))}%`
                }}></div>
            </div>
        )
    }

    /**
     * Updates the tab count
     */
    private updateTabsCount = () => {
        if (!this.tabsRef.current) {
            return
        }

        this.setState({
            tabsCount: this.tabsRef.current.children.length
        })
    }
}