import React from 'react'
import './progress-spinner.scss'
import RadialProgress from '../radial-progress/RadialProgress'
import Translations from '../../translations/Translations'
import Logo from '../logo/Logo'

/**
 * The props
 */
interface ProgressSpinnerProps {

    /**
     * Whether or not to use a logo instead of a label in the internal radial progress component intstance.
     */
    useLogo?: boolean

    /**
     * The size of the radial progress spinner
     */
    size?: number

}

/**
 * The state
 */
interface ProgressSpinnerState {
    
    /**
     * The percentage
     */
    percentage: number
}

/**
 * The progress spinner for when the API is loading data in a component.
 * 
 * @author Stan Hurks
 */
export default class ProgressSpinner extends React.Component<ProgressSpinnerProps, ProgressSpinnerState> {

    constructor(props: any) {
        super(props)
        
        this.state = {
            percentage: 0
        }
    }

    private interval!: NodeJS.Timeout

    public componentDidMount = () => {
        this.interval = setInterval(() => {
            this.setState({
                percentage: this.state.percentage + 100
            })
        }, 1000)
    }

    public componentWillUnmount = () => {
        clearInterval(this.interval)
    }

    public render = () => {
        return (
            <div className="progress-spinner">
                <RadialProgress
                    percentage={this.state.percentage}>
                    
                    {
                        this.props.useLogo
                        ?
                        <Logo labelColor='white' hideLabel />
                        :
                        <span>
                            {
                                Translations.translations.components.progressSpinner.loading
                            }
                        </span>
                    }
                </RadialProgress>
            </div>
        )
    }

    /**
     * Calculate the radius of the stroke
     * @param strokeSize the size of the stroke
     */
    private calculateRadius = (strokeSize: number): number => {
        return ((this.props.size || 80) / 2) - (strokeSize / 2)
    }

    /**
     * Calculate the circumference of the stroke
     * @param strokeSize the size of the stroke
     */
    public calculateCircumference = (strokeSize: number): number => {
        return this.calculateRadius(strokeSize) * 2 * Math.PI
    }
}