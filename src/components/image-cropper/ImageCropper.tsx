import React from 'react'
import './image-cropper.scss'
import FormSlider from '../form/slider/FormSlider'
import Translations from '../../translations/Translations'
import { Subscription } from 'rxjs'
import Events from '../../shared/Events'

/**
 * An image in the image cropper
 */
export interface ImageCropperImage {

    /**
     * The background position of image
     */
    backgroundPosition: string

    /**
     * The zoom applied to image
     */
    zoom: number
}

/**
 * The props
 */
interface ImageCropperProps {

    /**
     * The image
     */
    image: ImageCropperImage & {

        /**
         * The image data URL
         */
        url: string
    }

    /**
     * Whenever the values in the image cropper change
     */
    onChange: (image: ImageCropperImage) => void
}

/**
 * The state
 */
interface ImageCropperState {
    /**
     * The background position of image
     */
    backgroundPosition: string

    /**
     * The zoom applied to image
     */
    zoom: number

    /**
     * All properties for the drag and drop functionality.
     */
    dragAndDrop?: {
        startX: number

        currentX: number

        startY: number

        currentY: number
    }
}

/**
 * The image cropper.
 * 
 * @author Stan Hurks
 */
export default class ImageCropper extends React.Component<ImageCropperProps, ImageCropperState> {

    /**
     * The ref for the container element
     */
    private containerRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The ref for the container settings element
     */
    private settingsRef: React.RefObject<HTMLDivElement> = React.createRef()

     /**
     * The subscription for the mouseup event on the document
     */
    private subscriptionDocumentMouseup!: Subscription

    /**
     * The subscription for the mousemove event on the document
     */
    private subscriptionDocumentMousemove!: Subscription

    /**
     * The subscription for the touchmove event on the document
     */
    private subscriptionDocumentTouchmove!: Subscription

    /**
     * The subscription for the touchend event on the document
     */
    private subscriptionDocumentTouchend!: Subscription

    constructor(props: any) {
        super(props)
        
        this.state = {
            backgroundPosition: '50% 50%',
            zoom: 1
        }
    }

    public componentDidMount = () => {
        this.subscriptionDocumentMouseup = Events.document.mouseup.subscribe(this.onDocumentMouseup as any)
        this.subscriptionDocumentMousemove = Events.document.mousemove.subscribe(this.onDocumentMousemove as any)
        this.subscriptionDocumentTouchmove = Events.document.touchmove.subscribe(this.onDocumentTouchmove as any)
        this.subscriptionDocumentTouchend = Events.document.touchend.subscribe(this.onDocumentTouchend as any)

        setTimeout(() => {
            this.setState({
                backgroundPosition: this.props.image.backgroundPosition,
                zoom: this.props.image.zoom
            }, () => {
                this.props.onChange({
                    backgroundPosition: this.state.backgroundPosition,
                    zoom: this.state.zoom
                })
            })
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionDocumentMouseup.unsubscribe()
        this.subscriptionDocumentMousemove.unsubscribe()
        this.subscriptionDocumentTouchmove.unsubscribe()
        this.subscriptionDocumentTouchend.unsubscribe()
    }

    public render = () => {
        return (
            <div className="image-cropper">
                <div className="image-cropper-image-container" onMouseDown={this.onDocumentMousedown as any} onTouchStart={this.onDocumentTouchstart as any} ref={this.containerRef}>
                    <img className="image-cropper-image-container-background" src={this.props.image.url} height={200 * this.state.zoom} style={{
                        transform: `translate(${this.getTranslation()})`,
                        OTransform: `translate(${this.getTranslation()})`,
                        WebkitTransform: `translate(${this.getTranslation()}`
                    }} draggable={false} alt={'Img'} />

                    <div className="image-cropper-image-container-grid">
                        <div className="image-cropper-image-container-grid-vertical-left"></div>
                        <div className="image-cropper-image-container-grid-vertical-right"></div>
                        <div className="image-cropper-image-container-grid-horizontal-top"></div>
                        <div className="image-cropper-image-container-grid-horizontal-bottom"></div>
                    </div>

                    <div className="image-cropper-image-container-settings" ref={this.settingsRef}>
                        <div className="image-cropper-image-container-settings-title">
                            {
                                Translations.translations.components.imageCropper.settings.title
                            }
                        </div><div className="image-cropper-image-container-settings-slider">
                            <FormSlider label={Translations.translations.components.imageCropper.settings.slider.label}
                                value={this.state.zoom} minValue={0.5} maxValue={5} steps={45} onChange={(zoom) => {

                                this.setState({
                                    zoom
                                }, () => {
                                    this.props.onChange({
                                        backgroundPosition: this.state.backgroundPosition,
                                        zoom: this.state.zoom
                                    })
                                })
                            }} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the mousedown event is triggered on the document
     */
    private onDocumentMousedown = (event: MouseEvent) => {
        if (!this.settingsRef.current) {
            return
        }
        let isSettings = false
        const loop = (rootElement: Element) => {
            if (rootElement === this.settingsRef.current) {
                isSettings = true
            } else if (rootElement.parentElement !== null) {
                loop(rootElement.parentElement)
            }
        }
        loop(event.target as Element)
        if (isSettings) {
            return
        }
        this.setState({
            dragAndDrop: {
                startX: event.clientX,
                currentX: event.clientX,
                startY: event.clientY,
                currentY: event.clientY
            }
        })
    }

    /**
     * Whenever the mousemove event is triggered on the document
     */
    private onDocumentMousemove = (event: MouseEvent) => {
        if (!this.state.dragAndDrop || !this.containerRef.current) {
            return
        }
        const minX = this.containerRef.current.getBoundingClientRect().left
        const maxX = minX + this.containerRef.current.getBoundingClientRect().width
        const minY = this.containerRef.current.getBoundingClientRect().top
        const maxY = minY + this.containerRef.current.getBoundingClientRect().height
        this.setState({
            dragAndDrop: {
                ...this.state.dragAndDrop,
                currentX: Math.max(minX, Math.min(maxX, event.clientX)),
                currentY: Math.max(minY, Math.min(maxY, event.clientY))
            }
        }, () => {
            this.setState({
                backgroundPosition: this.getBackgroundPosition(),
                dragAndDrop: {
                    currentX: Math.max(minX, Math.min(maxX, event.clientX)),
                    startX: Math.max(minX, Math.min(maxX, event.clientX)),
                    currentY: Math.max(minY, Math.min(maxY, event.clientY)),
                    startY: Math.max(minY, Math.min(maxY, event.clientY))
                }
            }, () => {
                this.props.onChange({
                    backgroundPosition: this.state.backgroundPosition,
                    zoom: this.state.zoom
                })
            })
        })
    }

    /**
     * Whenever the mouseup event is triggered on the document
     */
    private onDocumentMouseup = (event: MouseEvent) => {
        if (!this.state.dragAndDrop) {
            return
        }
        this.setState({
            backgroundPosition: this.getBackgroundPosition(),
            dragAndDrop: undefined
        }, () => {
            this.props.onChange({
                backgroundPosition: this.props.image.backgroundPosition,
                zoom: this.props.image.zoom
            })
        })
    }

    /**
     * Whenever the touchstart event is triggered on the document
     */
    private onDocumentTouchstart = (event: TouchEvent) => {
        const touch = event.touches.item(0)
        if (!touch) {
            return
        }
        let isSettings = false
        const loop = (rootElement: Element) => {
            if (rootElement === this.settingsRef.current) {
                isSettings = true
            } else if (rootElement.parentElement !== null) {
                loop(rootElement.parentElement)
            }
        }
        loop(event.target as Element)
        if (isSettings) {
            return
        }
        this.setState({
            dragAndDrop: {
                startX: touch.clientX,
                currentX: touch.clientX,
                startY: touch.clientY,
                currentY: touch.clientY
            }
        })
    }

    /**
     * Whenever the touchmove event is triggered on the document
     */
    private onDocumentTouchmove = (event: TouchEvent) => {
        if (!this.state.dragAndDrop || !this.containerRef.current) {
            return
        }
        const touch = event.touches.item(0)
        if (!touch) {
            return
        }
        const minX = this.containerRef.current.getBoundingClientRect().left
        const maxX = minX + this.containerRef.current.getBoundingClientRect().width
        const minY = this.containerRef.current.getBoundingClientRect().top
        const maxY = minY + this.containerRef.current.getBoundingClientRect().height
        this.setState({
            dragAndDrop: {
                ...this.state.dragAndDrop,
                currentX: Math.max(minX, Math.min(maxX, touch.clientX)),
                currentY: Math.max(minY, Math.min(maxY, touch.clientY))
            }
        }, () => {
            this.setState({
                backgroundPosition: this.getBackgroundPosition(),
                dragAndDrop: {
                    currentX: Math.max(minX, Math.min(maxX, touch.clientX)),
                    startX: Math.max(minX, Math.min(maxX, touch.clientX)),
                    currentY: Math.max(minY, Math.min(maxY, touch.clientY)),
                    startY: Math.max(minY, Math.min(maxY, touch.clientY))
                }
            }, () => {
                this.props.onChange({
                    backgroundPosition: this.state.backgroundPosition,
                    zoom: this.state.zoom
                })
            })
        })
    }

    /**
     * Whenever the touchend event is triggered on the document
     */
    private onDocumentTouchend = () => {
        if (!this.state.dragAndDrop) {
            return
        }
        this.setState({
            backgroundPosition: this.getBackgroundPosition(),
            dragAndDrop: undefined
        }, () => {
            this.props.onChange({
                backgroundPosition: this.props.image.backgroundPosition,
                zoom: this.props.image.zoom
            })
        })
    }

    /**
     * Get the background position
     */
    private getBackgroundPosition = () => {
        let percentages = this.state.backgroundPosition.split(' ').map((x) => Number(x.replace('%', '').trim()))
        if (this.state.dragAndDrop && this.containerRef.current) {
            percentages = [
                percentages[0] - (100 * (this.state.dragAndDrop.currentX - this.state.dragAndDrop.startX) / this.containerRef.current.getBoundingClientRect().width),
                percentages[1] - (100 * (this.state.dragAndDrop.currentY - this.state.dragAndDrop.startY) / this.containerRef.current.getBoundingClientRect().height)
            ]
        }
        percentages = percentages.map((percentage) => Math.min(100, Math.max(0, percentage)))
        return `${percentages[0]}% ${percentages[1]}%`
    }

    /**
     * Get the translation
     */
    private getTranslation = () => {
        let backgroundPosition = this.getBackgroundPosition().split(' ').map((x) => Number(x.replace('%', '').trim()))
        return `${-backgroundPosition[0]}%, ${-backgroundPosition[1]}%`
    }
}