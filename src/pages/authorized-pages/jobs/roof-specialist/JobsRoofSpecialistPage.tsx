import React from 'react'
import './jobs-roof-specialist-page.scss'
import { RouteComponentProps } from 'react-router'
import { Route } from '../../../../enumeration/Route'
import Routes from '../../../../routes/Routes'
import Hero from '../../../../components/hero/Hero'
import Wizard from '../../../../components/wizard/Wizard'
import WizardTop from '../../../../components/wizard/top/WizardTop'
import Translations from '../../../../translations/Translations'
import Badge from '../../../../components/badge/Badge'
import Footer from '../../../../components/footer/Footer'
import FilterButton from '../../../../components/filter-button/FilterButton'
import RoofSpecialistJobResponse from '../../../../backend/controller/roofspecialist/job/response/RoofSpecialistJobResponse'
import Pagination from '../../../../backend/pagination/Pagination'
import Loader from '../../../../components/loader/Loader'
import Backend from '../../../../backend/Backend'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import PaginationBar from '../../../../components/pagination/bar/PaginationBar'
import Breadcrumbs from '../../../../components/breadcrumbs/Breadcrumbs'
import RoofSpecialistFilterResponse from '../../../../backend/response/entity/RoofSpecialistFilterResponse'
import Storage from '../../../../storage/Storage'
import StorageRoofSpecialistDataModel from '../../../../storage/data-model/roof-specialist/StorageRoofSpecialistDataModel'
import ProgressSpinner from '../../../../components/progress-spinner/ProgressSpinner'
import IconAlternateEmail from '../../../../components/icon/IconAlternateEmail'
import JobsRoofSpecialistJob from './job/JobsRoofSpecialistJob'
import Button from '../../../../components/button/Button'
import { KeyboardArrowLeft } from '@material-ui/icons'
import classNames from 'classnames'
import Window from '../../../../shared/Window'
import JobsRoofSpecialistJobDetail from './job/detail/JobsRoofSpecialistJobDetail'
import Modal from '../../../../components/modal/Modal'
import RoofSpecialistFiltersModal from '../../../../components/modal/roof-specialist-filters/RoofSpecialistFiltersModal'
import { Subscription } from 'rxjs'
import App from '../../../../core/app/App'
import WebSocket from '../../../../backend/WebSocket'

/**
 * The state
 */
interface JobsRoofSpecialistPageState {

    /**
     * Whether the page is done loading the jobs.
     */
    doneLoading: boolean

    /**
     * Pagination data
     */
    pagination: Pagination<RoofSpecialistJobResponse>

    /**
     * The filters for the roof specialist.
     */
    filters: RoofSpecialistFilterResponse[]

    /**
     * The selected job
     */
    selectedJob: RoofSpecialistJobResponse|null

    /**
     * The vertical index of the view
     */
    viewIndex: number

    /**
     * The index from which the view is changing
     */
    fromIndex: number|null

    /**
     * The index to which the view is changing
     */
    toIndex: number|null
}

/**
 * The jobs page for the roof specialist.
 * 
 * @author Stan Hurks
 */
export default class JobsRoofSpecialistPage extends React.Component<RouteComponentProps, JobsRoofSpecialistPageState> {

    /**
     * The amount of MS it takes to perform a full animation
     */
    private static readonly animationTimeMS: number = 500

    /**
     * The reference to the filter button element
     */
    private filterButtonRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The reference to the wizard
     */
    private wizardRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The subscription to when the app updates
     */
    private subscriptionAppUpdated!: Subscription

    /**
     * The subscription to when there is a new connection for the user
     */
    private subscriptionWebSocketUserConnection!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            doneLoading: false,

            pagination: {
                entries: [],

                currentPage: 1,

                lastPage: 1,

                total: 0
            },

            filters: (Storage.data.session.user.roofSpecialist as StorageRoofSpecialistDataModel).filters,

            selectedJob: null,

            viewIndex: 0,

            fromIndex: null,

            toIndex: null
        }
    }

    public componentDidMount = () => {
        if (Storage.data.session.user.roofSpecialist === undefined || !Storage.data.session.user.roofSpecialist.profileComplete) {
            Routes.redirectURL.next(Route.PROFILE)
        } else {
            const pageNumber = Number(this.props.match.params['pageNumber'] || 1)

            this.updatePagination(pageNumber)
    
            setTimeout(() => {
                Routes.setCurrentRoute.next(Route.ROOF_SPECIALIST_JOBS)
            })
        }

        this.subscriptionAppUpdated = App.updated.subscribe(this.onAppUpdated)
        this.subscriptionWebSocketUserConnection = WebSocket.userConnection.subscribe(this.onWebSocketUserConnection)
    }
    
    public componentWillUnmount = () => {
        this.subscriptionAppUpdated.unsubscribe()
        this.subscriptionWebSocketUserConnection.unsubscribe()
    }

    public render = () => {
        return (
            <div id="jobs-roof-specialist-page">
                <Hero id="jobs-roof-specialist-page-hero" absolute height={400} />

                <div id="jobs-roof-specialist-page-wizard" ref={this.wizardRef}>
                    <Wizard>
                        <WizardTop overlayColor={this.state.selectedJob === null ? undefined : 'rgba(255, 255, 255, 0.3)'}>
                            <div id="jobs-roof-specialist-page-wizard-top-title">
                                <h1 id="jobs-roof-specialist-page-wizard-top-title-label">
                                    {
                                        Translations.translations.pages.jobs.roofSpecialist.wizard.top.title
                                    }
                                </h1>

                                <div id="jobs-roof-specialist-page-wizard-top-title-badge">
                                    <Badge value={this.state.pagination.total} showFullNumber />
                                </div>
                            </div><div id="jobs-roof-specialist-page-wizard-top-description">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.wizard.top.description.map((part, partIndex) =>
                                        <div key={partIndex}>
                                            {
                                                part
                                            }
                                        </div>
                                    )   
                                }
                            </div><div id="jobs-roof-specialist-page-wizard-top-filter" ref={this.filterButtonRef}>
                                <FilterButton badge={this.state.filters.length} onClick={() => {
                                    if (!this.filterButtonRef.current) {
                                        return
                                    }
                                    Modal.mount.next({
                                        element: (
                                            <RoofSpecialistFiltersModal filters={this.state.filters} onChangeFilters={(filters) => {
                                                this.setState({
                                                    filters
                                                }, () => {
                                                    this.updatePagination(this.state.pagination.currentPage)
                                                })
                                            }} />
                                        ),
                                        dismissable: false,
                                        target: this.filterButtonRef.current,
                                        position: 'left',
                                        offsetY: 120
                                    })
                                }} />
                            </div>
                        </WizardTop>

                        <WizardContent>
                            {
                                this.state.doneLoading
                                ? this.renderContent()
                                : (
                                    <ProgressSpinner />
                                )
                            }
                        </WizardContent>
                    </Wizard>
                </div>

                <Breadcrumbs path={[
                    Route.HOME,
                    Route.ROOF_SPECIALIST_JOBS
                ]} />

                <Footer />
            </div>
        )
    }

    private renderContent = () => {
        return (
            <div id="jobs-roof-specialist-page-wizard-content">
                <div id="jobs-roof-specialist-page-wizard-content-container" className={classNames({
                    'job-detail': this.state.selectedJob !== null 
                })}>
                    <PaginationBar pagination={this.state.pagination} onChangePage={(pageNumber) => {
                        this.updatePagination(pageNumber)
                    }} />

                    <div id="jobs-roof-specialist-page-wizard-content-container-job-detail">
                        <Button color="white" size="small" onClick={() => {
                            if (!this.wizardRef.current) {
                                return
                            }
                            Window.scrollTo({
                                top: this.wizardRef.current.getBoundingClientRect().top + window.scrollY,
                                behavior: 'smooth'
                            }, () => {
                                this.setState({
                                    selectedJob: null
                                })
                                this.goToIndex(0)
                            })
                        }}>
                            <KeyboardArrowLeft />
                    
                            <span>
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.wizard.content.content.container.jobDetail.backToOverview
                                }
                            </span>
                        </Button>
                    </div>
                </div>
                
                <div id="jobs-roof-specialist-page-wizard-content-height-container" style={{
                    ...this.state.toIndex !== null && this.state.fromIndex !== null ? {
                        height: '100vh'
                    } : {}
                }}>
                    <div className={classNames({
                        'animation-cube': true,
                        'animation-cube-left-to-right': this.state.toIndex !== null && this.state.fromIndex !== null
                            && this.state.toIndex < this.state.fromIndex,
                        'animation-cube-right-to-left': this.state.toIndex !== null && this.state.fromIndex !== null
                            && this.state.toIndex > this.state.fromIndex
                    })}>
                        <div className="cube" style={{
                            ...this.state.toIndex !== null && this.state.fromIndex !== null ? {
                                position: 'absolute'
                            } : {
                                position: 'relative'
                            }
                        }}>
                            {
                                this.renderOverview()
                            }
                            {
                                this.renderJobDetail()
                            }
                        </div>
                    </div>
                </div>
            </div>            
        )
    }

    /**
     * Renders the overview
     */
    private renderOverview = () => {
        return (
            <div className={classNames({
                'active': this.state.viewIndex === 0,
                'inactive': this.state.viewIndex === 1
            })}>
                {
                    this.state.pagination.entries.length > 0
                    ?
                    (
                        <div id="jobs-roof-specialist-page-wizard-content-entries">
                            {
                                this.state.pagination.entries.map((job, jobIndex) =>
                                    <JobsRoofSpecialistJob job={job} key={jobIndex} onSelect={() => {
                                        if (!this.wizardRef.current) {
                                            return
                                        }
                                        Window.scrollTo({
                                            top: this.wizardRef.current.getBoundingClientRect().top + window.scrollY,
                                            behavior: 'smooth'
                                        }, () => {
                                            this.setState({
                                                selectedJob: job
                                            })
                                            this.goToIndex(1)
                                        })
                                    }} />
                                )
                            }
                        </div>
                    )
                    :
                    (
                        <div id="jobs-roof-specialist-page-wizard-content-entries-no-entries">
                            <div id="jobs-roof-specialist-page-wizard-content-entries-no-entries-icon">
                                <IconAlternateEmail />
                            </div><div id="jobs-roof-specialist-page-wizard-content-entries-no-entries-title">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.wizard.content.entries.noEntries.title
                                }
                            </div><div id="jobs-roof-specialist-page-wizard-content-entries-no-entries-description">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.wizard.content.entries.noEntries.description
                                }
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }

    /**
     * Renders the job detail
     */
    private renderJobDetail = () => {
        return (
            <div className={classNames({
                'active': this.state.viewIndex === 1,
                'inactive': this.state.viewIndex === 0
            })}>
                <div id="jobs-roof-specialist-page-wizard-content-job-detail">
                    {
                        this.state.selectedJob !== null
                        &&
                        <JobsRoofSpecialistJobDetail job={this.state.selectedJob} />
                    }
                </div> 
            </div>
        )
    }

    /**
     * Whenever the app updates
     */
    private onAppUpdated = () => {
        this.setState({
            filters: (Storage.data.session.user.roofSpecialist as StorageRoofSpecialistDataModel).filters
        }, () => {
            this.updatePagination(this.state.pagination.currentPage)
        })
    }

    /**
     * Whenever theres a new connection for the user
     */
    private onWebSocketUserConnection = () => {
        this.goToIndex(0, () => {
            this.setState({
                filters: (Storage.data.session.user.roofSpecialist as StorageRoofSpecialistDataModel).filters,
                selectedJob: null
            }, () => {
                this.updatePagination(this.state.pagination.currentPage)
            })
        })
    }

    /**
     * Go to the given index in the cube
     */
    private goToIndex = (index: number, callback?: () => void) => {
        this.setState({
            toIndex: index,

            fromIndex: this.state.viewIndex
        }, () => {
            setTimeout(() => {
                this.setState({
                    viewIndex: index,

                    toIndex: null,

                    fromIndex: null
                }, () => {
                    if (callback) {
                        callback()
                    }
                })
            }, JobsRoofSpecialistPage.animationTimeMS)
        })
    }

    /**
     * Update the pagination
     */
    private updatePagination = (pageNumber: number) => {
        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.job.getPaginatedJobs.loader)
        Backend.controllers.roofSpecialistJob.getPaginatedJobs(Math.max(1, pageNumber)).then((response) => {
            if (pageNumber > response.data.lastPage) {
                this.updatePagination(response.data.lastPage)
            }

            this.setState({
                pagination: response.data,
                doneLoading: true
            })

            window.history.pushState('', '', Route.ROOF_SPECIALIST_JOBS + '/' + pageNumber)
        })
    }
}
