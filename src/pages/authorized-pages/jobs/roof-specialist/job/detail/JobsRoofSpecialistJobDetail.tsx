import React from 'react'
import './jobs-roof-specialist-job-detail.scss'
import RoofSpecialistJobResponse from '../../../../../../backend/controller/roofspecialist/job/response/RoofSpecialistJobResponse'
import Tabs from '../../../../../../components/tabs/Tabs'
import Tab from '../../../../../../components/tabs/tab/Tab'
import Translations from '../../../../../../translations/Translations'
import AppInfo from '../../../../../../core/AppInfo'
import JobCosts from '../../../../../../core/JobCosts'
import { MeasurementUnit } from '../../../../../../backend/enumeration/MeasurementUnit'
import { CameraAlt, Chat } from '@material-ui/icons'
import Picture from '../../../../../../components/picture/Picture'
import SettingsButton from '../../../../../../components/settings-button/SettingsButton'
import Modal from '../../../../../../components/modal/Modal'
import JobSettingsRoofSpecialistModal from '../../../../../../components/modal/job-settings/roof-specialist/JobSettingsRoofSpecialistModal'
import IconCalendar from '../../../../../../components/icon/IconCalendar'
import moment from 'moment'
import IconLocation from '../../../../../../components/icon/IconLocation'
import ConsumerResponse from '../../../../../../backend/response/entity/ConsumerResponse'
import PictureModal from '../../../../../../components/modal/picture/PictureModal'
import Loader from '../../../../../../components/loader/Loader'
import Backend from '../../../../../../backend/Backend'
import ConnectionSuccessModal from '../../../../../../components/modal/connection-succes/ConnectionSuccessModal'
import PendingPaymentModal from '../../../../../../components/modal/pending-payment/PendingPaymentModal'
import Routes from '../../../../../../routes/Routes'
import { Route } from '../../../../../../enumeration/Route'

/**
 * The props
 */
interface JobsRoofSpecialistJobDetailProps {

    /**
     * The job
     */
    job: RoofSpecialistJobResponse
}

/**
 * The state
 */
interface JobsRoofSpecialistJobDetailState {

    /**
     * The index that is hovered
     */
    hoverIndex: number|null

    /**
     * The tab index
     */
    tabIndex: number
}

/**
 * The detail component of a job in the roof specialists job page.
 * 
 * @author Stan Hurks
 */
export default class JobsRoofSpecialistJobDetail extends React.Component<JobsRoofSpecialistJobDetailProps, JobsRoofSpecialistJobDetailState> {
    
    /**
     * The ref to the settings button.
     */
    private settingsButtonRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            hoverIndex: null,

            tabIndex: 0
        }
    }

    public render = () => {
        return (
            <div className="jobs-roof-specialist-job-detail">
                <div className="jobs-roof-specialist-job-detail-top">
                    <div className="jobs-roof-specialist-job-detail-top-left">
                        <div className="jobs-roof-specialist-job-detail-top-left-picture">
                            <Picture
                                image={this.props.job.images.length > 0 ? this.props.job.images[0].image : null}
                                size={124}
                                />
                        </div>
                    </div><div className="jobs-roof-specialist-job-detail-top-content">
                        <div className="jobs-roof-specialist-job-detail-top-content-title">
                            {
                                this.props.job.title
                            }
                        </div><div className="jobs-roof-specialist-job-detail-top-content-meta">
                            <div className="jobs-roof-specialist-job-detail-top-content-meta-left">
                                <IconCalendar />
                            </div><div className="jobs-roof-specialist-job-detail-top-content-meta-right">
                                {
                                    moment(this.props.job.createdAt).format('DD-MM-YYYY HH:mm')
                                }
                            </div>
                        </div><div className="jobs-roof-specialist-job-detail-top-content-meta">
                            <div className="jobs-roof-specialist-job-detail-top-content-meta-left">
                                <Chat />
                            </div><div className="jobs-roof-specialist-job-detail-top-content-meta-right">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.job.jobsRoofSpecialistJob.connectionCount(this.props.job.connectionCount)
                                }
                            </div>
                        </div><div className="jobs-roof-specialist-job-detail-top-content-meta">
                            <div className="jobs-roof-specialist-job-detail-top-content-meta-left">
                                <IconLocation />
                            </div><div className="jobs-roof-specialist-job-detail-top-content-meta-right">
                                {
                                    (this.props.job.user.consumer as ConsumerResponse).personalDetailsPostalCode.postalCode
                                }, {
                                    (this.props.job.user.consumer as ConsumerResponse).personalDetailsPostalCode.cityName
                                }
                            </div>
                        </div>
                    </div><div className="jobs-roof-specialist-job-detail-top-right" ref={this.settingsButtonRef}>
                        <SettingsButton onClick={() => {
                            if (!this.settingsButtonRef.current) {
                                return
                            }
                            Modal.mount.next({
                                element: (
                                    <JobSettingsRoofSpecialistModal job={this.props.job} />
                                ),
                                target: this.settingsButtonRef.current,
                                position: 'left',
                                offsetY: 120,
                                dismissable: true
                            })
                        }} />
                    </div>
                </div>

                <Tabs hoverIndex={this.state.hoverIndex} tabIndex={this.state.tabIndex}>
                    <Tab onHover={(isHovered) => {
                        this.setState({
                            hoverIndex: isHovered ? 0 : null
                        })
                    }} onClick={() => {
                        this.setState({
                            tabIndex: 0
                        })
                    }}>
                        {
                            Translations.translations.pages.jobs.roofSpecialist.job.detail.tabs.specifications
                        } 
                    </Tab>

                    <Tab onHover={(isHovered) => {
                        this.setState({
                            hoverIndex: isHovered ? 1 : null
                        })
                    }} onClick={() => {
                        this.setState({
                            tabIndex: 1
                        })
                    }}>
                        {
                            Translations.translations.pages.jobs.roofSpecialist.job.detail.tabs.pictures(this.props.job.images.length)
                        }
                    </Tab>

                    <Tab onHover={(isHovered) => {
                        this.setState({
                            hoverIndex: isHovered ? 2 : null
                        })
                    }} onClick={() => {
                        this.sendConnectionRequest()
                    }} color="white">
                        {
                            Translations.translations.pages.jobs.roofSpecialist.job.detail.tabs.comment
                        }
                    </Tab>
                </Tabs>

                <div className="jobs-roof-specialist-job-detail-content">
                    {
                        this.state.tabIndex === 0
                        ? this.renderSpecifications()
                        : this.renderPictures()
                    }
                </div>
            </div>
        )
    }

    /**
     * Render the specifications
     */
    private renderSpecifications = () => {
        return (
            <div className="jobs-roof-specialist-job-detail-content-specifications">
                <div className="jobs-roof-specialist-job-detail-content-specifications-top">
                    <div className="jobs-roof-specialist-job-detail-content-specifications-top-left">
                        <div className="jobs-roof-specialist-job-detail-content-specifications-top-left-title">
                            {
                                Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.top.left.title
                            }                            
                        </div><div className="jobs-roof-specialist-job-detail-content-specifications-top-left-content">
                            {
                                this.props.job.description === null || this.props.job.description.length === 0
                                ? Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.top.left.noDescription
                                : this.props.job.description
                            }
                        </div>
                    </div><div className="jobs-roof-specialist-job-detail-content-specifications-top-right">
                        <div className="jobs-roof-specialist-job-detail-content-specifications-top-right-title">
                            {
                                Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.top.right.title
                            }
                        </div><div className="jobs-roof-specialist-job-detail-content-specifications-top-right-subtitle">
                            {
                                Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.top.right.subtitle(JobCosts.calculateCosts(this.props.job), AppInfo.vatPercentage)
                            }
                        </div><div className="jobs-roof-specialist-job-detail-content-specifications-top-right-content">
                            {
                                Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.top.right.content
                            }
                        </div>
                    </div>
                </div><div className="jobs-roof-specialist-job-detail-content-specifications-hr">
                </div><div className="jobs-roof-specialist-job-detail-content-specifications-bottom">
                        {
                            this.getSpecifications().map((specification, specificationIndex) =>
                                <div className="jobs-roof-specialist-job-detail-content-specifications-bottom-specification" key={specificationIndex}> 
                                    <div className="jobs-roof-specialist-job-detail-content-specifications-bottom-specification-key">
                                        {
                                            specification.key
                                        }
                                    </div><div className="jobs-roof-specialist-job-detail-content-specifications-bottom-specification-value">
                                        {
                                            specification.value
                                        }
                                    </div>
                                </div>  
                            )
                        }
                </div>
            </div>
        )
    }

    /**
     * Render the job images
     */
    private renderPictures = () => {
        return (
            <div className="jobs-roof-specialist-job-detail-content-pictures">
                {
                    this.props.job.images.length > 0
                    ? this.props.job.images.map((image, imageIndex) =>
                        <div className="jobs-roof-specialist-job-detail-content-pictures-picture" key={imageIndex} onClick={() => {
                            Modal.mount.next({
                                element: (
                                    <PictureModal imgSrc={image.image.file.content} />
                                ),
                                dismissable: true
                            })
                        }}>
                            <Picture image={image.image} size={180} />
                        </div>
                    )
                    : (
                        <div className="jobs-roof-specialist-job-detail-content-pictures-no-picture">
                            <div className="jobs-roof-specialist-job-detail-content-pictures-no-picture-top">
                                <CameraAlt />
                            </div><div className="jobs-roof-specialist-job-detail-content-pictures-no-picture-title">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.job.detail.content.photos.noPhotos.title
                                }
                            </div><div className="jobs-roof-specialist-job-detail-content-pictures-no-picture-description">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.job.detail.content.photos.noPhotos.description
                                }
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }

    /**
     * Send the connection request to the consumer
     */
    private sendConnectionRequest = () => {
        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.job.sendConnectionRequestToConsumer.loader)
        Backend.controllers.roofSpecialistJob.sendConnectionRequestToConsumer(this.props.job.id).then((response) => {
            Modal.mount.next({
                element: (
                    <ConnectionSuccessModal job={this.props.job} />
                ),
                dismissable: false
            })
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.roofSpecialist.job.sendConnectionRequestToConsumer.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
            if (response.status === 402) {
                Loader.set.next(Translations.translations.backend.controllers.payment.getPaymentLinkForReversedPayments.loader)
                Backend.controllers.payment.getPaymentLinkForReversedPayments().then((response) => {
                    Modal.mount.next({
                        element: (
                            <PendingPaymentModal amount={response.data.amount} mollieLink={response.data.mollieLink} />
                        ),
                        dismissable: false
                    })
                }).catch((response) => {
                    const translation = Translations.translations.backend.controllers.payment.getPaymentLinkForReversedPayments.responses[response.status]
                    if (response.status === 400) {

                        // Recurse when the payment has been processed already while this
                        // request took place, even though the odds are one in a million.
                        this.sendConnectionRequest()
                    } else if (translation !== undefined) {
                        Modal.mountErrorHistory.next(translation)
                    }
                })
            } else if (response.status === 409) {
                Routes.redirectURL.next(Route.PROFILE)
            }
        })
    }

    /**
     * Get the specifications
     */
    private getSpecifications = (): Array<{key: string, value: string}> => {
        const specifications: Array<{key: string, value: string}> = []
        if (this.props.job.branche) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.branche,
                value: Translations.translations.backend.enumerations.branche(this.props.job.branche)
            })
        }
        if (this.props.job.surfaceMeasurementsUnit === MeasurementUnit.SQUARE_METER) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.surfaceMeasurements[MeasurementUnit.SQUARE_METER],
                value: (this.props.job.surfaceMeasurements as number).toFixed(0)
            })
        }
        if (this.props.job.surfaceMeasurementsUnit === MeasurementUnit.METER) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.surfaceMeasurements[MeasurementUnit.METER],
                value: (this.props.job.surfaceMeasurements as number).toFixed(0)
            })
        }
        if (this.props.job.roofType) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.roofType,
                value: Translations.translations.backend.enumerations.roofType(this.props.job.roofType)
            })
        }
        if (this.props.job.jobCategory) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.jobCategory,
                value: Translations.translations.backend.enumerations.jobCategory(this.props.job.jobCategory)
            })
        }
        if (this.props.job.houseType) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.houseType,
                value: Translations.translations.backend.enumerations.houseType(this.props.job.houseType)
            })
        }
        if (this.props.job.roofMaterialType) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.roofMaterialType,
                value: Translations.translations.backend.enumerations.roofMaterialType(this.props.job.roofMaterialType)
            })
        }
        if (this.props.job.roofIsolationType) {
            specifications.push({
                key: Translations.translations.pages.jobs.roofSpecialist.job.detail.content.specifications.bottom.roofIsolationType,
                value: Translations.translations.backend.enumerations.roofIsolationType(this.props.job.roofIsolationType)
            })
        }
        return specifications
    }
}