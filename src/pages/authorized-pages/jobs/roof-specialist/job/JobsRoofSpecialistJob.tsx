import React from 'react'
import RoofSpecialistJobResponse from '../../../../../backend/controller/roofspecialist/job/response/RoofSpecialistJobResponse'
import './jobs-roof-specialist-job.scss'
import Picture from '../../../../../components/picture/Picture'
import IconCalendar from '../../../../../components/icon/IconCalendar'
import moment from 'moment'
import Translations from '../../../../../translations/Translations'
import { Chat } from '@material-ui/icons'
import IconLocation from '../../../../../components/icon/IconLocation';
import ConsumerResponse from '../../../../../backend/response/entity/ConsumerResponse';

/**
 * The props
 */
interface JobsRoofSpecialistJobProps {

    /**
     * The job
     */
    job: RoofSpecialistJobResponse

    /**
     * Callback for when the job is selected
     */
    onSelect: () => void
}

/**
 * A job in the roof specialist jobs page.
 * 
 * @author Stan Hurks
 */
export default class JobsRoofSpecialistJob extends React.Component<JobsRoofSpecialistJobProps> {

    public render = () => {
        return (
            <div className="jobs-roof-specialist-job" onClick={() => {
                this.props.onSelect()
            }}>
                <div className="jobs-roof-specialist-job-image">
                    <Picture image={
                        this.props.job.images.length > 0
                        ?
                        this.props.job.images[0].image
                        :
                        null
                    } size={124} />
                </div>
                <div className="jobs-roof-specialist-job-content">
                    <div className="jobs-roof-specialist-job-content-top">
                        <div className="jobs-roof-specialist-job-content-top-meta">
                            <div className="jobs-roof-specialist-job-content-top-meta-icon">
                                <IconCalendar />
                            </div><div className="jobs-roof-specialist-job-content-top-meta-label">
                                {
                                    moment(this.props.job.createdAt).format('DD-MM-YYYY HH:mm')
                                }
                            </div>
                        </div>
                        <div className="jobs-roof-specialist-job-content-top-meta">
                            <div className="jobs-roof-specialist-job-content-top-meta-icon">
                                <Chat />
                            </div><div className="jobs-roof-specialist-job-content-top-meta-label">
                                {
                                    Translations.translations.pages.jobs.roofSpecialist.job.jobsRoofSpecialistJob.connectionCount(
                                        this.props.job.connectionCount
                                    )
                                }
                            </div>
                        </div>
                        <div className="jobs-roof-specialist-job-content-top-meta">
                            <div className="jobs-roof-specialist-job-content-top-meta-icon">
                                <IconLocation />
                            </div><div className="jobs-roof-specialist-job-content-top-meta-label">
                                {
                                    (this.props.job.user.consumer as ConsumerResponse).personalDetailsPostalCode.postalCode
                                }, {
                                    (this.props.job.user.consumer as ConsumerResponse).personalDetailsPostalCode.cityName
                                }
                            </div>
                        </div>
                    </div>
                    <h1 className="jobs-roof-specialist-job-content-title">
                        {
                            this.props.job.title
                        }
                    </h1>
                    <div className="jobs-roof-specialist-job-content-description">
                        {
                            this.props.job.description && this.props.job.description.length
                            ? this.props.job.description
                            : Translations.translations.pages.jobs.roofSpecialist.job.jobsRoofSpecialistJob.noDescription
                        }
                    </div>
                </div>
            </div>
        )
    }
}