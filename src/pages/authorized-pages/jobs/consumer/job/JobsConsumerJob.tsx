import React from 'react'
import './jobs-consumer-job.scss'
import Picture from '../../../../../components/picture/Picture'
import IconCalendar from '../../../../../components/icon/IconCalendar'
import { Chat } from '@material-ui/icons'
import moment from 'moment'
import ConsumerJobResponse from '../../../../../backend/controller/consumer/job/response/ConsumerJobResponse'
import Translations from '../../../../../translations/Translations'
import SettingsButton from '../../../../../components/settings-button/SettingsButton'
import Modal from '../../../../../components/modal/Modal'
import JobSettingsConsumerModal from '../../../../../components/modal/job-settings/consumer/JobSettingsConsumerModal'

/**
 * The props
 */
interface JobsConsumerJobProps {

    /**
     * The job
     */
    job: ConsumerJobResponse

    /**
     * Whenever the jobs deleted
     */
    onDeleteJob: () => void
}

/**
 * A job in the jobs page for consumers.
 * 
 * @author Stan Hurks
 */
export default class JobsConsumerJob extends React.Component<JobsConsumerJobProps> {

    /**
     * The ref to the settings button.
     */
    private settingsButtonRef: React.RefObject<HTMLDivElement> = React.createRef()

    public render = () => {
        return (
            <div className="jobs-consumer-job">
                <div className="jobs-consumer-job-image">
                    <Picture image={
                        this.props.job.images.length > 0
                        ?
                        this.props.job.images[0].image
                        :
                        null
                    } size={124} />
                </div>
                <div className="jobs-consumer-job-content">
                    <div className="jobs-consumer-job-content-top">
                        <div className="jobs-consumer-job-content-top-meta">
                            <div className="jobs-consumer-job-content-top-meta-icon">
                                <IconCalendar />
                            </div><div className="jobs-consumer-job-content-top-meta-label">
                                {
                                    moment(this.props.job.createdAt).format('DD-MM-YYYY HH:mm')
                                }
                            </div>
                        </div>
                        <div className="jobs-consumer-job-content-top-meta">
                            <div className="jobs-consumer-job-content-top-meta-icon">
                                <Chat />
                            </div><div className="jobs-consumer-job-content-top-meta-label">
                                {
                                    Translations.translations.pages.jobs.consumer.job.jobsConsumerJob.connectionCount(
                                        this.props.job.connectionCount
                                    )
                                }
                            </div>
                        </div>
                    </div>
                    <h1 className="jobs-consumer-job-content-title">
                        {
                            this.props.job.title
                        }
                    </h1>
                    <div className="jobs-consumer-job-content-description">
                        {
                            this.props.job.description && this.props.job.description.length
                            ? this.props.job.description
                            : Translations.translations.pages.jobs.consumer.job.jobsConsumerJob.noDescription
                        }
                    </div>
                </div>
                <div className="jobs-consumer-job-settings-button" ref={this.settingsButtonRef}>
                    <SettingsButton onClick={() => {
                        if (!this.settingsButtonRef.current) {
                            return
                        }
                        Modal.mount.next({
                            element: (
                                <JobSettingsConsumerModal job={this.props.job} onDeleteJob={() => {
                                    this.props.onDeleteJob()
                                }} />
                            ),
                            target: this.settingsButtonRef.current,
                            position: 'left',
                            offsetY: 120,
                            dismissable: true
                        })
                    }} />
                </div>
            </div>
        )
    }
}