import React from 'react'
import Hero from '../../../../components/hero/Hero'
import './jobs-consumer-page.scss'
import Wizard from '../../../../components/wizard/Wizard'
import WizardTop from '../../../../components/wizard/top/WizardTop'
import Badge from '../../../../components/badge/Badge'
import { RouteComponentProps } from 'react-router'
import Pagination from '../../../../backend/pagination/Pagination'
import FormInput from '../../../../components/form/input/FormInput'
import Translations from '../../../../translations/Translations'
import Loader from '../../../../components/loader/Loader'
import Backend from '../../../../backend/Backend'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import PaginationBar from '../../../../components/pagination/bar/PaginationBar'
import JobsConsumerJob from './job/JobsConsumerJob'
import Footer from '../../../../components/footer/Footer'
import { Route } from '../../../../enumeration/Route'
import Breadcrumbs from '../../../../components/breadcrumbs/Breadcrumbs'
import ProgressSpinner from '../../../../components/progress-spinner/ProgressSpinner'
import Routes from '../../../../routes/Routes'
import ConsumerJobResponse from '../../../../backend/controller/consumer/job/response/ConsumerJobResponse'
import Button from '../../../../components/button/Button'
import IconAlternateEmail from '../../../../components/icon/IconAlternateEmail'
import Modal from '../../../../components/modal/Modal'

/**
 * The state
 */
interface JobsConsumerPageState {

    /**
     * Whether the page is done loading the jobs.
     */
    doneLoading: boolean

    /**
     * Pagination data
     */
    pagination: Pagination<ConsumerJobResponse>

    /**
     * The filter
     */
    filter: string

    /**
     * The initial filter
     */
    initialFilter: string
}

/**
 * The jobs page for the consumer.
 * 
 * @author Stan Hurks
 */
export default class JobsConsumerPage extends React.Component<RouteComponentProps, JobsConsumerPageState> {

    constructor(props: any) {
        super(props)
         
        this.state = {
            doneLoading: false,
            
            pagination: {
                entries: [],

                currentPage: 1,

                lastPage: 1,

                total: 0
            },

            filter: '',

            initialFilter: ''
        }
    }

    public componentDidMount = () => {
        const filter = this.props.match.params['filter'] || ''
        const pageNumber = Number(this.props.match.params['pageNumber'] || 1)
    
        this.setState({
            filter,
            initialFilter: filter
        }, () => {
            this.updatePagination(pageNumber)

            setTimeout(() => {
                Routes.setCurrentRoute.next(Route.CONSUMER_JOBS)
            })
        })
    }

    public render = () => {
        return (
            <div id="jobs-consumer-page">
                <Hero id="jobs-consumer-page-hero" absolute height={400} />

                <Wizard>
                    <WizardTop>
                        <div id="jobs-consumer-page-wizard-top-title">
                            <h1 id="jobs-consumer-page-wizard-top-title-label">
                                {
                                    Translations.translations.pages.jobs.consumer.wizard.top.title
                                }
                            </h1>

                            <div id="jobs-consumer-page-wizard-top-title-badge">
                                {
                                    this.state.doneLoading
                                    &&
                                    <Badge value={this.state.pagination.total} />
                                }
                            </div>
                        </div><div id="jobs-consumer-page-wizard-top-description">
                            {
                                Translations.translations.pages.jobs.consumer.wizard.top.description
                            }
                        </div><div id="jobs-consumer-page-wizard-top-filter">
                            <FormInput
                                type="filter"
                                value={this.state.filter}
                                onChange={(filter) => {
                                    this.setState({
                                        filter
                                    })
                                }}
                                placeholder={Translations.translations.components.form.input.filter.placeholder}
                                onChangeFilter={() => {
                                    setTimeout(() => {
                                        if (this.state.filter === this.state.initialFilter) {
                                            return
                                        }
                                        
                                        this.updatePagination(1)
                                    })
                                }}
                                />
                        </div>
                    </WizardTop>
                    <WizardContent>
                        {
                            this.state.doneLoading
                            &&
                            <div id="jobs-consumer-page-wizard-content">
                                <div id="jobs-consumer-page-wizard-content-pagination-bar">
                                    <PaginationBar pagination={this.state.pagination} onChangePage={(pageNumber) => {
                                        this.setState({
                                            pagination: {
                                                ...this.state.pagination,
                                                currentPage: pageNumber
                                            }
                                        }, () => {
                                            this.updatePagination(pageNumber)
                                        })
                                    }} />
                                </div>
                                {
                                    this.state.pagination.entries.length > 0
                                    ?
                                    <div id="jobs-consumer-page-wizard-content-entries">
                                        {
                                            this.state.pagination.entries.map((job, jobIndex) => 
                                                <JobsConsumerJob job={job} key={jobIndex} onDeleteJob={() => {
                                                    Modal.dismiss.next('all')
                                                    this.updatePagination(this.state.pagination.currentPage)
                                                }} />
                                            )
                                        }
                                    </div>
                                    :
                                    <div id="jobs-consumer-page-wizard-content-entries-no-entries">
                                        <div id="jobs-consumer-page-wizard-content-entries-no-entries-icon">
                                            <IconAlternateEmail />
                                        </div><div id="jobs-consumer-page-wizard-content-entries-no-entries-title">
                                            {
                                                Translations.translations.pages.jobs.consumer.wizard.content.entries.noEntries.title
                                            }
                                        </div><div id="jobs-consumer-page-wizard-content-entries-no-entries-description">
                                            {
                                                Translations.translations.pages.jobs.consumer.wizard.content.entries.noEntries.description
                                            }
                                        </div><div id="jobs-consumer-page-wizard-content-entries-no-entries-button">
                                            <Button color="orange" size="small" href={Route.CONSUMER_PLACE_YOUR_JOB}>
                                                {
                                                    Translations.translations.pages.jobs.consumer.wizard.content.entries.noEntries.buttons
                                                }                                                
                                            </Button>
                                        </div>
                                    </div>
                                }
                            </div>
                        }

                        {
                            !this.state.doneLoading
                            &&
                            <div id="jobs-consumer-page-wizard-content-loading">
                                <ProgressSpinner />
                            </div>
                        }
                    </WizardContent>
                </Wizard>

                <Breadcrumbs path={[
                    Route.HOME,
                    Route.CONSUMER_JOBS
                ]}>

                </Breadcrumbs>

                <Footer />
            </div>
        )
    }

    /**
     * Updates the pagination
     */
    private updatePagination = (pageNumber: number) => {
        this.setState({
            doneLoading: false
        }, () => {
            Loader.set.next(Translations.translations.backend.controllers.consumer.job.getPaginatedJobs.loader)
            Backend.controllers.consumerJob.getPaginatedJobs(Math.max(1, pageNumber), this.state.filter || '').then((response) => {
                if (pageNumber > response.data.lastPage) {
                    this.updatePagination(response.data.lastPage)
                }
                
                this.setState({
                    pagination: response.data,
                    doneLoading: true,
                    initialFilter: this.state.filter
                })
                
                window.history.pushState('', '', Route.CONSUMER_JOBS + '/' + Math.max(1, pageNumber) + (this.state.filter && this.state.filter.length > 0 ? '/' + this.state.filter : ''))
            })
        })
    }
}