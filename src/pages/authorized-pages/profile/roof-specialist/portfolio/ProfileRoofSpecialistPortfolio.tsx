import React from 'react'
import AuthorizedUserResponse from '../../../../../backend/response/entity/AuthorizedUserResponse'
import { ProfileRoofSpecialistRoute } from '../ProfileRoofSpecialistPage'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import Button from '../../../../../components/button/Button'
import './profile-roof-specialist-portfolio.scss'
import ProfileRoofSpecialistPortfolioImage from './image/ProfileRoofSpecialistPortfolioImage'

/**
 * The props
 */
interface ProfileRoofSpecialistPortfolioProps {

    /**
     * The user
     */
    user: AuthorizedUserResponse

    /**
     * Checks whether a tab is disabled.
     */
    isTabDisabled: (route: ProfileRoofSpecialistRoute) => boolean

    /**
     * Navigate to the given route if possible
     */
    navigate: (route: ProfileRoofSpecialistRoute) => void

    /**
     * Whenever an update occurs in the portfolio image.
     */
    onChangePortfolioImage: () => void
}

/**
 * The portfolio tab in the roof specialist profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistPortfolio extends React.Component<ProfileRoofSpecialistPortfolioProps> {

    public render = () => {
        return (
            <div className="profile-roof-specialist-portfolio">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.profile.roofSpecialist.wizard.intro.portfolio.title}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.intro.portfolio.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {
                        this.renderPortfolioImages()
                    }

                    <WizardContentOutro title={Translations.translations.pages.profile.roofSpecialist.wizard.outro.portfolio.title}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.outro.portfolio.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.PROFILE,
                    Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>

                    <Button color="orange" disabled={this.props.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)} onClick={() => {
                        this.props.navigate(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    private renderPortfolioImages = () => {
        return (
            <div className="profile-roof-specialist-portfolio-images">
                <h1 className="profile-roof-specialist-portfolio-images-label">
                    {
                        Translations.translations.pages.profile.roofSpecialist.wizard.content.portfolio.title
                    }
                </h1><div className="profile-roof-specialist-portfolio-images-description">
                    {
                        Translations.translations.pages.profile.roofSpecialist.wizard.content.portfolio.description(
                            this.props.user.roofSpecialist
                            ? this.props.user.roofSpecialist.portfolioImages.length
                            : 0
                        )
                    }
                </div><div className="profile-roof-specialist-portfolio-images-entries">
                    {
                        this.props.user.roofSpecialist
                        &&
                        this.props.user.roofSpecialist.portfolioImages.map((portfolioImage, portfolioImageIndex) =>
                            <ProfileRoofSpecialistPortfolioImage
                                portfolioImage={portfolioImage}
                                key={portfolioImageIndex}
                                onNewPortfolioImage={() => {
                                    this.updatePortfolioImages()
                                }}
                                onChangePortfolioImage={() => {
                                    this.updatePortfolioImages()
                                }}
                                />
                        )
                    }
                    {
                        this.props.user.roofSpecialist
                        &&
                        this.props.user.roofSpecialist.portfolioImages.length < 5
                        &&
                        <ProfileRoofSpecialistPortfolioImage
                            onNewPortfolioImage={(portfolioImage) => {
                                this.updatePortfolioImages()
                            }}
                            onChangePortfolioImage={() => {
                                this.updatePortfolioImages()
                            }}
                            />
                    }
                </div>
            </div>
        )
    }

    /**
     * Update the portfolio images
     */
    private updatePortfolioImages = () => {
        this.props.onChangePortfolioImage()
    }
}