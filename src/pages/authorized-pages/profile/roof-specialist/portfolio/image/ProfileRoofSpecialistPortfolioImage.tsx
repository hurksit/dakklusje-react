import React from 'react'
import './profile-roof-specialist-portfolio-image.scss'
import PortfolioImageResponse from '../../../../../../backend/response/entity/PortfolioImageResponse'
import { CameraAlt } from '@material-ui/icons'
import Translations from '../../../../../../translations/Translations'
import Picture from '../../../../../../components/picture/Picture'
import './profile-roof-specialist-portfolio-image.scss'
import { Subscription } from 'rxjs'
import ImageUpload from '../../../../../../components/image-upload/ImageUpload'
import Loader from '../../../../../../components/loader/Loader'
import Backend from '../../../../../../backend/Backend'
import Modal from '../../../../../../components/modal/Modal'
import EditPortfolioImageModal from '../../../../../../components/modal/edit-portfolio-image/EditPortfolioImageModal'

/**
 * The props
 */
interface ProfileRoofSpecialistPortfolioImageProps {

    /**
     * The portfolio image
     */
    portfolioImage?: PortfolioImageResponse

    /**
     * Whenever a new portfolio image has been uploaded or an existing one has been edited.
     */
    onNewPortfolioImage: (portfolioImage: PortfolioImageResponse) => void

    /**
     * Whenever an update occurs in the portfolio image.
     */
    onChangePortfolioImage?: () => void
}

/**
 * The state
 */
interface ProfileRoofSpecialistPortfolioImageState {

    /**
     * Whether or not a new image has been requested from this picture.
     */
    newImageRequestedFromThisPicture: boolean
}

/**
 * An image in the portfolio tab in the roof specialist profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistPortfolioImage extends React.Component<ProfileRoofSpecialistPortfolioImageProps, ProfileRoofSpecialistPortfolioImageState> {

    /**
     * The reference to the element
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * Whenever a new image upload has been completed.
     */
    private subscriptionNewImageBase64!: Subscription

    /**
     * Whenever a new image upload has been cancelled or errored.
     */
    private subscriptionUploadFailed!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            newImageRequestedFromThisPicture: false
        }
    }

    public componentDidMount = () => {
        this.subscriptionNewImageBase64 = ImageUpload.newImageBase64.subscribe(this.onNewImageBase64)
        this.subscriptionUploadFailed = ImageUpload.uploadFailed.subscribe(this.onUploadFailed)
    }

    public componentWillUnmount = () => {
        this.subscriptionNewImageBase64.unsubscribe()
        this.subscriptionUploadFailed.unsubscribe()
    }

    public render = () => {
        return (
            <div className="profile-roof-specialist-portfolio-image" ref={this.ref} onClick={() => {
                if (!this.ref.current) {
                    return
                }

                if (this.props.portfolioImage) {
                    Modal.mount.next({
                        element: (
                            <EditPortfolioImageModal
                                portfolioImage={this.props.portfolioImage}
                                onNewImageRequest={() => {
                                    this.setState({
                                        newImageRequestedFromThisPicture: true
                                    })
                                }}
                                onChangePortfolioImage={() => {
                                    if (!this.props.onChangePortfolioImage) {
                                        return
                                    }
                                    this.props.onChangePortfolioImage()

                                    Modal.dismiss.next()
                                }}
                                />
                        ),
                        target: this.ref.current,
                        position: this.ref.current.getBoundingClientRect().right > window.innerWidth / 2 ? 'left' : 'right',
                        offsetY: 120,
                        dismissable: true
                    })
                } else {
                    this.setState({
                        newImageRequestedFromThisPicture: true
                    })
                    ImageUpload.prompt.next()
                }
            }}>
                <div className="profile-roof-specialist-portfolio-image-container">
                    {
                        this.props.portfolioImage
                        ? this.renderWithImage()
                        : this.renderWithoutImage()
                    }
                </div>
            </div>
        )
    }

    /**
     * Render the component without an image
     */
    private renderWithoutImage = () => {
        return (
            <div className="profile-roof-specialist-portfolio-image-no-image">
                <div className="profile-roof-specialist-portfolio-image-no-image-content">
                    <div className="profile-roof-specialist-portfolio-image-no-image-content-top">
                        <CameraAlt />
                    </div><div className="profile-roof-specialist-portfolio-image-no-image-content-label">
                        {
                            Translations.translations.pages.addJob.wizard.content.pictures.pictures.newPicture.label
                        }
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Render the component with an image
     */
    private renderWithImage = () => {
        return (
            <div className="profile-roof-specialist-portfolio-image-image">
                <Picture image={(this.props.portfolioImage as PortfolioImageResponse).image} size={180} />
            </div>
        )
    }

    /**
     * Whenever a new image is available
     */
    private onNewImageBase64 = (base64Content: string) => {
        if (!this.state.newImageRequestedFromThisPicture) {
            return
        }

        if (this.props.portfolioImage) {
            this.editPortfolioImageContent(base64Content, this.props.portfolioImage)
        } else {
            this.addPortfolioImage(base64Content)
        }

        this.setState({
            newImageRequestedFromThisPicture: false
        })
    }

    /**
     * Whenever the upload failed
     */
    private onUploadFailed = () => {
        if (this.state.newImageRequestedFromThisPicture) {
            this.setState({
                newImageRequestedFromThisPicture: false
            })
        }
    }

    /**
     * Edit the content of an existing portfolio image.
     */
    private editPortfolioImageContent = (base64Content: string, portfolioImage: PortfolioImageResponse) => {
        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.portfolio.editPortfolioImageContent.loader)
        Backend.controllers.roofSpecialistPortfolio.editPortfolioImageContent(portfolioImage.id, {
            content: base64Content
        }).then((response) => {
            this.props.onNewPortfolioImage(response.data)

            Modal.dismiss.next('all')
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.roofSpecialist.portfolio.editPortfolioImageContent.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
    
    /**
     * Add a new portfolio image.
     */
    private addPortfolioImage = (base64Content: string) => {
        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.portfolio.addPortfolioImage.loader)
        Backend.controllers.roofSpecialistPortfolio.addPortfolioImage({
            content: base64Content
        }).then((response) => {
            this.props.onNewPortfolioImage(response.data)
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.roofSpecialist.portfolio.addPortfolioImage.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}