import React from 'react'
import AuthorizedUserResponse from '../../../../../backend/response/entity/AuthorizedUserResponse'
import { ProfileRoofSpecialistRoute } from '../ProfileRoofSpecialistPage'
import ReviewResponse from '../../../../../backend/response/entity/ReviewResponse'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import Translations from '../../../../../translations/Translations'
import Button from '../../../../../components/button/Button'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import ProfileRoofSpecialistReviewsReview from './review/ProfileRoofSpecialistReviewsReview'
import ProgressSpinner from '../../../../../components/progress-spinner/ProgressSpinner'
import Loader from '../../../../../components/loader/Loader'
import Backend from '../../../../../backend/Backend'
import Pagination from '../../../../../backend/pagination/Pagination'
import './profile-roof-specialist-reviews.scss'
import PaginationBar from '../../../../../components/pagination/bar/PaginationBar'

/**
 * The props
 */
interface ProfileRoofSpecialistReviewsProps {

    /**
     * The user
     */
    user: AuthorizedUserResponse

    /**
     * Checks whether a tab is disabled.
     */
    isTabDisabled: (route: ProfileRoofSpecialistRoute) => boolean

    /**
     * Navigate to the given route if possible
     */
    navigate: (route: ProfileRoofSpecialistRoute) => void
}

/**
 * The state
 */
interface ProfileRoofSpecialistReviewsState {

    /**
     * The pagination
     */
    pagination: Pagination<ReviewResponse>

    /**
     * Whether or not the tab is done loading the reviews
     */
    doneLoading: boolean
}

/**
 * The reviews tab in the roof specialist profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistReviews extends React.Component<ProfileRoofSpecialistReviewsProps, ProfileRoofSpecialistReviewsState> {

    constructor(props: any) {
        super(props)

        this.state = {
            pagination: {
                entries: [],

                currentPage: 1,

                lastPage: 1,

                total: 0
            },

            doneLoading: false
        }
    }

    public componentDidMount = () => {
        this.updateReviews(1)
    }

    public render = () => {
        return (
            <div className="profile-roof-specialist-reviews">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.profile.roofSpecialist.wizard.intro.reviews.title} badge={this.state.pagination.total}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.intro.reviews[
                                this.state.pagination.total > 0
                                ? 'descriptionProfileComplete'
                                : 'description'
                            ].map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {
                        this.state.doneLoading
                        &&
                        <PaginationBar
                            pagination={this.state.pagination}
                            onChangePage={(pageNumber) => {
                                this.updateReviews(pageNumber)
                            }}
                            />
                    }

                    {
                        this.state.doneLoading
                        ?
                        this.renderReviews()
                        :
                        (
                            <ProgressSpinner />
                        )
                    }

                    <WizardContentOutro title={Translations.translations.pages.profile.roofSpecialist.wizard.outro.reviews.title}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.outro.reviews.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.PROFILE,
                    Route.PROFILE_ROOF_SPECIALIST_REVIEWS
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>

                    <Button color="orange" disabled={this.props.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)} onClick={() => {
                        this.props.navigate(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Render the reviews
     */
    private renderReviews = () => {
        return (
            <div className="profile-roof-specialist-reviews-reviews">
                {
                    this.state.pagination.total === 0
                    ?
                    <div className="profile-roof-specialist-reviews-reviews-content">
                        {
                            [1].map((review, reviewIndex) =>
                                <ProfileRoofSpecialistReviewsReview review={{
                                    id: '',
                                    consumerUserId: '',
                                    title: Translations.translations.pages.profile.roofSpecialist.wizard.content.reviews.example.title,
                                    rating: 5,
                                    jobCategory: Translations.translations.pages.profile.roofSpecialist.wizard.content.reviews.example.category as any,
                                    description: Translations.translations.pages.profile.roofSpecialist.wizard.content.reviews.example.description,
                                    image: null,
                                    consumerPersonalDetailsFullName: Translations.translations.pages.profile.roofSpecialist.wizard.content.reviews.example.consumerName,
                                    createdAt: new Date().getTime()
                                }} dummyData={true} key={reviewIndex} />
                            )
                        }
                    </div>
                    :
                    <div className="profile-roof-specialist-reviews-reviews-content">
                        {
                            this.state.pagination.entries.map((review, reviewIndex) =>
                                <ProfileRoofSpecialistReviewsReview review={review} key={reviewIndex} />
                            )
                        }
                    </div>
                }
            </div>
        )
    }

    /**
     * Update the reviews
     */
    private updateReviews = (pageNumber: number) => {
        this.setState({
            doneLoading: false
        })

        Loader.set.next(Translations.translations.backend.controllers.review.getPaginatedReviews.loader)
        Backend.controllers.review.getPaginatedReviews(pageNumber).then((response) => {
            this.setState({
                pagination: response.data,
                doneLoading: true
            })
        })
    }
}