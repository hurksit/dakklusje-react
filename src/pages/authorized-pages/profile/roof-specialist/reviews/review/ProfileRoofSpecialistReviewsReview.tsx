import React from 'react'
import ReviewResponse from '../../../../../../backend/response/entity/ReviewResponse'
import SettingsButton from '../../../../../../components/settings-button/SettingsButton'
import { Person } from '@material-ui/icons'
import IconCalendar from '../../../../../../components/icon/IconCalendar'
import moment from 'moment'
import './profile-roof-specialist-reviews-review.scss'
import Rating from '../../../../../../components/rating/Rating'
import Translations from '../../../../../../translations/Translations'
import Modal from '../../../../../../components/modal/Modal'
import ReviewSettingsModal from '../../../../../../components/modal/review-settings/ReviewSettingsModal'

/**
 * The props
 */
interface ProfileRoofSpecialistReviewsReviewProps {

    /**
     * The review or null when dummy data should be used.
     */
    review: ReviewResponse

    /**
     * Whether or not dummy data is used
     */
    dummyData?: boolean
}

/**
 * A review in the reviews tab of the roof specialist profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistReviewsReview extends React.Component<ProfileRoofSpecialistReviewsReviewProps> {

    /**
     * The reference to the settings button container
     */
    private settingsButtonContainerRef: React.RefObject<HTMLDivElement> = React.createRef()

    public render = () => {
        return (
            <div className="profile-roof-specialist-reviews-review">
                <div className="profile-roof-specialist-reviews-review-top">
                    <div className="profile-roof-specialist-reviews-review-top-left">
                        <div className="profile-roof-specialist-reviews-review-top-left-top">
                            <div className="profile-roof-specialist-reviews-review-top-left-top-title">
                                {
                                    this.props.review.title
                                }
                            </div><div className="profile-roof-specialist-reviews-review-top-left-top-category">
                                {
                                    Translations.translations.backend.enumerations.jobCategory(this.props.review.jobCategory)
                                }
                            </div>
                        </div>
                    </div><div className="profile-roof-specialist-reviews-review-top-right">
                        <div className="settings-button-container" ref={this.settingsButtonContainerRef}>
                            <SettingsButton
                                onClick={() => {
                                    if (!this.settingsButtonContainerRef.current) {
                                        return
                                    }
                                    Modal.mount.next({
                                        element: (
                                            <ReviewSettingsModal review={this.props.review} />
                                        ),
                                        target: this.settingsButtonContainerRef.current,
                                        position: this.settingsButtonContainerRef.current.getBoundingClientRect().right >= window.innerWidth / 2
                                            ? 'left'
                                            : 'right',
                                        offsetY: 120,
                                        dismissable: false
                                    })
                                }}
                                disabled={this.props.dummyData}
                                />
                        </div>
                    </div>
                </div><div className="profile-roof-specialist-reviews-review-rating">
                    <Rating value={5} />
                </div>
                <div className="profile-roof-specialist-reviews-review-metas">
                    <div className="profile-roof-specialist-reviews-review-meta">
                        <div className="profile-roof-specialist-reviews-review-meta-left">
                            <IconCalendar />
                        </div><div className="profile-roof-specialist-reviews-review-meta-right">
                            {
                                moment(this.props.review.createdAt).format('DD-MM-YYYY HH:mm')
                            }
                        </div>
                    </div><div className="profile-roof-specialist-reviews-review-meta">
                        <div className="profile-roof-specialist-reviews-review-meta-left">
                            <Person />
                        </div><div className="profile-roof-specialist-reviews-review-meta-right">
                            {
                                this.props.review.consumerPersonalDetailsFullName
                            }
                        </div>
                    </div>
                </div><div className="profile-roof-specialist-reviews-review-description">
                    {
                        this.props.review.description
                    }
                </div>
            </div>
        )
    }
}