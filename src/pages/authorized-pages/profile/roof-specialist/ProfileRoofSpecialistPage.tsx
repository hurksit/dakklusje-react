import React from 'react'
import Hero from '../../../../components/hero/Hero'
import Wizard from '../../../../components/wizard/Wizard'
import WizardTop from '../../../../components/wizard/top/WizardTop'
import WizardTabs from '../../../../components/wizard/tabs/WizardTabs'
import WizardTab from '../../../../components/wizard/tabs/tab/WizardTab'
import Footer from '../../../../components/footer/Footer'
import { Route } from '../../../../enumeration/Route'
import Routes from '../../../../routes/Routes'
import Window from '../../../../shared/Window'
import Translations from '../../../../translations/Translations'
import './profile-roof-specialist-page.scss'
import AuthorizedUserResponse from '../../../../backend/response/entity/AuthorizedUserResponse'
import ProgressSpinner from '../../../../components/progress-spinner/ProgressSpinner'
import ProfileRoofSpecialistCompanyDetails from './company-details/ProfileRoofSpecialistCompanyDetails'
import ProfileRoofSpecialistPortfolio from './portfolio/ProfileRoofSpecialistPortfolio'
import ProfileRoofSpecialistReviews from './reviews/ProfileRoofSpecialistReviews'
import ProfileRoofSpecialistPaymentDetails from './payment-details/ProfileRoofSpecialistPaymentDetails'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import Loader from '../../../../components/loader/Loader'
import Backend from '../../../../backend/Backend'
import { Subscription } from 'rxjs'
import App from '../../../../core/app/App'

/**
 * All routes for the roof specialist profile
 */
export type ProfileRoofSpecialistRoute = Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS
    | Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO
    | Route.PROFILE_ROOF_SPECIALIST_REVIEWS
    | Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS

/**
 * The props
 */
interface ProfileRoofSpecialistPageProps {

    /**
     * The route of the active tab
     */
    route: ProfileRoofSpecialistRoute
}

/**
 * The state
 */
interface ProfileRoofSpecialistPageState {

    /**
     * The route of the active tab
     */
    route: ProfileRoofSpecialistRoute

    /**
     * The user
     */
    user: AuthorizedUserResponse|null
}

/**
 * The profile page for roof specialists.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistPage extends React.Component<ProfileRoofSpecialistPageProps, ProfileRoofSpecialistPageState> {

    /**
     * The subscription for when the app has updated
     */
    private subscriptionAppUpdated!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            route: this.props.route,

            user: null
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            this.updateUser()
        })

        this.subscriptionAppUpdated = App.updated.subscribe(this.updateUser)
    }

    public componentWillUnmount = () => {
        this.subscriptionAppUpdated.unsubscribe()
    }

    public render = () => {
        return (
            <div id="profile-roof-specialist-page">
                <Hero id="profile-roof-specialist-page-hero" absolute height={400} />

                <Wizard>
                    <WizardTop
                        title={Translations.translations.pages.profile.roofSpecialist.wizard.top.title}
                        percentage={this.getPercentage()}
                        />

                    <WizardTabs>
                        <WizardTab
                            badge={this.getBadgeForRoute(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)}
                            active={this.state.route === Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS}
                            onClick={() => this.navigate(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)}
                            disabled={this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)}>
                            {
                                Translations.translations.pages.profile.roofSpecialist.wizard.tabs.companyDetails
                            }
                        </WizardTab>

                        <WizardTab
                            badge={this.getBadgeForRoute(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)}
                            active={this.state.route === Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO}
                            onClick={() => this.navigate(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)}
                            disabled={this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)}>
                            {
                                Translations.translations.pages.profile.roofSpecialist.wizard.tabs.portfolio
                            }
                        </WizardTab>

                        <WizardTab
                            badge={this.getBadgeForRoute(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)}
                            active={this.state.route === Route.PROFILE_ROOF_SPECIALIST_REVIEWS}
                            onClick={() => this.navigate(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)}
                            disabled={this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)}>
                            {
                                Translations.translations.pages.profile.roofSpecialist.wizard.tabs.reviews
                            }
                        </WizardTab>

                        <WizardTab
                            badge={this.getBadgeForRoute(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)}
                            active={this.state.route === Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS}
                            onClick={() => this.navigate(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)}
                            disabled={this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)}>
                            {
                                Translations.translations.pages.profile.roofSpecialist.wizard.tabs.paymentDetails
                            }
                        </WizardTab>
                    </WizardTabs>

                    {
                        this.state.user === null
                        ? (
                            <WizardContent>
                                <ProgressSpinner />
                            </WizardContent>
                        )
                        : this.renderRoute()
                    }
                </Wizard>

                <Footer />
            </div>
        )
    }

    private renderRoute = () => {
        return (
            <div id="profile-roof-specialist-page-wizard-content">
                {
                    this.state.route === Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS
                    &&
                    <ProfileRoofSpecialistCompanyDetails user={this.state.user as AuthorizedUserResponse} isTabDisabled={this.isTabDisabled} navigate={this.navigate} />
                }

                {
                    this.state.route === Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO
                    &&
                    <ProfileRoofSpecialistPortfolio user={this.state.user as AuthorizedUserResponse} isTabDisabled={this.isTabDisabled} navigate={this.navigate} onChangePortfolioImage={() => {
                        this.updateUser()
                    }} />
                }

                {
                    this.state.route === Route.PROFILE_ROOF_SPECIALIST_REVIEWS
                    &&
                    <ProfileRoofSpecialistReviews user={this.state.user as AuthorizedUserResponse} isTabDisabled={this.isTabDisabled} navigate={this.navigate} />
                }

                {
                    this.state.route === Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS
                    &&
                    <ProfileRoofSpecialistPaymentDetails user={this.state.user as AuthorizedUserResponse} isTabDisabled={this.isTabDisabled} navigate={this.navigate} />
                }
            </div>
        )
    }

    /**
     * Update the user
     */
    private updateUser = () => {
        Loader.set.next(Translations.translations.backend.controllers.authentication.me.loader)
        Backend.controllers.authentication.me().then((response) => {
            this.setState({
                user: response.data
            }, () => {
                this.initializeRoute()
            })
        })
    }

    /**
     * Gets the badge for a route
     */
    private getBadgeForRoute = (route: ProfileRoofSpecialistRoute): 1|2|3|4|'done' => {
        switch (route) {
            case Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS:
                return this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    ? 'done'
                    : 1
            case Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO:
                return this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                    && this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    ? 'done'
                    : 2
            case Route.PROFILE_ROOF_SPECIALIST_REVIEWS:
                return this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                    && this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    && this.validateTab(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)
                    ? 'done'
                    : 3
            case Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS:
                return this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                    && this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    && this.validateTab(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)
                    && this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)
                    ? 'done'
                    : 4
        }
    }

    /**
     * Checks whether the tab is disabled for a route.
     */
    private isTabDisabled = (route: ProfileRoofSpecialistRoute): boolean => {
        switch (route) {
            case Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS:
                return false
            case Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO:
                return !this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
            case Route.PROFILE_ROOF_SPECIALIST_REVIEWS:
                return !this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    || !this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
            case Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS:
                return !this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                    || !this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
        }
    }

    /**
     * Validates the tabs
     */
    private validateTab = (route: ProfileRoofSpecialistRoute): boolean => {
        switch (route) {
            case Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS:
                return true
            case Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO:
                return Boolean(
                    this.state.user
                    && this.state.user.roofSpecialist
                    && this.state.user.roofSpecialist.portfolioImages.length > 0
                )
            case Route.PROFILE_ROOF_SPECIALIST_REVIEWS:
                return true
            case Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS:
                return Boolean(
                    this.state.user
                    && this.state.user.roofSpecialist
                    && this.state.user.roofSpecialist.paymentType !== null
                )
        }
    }

    /**
     * Navigate to a route
     */
    private navigate = (route: ProfileRoofSpecialistRoute) => {
        if (this.isTabDisabled(route)) {
            return
        }
        
        Routes.setCurrentRoute.next(route)

        const el = document.querySelector('.wizard') as HTMLElement
        if (el) {
            Window.scrollTo({
                top: el.getBoundingClientRect().top + window.scrollY,
                behavior: 'smooth'
            }, () => {
                this.setState({
                    route
                })    
            })
        } else {
            this.setState({
                route
            })
        }
    }

    /**
     * Initialize the current route
     */
    private initializeRoute = () => {
        switch (this.state.route) {
            case Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS:
                break
            case Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO:
                if (this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)) {
                    Routes.redirectURL.next(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                }
                break
            case Route.PROFILE_ROOF_SPECIALIST_REVIEWS:
                if (this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)) {
                    Routes.redirectURL.next(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                }
                break
            case Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS:
                if (this.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)) {
                    Routes.redirectURL.next(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)
                }
                break
        }
        Routes.setCurrentRoute.next(this.state.route)
    }

    /**
     * Get the percentage of the wizard
     */
    private getPercentage = (): number => {
        return this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS)
            ? 100
            : (
                this.validateTab(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)
                ? 75
                : (
                    this.validateTab(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                    ? 50
                    : (
                        this.validateTab(Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS)
                        ? 25
                        : 0
                    )
                )
            )
    }
}