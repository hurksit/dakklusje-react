import React from 'react'
import AuthorizedUserResponse from '../../../../../backend/response/entity/AuthorizedUserResponse'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import FormGroup from '../../../../../components/form/group/FormGroup'
import FormInput from '../../../../../components/form/input/FormInput'
import { Branche } from '../../../../../backend/enumeration/Branche'
import FormValidation from '../../../../../core/FormValidation'
import FormSelect from '../../../../../components/form/select/FormSelect'
import AuthorizedRoofSpecialistResponse from '../../../../../backend/response/entity/AuthorizedRoofSpecialistResponse'
import Button from '../../../../../components/button/Button'
import Loader from '../../../../../components/loader/Loader'
import Backend from '../../../../../backend/Backend'
import Modal from '../../../../../components/modal/Modal'
import Collapse from '../../../../../components/collapse/Collapse'
import Storage from '../../../../../storage/Storage'
import { ProfileRoofSpecialistRoute } from '../ProfileRoofSpecialistPage'

/**
 * The props
 */
interface ProfileRoofSpecialistCompanyDetailsProps {

    /**
     * The user
     */
    user: AuthorizedUserResponse

    /**
     * Checks whether a tab is disabled.
     */
    isTabDisabled: (route: ProfileRoofSpecialistRoute) => boolean

    /**
     * Navigate to the given route if possible
     */
    navigate: (route: ProfileRoofSpecialistRoute) => void
}

/**
 * The state
 */
interface ProfileRoofSpecialistCompanyDetailsState {
    
    /**
     * The branche
     */
    branche: Branche|null

    /**
     * The company description
     */
    companyDescription: string|null

    /**
     * The contact person data
     */
    contactPerson: {
        
        /**
         * The roof specialists full name
         */
        fullName: string|null

        /**
         * The roof specialists phone number
         */
        phoneNumber: string|null
    }

    /**
     * The login details data
     */
    loginDetails: {

        /**
         * The emailaddress
         */
        emailAddress: string|null

        /**
         * The new password
         */
        password: string|null

        /**
         * The repeated new password
         */
        passwordRepeat: string|null

        /**
         * The current password for security
         */
        passwordConfirmation: string|null
    }
}

/**
 * The company details tab of the roof specialist profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistCompanyDetails extends React.Component<ProfileRoofSpecialistCompanyDetailsProps, ProfileRoofSpecialistCompanyDetailsState> {

    constructor(props: any) {
        super(props)

        this.state = {
            branche: (this.props.user.roofSpecialist as AuthorizedRoofSpecialistResponse).branche,

            companyDescription: (this.props.user.roofSpecialist as AuthorizedRoofSpecialistResponse).companyDescription,

            contactPerson: {
                fullName: (this.props.user.roofSpecialist as AuthorizedRoofSpecialistResponse).contactPersonName,
                phoneNumber: (this.props.user.roofSpecialist as AuthorizedRoofSpecialistResponse).contactPersonPhoneNumber
            },

            loginDetails: {
                emailAddress: this.props.user.emailAddress,
                password: null,
                passwordRepeat: null,
                passwordConfirmation: null
            }
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public render = () => {
        return (
            <div className="profile-roof-specialist-company-details">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.profile.roofSpecialist.wizard.intro.companyDetails.title}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.intro.companyDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {/* The branche */}
                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.branche.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.branche.helperText}
                        validation={{
                            ...FormValidation.entity.roofSpecialist.branche,
                            evaluateValue: () => {
                                return this.state.branche
                            }
                        }}
                        >

                        <FormSelect
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.branche.placeholder}
                            value={this.state.branche}
                            options={Object.keys(Branche).map((key) => ({
                                key,
                                value: Translations.translations.backend.enumerations.branche(key as Branche)
                            }))}
                            onChange={(branche) => {
                                this.setState({
                                    branche
                                }, () => {
                                    this.validate()
                                })
                            }}
                            />
                    </FormGroup>

                    {/* The description */}
                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.companyDescription.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.companyDescription.helperText}
                        validation={{
                            ...FormValidation.entity.roofSpecialist.companyDescription,
                            evaluateValue: () => {
                                return this.state.companyDescription
                            }
                        }}
                        >

                        <FormInput
                            type="multiline"
                            value={this.state.companyDescription}
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.companyDescription.placeholder}
                            onChange={(companyDescription) => {
                                this.setState({
                                    companyDescription
                                }, () => {
                                    this.validate()
                                })
                            }}
                            />
                    </FormGroup>

                    {/* Save the company details */}
                    {
                        this.companyDetailsChanged()
                        &&
                        (
                            <Button color="orange" size="small" onClick={() => {
                                Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.saveCompanyDetails.loader)
                                Backend.controllers.roofSpecialist.saveCompanyDetails({
                                    branche: this.state.branche as Branche,
                                    description: this.state.companyDescription
                                }).then(() => {
                                    Modal.mountSuccessHistory.next(Translations.translations.backend.controllers.roofSpecialist.saveCompanyDetails.responses[200])
                                }).catch((response) => {
                                    const translation = Translations.translations.backend.controllers.roofSpecialist.saveCompanyDetails.responses[response.status]
                                    if (translation !== undefined) {
                                        Modal.mountErrorHistory.next(translation)
                                    }
                                })
                            }} disabled={!this.validateCompanyDetails()}>
                                {
                                    Translations.translations.modals.defaultButtons.save
                                }
                            </Button>
                        )
                    }

                    <div className="hr"></div>

                    <div className="margin"></div>

                    {
                        this.renderCoc()
                    }

                    {
                        this.renderContactPerson()
                    }

                    {
                        this.renderLoginDetails()
                    }

                    <WizardContentOutro title={Translations.translations.pages.profile.roofSpecialist.wizard.outro.companyDetails.title}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.outro.companyDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.PROFILE,
                    Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS
                ]}>
                    <Button color="orange" disabled={this.props.isTabDisabled(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)} onClick={() => {
                        this.props.navigate(Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Render the coc collapse
     */
    private renderCoc = () => {
        return (
            <Collapse
                title={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.title}
                description={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.description}
                >
                
                <div className="inline">
                    <FormGroup
                        label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.cocNumber.label}
                        helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.cocNumber.helperText}
                        >
                        
                        <FormInput
                            type="text"
                            value={this.props.user.roofSpecialist ? this.props.user.roofSpecialist.cocCompany.cocNumber : null}
                            disabled={true}
                            />
                    </FormGroup>

                    <FormGroup
                        label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.companyName.label}
                        helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.companyName.helperText}
                        >
                        
                        <FormInput
                            type="text"
                            value={this.props.user.roofSpecialist ? this.props.user.roofSpecialist.cocCompany.companyName : null}
                            disabled={true}
                            />
                    </FormGroup>
                </div><div className="inline">
                    <FormGroup
                        label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.address.label}
                        helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.address.helperText}
                        >
                        
                        <FormInput
                            type="text"
                            value={this.props.user.roofSpecialist ? this.props.user.roofSpecialist.cocCompany.addressLine : null}
                            disabled={true}
                            />
                    </FormGroup>

                    <FormGroup
                        label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.postalCode.label}
                        helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.coc.content.postalCode.helperText}
                        >
                        
                        <FormInput
                            type="text"
                            value={this.props.user.roofSpecialist ? this.props.user.roofSpecialist.cocCompany.postalCode.postalCode : null}
                            disabled={true}
                            />
                    </FormGroup>
                </div>
            </Collapse>
        )
    }

    /**
     * Render the contact person collapse
     */
    private renderContactPerson = () => {
        return (
            <Collapse
                title={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.contactPerson.title}
                description={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.contactPerson.description}
                >
                
                <FormGroup
                    label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.name.label}
                    helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.name.helperText}
                    validation={{
                        ...FormValidation.entity.user.fullName,
                        evaluateValue: () => {
                            return this.state.contactPerson.fullName
                        }
                    }}>

                    <FormInput
                        value={this.state.contactPerson.fullName}
                        placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.name.placeholder}
                        type="text"
                        onChange={(fullName) => {
                            this.setState({
                                contactPerson: {
                                    ...this.state.contactPerson,
                                    fullName
                                }
                            }, () => {
                                this.validate()
                            })
                        }}
                        />
                </FormGroup>

                <FormGroup
                    label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.phoneNumber.label}
                    helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.phoneNumber.helperText}
                    validation={{
                        ...FormValidation.entity.roofSpecialist.phoneNumber,
                        evaluateValue: () => {
                            return this.state.contactPerson.phoneNumber
                        }
                    }}>

                    <FormInput
                        value={this.state.contactPerson.phoneNumber}
                        placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.phoneNumber.placeholder}
                        type="text"
                        onChange={(phoneNumber) => {
                            this.setState({
                                contactPerson: {
                                    ...this.state.contactPerson,
                                    phoneNumber
                                }
                            }, () => {
                                this.validate()
                            })
                        }}
                        />
                </FormGroup>

                {
                    this.contactPersonChanged()
                    &&
                    <Button color="orange" size="small" onClick={() => {
                        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.saveContactPerson.loader)
                        Backend.controllers.roofSpecialist.saveContactPerson({
                            fullName: this.state.contactPerson.fullName as string,
                            phoneNumber: this.state.contactPerson.phoneNumber as string
                        }).then(() => {
                            Modal.mountSuccessHistory.next(Translations.translations.backend.controllers.roofSpecialist.saveContactPerson.responses[200])
                        }).catch((response) => {
                            const translation = Translations.translations.backend.controllers.roofSpecialist.saveContactPerson.responses[response.status]
                            if (translation !== undefined) {
                                Modal.mountErrorHistory.next(translation)
                            }
                        })
                    }} disabled={!this.validateContactPerson()}>
                        {
                            Translations.translations.modals.defaultButtons.save
                        }
                    </Button>
                }
            </Collapse>
        )
    }

    /**
     * Render the login details collapse
     */
    private renderLoginDetails = () => {
        return (
            <Collapse
                title={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.title}
                description={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.description}
                >
                
                <FormGroup label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.emailAddress.label}
                    helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.emailAddress.helperText}
                    validation={{
                        ...FormValidation.entity.user.email,
                        evaluateValue: () => {
                            return this.state.loginDetails.emailAddress
                        }
                    }}>

                    <FormInput
                        type="email"
                        value={this.state.loginDetails.emailAddress}
                        placeholder={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.emailAddress.placeholder}
                        onChange={(emailAddress) => {
                            this.setState({
                                loginDetails: {
                                    ...this.state.loginDetails,
                                    emailAddress
                                }
                            }, () => {
                                this.validate()   
                            })
                        }}/>
                </FormGroup>

                <FormGroup label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.password.label}
                    helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.password.helperText}
                    validation={{
                        ...FormValidation.entity.user.password,
                        allowNull: true,
                        string: {
                            ...FormValidation.entity.user.password.string,
                            allowEmpty: true
                        },
                        evaluateValue: () => {
                            return this.state.loginDetails.password
                        }
                    }}>

                    <FormInput
                        type="password"
                        value={this.state.loginDetails.password}
                        placeholder={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.password.placeholder}
                        onChange={(password) => {
                            this.setState({
                                loginDetails: {
                                    ...this.state.loginDetails,
                                    password
                                }
                            }, () => {
                                this.validate()   
                            })
                        }}/>
                </FormGroup>

                {
                    this.state.loginDetails.password !== null && this.state.loginDetails.password.length > 0
                    &&
                    <FormGroup label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.passwordRepeat.label}
                        helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.passwordRepeat.helperText}
                        validation={{
                            evaluateValue: () => {
                                return this.state.loginDetails.passwordRepeat
                            },
                            string: {
                                matchEvaluatedValue: () => {
                                    return this.state.loginDetails.password || ''
                                }
                            }
                        }}>

                        <FormInput
                            type="password"
                            value={this.state.loginDetails.passwordRepeat}
                            placeholder={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.passwordRepeat.placeholder}
                            onChange={(passwordRepeat) => {
                                this.setState({
                                    loginDetails: {
                                        ...this.state.loginDetails,
                                        passwordRepeat
                                    }
                                }, () => {
                                    this.validate()   
                                })
                            }}/>
                    </FormGroup>
                }

                {
                    this.state.loginDetails.password !== null && this.state.loginDetails.password.length > 0
                    &&
                    <FormGroup label={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.passwordConfirmation.label}
                        helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.passwordConfirmation.helperText}
                        validation={{
                            ...FormValidation.entity.user.password,
                            evaluateValue: () => {
                                return this.state.loginDetails.passwordConfirmation
                            }
                        }}>

                        <FormInput
                            type="password"
                            value={this.state.loginDetails.passwordConfirmation}
                            placeholder={Translations.translations.pages.profile.roofSpecialist.wizard.content.companyDetails.loginDetails.content.passwordConfirmation.placeholder}
                            onChange={(passwordConfirmation) => {
                                this.setState({
                                    loginDetails: {
                                        ...this.state.loginDetails,
                                        passwordConfirmation
                                    }
                                }, () => {
                                    this.validate()   
                                })
                            }}/>
                    </FormGroup>
                }

                {
                    this.loginDetailsChanged()
                    &&
                    <Button color="orange" size="small" disabled={!this.validateLoginDetails()} onClick={() => {
                        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.saveLoginDetails.loader)
                        Backend.controllers.roofSpecialist.saveLoginDetails({
                            emailAddress: this.state.loginDetails.emailAddress,
                            password: this.state.loginDetails.password,
                            currentPassword: this.state.loginDetails.passwordConfirmation
                        }).then((response) => {
                            Storage.data.session.uuid = response.data.session.uuid

                            const animationTimeMS = Modal.animationTimeMS
                            Modal.animationTimeMS = 0

                            if (response.data.emailSuccess) {
                                this.setState({
                                    loginDetails: {
                                        ...this.state.loginDetails,
                                        emailAddress: this.props.user.emailAddress
                                    }
                                })
                            }

                            if (response.data.passwordSuccess) {
                                this.setState({
                                    loginDetails: {
                                        emailAddress: this.props.user.emailAddress,
                                        password: '',
                                        passwordRepeat: '',
                                        passwordConfirmation: ''
                                    }
                                })
                            }

                            for (const propertyName of Object.keys(response.data).reverse()) {
                                const translation = Translations.translations.backend.controllers.roofSpecialist.saveLoginDetails.responses[propertyName]
                                if (translation === undefined || !(typeof translation === 'string') || !response.data[propertyName]) {
                                    continue
                                }

                                if (propertyName.indexOf("Fail") !== -1) {
                                    Modal.mountErrorHistory.next(translation)
                                } else if (propertyName.indexOf("Success") !== -1) {
                                    Modal.mountSuccessHistory.next(translation)
                                }
                            }

                            Modal.animationTimeMS = animationTimeMS
                        }).catch((response) => {
                            const translation = Translations.translations.backend.controllers.roofSpecialist.saveLoginDetails.responses[response.status]
                            if (translation !== undefined) {
                                Modal.mountErrorHistory.next(translation)
                            }
                        })
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.save
                        }
                    </Button>
                }
            </Collapse>
        )
    }

    /**
     * Checks whether the company details have changed
     */
    private companyDetailsChanged = (): boolean => {
        if (!this.props.user.roofSpecialist) {
            return false
        }
        return this.props.user.roofSpecialist.branche !== this.state.branche
            || this.props.user.roofSpecialist.companyDescription !== this.state.companyDescription
    }

    /**
     * Checks whether the contact person data has changed
     */
    private contactPersonChanged = (): boolean => {
        if (!this.props.user.roofSpecialist) {
            return false
        }
        return this.props.user.roofSpecialist.contactPersonName !== this.state.contactPerson.fullName
            || this.props.user.roofSpecialist.contactPersonPhoneNumber !== this.state.contactPerson.phoneNumber
    }

    /**
     * Checks whether the login details have changed
     */
    private loginDetailsChanged = (): boolean => {
        return this.state.loginDetails.emailAddress !== this.props.user.emailAddress
            || (this.state.loginDetails.password !== null && this.state.loginDetails.password.length > 0)
    }

    /**
     * Validate the form
     */
    private validate = () => {
        FormValidation.validate()
    }

    /**
     * Validate the company details
     */
    private validateCompanyDetails = (): boolean => {
        FormValidation.stash()

        FormValidation.addField({
            ...FormValidation.entity.roofSpecialist.branche,
            evaluateValue: () => {
                return this.state.branche
            },
            label: 'branche'
        })

        FormValidation.addField({
            ...FormValidation.entity.roofSpecialist.companyDescription,
            evaluateValue: () => {
                return this.state.companyDescription
            },
            label: 'companyDescription'
        })

        return FormValidation.validateAndUnstash()
    }

    /**
     * Validate the login details
     */
    private validateLoginDetails = (): boolean => {
        FormValidation.stash()

        FormValidation.addField({
            ...FormValidation.entity.user.email,
            evaluateValue: () => {
                return this.state.loginDetails.emailAddress
            },
            label: 'emailAddress'
        })

        if (this.state.loginDetails.password !== null && this.state.loginDetails.password.length > 0) {
            FormValidation.addField({
                ...FormValidation.entity.user.password,
                allowNull: true,
                string: {
                    ...FormValidation.entity.user.password.string,
                    allowEmpty: true
                },
                evaluateValue: () => {
                    return this.state.loginDetails.password
                },
                label: 'password'
            })
    
            FormValidation.addField({
                evaluateValue: () => {
                    return this.state.loginDetails.passwordRepeat
                },
                string: {
                    matchEvaluatedValue: () => {
                        return this.state.loginDetails.password || ''
                    }
                },
                label: 'passwordRepeat'
            })
    
            FormValidation.addField({
                ...FormValidation.entity.user.password,
                evaluateValue: () => {
                    return this.state.loginDetails.passwordConfirmation
                },
                label: 'passwordConfirmation'
            })
        }

        return FormValidation.validateAndUnstash()
    }

    /**
     * Validate the contact person
     */
    private validateContactPerson = (): boolean => {
        FormValidation.stash()

        FormValidation.addField({
            ...FormValidation.entity.user.fullName,
            evaluateValue: () => {
                return this.state.contactPerson.fullName
            },
            label: 'fullName'
        })

        FormValidation.addField({
            ...FormValidation.entity.roofSpecialist.phoneNumber,
            evaluateValue: () => {
                return this.state.contactPerson.phoneNumber
            },
            label: 'phoneNumber'
        })

        return FormValidation.validateAndUnstash()
    }
}