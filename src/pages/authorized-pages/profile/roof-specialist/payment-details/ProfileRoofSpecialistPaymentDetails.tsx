import React from 'react'
import AuthorizedUserResponse from '../../../../../backend/response/entity/AuthorizedUserResponse'
import { ProfileRoofSpecialistRoute } from '../ProfileRoofSpecialistPage'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import Button from '../../../../../components/button/Button'
import FormGroup from '../../../../../components/form/group/FormGroup'
import { PaymentType } from '../../../../../backend/enumeration/PaymentType'
import AuthorizedRoofSpecialistResponse from '../../../../../backend/response/entity/AuthorizedRoofSpecialistResponse'
import Backend from '../../../../../backend/Backend'
import ProgressSpinner from '../../../../../components/progress-spinner/ProgressSpinner'
import FormSelect from '../../../../../components/form/select/FormSelect'
import FormValidation from '../../../../../core/FormValidation'
import './profile-roof-specialist-payment-details.scss'
import Loader from '../../../../../components/loader/Loader'
import FormInput from '../../../../../components/form/input/FormInput'
import Modal from '../../../../../components/modal/Modal'
import AddCreditsModal from '../../../../../components/modal/add-credits/AddCreditsModal'
import SuccessModal from '../../../../../components/modal/generic/success/SuccessModal'
import Routes from '../../../../../routes/Routes'
import Device from '../../../../../shared/Device'
import Http from '../../../../../backend/Http'
import PwaDownloadInvoiceModal from '../../../../../components/modal/pwa-download-invoice/PwaDownloadInvoiceModal'

/**
 * The props
 */
interface ProfileRoofSpecialistPaymentDetailsProps {

    /**
     * The user
     */
    user: AuthorizedUserResponse

    /**
     * Checks whether a tab is disabled.
     */
    isTabDisabled: (route: ProfileRoofSpecialistRoute) => boolean

    /**
     * Navigate to the given route if possible
     */
    navigate: (route: ProfileRoofSpecialistRoute) => void
}

/**
 * The state
 */
interface ProfileRoofSpecialistPaymentDetailsState {

    /**
     * The payment type
     */
    paymentType: PaymentType|null

    /**
     * Whether or not the page is done loading
     */
    doneLoading: boolean

    /**
     * All available invoice months in format YYYY-MM
     */
    invoiceMonths: string[]

    /**
     * The selected invoice month
     */
    selectedInvoiceMonth: string|null
}

/**
 * The payment details for the roof specialist profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileRoofSpecialistPaymentDetails extends React.Component<ProfileRoofSpecialistPaymentDetailsProps, ProfileRoofSpecialistPaymentDetailsState> {

    /**
     * The reference to the add credits 
     */
    private addCreditsButtonRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The reference to the iframe element
     */
    private iframeRef: React.RefObject<HTMLIFrameElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            paymentType: (this.props.user.roofSpecialist as AuthorizedRoofSpecialistResponse).paymentType,

            doneLoading: false,

            invoiceMonths: [],

            selectedInvoiceMonth: null
        }
    }

    public componentDidMount = () => {
        this.updateInvoiceMonths()
    }

    public render = () => {
        return (
            <div className="profile-roof-specialist-payment-details">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.profile.roofSpecialist.wizard.intro.paymentDetails.title}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.intro.paymentDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {
                        this.state.doneLoading
                        ? this.renderContent()
                        : (
                            <ProgressSpinner />
                        )
                    }

                    {
                        this.props.user.roofSpecialist && this.props.user.roofSpecialist.paymentType === null
                        &&
                        <WizardContentOutro title={Translations.translations.pages.profile.roofSpecialist.wizard.outro.paymentDetails.title}>
                            {
                                Translations.translations.pages.profile.roofSpecialist.wizard.outro.paymentDetails.description.map((part, partIndex) =>
                                    <div key={partIndex}>
                                        {
                                            part
                                        }
                                    </div>
                                )
                            }
                        </WizardContentOutro>
                    }
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.PROFILE,
                    Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.PROFILE_ROOF_SPECIALIST_REVIEWS)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Render the content
     */
    private renderContent = () => {
        return (
            <div className="profile-roof-specialist-payment-details-content">
                <iframe ref={this.iframeRef} title="must-have-title" style={{
                    display: 'none'
                }} />

                <FormGroup
                    label={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.invoices.label}
                    helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.invoices.helperText}
                    >

                    <FormSelect
                        value={this.state.selectedInvoiceMonth}
                        options={this.state.invoiceMonths.map((invoiceMonth) => ({
                            key: invoiceMonth,
                            value: this.getInvoiceMonthLabel(invoiceMonth)
                        }))}
                        disabled={this.state.invoiceMonths.length === 0}
                        placeholder={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.invoices.placeholder}
                        buttons={[
                            {
                                color: 'orange',
                                label: Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.invoices.buttons.download,
                                onClick: () => {
                                    Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.requestInvoiceToken.loader)
                                    Backend.controllers.roofSpecialist.requestInvoiceToken(this.state.selectedInvoiceMonth as string).then((response) => {
                                        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.downloadInvoiceForMonth.loader)
                                        const url = `${Http.options.apiBase}roof-specialist/invoice-months/${response.data}`
                                        if (Device.isMobile) {
                                            if (Device.isPWAStandalone) {
                                                Modal.mount.next({
                                                    element: (
                                                        <PwaDownloadInvoiceModal href={url} />
                                                    ),
                                                    history: true
                                                })
                                            } else {
                                                window.open(url, '_blank')
                                            }
                                        } else {
                                            if (!this.iframeRef.current) {
                                                return
                                            }
                                            this.iframeRef.current.src = url
                                            setTimeout(() => {
                                                Loader.set.next(null)
                                                Modal.dismiss.next()
                                                this.setState({
                                                    selectedInvoiceMonth: null 
                                                })
                                            }, 1000)
                                        }  
                                    }).catch((response) => {
                                        const translation = Translations.translations.backend.controllers.roofSpecialist.requestInvoiceToken.responses[response.status]
                                        if (translation !== undefined) {
                                            Modal.mountErrorHistory.next(translation)
                                        }
                                    })
                                }
                            }
                        ]}
                        onChange={(selectedInvoiceMonth) => {
                            this.setState({
                                selectedInvoiceMonth
                            })
                        }}
                        />
                </FormGroup>

                <FormGroup
                    label={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.paymentMethod.label}
                    helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.paymentMethod.helperText}
                    validation={{
                        ...FormValidation.entity.roofSpecialist.paymentType,
                        evaluateValue: () => {
                            return this.state.paymentType
                        }
                    }}>

                    <FormSelect
                        value={this.state.paymentType}
                        options={Object.keys(PaymentType).map((paymentType) => ({
                            key: paymentType,
                            value: Translations.translations.backend.enumerations.paymentType(paymentType as any)
                        }))}
                        placeholder={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.paymentMethod.placeholder}
                        onChange={(paymentType) => {
                            this.setState({
                                paymentType
                            })
                        }}
                        />
                </FormGroup>

                {
                    this.props.user.roofSpecialist && this.props.user.roofSpecialist.paymentType !== this.state.paymentType
                    &&
                    <Button color="orange" size="small" disabled={this.state.paymentType === null} onClick={() => {
                        const profileCompleted = this.props.user.roofSpecialist && this.props.user.roofSpecialist !== null
                        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.savePaymentDetails.loader)
                        Backend.controllers.roofSpecialist.savePaymentDetails({
                            paymentType: this.state.paymentType as PaymentType
                        }).then(() => {
                            if (profileCompleted) {
                                Modal.mountSuccess.next(Translations.translations.backend.controllers.roofSpecialist.savePaymentDetails.responses[200].profileAlreadyComplete)
                            }

                            Modal.mount.next({
                                element: (
                                    <SuccessModal title={Translations.translations.modals.generic.success.title} buttons={(
                                        <div>
                                            <div className="modal-content-content-left">
                                                <Button color="white" size="small" fullWidth onClick={() => {
                                                    Modal.dismiss.next()
                                                }}>
                                                    {
                                                        Translations.translations.modals.defaultButtons.ok
                                                    }
                                                </Button>
                                            </div><div className="modal-content-content-right">
                                                <Button color="orange" size="small" fullWidth onClick={() => {
                                                    Modal.dismiss.next()
                                                    Routes.redirectURL.next(Route.ROOF_SPECIALIST_JOBS)
                                                }}>
                                                    {
                                                        Translations.translations.backend.controllers.roofSpecialist.savePaymentDetails.responses[200].profileComplete.toJobs
                                                    }
                                                </Button>
                                            </div>
                                        </div>
                                    )}>
                                        {
                                            Translations.translations.backend.controllers.roofSpecialist.savePaymentDetails.responses[200].profileComplete.title.map((part, partIndex) =>
                                                <div key={partIndex}>
                                                    {
                                                        part
                                                    }
                                                </div>
                                            )
                                        }
                                    </SuccessModal>
                                )
                            })
                        }).catch((response) => {
                            const translation = Translations.translations.backend.controllers.roofSpecialist.savePaymentDetails.responses[response.status]
                            if (translation !== undefined) {
                                Modal.mountErrorHistory.next(translation)
                            }
                        })
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.save
                        }
                    </Button>
                }

                <div className="hr w50 left"></div>

                <FormGroup
                    label={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.credits.label}
                    helperText={Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.credits.helperText}
                    >

                    <FormInput
                        type="text"
                        value={'€ ' + (this.props.user.roofSpecialist as AuthorizedRoofSpecialistResponse).credits.toFixed(2)}
                        disabled={true}
                        />
                </FormGroup>

                <div className="add-credits-button" ref={this.addCreditsButtonRef} style={{
                    display: 'table'
                }}>
                    <Button color="orange" size="small" onClick={() => {
                        if (!this.addCreditsButtonRef.current) {
                            return
                        }
                        Modal.mount.next({
                            element: (
                                <AddCreditsModal />
                            ),
                            dismissable: false,
                            target: this.addCreditsButtonRef.current,
                            position: 'bottom-left',
                            offsetY: 120
                        })
                    }}>
                        {
                            Translations.translations.pages.profile.roofSpecialist.wizard.content.paymentDetails.credits.button
                        }
                    </Button>
                </div>
            </div>
        )
    }

    /**
     * Updates the invoice months
     */
    private updateInvoiceMonths = () => {
        Backend.controllers.roofSpecialist.getInvoiceMonths().then((response) => {
            this.setState({
                invoiceMonths: response.data,
                doneLoading: true
            })
        })
    }

    /**
     * Get the label for an invoice month
     */
    private getInvoiceMonthLabel = (invoiceMonth: string) => {
        const parts = invoiceMonth.split('-')
        if (parts.length === 2) {
            return `${Translations.translations.date.month[Number(parts[1]) - 1]} ${parts[0]}`
        }
        return invoiceMonth
    }
}