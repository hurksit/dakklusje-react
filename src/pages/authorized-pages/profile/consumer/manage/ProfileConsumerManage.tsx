import React from 'react'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import Button from '../../../../../components/button/Button'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import Modal from '../../../../../components/modal/Modal'
import DestructiveActionModal from '../../../../../components/modal/destructive-action/DestructiveActionModal'
import Loader from '../../../../../components/loader/Loader'
import Backend from '../../../../../backend/Backend'
import Routes from '../../../../../routes/Routes'
import App from '../../../../../core/app/App';
import Window from '../../../../../shared/Window';

/**
 * The manage profile tab in the consumers profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileConsumerManage extends React.Component {

    /**
     * The reference to the deactivate account button
     */
    private deactivateAccountButtonRef: React.RefObject<HTMLDivElement> = React.createRef()

    public render = () => {
        return (
            <div className="profile-consumer-manage">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.profile.consumer.tabs.manage}>
                        {
                            Translations.translations.pages.profile.consumer.manage.wizard.content.intro
                        }
                    </WizardContentIntro>

                    <div className="profile-consumer-manage-form">
                        <div className="button-ref" style={{
                            display: 'inline-block'
                        }} ref={this.deactivateAccountButtonRef}>
                            <Button color="orange" size="small" onClick={() => {
                                if (!this.deactivateAccountButtonRef.current) {
                                    return
                                }
                                Modal.mount.next({
                                    element: (
                                        <DestructiveActionModal onContinue={() => {
                                            Loader.set.next(Translations.translations.backend.controllers.consumer.deactivateAccount.loader)
                                            Backend.controllers.consumer.deactivateAccount().then(() => {
                                                App.bodyScroll.next(true)
                                                Window.scrollTo({
                                                    top: 0,
                                                    behavior: 'smooth'
                                                }, () => {
                                                    Modal.dismiss.next()
                                                    Modal.mountSuccess.next(Translations.translations.backend.controllers.consumer.deactivateAccount.responses[200])
                                                    Routes.redirectURL.next(Route.HOME)
                                                    App.logout.next()
                                                })
                                            })
                                        }}>
                                            {
                                                Translations.translations.pages.profile.consumer.manage.wizard.content.form.destructiveActionModal
                                            }
                                        </DestructiveActionModal>
                                    ),
                                    target: this.deactivateAccountButtonRef.current,
                                    position: 'right',
                                    offsetY: 120
                                })
                            }}>
                                {
                                    Translations.translations.pages.profile.consumer.manage.wizard.content.form.buttons.deactivateAccount
                                }
                            </Button>
                        </div>
                    </div>
                </WizardContent>
                <WizardBottom path={[
                    Route.HOME,
                    Route.PROFILE,
                    Route.PROFILE_CONSUMER_MANAGE
                ]}></WizardBottom>
            </div>
        )
    }
}