import React from 'react'
import './profile-consumer-page.scss'
import Hero from '../../../../components/hero/Hero'
import Footer from '../../../../components/footer/Footer'
import Routes from '../../../../routes/Routes'
import { Route } from '../../../../enumeration/Route'
import Wizard from '../../../../components/wizard/Wizard'
import WizardTop from '../../../../components/wizard/top/WizardTop'
import WizardTabs from '../../../../components/wizard/tabs/WizardTabs'
import WizardTab from '../../../../components/wizard/tabs/tab/WizardTab'
import ProfileConsumerPersonalDetails from './personal-details/ProfileConsumerPersonalDetails'
import ProfileConsumerManage from './manage/ProfileConsumerManage'
import Translations from '../../../../translations/Translations'

/**
 * The props
 */
interface ProfileConsumerPageProps {

    /**
     * The active tab in the profile page
     */
    route: Route.PROFILE_CONSUMER_PERSONAL_DETAILS | Route.PROFILE_CONSUMER_MANAGE
}

/**
 * The profile page for consumers.
 * 
 * @author Stan Hurks
 */
export default class ProfileConsumerPage extends React.Component<ProfileConsumerPageProps, ProfileConsumerPageProps> {

    constructor(props: any) {
        super(props)

        this.state = {
            route: this.props.route
        }
    }

    public componentDidMount = () => {
        setTimeout(() => {
            Routes.setCurrentRoute.next(this.state.route)
        })
    }

    public render = () => {
        return (
            <div id="profile-consumer-page">
                <Hero id="profile-consumer-page-hero" absolute height={400}></Hero>
                <Wizard>
                    <WizardTop title={Translations.translations.pages.profile.consumer.title} percentage={100} />
                    <WizardTabs>
                        <WizardTab badge="done" active={this.state.route === Route.PROFILE_CONSUMER_PERSONAL_DETAILS} onClick={() => {
                            this.setState({
                                route: Route.PROFILE_CONSUMER_PERSONAL_DETAILS
                            }, () => {
                                setTimeout(() => {
                                    Routes.setCurrentRoute.next(this.state.route)
                                })
                            })
                        }}>
                            Persoons gegevens
                        </WizardTab><WizardTab badge={2} active={this.state.route === Route.PROFILE_CONSUMER_MANAGE} onClick={() => {
                            this.setState({
                                route: Route.PROFILE_CONSUMER_MANAGE
                            }, () => {
                                setTimeout(() => {
                                    Routes.setCurrentRoute.next(this.state.route)
                                })
                            })
                        }}>
                            Account beheren
                        </WizardTab>
                    </WizardTabs>
                    {
                        this.state.route === Route.PROFILE_CONSUMER_PERSONAL_DETAILS
                        && <ProfileConsumerPersonalDetails />
                    }
                    {
                        this.state.route === Route.PROFILE_CONSUMER_MANAGE
                        && <ProfileConsumerManage />
                    }
                </Wizard>
                <Footer />
            </div>
        )
    }
}