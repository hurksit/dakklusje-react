import React from 'react'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import Button from '../../../../../components/button/Button'
import { Route } from '../../../../../enumeration/Route'
import Translations from '../../../../../translations/Translations'
import FormGroup from '../../../../../components/form/group/FormGroup'
import FormInput from '../../../../../components/form/input/FormInput'
import FormValidation from '../../../../../core/FormValidation'
import Storage from '../../../../../storage/Storage'
import { Subscription } from 'rxjs'
import Loader from '../../../../../components/loader/Loader'
import Backend from '../../../../../backend/Backend'
import Modal from '../../../../../components/modal/Modal'
import App from '../../../../../core/app/App'

/**
 * The state
 */
interface ProfileConsumerPersonalDetailsState {

    /**
     * The users full name
     */
    fullName: string

    /**
     * The users postal code
     */
    postalCode: string

    /**
     * The users email address
     */
    email: string

    /**
     * The password
     */
    password: string

    /**
     * The repeated password
     */
    passwordRepeat: string

    /**
     * The current password
     */
    currentPassword: string

    /**
     * The initial state
     */
    initialState?: ProfileConsumerPersonalDetailsState
}

/**
 * The personal details tab in the consumers profile.
 * 
 * @author Stan Hurks
 */
export default class ProfileConsumerPersonalDetails extends React.Component<any, ProfileConsumerPersonalDetailsState> {

    /**
     * The subscription for when the app updates
     */
    private subscriptionAppUpdated!: Subscription

    constructor(props: any) {
        super(props)

        const state = {
            fullName: Storage.data.session.user.name || '',

            postalCode: Storage.data.session.user.postalCode || '',

            email: Storage.data.session.user.email || '',

            password: '',

            passwordRepeat: '',

            currentPassword: ''
        }
        this.state = {
            ...state,
            initialState: state
        }
        
        FormValidation.reset()

        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.subscriptionAppUpdated = App.updated.subscribe(this.onAppUpdated)
    }

    public componentWillUnmount = () => {
        this.subscriptionAppUpdated.unsubscribe()
    }

    public render = () => {
        return (
            <form className="profile-consumer-personal-details" onSubmit={() => {
                this.changePersonalDetails()
            }}>
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.profile.consumer.tabs.personalDetails}>
                        {
                            Translations.translations.pages.profile.consumer.personalDetails.wizard.content.intro.map((sentence, index) =>
                                <div key={index}>
                                    {
                                        sentence
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    <FormGroup label={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.fullName.label}
                        helperText={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.fullName.helperText}
                        validation={{
                            ...FormValidation.entity.user.fullName,
                            evaluateValue: () => {
                                return this.state.fullName
                            }
                        }}>

                        <FormInput
                            type="text"
                            value={this.state.fullName}
                            placeholder={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.fullName.placeholder}
                            onChange={(fullName) => {
                                this.setState({
                                    fullName
                                })
                            }}/>
                    </FormGroup>

                    <FormGroup label={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.postalCode.label}
                        helperText={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.postalCode.helperText}
                        validation={{
                            ...FormValidation.entity.user.postalCode,
                            evaluateValue: () => {
                                return this.state.postalCode
                            }
                        }}>

                        <FormInput
                            type="text"
                            value={this.state.postalCode}
                            placeholder={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.postalCode.placeholder}
                            onChange={(postalCode) => {
                                this.setState({
                                    postalCode
                                })
                            }}/>
                    </FormGroup>

                    <FormGroup label={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.emailAddress.label}
                        helperText={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.emailAddress.helperText}
                        validation={{
                            ...FormValidation.entity.user.email,
                            evaluateValue: () => {
                                return this.state.email
                            }
                        }}>

                        <FormInput
                            type="email"
                            value={this.state.email}
                            placeholder={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.emailAddress.placeholder}
                            onChange={(email) => {
                                this.setState({
                                    email
                                })
                            }}/>
                    </FormGroup>

                    <FormGroup label={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.password.label}
                        helperText={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.password.helperText}
                        validation={{
                            ...FormValidation.entity.user.password,
                            string: {
                                ...FormValidation.entity.user.password.string,
                                allowEmpty: true
                            },
                            evaluateValue: () => {
                                return this.state.password
                            }
                        }}>

                        <FormInput
                            type="password"
                            value={this.state.password}
                            placeholder={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.password.placeholder}
                            onChange={(password) => {
                                this.setState({
                                    password
                                })
                            }}/>
                    </FormGroup>

                    {
                        this.state.password.length > 0
                        &&
                        <FormGroup label={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.passwordRepeat.label}
                            helperText={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.passwordRepeat.helperText}
                            validation={{
                                evaluateValue: () => {
                                    return this.state.passwordRepeat
                                },
                                string: {
                                    matchEvaluatedValue: () => {
                                        return this.state.password
                                    }
                                }
                            }}>

                            <FormInput
                                type="password"
                                value={this.state.passwordRepeat}
                                placeholder={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.passwordRepeat.placeholder}
                                onChange={(passwordRepeat) => {
                                    this.setState({
                                        passwordRepeat
                                    })
                                }}/>
                        </FormGroup>
                    }

                    {
                        this.state.password.length > 0
                        &&
                        <FormGroup label={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.currentPassword.label}
                            helperText={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.currentPassword.helperText}
                            validation={{
                                ...FormValidation.entity.user.password,
                                evaluateValue: () => {
                                    return this.state.currentPassword
                                }
                            }}>

                            <FormInput
                                type="password"
                                value={this.state.currentPassword}
                                placeholder={Translations.translations.pages.profile.consumer.personalDetails.wizard.content.form.currentPassword.placeholder}
                                onChange={(currentPassword) => {
                                    this.setState({
                                        currentPassword
                                    })
                                }}/>
                        </FormGroup>
                    }

                </WizardContent>
                <WizardBottom path={[
                    Route.HOME,
                    Route.PROFILE,
                    Route.PROFILE_CONSUMER_PERSONAL_DETAILS
                ]}>
                    <Button color="orange" disabled={!this.validate()} type="submit">
                        {
                            Translations.translations.modals.defaultButtons.save
                        }
                    </Button>
                </WizardBottom>
            </form>
        )
    }

    /**
     * Whenever the app has been updated
     */
    private onAppUpdated = () => {
        setTimeout(() => {
            const state = {
                fullName: Storage.data.session.user.name || '',

                postalCode: Storage.data.session.user.postalCode || '',

                email: Storage.data.session.user.email || '',

                password: '',

                passwordRepeat: '',

                currentPassword: ''
            }
            this.setState({
                ...state,
                initialState: {
                    ...state
                }
            }, () => {

                // Force update to validate the form
                this.forceUpdate()
            })
        })
    }

    /**
     * Validate the form
     */
    private validate = () => {
        const changes = Object.keys(this.state)
            .map((property) => 
                ['initialState', 'passwordRepeat', 'currentPassword'].indexOf(property) === -1
                && (this.state.initialState as any)[property] !== this.state[property]
            )
            .filter((bool) => bool)
            .length > 0
        return FormValidation.validate() && changes
    }

    /**
     * Change the personal details
     */
    private changePersonalDetails = () => {
        if (!this.validate()) {
            return
        }

        Loader.set.next(Translations.translations.backend.controllers.consumer.changePersonalDetails.loader)
        Backend.controllers.consumer.changePersonalDetails({
            emailAddress: this.state.email,
            password: this.state.password.length === 0 ? null : this.state.password,
            currentPassword: this.state.password.length === 0 ? null : this.state.currentPassword,
            postalCode: this.state.postalCode,
            fullName: this.state.fullName
        }).then((response) => {
            Storage.data.session.uuid = response.data.session.uuid

            const animationTimeMS = Modal.animationTimeMS
            Modal.animationTimeMS = 0

            let hasFails: boolean = false

            for (const propertyName of Object.keys(response.data).reverse()) {
                const translation = Translations.translations.backend.controllers.consumer.changePersonalDetails.responses[propertyName]
                if (translation === undefined || !(typeof translation === 'string') || !response.data[propertyName]) {
                    continue
                }

                if (propertyName.indexOf("Fail") !== -1) {
                    Modal.mountErrorHistory.next(translation)
                    hasFails = true
                } else if (propertyName.indexOf("Success") !== -1) {
                    Modal.mountSuccessHistory.next(translation)
                }
            }

            if (response.data.emailSuccess) {
                this.setState({
                    email: Storage.data.session.user.email || ''
                })
            }

            if (response.data.passwordSuccess) {
                this.setState({
                    password: '',
                    passwordRepeat: '',
                    currentPassword: ''
                })
            }

            if (!hasFails) {
                setTimeout(() => {
                    this.setState({
                        initialState: {
                            ...this.state,
                            initialState: undefined
                        }
                    })
                })
            }

            Modal.animationTimeMS = animationTimeMS
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.consumer.changePersonalDetails.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}