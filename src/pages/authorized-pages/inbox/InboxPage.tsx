import React from 'react'
import './inbox-page.scss'
import { RouteComponentProps } from 'react-router'
import Hero from '../../../components/hero/Hero'
import Routes from '../../../routes/Routes'
import { Route } from '../../../enumeration/Route'
import { Subscription, Subject } from 'rxjs'
import Events from '../../../shared/Events'
import InboxContacts from './contacts/InboxContacts'
import InboxContent from './content/InboxContent'
import Breadcrumbs from '../../../components/breadcrumbs/Breadcrumbs'
import Loader from '../../../components/loader/Loader'
import Translations from '../../../translations/Translations'
import Backend from '../../../backend/Backend'
import InboxConnectionResponse from '../../../backend/controller/inbox/response/InboxConnectionResponse'
import InboxContentNoContacts from './content/no-contacts/InboxContentNoContacts'
import InboxContentNoSelection from './content/no-selection/InboxContentNoSelection'
import InboxContentConnection from './content/connection/InboxContentConnection'
import Navigation from '../../../components/navigation/Navigation'
import Window from '../../../shared/Window'
import WebSocket from '../../../backend/WebSocket'
import MessageResponse from '../../../backend/response/entity/MessageResponse'
import App from '../../../core/app/App'
import MobileScrolling from '../../../shared/MobileScrolling'
import InboxContentCompanyProfile from './content/company-profile/InboxContentCompanyProfile'
import Device from '../../../shared/Device'
import classNames from 'classnames'
import Storage from '../../../storage/Storage'

/**
 * The props
 */
interface InboxProps extends RouteComponentProps {
}

/**
 * The state
 */
interface InboxState {

    /**
     * The window dimensions
     */
    windowDimensions: {
        width: number

        height: number
    }

    /**
     * The height of the inbox content component
     */
    contentHeight: number

    /**
     * The connections
     */
    connections: InboxConnectionResponse[]

    /**
     * The selected connection
     */
    selectedConnection: InboxConnectionResponse | null

    /**
     * The selected company profile connection
     */
    selectedCompanyProfileConnection: InboxConnectionResponse | null

    /**
     * The view to show on mobile
     */
    viewOnMobile: 'contacts' | 'content'

    /**
     * The window scroll Y to go back to when switching back from content to contacts on mobile.
     */
    windowScrollY: number

    /**
     * If a payment has been done by a roofspecialist this will make sure that the connection will be opened
     * when the connection has been updated through socket update.
     */
    openConnectionIdAfterSocketUpdate: string|null
}

/**
 * The inbox page.
 * 
 * @author Stan Hurks
 */
export default class InboxPage extends React.Component<InboxProps, InboxState> {

    /**
     * If a payment has been done by a roofspecialist this will make sure that the connection will be opened
     * when the connection has been updated through socket update.
     */
    public static readonly setOpenConnectionIdAfterSocketUpdate: Subject<string|null> = new Subject()

    /**
     * Set the selected connection
     */
    public static readonly setSelectedConnection: Subject<InboxConnectionResponse | null> = new Subject()

    /**
     * Set the selected company profile connection
     */
    public static readonly setSelectedCompanyProfileConnection: Subject<InboxConnectionResponse | null> = new Subject()

    /**
     * The subscription to the window resize event
     */
    private subscriptionWindowResize!: Subscription

    /**
     * The internal subscription to the setSelectedConnection subject
     */
    private subscriptionSetSelectedConnection!: Subscription

    /**
     * The internal subscription to the setSelectedCompanyProfileConnection subject
     */
    private subscriptionSetSelectedCompanyProfileConnection!: Subscription

    /**
     * Subscription for the web socket channel
     */
    private subscriptionUserInboxMessage!: Subscription

    /**
     * Subscription for the web socket channel
     */
    private subscriptionUserInboxMessageRead!: Subscription

    /**
     * Subscription for the web socket channel
     */
    private subscriptionUserConnection!: Subscription

    /**
     * Subscription for the web socket channel
     */
    private subscriptionRoofSpecialistConnectionAccepted!: Subscription

    /**
     * Subscription for the web socket channel
     */
    private subscriptionRoofSpecialistConnectionDeclined!: Subscription

    /**
     * Subscription for the web socket channel
     */
    private subscriptionConsumerConnectionDeclined!: Subscription

    /**
     * Internal subscription to make sure a connection id is opened after the next
     * web socket update
     */
    private subscriptionSetOpenConnectionIdAfterSocketUpdate!: Subscription

    /**
     * The subscription to when the payment has not been paid
     */
    private subscriptionUserConnectionPaymentFail!: Subscription

    /**
     * The subscription to when the payment has been paid successfully
     */
    private subscriptionUserConnectionPaymentSuccess!: Subscription

    /**
     * The subscription for when the orientation changes on mobile
     */
    private subscriptionWindowOrientationChange!: Subscription

    /**
     * The interval
     */
    private interval!: NodeJS.Timeout

    constructor(props: any) {
        super(props)

        this.state = {
            windowDimensions: {
                width: window.innerWidth,
                height: window.innerHeight
            },

            contentHeight: 0,

            connections: [],

            selectedConnection: null,
            
            selectedCompanyProfileConnection: null,

            viewOnMobile: 'contacts',

            windowScrollY: 0,

            openConnectionIdAfterSocketUpdate: null
        }
    }

    public componentDidMount = () => {
        if (Storage.data.session.user.roofSpecialist !== undefined && !Storage.data.session.user.roofSpecialist.profileComplete) {
            Routes.redirectURL.next(Route.PROFILE)
        } else {
            setTimeout(() => {
                Routes.setCurrentRoute.next(Route.INBOX)
                this.calculateContentHeight()
                this.updateConnections()
    
                Window.scrollTo({
                    top: 0
                }, () => {
                    App.bodyScroll.next(false)
                    MobileScrolling.instance.enable()
                })
            })
    
            this.interval = setInterval(() => {
                this.forceUpdate()
            }, 60 * 1000)
        }

        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
        this.subscriptionSetSelectedConnection = InboxPage.setSelectedConnection.subscribe(this.onSetSelectedConnection)
        this.subscriptionSetSelectedCompanyProfileConnection = InboxPage.setSelectedCompanyProfileConnection.subscribe(this.onSetSelectedCompanyProfileConnection)
        this.subscriptionUserInboxMessage = WebSocket.userInboxMessage.subscribe(this.onUserInboxMessage)
        this.subscriptionUserInboxMessageRead = WebSocket.userInboxMessageRead.subscribe(this.onUserInboxMessageRead)
        this.subscriptionUserConnection = WebSocket.userConnection.subscribe(this.onUserConnection)
        this.subscriptionRoofSpecialistConnectionAccepted = WebSocket.roofSpecialistConnectionAccepted.subscribe(this.onRoofSpecialistConnectionAccepted)
        this.subscriptionRoofSpecialistConnectionDeclined = WebSocket.roofSpecialistConnectionDeclined.subscribe(this.onRoofSpecialistConnectionDeclined)
        this.subscriptionConsumerConnectionDeclined = WebSocket.consumerConnectionDeclined.subscribe(this.onConsumerConnectionDeclined)
        this.subscriptionSetOpenConnectionIdAfterSocketUpdate = InboxPage.setOpenConnectionIdAfterSocketUpdate.subscribe(this.onOpenConnectionIdAfterSocketUpdate)
        this.subscriptionUserConnectionPaymentFail = WebSocket.userConnectionPaymentFail.subscribe(this.onUserConnectionPaymentFail)
        this.subscriptionUserConnectionPaymentSuccess = WebSocket.userConnectionPaymentFail.subscribe(this.onUserConnectionPaymentSuccess)
        this.subscriptionWindowOrientationChange = Events.window.orientation.subscribe(this.onWindowOrientationChange)

        setTimeout(() => {
            this.onWindowResize()
            document.body.classList.add('background-white')
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionWindowResize.unsubscribe()
        this.subscriptionSetSelectedConnection.unsubscribe()
        this.subscriptionSetSelectedCompanyProfileConnection.unsubscribe()
        this.subscriptionUserInboxMessage.unsubscribe()
        this.subscriptionUserInboxMessageRead.unsubscribe()
        this.subscriptionUserConnection.unsubscribe()
        this.subscriptionRoofSpecialistConnectionAccepted.unsubscribe()
        this.subscriptionRoofSpecialistConnectionDeclined.unsubscribe()
        this.subscriptionConsumerConnectionDeclined.unsubscribe()
        this.subscriptionSetOpenConnectionIdAfterSocketUpdate.unsubscribe()
        this.subscriptionUserConnectionPaymentFail.unsubscribe()
        this.subscriptionUserConnectionPaymentSuccess.unsubscribe()
        this.subscriptionWindowOrientationChange.unsubscribe()

        clearInterval(this.interval)

        App.bodyScroll.next(true)

        MobileScrolling.instance.disable()
    }

    public render = () => {
        return (
            <div id="inbox-page" style={{
                minHeight: this.state.contentHeight
            }}>
                {
                    this.state.windowDimensions.width >= 1000
                    &&
                    <Hero id="inbox-page-hero" absolute />
                }

                <Breadcrumbs path={[
                    Route.HOME,
                    Route.INBOX
                ]} />

                <div id="inbox-page-content" style={{
                    top: this.state.selectedConnection === null || window.innerWidth >= 1000
                        ?
                        this.state.windowDimensions.height - this.state.contentHeight + (
                            this.state.windowDimensions.width >= 1000
                                ? 60
                                : 0
                        )
                        : 0,
                    maxHeight: this.state.windowDimensions.height < 1000
                        ? (
                            this.state.viewOnMobile === 'content'
                            ? undefined//this.state.windowDimensions.height
                            : undefined
                        )
                        : undefined,
                    overflow: this.state.windowDimensions.height < 1000
                        ? (
                            this.state.viewOnMobile === 'content'
                            ? 'hidden'
                            : undefined
                        ) : undefined
                }}>
                    <div id="inbox-page-content-container" style={{
                        ...this.state.windowDimensions.width < 1000 ? {
                            transform: this.calculateTranslation(),
                            msTransform: this.calculateTranslation(),
                            OTransform: this.calculateTranslation(),
                            WebkitTransform: this.calculateTranslation(),
                            MozTransform: this.calculateTranslation()
                        } : {},

                        height: this.state.selectedConnection === null || window.innerWidth >= 1000
                            ? this.state.contentHeight - (
                                this.state.windowDimensions.width >= 1000
                                    ? 120
                                    : 0
                            )
                            : window.innerHeight - 120
                    }}>
                        <div id="inbox-page-content-container-left" className={classNames({
                            'pwa': Device.isPWAStandalone
                        })} style={{
                            ...this.state.selectedConnection !== null && this.state.windowDimensions.width < 1000
                            ? {
                                overflow: 'hidden',
                                maxHeight: 10
                            }
                            : {}
                        }}>
                            <InboxContacts
                                connections={this.state.connections}
                                height={
                                    this.state.selectedConnection === null || window.innerWidth >= 1000
                                        ? this.state.contentHeight - (
                                            this.state.windowDimensions.width >= 1000
                                                ? 120
                                                : 0
                                        )
                                        : this.state.windowDimensions.height
                                }
                                onSelect={(selectedConnection) => {
                                    InboxPage.setSelectedConnection.next(selectedConnection)
                                    InboxPage.setSelectedCompanyProfileConnection.next(null)
                                }}
                                selectedConnection={this.state.selectedConnection}
                            />
                        </div><div id="inbox-page-content-container-right" className={classNames({
                            'pwa': Device.isPWAStandalone
                        })}>
                            <InboxContent height={
                                this.state.selectedConnection === null || window.innerWidth >= 1000
                                    ? this.state.contentHeight - (
                                        this.state.windowDimensions.width >= 1000
                                            ? 120
                                            : 0
                                    )
                                    : this.state.windowDimensions.height
                            }>
                                {
                                    this.state.connections.length === 0
                                        ? <InboxContentNoContacts />
                                        : (
                                            this.state.selectedConnection === null
                                                ? <InboxContentNoSelection />
                                                : <InboxContentConnection
                                                    connection={this.state.selectedConnection}
                                                    hide={this.state.selectedCompanyProfileConnection !== null}
                                                    />
                                        )
                                }
                                {
                                    this.state.selectedCompanyProfileConnection
                                    &&
                                    <InboxContentCompanyProfile connection={this.state.selectedCompanyProfileConnection} />
                                }
                            </InboxContent>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the window changes size
     */
    private onWindowResize = () => {
        setTimeout(() => {
            this.setState({
                windowDimensions: {
                    width: window.innerWidth,
                    height: window.innerHeight
                }
            }, () => {
                this.calculateContentHeight()
            })
        })
    }

    /**
     * Whenever the selected connection changes
     */
    private onSetSelectedConnection = (selectedConnection: InboxConnectionResponse | null) => {
        this.setState({
            selectedConnection,
            viewOnMobile: selectedConnection === null
                ? 'contacts'
                : 'content',
            windowScrollY: selectedConnection === null
                ? this.state.windowScrollY
                : 0
        }, () => {
            if (!this.state.selectedConnection) {
                Navigation.hide.next(false)
            } else {
                Navigation.hide.next(true)
            }
            setTimeout(() => {
                Window.scrollTo({
                    top: this.state.windowScrollY
                }, () => {
                    setTimeout(() => {
                        this.setState({
                            windowScrollY: 0
                        })
                    })
                })
                if (!this.state.selectedConnection) {
                    setTimeout(() => {
                        this.onWindowResize() 
                    })
                }
            })
        })
    }

    /**
     * Whenever the selected company profile connection changes
     */
    private onSetSelectedCompanyProfileConnection = (selectedConnection: InboxConnectionResponse | null) => {
        this.setState({
            selectedCompanyProfileConnection: selectedConnection
        })
    }

    /**
     * Update the connections when a socket message comes through
     */
    private onUserInboxMessage = (message: MessageResponse) => {
        this.setState({
            connections: this.state.connections.map((v) => v.id === message.connectionId
                ? {
                    ...v,
                    messages: v.messages.filter((m) => m.id !== message.id).concat([message])
                }
                : v
            )
        }, () => {
            this.processConnections()
        })
    }

    /**
     * Update the connections when a socket message comes through
     */
    private onUserInboxMessageRead = (connection: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== connection.id).concat([connection])
        }, () => {
            this.processConnections()
        })
    }

    /**
     * Update the connections when a socket message comes through
     */
    private onUserConnection = (inboxConnectionResponse: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== inboxConnectionResponse.id).concat([inboxConnectionResponse])
        }, () => {
            this.processConnections()
        })
    }

    /**
     * When the payment resulted in status fail
     */
    private onUserConnectionPaymentFail = (inboxConnectionResponse: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== inboxConnectionResponse.id).concat([inboxConnectionResponse])
        }, () => {
            this.processConnections()
        })
    }

    /**
     * When the payment resulted in status success
     */
    private onUserConnectionPaymentSuccess = (inboxConnectionResponse: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== inboxConnectionResponse.id).concat([inboxConnectionResponse]),
            selectedConnection: inboxConnectionResponse
        }, () => {
            this.processConnections()
        })
    }

    /**
     * Update the connections when a socket message comes through
     */
    private onRoofSpecialistConnectionAccepted = (inboxConnectionResponse: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== inboxConnectionResponse.id).concat([inboxConnectionResponse])
        }, () => {
            this.processConnections()
        })
    }

    /**
     * Update the connections when a socket message comes through
     */
    private onRoofSpecialistConnectionDeclined = (inboxConnectionResponse: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== inboxConnectionResponse.id).concat([inboxConnectionResponse])
        }, () => {
            this.processConnections()
        })
    }

    /**
     * Update the connections when a socket message comes through
     */
    private onConsumerConnectionDeclined = (inboxConnectionResponse: InboxConnectionResponse) => {
        this.setState({
            connections: this.state.connections.filter((v) => v.id !== inboxConnectionResponse.id).concat([inboxConnectionResponse])
        }, () => {
            this.processConnections()
        })
    }

    /**
     * Set the connection id to open after the next socket update comes through
     */
    private onOpenConnectionIdAfterSocketUpdate = (connectionId: string|null) => {
        this.setState({
            openConnectionIdAfterSocketUpdate: connectionId
        })
    }

    /**
     * Whenever the orientation has changed on window
     */
    private onWindowOrientationChange = () => {
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    /**
     * Calculate the translation
     */
    private calculateTranslation = () => {
        if (this.state.connections.length === 0) {
            return 'translateX(-100vw)'
        }
        if (this.state.viewOnMobile === 'contacts') {
            return 'translateX(0)'
        } else if (this.state.viewOnMobile === 'content') {
            return 'translateX(-100vw)'
        }
    }

    /**
     * Calculates the height of the inbox content
     */
    private calculateContentHeight = () => {
        const navigationElement = document.querySelector('#navigation-inbox') as HTMLDivElement
        if (navigationElement == null) {
            return
        }

        this.setState({
            contentHeight: window.innerHeight
                - navigationElement.offsetHeight
        })
    }

    /**
     * Updates the connections
     */
    private updateConnections = () => {
        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.listConnections.loader)
        Backend.controllers.inboxConnection.listConnections().then((response) => {
            this.setState({
                connections: response.data
            }, () => {
                this.processConnections()
            })
        })
    }

    /**
     * Processes the connections after updates
     */
    private processConnections = () => {
        this.setState({
            connections: this.state.connections.sort((x, y) => {
                if (x.lastActivity > y.lastActivity) {
                    return -1
                } else if (y.lastActivity > x.lastActivity) {
                    return 1
                }
                return 0
            })
        }, () => {
            if (this.state.openConnectionIdAfterSocketUpdate) {
                const connection = this.state.connections.filter((c) => c.id === this.state.openConnectionIdAfterSocketUpdate)[0] || null
                if (connection !== null && connection.costsAcceptedAt !== null) {
                    this.setState({
                        selectedConnection: connection,
                        openConnectionIdAfterSocketUpdate: null
                    }, () => {
                        setTimeout(() => {
                            this.forceUpdate()
                        })
                    })
                }
            }
        })
    }
}