import React from 'react'
import './inbox-content-roof-specialist-declined.scss'
import InboxConnectionResponse from '../../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import { DeleteForever } from '@material-ui/icons'
import Loader from '../../../../../../../components/loader/Loader'
import Backend from '../../../../../../../backend/Backend'
import Modal from '../../../../../../../components/modal/Modal'
import InboxPage from '../../../../InboxPage'
import Translations from '../../../../../../../translations/Translations'
import Button from '../../../../../../../components/button/Button'
import IconError from '../../../../../../../components/icon/IconError'

/**
 * The props
 */
interface InboxContentRoofSpecialistDeclinedProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The inbox content for when the consumer has declined the connection request of the roof specialist.
 * 
 * @author Stan Hurks
 */
export default class InboxContentRoofSpecialistDeclined extends React.Component<InboxContentRoofSpecialistDeclinedProps> {

    public render = () => {
        return (
            <div className="inbox-content-roof-specialist-declined scroll-hack">
                <div className="inbox-content-roof-specialist-declined-return-to-inbox">
                    <Button color="white" onClick={() => {
                        InboxPage.setSelectedConnection.next(null)
                    }}>
                        {
                            Translations.translations.pages.inbox.content.returnToInbox
                        }
                    </Button>
                </div>

                <div className="inbox-content-roof-specialist-declined-container">
                    <div className="inbox-content-roof-specialist-declined-icon">
                        <IconError />
                    </div>
                    <div className="inbox-content-roof-specialist-declined-hr"></div>
                    <div className="inbox-content-roof-specialist-declined-title">
                        {
                            Translations.translations.pages.inbox.content.roofSpecialist.declined.title(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-roof-specialist-declined-hr"></div>
                    <div className="inbox-content-roof-specialist-declined-buttons">
                        <button className="inbox-content-button" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.loader.delete)
                            Backend.controllers.inboxConnectionRoofSpecialist.declineCosts(this.props.connection.id).then(() => {
                                Modal.dismiss.next('all')
                                InboxPage.setSelectedConnection.next(null)
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.responses.delete[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            <div className="inbox-content-button-icon">
                                <DeleteForever />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.roofSpecialist.declined.buttons.delete
                                }
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}