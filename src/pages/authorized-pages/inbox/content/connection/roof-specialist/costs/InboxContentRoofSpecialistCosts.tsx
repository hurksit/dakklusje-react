import React from 'react'
import './inbox-content-roof-specialist-costs.scss'
import InboxConnectionResponse from '../../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import ProfilePicture from '../../../../../../../components/profile-picture/ProfilePicture'
import { DeleteForever, Check } from '@material-ui/icons'
import Translations from '../../../../../../../translations/Translations'
import Button from '../../../../../../../components/button/Button'
import InboxPage from '../../../../InboxPage'
import Modal from '../../../../../../../components/modal/Modal'
import DestructiveActionModal from '../../../../../../../components/modal/destructive-action/DestructiveActionModal'
import Loader from '../../../../../../../components/loader/Loader'
import Backend from '../../../../../../../backend/Backend'
import JobPaymentValidateModal from '../../../../../../../components/modal/job-payment-validate/JobPaymentValidateModal'

/**
 * The props
 */
interface InboxContentRoofSpecialistCostsProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The inbox content for the roof specialist to pay or decline the costs.
 * 
 * @author Stan Hurks
 */
export default class InboxContentRoofSpecialistCosts extends React.Component<InboxContentRoofSpecialistCostsProps> {

    public render = () => {
        return (
            <div className="inbox-content-roof-specialist-costs scroll-hack">
                <div className="inbox-content-roof-specialist-costs-return-to-inbox">
                    <Button color="purple" onClick={() => {
                        InboxPage.setSelectedConnection.next(null)
                    }}>
                        {
                            Translations.translations.pages.inbox.content.returnToInbox
                        }
                    </Button>
                </div>
            
                <div className="inbox-content-roof-specialist-costs-container">
                    <div className="inbox-content-roof-specialist-costs-profile-picture">
                        <ProfilePicture image={this.props.connection.consumer ? this.props.connection.consumer.profilePicture : null} size={124} />
                    </div>
                    <div className="inbox-content-roof-specialist-costs-hr"></div>
                    <div className="inbox-content-roof-specialist-costs-title">
                        {
                            Translations.translations.pages.inbox.content.roofSpecialist.costs.title(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-roof-specialist-costs-description">
                        {
                            Translations.translations.pages.inbox.content.roofSpecialist.costs.description(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-roof-specialist-costs-hr"></div>
                    <div className="inbox-content-roof-specialist-costs-buttons">
                        <button className="inbox-content-button" onClick={() => {
                            Modal.mount.next({
                                element: (
                                    <DestructiveActionModal onContinue={() => {
                                        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.loader.declineCosts)
                                        Backend.controllers.inboxConnectionRoofSpecialist.declineCosts(this.props.connection.id).then((response) => {
                                            Modal.dismiss.next('all')
                                            InboxPage.setSelectedConnection.next(null)
                                        }).catch((response) => {
                                            const translation = Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.responses.declineCosts[response.status]
                                            if (translation !== undefined) {
                                                Modal.mountErrorHistory.next(translation)
                                            }
                                        })
                                    }}>
                                        {
                                            Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.destructiveAction.declineCosts
                                        }
                                    </DestructiveActionModal>
                                )
                            })
                        }}>
                            <div className="inbox-content-button-icon destructive">
                                <DeleteForever />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.roofSpecialist.costs.buttons.declineCosts
                                }
                            </div>
                        </button><button className="inbox-content-button" onClick={() => {
                            this.acceptCosts()
                        }}>
                            <div className="inbox-content-button-icon">
                                <Check />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.roofSpecialist.costs.buttons.acceptCosts
                                }
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Accept the costs
     */
    private acceptCosts = (skipLoader?: boolean) => {
        if (!skipLoader) {
            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.acceptCosts.loader)
        }
        InboxPage.setOpenConnectionIdAfterSocketUpdate.next(this.props.connection.id)
        Backend.controllers.inboxConnectionRoofSpecialist.acceptCosts(this.props.connection.id).then((response) => {
            switch (response.status) {
                case 201:
                    Modal.mount.next({
                        element: (
                            <JobPaymentValidateModal
                                connection={this.props.connection}
                                onSuccess={() => {
                                    InboxPage.setSelectedConnection.next(this.props.connection)
                                    Modal.dismiss.next('all')
                                }}
                                onError={() => {
                                    Modal.dismiss.next('all')
                                    InboxPage.setOpenConnectionIdAfterSocketUpdate.next(null)
                                }}
                                checkoutHref={response.data}
                                />
                        ),
                        dismissable: false
                    })

                    window.open(response.data)
                break
            }
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.inbox.connection.roofSpecialist.acceptCosts.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
            InboxPage.setOpenConnectionIdAfterSocketUpdate.next(null)
        })
    }
}