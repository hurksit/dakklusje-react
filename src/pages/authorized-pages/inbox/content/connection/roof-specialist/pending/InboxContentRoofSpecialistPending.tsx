import React from 'react'
import './inbox-content-roof-specialist-pending.scss'
import InboxConnectionResponse from '../../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import { Timer, DeleteForever } from '@material-ui/icons'
import Translations from '../../../../../../../translations/Translations'
import Loader from '../../../../../../../components/loader/Loader'
import Backend from '../../../../../../../backend/Backend'
import Modal from '../../../../../../../components/modal/Modal'
import InboxPage from '../../../../InboxPage'
import DestructiveActionModal from '../../../../../../../components/modal/destructive-action/DestructiveActionModal'
import Button from '../../../../../../../components/button/Button'

/**
 * The props
 */
interface InboxContentRoofSpecialistPendingProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The inbox content for when the roof specialist is waiting for the response of consumer.
 * 
 * @author Stan Hurks
 */
export default class InboxContentRoofSpecialistPending extends React.Component<InboxContentRoofSpecialistPendingProps> {

    public render = () => {
        return (
            <div className="inbox-content-roof-specialist-pending scroll-hack">
                <div className="inbox-content-roof-specialist-pending-return-to-inbox">
                    <Button color="purple" size="small" onClick={() => {
                        InboxPage.setSelectedConnection.next(null)
                    }}>
                        {
                            Translations.translations.pages.inbox.content.returnToInbox
                        }
                    </Button>
                </div>

                <div className="inbox-content-roof-specialist-pending-container">
                    <div className="inbox-content-roof-specialist-pending-icon">
                        <Timer />
                    </div>
                    <div className="inbox-content-roof-specialist-pending-hr"></div>
                    <div className="inbox-content-roof-specialist-pending-title">
                        {
                            Translations.translations.pages.inbox.content.roofSpecialist.pending.title(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-roof-specialist-pending-description">
                        {
                            Translations.translations.pages.inbox.content.roofSpecialist.pending.description
                        }
                    </div>
                    <div className="inbox-content-roof-specialist-pending-hr"></div>
                    <div className="inbox-content-roof-specialist-pending-buttons">
                        <button className="inbox-content-button" onClick={() => {
                            Modal.mount.next({
                                element: (
                                    <DestructiveActionModal onContinue={() => {
                                        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.loader.rejectOffer)
                                        Backend.controllers.inboxConnectionRoofSpecialist.declineCosts(this.props.connection.id).then(() => {
                                            Modal.dismiss.next('all')
                                            InboxPage.setSelectedConnection.next(null)
                                        }).catch((response) => {
                                            const translation = Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.responses.rejectOffer[response.status]
                                            if (translation !== undefined) {
                                                Modal.mountErrorHistory.next(translation)
                                            }
                                        })
                                    }}>
                                        {
                                            Translations.translations.backend.controllers.inbox.connection.roofSpecialist.declineCosts.destructiveAction.rejectOffer
                                        }
                                    </DestructiveActionModal>
                                )
                            })
                        }}>
                            <div className="inbox-content-button-icon destructive">
                                <DeleteForever />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.roofSpecialist.pending.buttons.rejectOffer
                                }
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}