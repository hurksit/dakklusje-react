import React from 'react'
import './inbox-content-consumer-costs-declined.scss'
import InboxConnectionResponse from '../../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import IconError from '../../../../../../../components/icon/IconError'
import { DeleteForever } from '@material-ui/icons'
import Translations from '../../../../../../../translations/Translations'
import Button from '../../../../../../../components/button/Button'
import InboxPage from '../../../../InboxPage'
import Loader from '../../../../../../../components/loader/Loader'
import Backend from '../../../../../../../backend/Backend'
import Modal from '../../../../../../../components/modal/Modal'

/**
 * The props
 */
interface InboxContentConsumerCostsDeclinedProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The inbox content for whem the roof specialist has declined the costs.
 * 
 * @author Stan Hurks
 */
export default class InboxContentConsumerCostsDeclined extends React.Component<InboxContentConsumerCostsDeclinedProps> {

    public render = () => {
        return (
            <div className="inbox-content-consumer-costs-declined scroll-hack">
                <div className="inbox-content-consumer-costs-declined-return-to-inbox">
                    <Button color="white" onClick={() => {
                        InboxPage.setSelectedConnection.next(null)
                    }}>
                        {
                            Translations.translations.pages.inbox.content.returnToInbox
                        }
                    </Button>
                </div>

                <div className="inbox-content-consumer-costs-declined-container">
                    <div className="inbox-content-consumer-costs-declined-icon">
                        <IconError />
                    </div>
                    <div className="inbox-content-consumer-costs-declined-hr"></div>
                    <div className="inbox-content-consumer-costs-declined-title">
                        {
                            Translations.translations.pages.inbox.content.consumer.costsDeclined.title(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-consumer-costs-declined-hr"></div>
                    <div className="inbox-content-consumer-costs-declined-buttons">
                        <button className="inbox-content-button" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.loader.delete)
                            Backend.controllers.inboxConnectionConsumer.declineConnection(this.props.connection.id).then(() => {
                                Modal.dismiss.next('all')
                                InboxPage.setSelectedConnection.next(null)
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.responses.delete[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            <div className="inbox-content-button-icon">
                                <DeleteForever />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.consumer.costsDeclined.buttons.delete
                                }
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}