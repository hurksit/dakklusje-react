import React from 'react'
import './inbox-content-consumer-pending.scss'
import InboxConnectionResponse from '../../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import { Timer, Person } from '@material-ui/icons'
import Button from '../../../../../../../components/button/Button'
import InboxPage from '../../../../InboxPage'
import Translations from '../../../../../../../translations/Translations'

/**
 * The props
 */
interface InboxContentConsumerPendingProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The inbox content for when the consumer is waiting for the response of the roof specialist.
 * 
 * @author Stan Hurks
 */
export default class InboxContentConsumerPending extends React.Component<InboxContentConsumerPendingProps> {

    public render = () => {
        return (
            <div className="inbox-content-consumer-pending scroll-hack">
                <div className="inbox-content-consumer-pending-return-to-inbox">
                    <Button color="purple" size="small" onClick={() => {
                        InboxPage.setSelectedConnection.next(null)
                    }}>
                        {
                            Translations.translations.pages.inbox.content.returnToInbox
                        }
                    </Button>
                </div>

                <div className="inbox-content-consumer-pending-container">
                    <div className="inbox-content-consumer-pending-icon">
                        <Timer />
                    </div>
                    <div className="inbox-content-consumer-pending-hr"></div>
                    <div className="inbox-content-consumer-pending-title">
                        {
                            Translations.translations.pages.inbox.content.consumer.pending.title(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-consumer-pending-description">
                        {
                            Translations.translations.pages.inbox.content.consumer.pending.description
                        }
                    </div>
                    <div className="inbox-content-consumer-pending-hr"></div>
                    <div className="inbox-content-consumer-pending-buttons">
                        <button className="inbox-content-button" onClick={() => {
                            InboxPage.setSelectedCompanyProfileConnection.next(this.props.connection)
                        }}>
                            <div className="inbox-content-button-icon">
                                <Person />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.consumer.pending.buttons.goToProfile
                                }
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}