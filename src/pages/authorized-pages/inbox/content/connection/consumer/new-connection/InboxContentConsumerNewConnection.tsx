import React from 'react'
import './inbox-content-consumer-new-connection.scss'
import InboxConnectionResponse from '../../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import Button from '../../../../../../../components/button/Button'
import ProfilePicture from '../../../../../../../components/profile-picture/ProfilePicture'
import Translations from '../../../../../../../translations/Translations'
import { DeleteForever, Check } from '@material-ui/icons'
import InboxPage from '../../../../InboxPage'
import Loader from '../../../../../../../components/loader/Loader'
import Modal from '../../../../../../../components/modal/Modal'
import DestructiveActionModal from '../../../../../../../components/modal/destructive-action/DestructiveActionModal'
import Backend from '../../../../../../../backend/Backend'

/**
 * The props
 */
interface InboxContentConsumerNewConnectionProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The inbox content for when the consumer receives a new connection request.
 * 
 * @author Stan Hurks
 */
export default class InboxContentConsumerNewConnection extends React.Component<InboxContentConsumerNewConnectionProps> {

    public render = () => {
        return (
            <div className="inbox-content-consumer-new-connection scroll-hack">
                <div className="inbox-content-consumer-new-connection-return-to-inbox">
                    <Button color="purple" onClick={() => {
                        InboxPage.setSelectedConnection.next(null)
                    }}>
                        {
                            Translations.translations.pages.inbox.content.returnToInbox
                        }
                    </Button>
                </div>

                <div className="inbox-content-consumer-new-connection-container">
                    <div className="inbox-content-consumer-new-connection-profile-picture">
                        <ProfilePicture image={this.props.connection.roofSpecialist.profilePicture} size={124} />
                    </div>
                    <div className="inbox-content-consumer-new-connection-button">
                        <Button color="orange" size="small" onClick={() => {
                            InboxPage.setSelectedCompanyProfileConnection.next(this.props.connection)
                        }}>
                            {
                                Translations.translations.pages.inbox.content.consumer.newConnection.lookAtProfile
                            }
                        </Button>
                    </div>
                    <div className="inbox-content-consumer-new-connection-hr"></div>
                    <div className="inbox-content-consumer-new-connection-title">
                        {
                            Translations.translations.pages.inbox.content.consumer.newConnection.title(this.props.connection)
                        }
                    </div>
                    <div className="inbox-content-consumer-new-connection-hr"></div>
                    <div className="inbox-content-consumer-new-connection-buttons">
                        <button className="inbox-content-button" onClick={() => {
                            Modal.mount.next({
                                element: (
                                    <DestructiveActionModal onContinue={() => {
                                        Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.loader.declineConnection)
                                        Backend.controllers.inboxConnectionConsumer.declineConnection(this.props.connection.id).then((response) => {
                                            Modal.dismiss.next('all')
                                            InboxPage.setSelectedConnection.next(null)
                                        }).catch((response) => {
                                            const translation = Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.responses.declineConnection[response.status]
                                            if (translation !== undefined) {
                                                Modal.mountErrorHistory.next(translation)
                                            }
                                        })
                                    }}>
                                        {
                                            Translations.translations.backend.controllers.inbox.connection.consumer.declineConnection.destructiveAction
                                        }
                                    </DestructiveActionModal>
                                )
                            })
                        }}>
                            <div className="inbox-content-button-icon destructive">
                                <DeleteForever />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.consumer.newConnection.buttons.decline
                                }
                            </div>
                        </button><button className="inbox-content-button" onClick={() => {
                            Loader.set.next(Translations.translations.backend.controllers.inbox.connection.consumer.acceptConnection.loader)
                            Backend.controllers.inboxConnectionConsumer.acceptConnection(this.props.connection.id).then((response) => {
                                Modal.dismiss.next('all')
                                InboxPage.setSelectedConnection.next(null)
                            }).catch((response) => {
                                const translation = Translations.translations.backend.controllers.inbox.connection.consumer.acceptConnection.responses[response.status]
                                if (translation !== undefined) {
                                    Modal.mountErrorHistory.next(translation)
                                }
                            })
                        }}>
                            <div className="inbox-content-button-icon">
                                <Check />
                            </div><div className="inbox-content-button-label">
                                {
                                    Translations.translations.pages.inbox.content.consumer.newConnection.buttons.accept
                                }
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}