import React from 'react'
import './inbox-content-chat-message.scss'
import MessageResponse from '../../../../../../../backend/response/entity/MessageResponse'
import moment from 'moment'
import classNames from 'classnames'
import Storage from '../../../../../../../storage/Storage'

/**
 * The props
 */
interface InboxContentChatMessageProps {

    /**
     * The message
     */
    message: MessageResponse
}

/**
 * A chat message in the inbox chat.
 * 
 * @author Stan Hurks
 */
export default class InboxContentChatMessage extends React.Component<InboxContentChatMessageProps> {

    public render = () => {
        return (
            <div className={classNames({
                'inbox-content-chat-message': true,
                'right': this.props.message.sender.id === Storage.data.session.user.id
            })}>
                <div className="inbox-content-chat-message-date">
                    {
                        moment(this.props.message.createdAt).format('DD-MM-YYYY HH:mm')
                    }
                </div><div className="inbox-content-chat-message-message">
                    {
                        this.props.message.content.split('\n').map((part, partIndex) =>
                            <div className="inbox-content-chat-message-message-part" key={partIndex}>
                                {
                                    part
                                }
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }
}