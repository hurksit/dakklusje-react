import React from 'react'
import './inbox-content-chat.scss'
import InboxConnectionResponse from '../../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import MessageResponse from '../../../../../../backend/response/entity/MessageResponse'
import InboxContentChatMessage from './message/InboxContentChatMessage'
import FormInput from '../../../../../../components/form/input/FormInput'
import Translations from '../../../../../../translations/Translations'
import Button from '../../../../../../components/button/Button'
import { Send } from '@material-ui/icons'
import Device from '../../../../../../shared/Device'
import { Subscription } from 'rxjs'
import Events from '../../../../../../shared/Events'
import FormValidation from '../../../../../../core/FormValidation'
import IconAlternateEmail from '../../../../../../components/icon/IconAlternateEmail'
import Storage from '../../../../../../storage/Storage'
import ProfilePicture from '../../../../../../components/profile-picture/ProfilePicture'
import Location from '../../../../../../core/Location'
import InboxPage from '../../../InboxPage'
import WebSocket from '../../../../../../backend/WebSocket'
import Backend from '../../../../../../backend/Backend'
import Modal from '../../../../../../components/modal/Modal'
import SettingsButton from '../../../../../../components/settings-button/SettingsButton'
import ConnectionSettingsModal from '../../../../../../components/modal/connection-settings/ConectionSettingsModal'
import classNames from 'classnames'
import App from '../../../../../../core/app/App'

/**
 * The props
 */
interface InboxContentChatProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The state
 */
interface InboxContentChatState {

    /**
     * The new message to be sent
     */
    newMessage: string|null

    /**
     * The height of the messages element
     */
    messagesHeight: number

    /**
     * The height of the container element
     */
    containerHeight: number

    /**
     * The height of the profile picture
     */
    profilePictureHeight: number

    /**
     * The padding bottom of the messages element
     */
    messagesPaddingBottom: number

    /**
     * Whether the component is done sending the last message
     */
    doneSending: boolean

    /**
     * The connection
     */
    connection: InboxConnectionResponse

    /**
     * The messages
     */
    messages: MessageResponse[]

    /**
     * The message count at the last update
     */
    lastUpdateMessageCount: number

    /**
     * The amount of batches of messagses loaded
     */
    batchesLoaded: number

    /**
     * The amount of messages per batch
     */
    batchSize: number

    /**
     * Whether the messages scroll event is initialized
     */
    messagesScrollEventInitialized: boolean

    /**
     * The previous height of the messages element
     */
    messagesPreviousHeight: number

    /**
     * The inbox badge count
     */
    inboxBadgeCount: number
}

/**
 * The chat in the inbox content
 * 
 * @author Stan Hurks
 */
export default class InboxContentChat extends React.Component<InboxContentChatProps, InboxContentChatState> {

    /**
     * The last conection
     */
    private static lastConnection: InboxConnectionResponse|null = null

    /**
     * The reference to the container element
     */
    private containerRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The reference to the top element
     */
    private topRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The reference to the messages element
     */
    private messagesRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The reference to the bottom element
     */
    private bottomRef: React.RefObject<HTMLFormElement> = React.createRef()

    /**
     * The subscription to the window resize event
     */
    private subscriptionWindowResize!: Subscription

    /**
     * The subscription for when a connection changes
     */
    private subscriptionWebSocketUserConnection!: Subscription

    /**
     * The settings button ref
     */
    private settingsButtonRef: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * The subscription to the inbox badge count
     */
    private subscriptionInboxBadgeCount!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            newMessage: null,
            
            messagesHeight: 0,

            containerHeight: window.innerHeight,

            profilePictureHeight: 100,

            messagesPaddingBottom: 0,

            doneSending: true,

            connection: this.props.connection,

            messages: [],

            batchSize: 25,

            batchesLoaded: 1,

            lastUpdateMessageCount: 0,
            
            messagesScrollEventInitialized: false,

            messagesPreviousHeight: 0,

            inboxBadgeCount: App.inboxBadgeCount
        }
    }

    public componentDidMount = () => {
        this.setState({
            batchesLoaded: 1
        })
        setTimeout(() => {
            this.calculateHeights()

            if (this.containerRef.current && !Device.isMobile) {
                const textareaElement = this.containerRef.current.querySelector('textarea')
                if (textareaElement) {
                    textareaElement.focus()
                }
            }

            this.readMessages()
        })

        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
        this.subscriptionWebSocketUserConnection = WebSocket.userConnection.subscribe(this.onWebSocketUserConnection)
        this.subscriptionInboxBadgeCount = WebSocket.userInboxBadgeCount.subscribe(this.onUserInboxBadgeCount)
    }
    
    public componentWillUnmount = () => {
        this.subscriptionWindowResize.unsubscribe()
        this.subscriptionWebSocketUserConnection.unsubscribe()
        this.subscriptionInboxBadgeCount.unsubscribe()
    }

    public componentWillReceiveProps = () => {
        this.setState({
            connection: this.props.connection,
            messages: this.state.messages.concat(this.getMessages().filter((message) => {
                if (this.state.messages.map((m) => m.id).indexOf(message.id) === -1) {
                    return true
                }
                return false
            }))
        }, () => {
            if (InboxContentChat.lastConnection !== null && this.state.connection.id !== InboxContentChat.lastConnection.id) {
                this.setState({
                    batchesLoaded: 1,
                    messages: this.state.connection.messages
                }, () => {
                    this.scrollMessagesToBottom()
                    this.readMessages()
                })
            }
            InboxContentChat.lastConnection = this.state.connection
        })
    }

    public componentWillUpdate = () => {
        if (this.state.lastUpdateMessageCount !== this.state.connection.messages.length) {
            this.setState({
                lastUpdateMessageCount: this.state.connection.messages.length
            }, () => {
                setTimeout(() => {
                    this.scrollMessagesToBottom()
                })
            })
        }
    }

    public render = () => {
        return (
            <div className="inbox-content-chat" ref={this.containerRef} style={{
                ...window.innerWidth >= 1000 ? {
                    minHeight: this.state.containerHeight
                } : {}
            }}>
                <div className="inbox-content-chat-top" ref={this.topRef}>
                    {
                        Storage.data.session.user.consumer
                        ? this.renderTopConsumer()
                        : this.renderTopRoofSpecialist()
                    }
                </div><div className={classNames({
                    'inbox-content-chat-messages': true,
                    'scroll-hack': true,
                    'pwa-iphone-x': Device.isIPhoneX && Device.isPWAStandalone
                })} style={{
                    ...window.innerWidth >= 1000 ? {
                        minHeight: this.state.messagesHeight,
                        maxHeight: this.state.messagesHeight
                    } : {}
                }} ref={this.messagesRef}>
                    <div className={classNames({
                        'inbox-content-chat-messages-content': true,
                        'pwa-iphone-x': Device.isIPhoneX && Device.isPWAStandalone
                    })} style={{
                        ...window.innerWidth >= 1000 ? {
                            minHeight: this.state.messagesHeight - 10
                        } : {}
                    }}>
                        {
                            this.state.messages.length > 0
                            ?
                            this.state.messages.sort((x, y) => {
                                if (x.createdAt > y.createdAt) {
                                    return -1
                                } else if (y.createdAt > x.createdAt) {
                                    return 1
                                }
                                return 0
                            })
                            .filter((v, i) => i < this.state.batchSize * this.state.batchesLoaded)
                            .sort((x, y) => {
                                if (x.createdAt > y.createdAt) {
                                    return 1
                                } else if (y.createdAt > x.createdAt) {
                                    return -1
                                }
                                return 0
                            })
                            .map((message, messageIndex) => 
                                <InboxContentChatMessage message={message} key={messageIndex} />
                            )
                            : (
                                <div className="inbox-content-chat-messages-content-no-messages">
                                    <div className="inbox-content-chat-messages-content-no-messages-icon">
                                        <IconAlternateEmail />
                                    </div><div className="inbox-content-chat-messages-content-no-messages-title">
                                        {
                                            Translations.translations.pages.inbox.content.chat.messages.noMessages.title
                                        }
                                    </div><div className="inbox-content-chat-messages-content-no-messages-description">
                                        {
                                            Translations.translations.pages.inbox.content.chat.messages.noMessages.description
                                        }
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                <form className={classNames({
                    'inbox-content-chat-bottom': true,
                    'pwa-iphone-x': Device.isIPhoneX && Device.isPWAStandalone
                })} ref={this.bottomRef} onSubmit={() => {
                    if (!Device.isMobile) {
                        return
                    }
                    if (this.state.newMessage === null || this.state.newMessage === '') {
                        return
                    }
                    this.sendMessage()
                }}>
                    <div className="inbox-content-chat-bottom-table">
                        <div className="inbox-content-chat-bottom-table-left">
                            <FormInput
                                type="multiline"
                                value={this.state.newMessage}
                                onChange={(rawNewMessage) => {
                                    let newMessage = rawNewMessage as string
                                    if ((newMessage as string).length > FormValidation.entity.message.content.string.maxLength) {
                                        newMessage = newMessage.substring(0, FormValidation.entity.message.content.string.maxLength)
                                    }
                                    this.setState({
                                        newMessage
                                    }, () => {
                                        if (!Device.isMobile) {
                                            this.onWindowResize()
                                        }
                                    })
                                }}
                                onBlur={() => {
                                    if (Device.isMobile) {
                                        this.onWindowResize()
                                    }
                                }}
                                onEnter={(metaKey) => {
                                    if (Device.isMobile) {
                                        return
                                    }
                                    if (this.state.newMessage === null || this.state.newMessage === '') {
                                        return
                                    }
                                    if (!metaKey) {
                                        return
                                    }
                                    this.sendMessage()
                                }}
                                placeholder={Translations.translations.pages.inbox.content.chat.bottom.newMessage.placeholder}
                                minColumns={window.innerWidth < 1000 ? 1 : 1}
                                maxColumns={window.innerWidth < 1000 ? 1 : 3}
                                />
                        </div><div className="inbox-content-chat-bottom-table-right">
                            <Button color="purple" size="small" type="submit" disabled={this.state.newMessage === null || this.state.newMessage === '' || !this.state.doneSending} onClick={() => {
                                this.sendMessage()
                            }}>
                                <Send />
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

    /**
     * Render the top for the consumer
     */
    private renderTopConsumer = () => {
        return (
            <div className="inbox-content-chat-top-consumer">
                <div className="inbox-content-chat-top-consumer-left">
                    <div className="inbox-content-chat-top-consumer-left-return-to-inbox">
                        <Button color="purple" size="small" onClick={() => {
                            InboxPage.setSelectedConnection.next(null)
                        }} badge={this.state.inboxBadgeCount === 0 ? undefined : this.state.inboxBadgeCount}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="inbox-content-chat-top-consumer-left-settings" ref={this.settingsButtonRef}>
                        <SettingsButton onClick={() => {
                            if (!this.settingsButtonRef.current) {
                                return
                            }
                            Modal.mount.next({
                                element: (
                                    <ConnectionSettingsModal connection={this.props.connection} />
                                ),
                                target: this.settingsButtonRef.current,
                                position: 'right',
                                offsetY: 120,
                                dismissable: false
                            })
                        }} />
                    </div><div className="inbox-content-chat-top-consumer-left-profile">
                        <Button color="orange" size="small" onClick={() => {
                            InboxPage.setSelectedCompanyProfileConnection.next(this.props.connection)
                        }}>
                            {
                                Translations.translations.pages.inbox.content.chat.top.consumer.buttons.viewProfile
                            }
                        </Button>
                    </div>
                </div><div className="inbox-content-chat-top-consumer-right">
                    <div className="inbox-content-chat-top-consumer-right-personal-details">
                        <div className="inbox-content-chat-top-consumer-right-personal-details-full-name">
                            {
                                this.state.connection.roofSpecialist.cocCompany.companyName
                            }
                        </div><div className="inbox-content-chat-top-consumer-right-personal-details-address">
                            {
                                this.state.connection.roofSpecialist.cocCompany.postalCode.postalCode
                            } {
                                this.state.connection.roofSpecialist.cocCompany.postalCode.cityName
                            } ({
                                Number(
                                    this.state.connection.consumer
                                    ? Math.round(
                                        Location.calculateDistanceMeters(
                                            this.state.connection.consumer.personalDetailsPostalCode.latitude,
                                            this.state.connection.consumer.personalDetailsPostalCode.longitude,
                                            this.state.connection.roofSpecialist.cocCompany.postalCode.latitude,
                                            this.state.connection.roofSpecialist.cocCompany.postalCode.longitude
                                        ) / 1000
                                    )
                                    : 0
                                )
                            } km)
                        </div>
                    </div>
                    <div className="inbox-content-chat-top-consumer-right-profile-picture">
                        <ProfilePicture image={this.state.connection.roofSpecialist.profilePicture} size={this.state.profilePictureHeight} />
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Render the top for the roof specialist
     */
    private renderTopRoofSpecialist = () => {
        return (
            <div className="inbox-content-chat-top-roof-specialist">
                <div className="inbox-content-chat-top-roof-specialist-left">
                    <div className="inbox-content-chat-top-roof-specialist-left-return-to-inbox">
                        <Button color="purple" size="small" onClick={() => {
                            InboxPage.setSelectedConnection.next(null)
                        }} badge={this.state.inboxBadgeCount === 0 ? undefined : this.state.inboxBadgeCount}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    </div><div className="inbox-content-chat-top-roof-specialist-left-settings" ref={this.settingsButtonRef}>
                        <SettingsButton onClick={() => {
                            if (!this.settingsButtonRef.current) {
                                return
                            }
                            Modal.mount.next({
                                element: (
                                    <ConnectionSettingsModal connection={this.props.connection} />
                                ),
                                target: this.settingsButtonRef.current,
                                position: 'right',
                                offsetY: 120,
                                dismissable: false
                            })
                        }} />
                    </div>
                </div><div className="inbox-content-chat-top-roof-specialist-right">
                    <div className="inbox-content-chat-top-roof-specialist-right-personal-details">
                        <div className="inbox-content-chat-top-roof-specialist-right-personal-details-full-name">
                            {
                                this.state.connection.consumer && this.state.connection.consumer.personalDetailsFullName
                                ? this.state.connection.consumer.personalDetailsFullName
                                : Translations.translations.pages.inbox.contacts.content.connection.name.anonymous
                            }
                        </div><div className="inbox-content-chat-top-roof-specialist-right-personal-details-address">
                            {
                                this.state.connection.consumer && this.state.connection.consumer.personalDetailsPostalCode.postalCode
                            } {
                                this.state.connection.consumer && this.state.connection.consumer.personalDetailsPostalCode.cityName
                            } ({
                                Number(
                                    this.state.connection.consumer
                                    ? Math.round(
                                        Location.calculateDistanceMeters(
                                            this.state.connection.consumer.personalDetailsPostalCode.latitude,
                                            this.state.connection.consumer.personalDetailsPostalCode.longitude,
                                            this.state.connection.roofSpecialist.cocCompany.postalCode.latitude,
                                            this.state.connection.roofSpecialist.cocCompany.postalCode.longitude
                                        ) / 1000
                                    )
                                    : 0
                                )
                            } km)
                        </div>
                    </div>
                    <div className="inbox-content-chat-top-roof-specialist-right-profile-picture">
                        <ProfilePicture image={
                            this.state.connection.consumer && this.state.connection.consumer.profilePicture
                            ? this.state.connection.consumer.profilePicture
                            : null
                        } size={this.state.profilePictureHeight} />
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Whenever the window resizes
     */
    private onWindowResize = () => {
        setTimeout(() => {
            this.calculateHeights()
            this.scrollMessagesToBottom()
        })
    }
    
    /**
     * Whenever a connection changes
     */
    private onWebSocketUserConnection = (connection: InboxConnectionResponse) => {
        if (connection.id !== this.state.connection.id) {
            return
        }
        setTimeout(() => {
            this.setState({
                connection,
                messages: this.state.messages.concat(connection.messages.filter((message) => {
                    if (this.state.messages.map((m) => m.id).indexOf(message.id) === -1) {
                        return true
                    }
                    return false
                }))
            }, () => {
                this.scrollMessagesToBottom()
                this.readMessages()
            })
        })
    }

    /**
     * Calculates the heights that are dependant on the window viewport
     */
    private calculateHeights = () => {
        if (!this.containerRef.current || !this.topRef.current || !this.bottomRef.current) {
            return
        }
        this.setState({
            messagesHeight: this.containerRef.current.offsetHeight
                - this.topRef.current.offsetHeight
                - this.bottomRef.current.offsetHeight,
            profilePictureHeight: window.innerWidth < 1000 ? 60 : 100,
            messagesPaddingBottom: this.bottomRef.current.offsetHeight,
            containerHeight: window.innerHeight
        })
    }

    /**
     * Scrolls the messages container to the bottom
     */
    private scrollMessagesToBottom = () => {
        setTimeout(() => {
            const element = window.innerWidth >= 1000 ? window : document.querySelector('#inbox-page-content-container-right') as HTMLDivElement
            element.scrollTo({
                top: 0
            })
            if (window.innerWidth >= 1000) {
                if (!this.messagesRef.current) {
                    return
                }
                this.messagesRef.current.scrollTo({
                    top: this.messagesRef.current.scrollHeight - this.messagesRef.current.offsetHeight
                })
                if (!this.state.messagesScrollEventInitialized) {
                    setTimeout(() => {
                        if (!this.messagesRef.current) {
                            return
                        }
                        this.messagesRef.current.addEventListener('scroll', this.onMessagesScroll)
                        this.setState({
                            messagesScrollEventInitialized: true
                        })
                    })
                }
            } else {
                const scrollEl = this.messagesRef.current as HTMLDivElement
                if (scrollEl.scrollHeight === scrollEl.offsetHeight) {
                    return
                }
                scrollEl.scrollTo({
                    top: scrollEl.scrollHeight - scrollEl.offsetHeight
                })
                if (!this.state.messagesScrollEventInitialized) {
                    setTimeout(() => {
                        scrollEl.addEventListener('scroll', this.onMessagesScroll)
                        this.setState({
                            messagesScrollEventInitialized: true
                        })
                    })
                }
            }
        })
    }
    
    /**
     * Whenever the user scrolls in the messages element
     */
    private onMessagesScroll = () => {
        if (!this.messagesRef.current) {
            return
        }
        if (this.messagesRef.current.scrollTop <= 0) {

            // Load new batch
            this.setState({
                batchesLoaded: Math.min(this.state.messages.length / this.state.batchSize, this.state.batchesLoaded + 1),
                messagesPreviousHeight: this.messagesRef.current.scrollHeight
            }, () => {
                if (!this.messagesRef.current) {
                    return
                }
                this.messagesRef.current.scrollTo({
                    top: this.messagesRef.current.scrollHeight - this.state.messagesPreviousHeight
                })
            })
        }
    }

    /**
     * Whenever the badge count changes
     */
    private onUserInboxBadgeCount = (badgeCount: number) => {
        this.setState({
            inboxBadgeCount: badgeCount
        })
    }

    /**
     * Send the message
     */
    private sendMessage = () => {
        if (this.state.newMessage === null || this.state.newMessage === '' || !this.state.doneSending) {
            return
        }
        this.setState({
            doneSending: false
        })
        Backend.controllers.inboxConnection.sendMessage(this.state.connection.id, {
            message: this.state.newMessage as string
        }).then(() => {
            this.setState({
                newMessage: null,
                doneSending: true
            }, () => {
                if (this.messagesRef.current) {
                    this.messagesRef.current.focus()
                }
                setTimeout(() => {
                    this.calculateHeights()
                    this.scrollMessagesToBottom()
                })
            })
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.inbox.connection.sendMessage.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }

    /**
     * Read all messages
     */
    private readMessages = () => {
        Backend.controllers.inboxConnection.readMessages(this.state.connection.id)
    }

    /**
     * Get all messages
     */
    private getMessages = (): MessageResponse[] => {
        const messages = this.state.connection.messages
        return messages.sort((x, y) => {
            if (x.createdAt > y.createdAt) {
                return 1
            } else if (y.createdAt > x.createdAt) {
                return -1
            }
            return 0
        })
    }
}