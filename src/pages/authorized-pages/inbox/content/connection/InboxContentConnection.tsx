import React from 'react'
import InboxConnectionResponse from '../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import Storage from '../../../../../storage/Storage'
import InboxContentConsumerNewConnection from './consumer/new-connection/InboxContentConsumerNewConnection'
import InboxContentConsumerPending from './consumer/pending/InboxContentConsumerPending'
import InboxContentConsumerCostsDeclined from './consumer/costs-declined/InboxContentConsumerCostsDeclined'
import InboxContentRoofSpecialistPending from './roof-specialist/pending/InboxContentRoofSpecialistPending'
import InboxContentRoofSpecialistDeclined from './roof-specialist/declined/InboxContentRoofSpecialistDeclined'
import InboxContentChat from './chat/InboxContentChat'
import InboxContentRoofSpecialistCosts from './roof-specialist/costs/InboxContentRoofSpecialistCosts'

/**
 * The props
 */
interface InboxContentConnectionProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse

    /**
     * Whether or not to hide the element
     */
    hide: boolean
}

/**
 * The inbox content for a connection.
 * 
 * @author Stan Hurks
 */
export default class InboxContentConnection extends React.Component<InboxContentConnectionProps> {

    public render = () => {
        return (
            <div className="inbox-content-connection" style={{
                ...this.props.hide ? {
                    display: 'none'
                } : {}
            }}>
                {
                    Storage.data.session.user.consumer !== undefined
                    ? (
                        this.props.connection.acceptedAt === null
                        ? (
                            this.props.connection.costsDeclinedAt === null
                            ? <InboxContentConsumerNewConnection connection={this.props.connection} />
                            : <InboxContentConsumerCostsDeclined connection={this.props.connection} />
                        )
                        : (
                            this.props.connection.costsAcceptedAt === null
                            ? (
                                this.props.connection.costsDeclinedAt === null
                                ? <InboxContentConsumerPending connection={this.props.connection} />
                                : <InboxContentConsumerCostsDeclined connection={this.props.connection} />
                            )
                            : (
                                this.props.connection.costsDeclinedAt === null
                                ? (
                                    this.props.connection.messages.length > 0
                                    ? <InboxContentChat connection={this.props.connection} />
                                    : <InboxContentConsumerPending connection={this.props.connection} />
                                )
                                : <InboxContentConsumerCostsDeclined connection={this.props.connection} />
                            )
                        )
                    )
                    : (
                        Storage.data.session.user.roofSpecialist !== undefined
                        ? (
                            this.props.connection.acceptedAt === null
                            ? (
                                this.props.connection.declinedAt === null
                                ? <InboxContentRoofSpecialistPending connection={this.props.connection} />
                                : <InboxContentRoofSpecialistDeclined connection={this.props.connection} />
                            )
                            : (
                                this.props.connection.declinedAt === null
                                ? (
                                    this.props.connection.costsAcceptedAt === null
                                    ? <InboxContentRoofSpecialistCosts connection={this.props.connection} />
                                    : <InboxContentChat connection={this.props.connection} />
                                )
                                : <InboxContentRoofSpecialistDeclined connection={this.props.connection} />
                            )
                        )
                        : undefined // this scenario will never occur
                    )
                }
            </div>
        )
    }
}