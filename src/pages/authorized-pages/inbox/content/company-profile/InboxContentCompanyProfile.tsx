import React from 'react'
import InboxConnectionResponse from '../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import './inbox-content-company-profile.scss'
import ReviewResponse from '../../../../../backend/response/entity/ReviewResponse'
import Loader from '../../../../../components/loader/Loader'
import Translations from '../../../../../translations/Translations'
import Backend from '../../../../../backend/Backend'
import Modal from '../../../../../components/modal/Modal'
import RoofSpecialistResponse from '../../../../../backend/response/entity/RoofSpecialistResponse'
import Tabs from '../../../../../components/tabs/Tabs'
import Tab from '../../../../../components/tabs/tab/Tab'
import Button from '../../../../../components/button/Button'
import InboxPage from '../../InboxPage'
import Rating from '../../../../../components/rating/Rating'
import Location from '../../../../../core/Location'
import ProfilePicture from '../../../../../components/profile-picture/ProfilePicture'
import { Subscription } from 'rxjs'
import Events from '../../../../../shared/Events'
import Picture from '../../../../../components/picture/Picture'
import PictureModal from '../../../../../components/modal/picture/PictureModal'
import Storage from '../../../../../storage/Storage'
import IconCalendar from '../../../../../components/icon/IconCalendar'
import { Person, Chat } from '@material-ui/icons'
import moment from 'moment'
import ReviewModal from '../../../../../components/modal/review/ReviewModal'
import VCACheck from '../../../../../components/vca-check/VCACheck'


/**
 * The props
 */
interface InboxContentCompanyProfileProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse
}

/**
 * The state
 */
interface InboxContentCompanyProfileState {

    /**
     * The reviews
     */
    reviews: ReviewResponse[]

    /**
     * The roof specialist
     */
    roofSpecialist: RoofSpecialistResponse

    /**
     * The index of the tab
     */
    tabIndex: number

    /**
     * The index of the tab that is hovered
     */
    hoverIndex: number|null

    /**
     * The height of the profile picture
     */
    profilePictureHeight: number

    /**
     * The amount of batches of reviews loaded
     */
    reviewBatchesLoaded: number

    /**
     * The amount of reviews per batch
     */
    reviewBatchSize: number

    /**
     * The value of the selected rating
     */
    selectedRatingValue: number|null
}

/**
 * A company profile in the inbox content
 * 
 * @author Stan Hurks
 */
export default class InboxContentCompanyProfile extends React.Component<InboxContentCompanyProfileProps, InboxContentCompanyProfileState> {

    /**
     * The subscription to the window resize event
     */
    private subscriptionWindowResize!: Subscription

    /**
     * The reference to the rating element
     */
    private ratingTopRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            reviews: [],

            roofSpecialist: this.props.connection.roofSpecialist,

            tabIndex: 0,

            hoverIndex: null,

            profilePictureHeight: window.innerWidth < 1000 ? 60 : 100,

            reviewBatchesLoaded: 1,
            
            reviewBatchSize: 3,

            selectedRatingValue: null
        }
    }

    public componentDidMount = () => {
        this.updateReviews()
        this.subscriptionWindowResize = Events.window.resize.subscribe(this.onWindowResize)
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.setState({
                roofSpecialist: Object.assign(this.state.roofSpecialist, this.props.connection.roofSpecialist)
            })
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionWindowResize.unsubscribe() 
    }

    public render = () => {
        return (
            <div className="inbox-content-company-profile">
                <div className="inbox-content-company-profile-top">
                    <div className="inbox-content-company-profile-top-left">
                        <div className="inbox-content-company-profile-top-left-return-to-chat">
                            <Button color="purple" size="small" onClick={() => {
                                InboxPage.setSelectedCompanyProfileConnection.next(null)
                            }}>
                                {
                                    Translations.translations.modals.defaultButtons.back
                                }
                            </Button>
                        </div>
                    </div><div className="inbox-content-company-profile-top-right">
                        <div className="inbox-content-company-profile-top-right-company">
                            <div className="inbox-content-company-profile-top-right-company-name">
                                {
                                    this.state.roofSpecialist.cocCompany.companyName
                                }
                            </div><div className="inbox-content-company-profile-top-right-company-address">
                                {
                                    this.state.roofSpecialist.cocCompany.postalCode.postalCode
                                } {
                                    this.state.roofSpecialist.cocCompany.postalCode.cityName
                                } ({
                                    Number(
                                        Math.round(
                                            Location.calculateDistanceMeters(
                                                Storage.data.session.user.postalCodeLatitude || 0,
                                                Storage.data.session.user.postalCodeLongitude || 0,
                                                this.state.roofSpecialist.cocCompany.postalCode.latitude,
                                                this.state.roofSpecialist.cocCompany.postalCode.longitude
                                            ) / 1000
                                        )
                                    )
                                } km)
                            </div><div className="inbox-content-company-profile-top-right-company-rating">
                                <div className="inbox-content-company-profile-top-right-company-rating-top" ref={this.ratingTopRef}>
                                    <Rating value={
                                        this.state.reviews.length > 0
                                        ? this.state.reviews.reduce((x, y) => x + y.rating, 0) / this.state.reviews.length
                                        : 0
                                    } onSelect={
                                        this.props.connection.acceptedAt
                                        && this.props.connection.costsAcceptedAt
                                        && !this.props.connection.declinedAt
                                        && !this.props.connection.costsDeclinedAt
                                        && this.props.connection.messages.length > 0
                                        ? (value) => {
                                            this.setState({
                                                selectedRatingValue: value
                                            }, () => {
                                                if (!this.ratingTopRef.current) {
                                                    return
                                                }
                                                Modal.mount.next({
                                                    element: (
                                                        <ReviewModal connection={this.props.connection} rating={value} onUpdate={(review) => {
                                                            if (review === null) {
                                                                this.forceUpdate()
                                                            } else {
                                                                this.setState({
                                                                    reviews: this.state.reviews.filter((v) => v.id !== review.id).concat([review])
                                                                })
                                                            }
                                                        }} />
                                                    ),
                                                    target: this.ratingTopRef.current,
                                                    position: 'bottom-right',
                                                    offsetY: 120
                                                })
                                            })
                                        }
                                        : undefined
                                    } />
                                </div><div className="inbox-content-company-profile-top-right-company-rating-bottom">
                                    {
                                        Translations.translations.pages.inbox.content.companyProfile.top.reviews(this.state.reviews)
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="inbox-content-company-profile-top-right-profile-picture">
                            <ProfilePicture image={this.state.roofSpecialist.profilePicture} size={this.state.profilePictureHeight} />
                        </div>
                    </div>
                </div><div className="inbox-content-company-profile-tabs">
                    <Tabs hoverIndex={this.state.hoverIndex} tabIndex={this.state.tabIndex}>
                        <Tab
                            onClick={() => {
                                this.setState({
                                    tabIndex: 0
                                })
                            }}
                            onHover={(isHovered) => {
                                this.setState({
                                    hoverIndex: isHovered ? 0 : null
                                })
                            }}>
                            {
                                Translations.translations.pages.inbox.content.companyProfile.tabs.companyDetails.label
                            }
                        </Tab>
                        <Tab
                            onClick={() => {
                                this.setState({
                                    tabIndex: 1
                                })
                            }}
                            onHover={(isHovered) => {
                                this.setState({
                                    hoverIndex: isHovered ? 1 : null
                                })
                            }}>
                            {
                                Translations.translations.pages.inbox.content.companyProfile.tabs.portfolio.label(this.props.connection)
                            }
                        </Tab>
                        <Tab
                            onClick={() => {
                                this.setState({
                                    tabIndex: 2
                                })
                            }}
                            onHover={(isHovered) => {
                                this.setState({
                                    hoverIndex: isHovered ? 2 : null
                                })
                            }}>
                            {
                                Translations.translations.pages.inbox.content.companyProfile.tabs.reviews.label(this.state.reviews)
                            }
                        </Tab>
                    </Tabs>
                </div><div className="inbox-content-company-profile-content scroll-hack" onScroll={(event) => {
                    if (this.state.tabIndex !== 2) {
                        return
                    }
                    const element = event.target as HTMLElement
                    if (element.scrollTop + element.offsetHeight >= element.scrollHeight) {
                        this.setState({
                            reviewBatchesLoaded: Math.min(this.state.reviews.length / this.state.reviewBatchSize, this.state.reviewBatchesLoaded + 1)
                        })
                    }
                }}>
                    {
                        this.state.tabIndex === 0
                        ? this.renderCompanyDetails()
                        : (
                            this.state.tabIndex === 1
                            ? this.renderPortfolio()
                            : this.renderReviews()
                        )
                    }
                </div><div className="inbox-content-company-profile-gradient" style={{
                    ...this.state.tabIndex !== 2
                    ? {
                        display: 'none'
                    } : {}
                }}>
                </div>
            </div>
        )
    }

    /**
     * Renders the company details
     */
    private renderCompanyDetails = () => {
        return (
            <div className="inbox-content-company-profile-content-company-details">
                <div className="inbox-content-company-profile-content-company-details-entry">
                    <div className="inbox-content-company-profile-content-company-details-entry-key">
                        {
                            Translations.translations.pages.inbox.content.companyProfile.tabs.companyDetails.content.branche.label
                        }
                    </div><div className="inbox-content-company-profile-content-company-details-entry-value">
                        {
                            Translations.translations.backend.enumerations.branche(this.state.roofSpecialist.branche)
                        }
                    </div>
                </div><div className="inbox-content-company-profile-content-company-details-entry">
                    <div className="inbox-content-company-profile-content-company-details-entry-key">
                        {
                            Translations.translations.pages.inbox.content.companyProfile.tabs.companyDetails.content.cocNumber.label
                        }
                    </div><div className="inbox-content-company-profile-content-company-details-entry-value">
                        {
                            this.state.roofSpecialist.cocCompany.cocNumber
                        }
                    </div>
                </div>
                {
                    this.state.roofSpecialist.companyDescription && this.state.roofSpecialist.companyDescription.length
                    &&
                    <div className="inbox-content-company-profile-content-company-details-entry">
                        <div className="inbox-content-company-profile-content-company-details-entry-key">
                            {
                                Translations.translations.pages.inbox.content.companyProfile.tabs.companyDetails.content.description.label
                            }
                        </div><div className="inbox-content-company-profile-content-company-details-entry-value">
                            {
                                this.state.roofSpecialist.companyDescription
                            }
                        </div>
                    </div>
                }
                {
                    this.state.roofSpecialist.vcaNumber
                    &&
                    <div className="inbox-content-company-profile-content-company-details-entry">
                        <div className="inbox-content-company-profile-content-company-details-entry-key">
                            {
                                Translations.translations.pages.inbox.content.companyProfile.tabs.companyDetails.content.vca.label
                            }
                            <VCACheck />
                        </div><div className="inbox-content-company-profile-content-company-details-entry-value">
                            {
                                Translations.translations.pages.inbox.content.companyProfile.tabs.companyDetails.content.vca.value(this.state.roofSpecialist.vcaNumber)
                            }
                        </div>
                    </div>
                }
            </div>
        )
    }

    /**
     * Render the portfolio
     */
    private renderPortfolio = () => {
        return (
            <div className="inbox-content-company-profile-content-portfolio">
                {
                    this.state.roofSpecialist.portfolioImages.map((portfolioImage, portfolioImageKey) =>
                        <div className="inbox-content-company-profile-content-portfolio-image-container">
                            <div className="inbox-content-company-profile-content-portfolio-image-container-image" key={portfolioImageKey} onClick={() => {
                                Modal.mount.next({
                                    element: (
                                        <PictureModal imgSrc={portfolioImage.image.file.content} />
                                    ),
                                    dismissable: true
                                })
                            }}>
                                <Picture image={portfolioImage.image} size={150} />
                            </div>
                        </div>
                    )
                }
            </div>
        )
    }

    /**
     * Render the reviews
     */
    private renderReviews = () => {
        return (
            <div className="inbox-content-company-profile-content-reviews">
                {
                    this.state.reviews.length === 0
                    ?
                    (
                        <div className="inbox-content-company-profile-content-reviews-no-reviews">
                            <div className="inbox-content-company-profile-content-reviews-no-reviews-icon">
                                <Chat />
                            </div><div className="inbox-content-company-profile-content-reviews-no-reviews-title">
                                {
                                    Translations.translations.pages.inbox.content.companyProfile.tabs.reviews.content.noReviews.title
                                }
                            </div><div className="inbox-content-company-profile-content-reviews-no-reviews-description">
                                {
                                    Translations.translations.pages.inbox.content.companyProfile.tabs.reviews.content.noReviews.description
                                }
                            </div><div className="inbox-content-company-profile-content-reviews-no-reviews-rating">
                                <Rating value={0} onSelect={
                                    this.props.connection.acceptedAt
                                    && this.props.connection.costsAcceptedAt
                                    && !this.props.connection.declinedAt
                                    && !this.props.connection.costsDeclinedAt
                                    && this.props.connection.messages.length > 0
                                    ? (value) => {
                                        this.setState({
                                            selectedRatingValue: value
                                        }, () => {
                                            if (!this.ratingTopRef.current) {
                                                return
                                            }
                                            Modal.mount.next({
                                                element: (
                                                    <ReviewModal connection={this.props.connection} rating={value} onUpdate={(review) => {
                                                        if (review === null) {
                                                            this.forceUpdate()
                                                        } else {
                                                            this.setState({
                                                                reviews: this.state.reviews.filter((v) => v.id !== review.id).concat([review])
                                                            })
                                                        }
                                                    }} />
                                                ),
                                                target: this.ratingTopRef.current,
                                                position: 'bottom-right',
                                                offsetY: 120
                                            })
                                        })
                                    }
                                    : undefined
                                } />
                            </div>
                        </div>
                    )
                    :
                    this.state.reviews
                    .sort((x, y) => {
                        if (x.createdAt > y.createdAt) {
                            return -1
                        } else if (y.createdAt > x.createdAt) {
                            return 1
                        }
                        return 0
                    })
                    .filter((v, i) => i < this.state.reviewBatchSize * this.state.reviewBatchesLoaded)
                    .map((review, reviewIndex) =>
                        <div className="inbox-content-company-profile-content-reviews-review" key={reviewIndex}>
                            <div className="inbox-content-company-profile-content-reviews-review-top">
                                <div className="inbox-content-company-profile-content-reviews-review-left">
                                    <div className="inbox-content-company-profile-content-reviews-review-left-title">
                                        {
                                            review.title
                                        }
                                    </div><div className="inbox-content-company-profile-content-reviews-review-left-rating">
                                        <Rating value={review.rating} />
                                    </div><div className="inbox-content-company-profile-content-reviews-review-left-job-category">
                                        {
                                            Translations.translations.backend.enumerations.jobCategory(review.jobCategory)
                                        }
                                    </div><div className="inbox-content-company-profile-content-reviews-review-left-meta">
                                        <div className="inbox-content-company-profile-content-reviews-review-left-meta-entry">
                                            <div className="inbox-content-company-profile-content-reviews-review-left-meta-entry-key">
                                                <IconCalendar />
                                            </div><div className="inbox-content-company-profile-content-reviews-review-left-meta-entry-value">
                                                {
                                                    moment(review.createdAt).format('DD-MM-YYYY HH:mm')
                                                }
                                            </div>
                                        </div><div className="inbox-content-company-profile-content-reviews-review-left-meta-entry">
                                            <div className="inbox-content-company-profile-content-reviews-review-left-meta-entry-key">
                                                <Person />
                                            </div><div className="inbox-content-company-profile-content-reviews-review-left-meta-entry-value">
                                                {
                                                    review.consumerPersonalDetailsFullName || Translations.translations.pages.inbox.contacts.content.connection.name.anonymous
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div><div className="inbox-content-company-profile-content-reviews-review-right">
                                    <div className="inbox-content-company-profile-content-reviews-review-right-picture">
                                        <Picture image={review.image} size={150} />
                                    </div>
                                </div>
                            </div><div className="inbox-content-company-profile-content-reviews-review-bottom">
                                <div className="inbox-content-company-profile-content-reviews-review-description">
                                    {
                                        review.description
                                    }
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>

        )
    }

    /**
     * Whenever the window resizes
     */
    private onWindowResize = () => {
        this.setState({
            profilePictureHeight: window.innerWidth < 1000 ? 60 : 100
        })
    }

    /**
     * Update the reviews
     */
    private updateReviews = () => {
        Loader.set.next(Translations.translations.backend.controllers.review.listReviewsForConnection.loader)
        Backend.controllers.review.listReviewsForConnection(this.props.connection.id).then((response) => {
            this.setState({
                reviews: response.data
            })
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.review.listReviewsForConnection.responses[response.status]
            if (translation !== undefined) {
                Modal.dismiss.next('all')
            }
        })
    }
}