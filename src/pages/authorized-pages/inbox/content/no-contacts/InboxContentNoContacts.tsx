import React from 'react'
import './inbox-content-no-contacts.scss'
import Storage from '../../../../../storage/Storage'
import Button from '../../../../../components/button/Button'
import Translations from '../../../../../translations/Translations'
import IconAlternateEmail from '../../../../../components/icon/IconAlternateEmail'
import { Route } from '../../../../../enumeration/Route'

/**
 * The inbox content when theres no contacts.
 * 
 * @author Stan Hurks
 */
export default class InboxContentNoContacts extends React.Component {

    public render = () => {
        return (
            <div className="inbox-content-no-contacts">
                <div className="inbox-content-no-contacts-content">
                    <div className="inbox-content-no-contacts-content-icon">
                        <IconAlternateEmail />
                    </div><div className="inbox-content-no-contacts-content-label">
                        {
                            Storage.data.session.user.consumer
                            ? Translations.translations.pages.inbox.content.noContacts.consumer.title
                            : Translations.translations.pages.inbox.content.noContacts.roofSpecialist.title
                        }
                    </div><div className="inbox-content-no-contacts-content-description">
                        {
                            Storage.data.session.user.consumer
                            ? Translations.translations.pages.inbox.content.noContacts.consumer.description
                            : Translations.translations.pages.inbox.content.noContacts.roofSpecialist.description
                        }
                    </div>
                    {
                        Storage.data.session.user.roofSpecialist
                        &&
                        <div className="inbox-content-no-contacts-content-button">
                            <Button color="orange" size="small" href={Route.ROOF_SPECIALIST_JOBS}>
                                {
                                    Translations.translations.pages.inbox.content.noContacts.roofSpecialist.button
                                }
                            </Button>
                        </div>
                    }
                </div>
            </div>
        )
    }
}