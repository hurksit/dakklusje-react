import React from 'react'
import './inbox-content-no-selection.scss'
import Storage from '../../../../../storage/Storage'
import Translations from '../../../../../translations/Translations'
import { MailOutline } from '@material-ui/icons'

/**
 * The inbox content for when theres no selection.
 * 
 * @author Stan Hurks
 */
export default class InboxContentNoSelection extends React.Component {

    public render = () => {
        return (
            <div className="inbox-content-no-selection">
                <div className="inbox-content-no-selection-content">
                    <div className="inbox-content-no-selection-content-icon">
                        <MailOutline />
                    </div><div className="inbox-content-no-selection-content-label">
                        {
                            Storage.data.session.user.consumer
                            ? Translations.translations.pages.inbox.content.noSelection.consumer.title
                            : Translations.translations.pages.inbox.content.noSelection.roofSpecialist.title
                        }
                    </div><div className="inbox-content-no-selection-content-description">
                        {
                            Storage.data.session.user.consumer
                            ? Translations.translations.pages.inbox.content.noSelection.consumer.description
                            : Translations.translations.pages.inbox.content.noSelection.roofSpecialist.description
                        }
                    </div>
                </div>
            </div>
        )
    }
}