import React from 'react'
import './inbox-content.scss'

/**
 * The props
 */
interface InboxContentProps {

    /**
     * The height
     */
    height: number
}

/**
 * The content section in the inbox page.
 * 
 * @author Stan Hurks
 */
export default class InboxContent extends React.Component<InboxContentProps> {

    public render = () => {
        return (
            <div id="inbox-content" style={{
                height: this.props.height,
                maxHeight: this.props.height
            }}>
                {
                    this.props.children
                }
            </div>
        )
    }
}