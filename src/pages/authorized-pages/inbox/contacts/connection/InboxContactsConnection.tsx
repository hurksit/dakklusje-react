import React from 'react'
import InboxConnectionResponse from '../../../../../backend/controller/inbox/response/InboxConnectionResponse'
import './inbox-contacts-connection.scss'
import ProfilePicture from '../../../../../components/profile-picture/ProfilePicture'
import ImageResponse from '../../../../../backend/response/entity/ImageResponse'
import Storage from '../../../../../storage/Storage'
import Translations from '../../../../../translations/Translations'
import classNames from 'classnames'
import { Check } from '@material-ui/icons'
import IconError from '../../../../../components/icon/IconError'
import { Subscription } from 'rxjs'
import WebSocket from '../../../../../backend/WebSocket'
import VCACheck from '../../../../../components/vca-check/VCACheck'

/**
 * The props
 */
interface InboxContactsConnectionProps {

    /**
     * The connection
     */
    connection: InboxConnectionResponse

    /**
     * Whether this connection is active
     */
    active: boolean

    /**
     * Whenever the connection is selected
     */
    onSelect: () => void
}

/**
 * The state
 */
interface InboxContactsConnectionState {

    /**
     * The connection
     */
    connection: InboxConnectionResponse

    /**
     * Whether or not the connection contains unread messages
     */
    unread: boolean
}

/**
 * A connection in the inbox contacts.
 * 
 * @author Stan Hurks
 */
export default class InboxContactsConnection extends React.Component<InboxContactsConnectionProps, InboxContactsConnectionState> {

    /**
     * The subscription for when a connection changes
     */
    private subscriptionWebSocketUserConnection!: Subscription

    constructor(props: any) {
        super(props)
        
        this.state = {
            connection: this.props.connection,
            unread: false
        }
    }

    public componentDidMount = () => {
        this.subscriptionWebSocketUserConnection = WebSocket.userConnection.subscribe(this.onWebSocketUserConnection)

        setTimeout(() => {
            this.setState({
                unread: this.isUnread()
            })
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionWebSocketUserConnection.unsubscribe()
    }

    public componentWillReceiveProps = () => {
        this.setState({
            connection: this.props.connection
        }, () => {
            this.setState({
                unread: this.isUnread()
            })
        })
    }

    public render = () => {
        return (
            <div className={classNames({
                'inbox-contacts-connection': true,
                'active': this.isActive(),
                'unread': this.state.unread && !this.isAccepted() && !this.isDeclined() && !this.isActive(),
                'accepted': this.isAccepted(),
                'declined': this.isDeclined()
            })} onClick={() => {
                this.props.onSelect()
            }}>
                <div className="inbox-contacts-connection-top">
                    <div className="inbox-contacts-connection-top-picture">
                        <ProfilePicture image={this.getProfilePicture()} size={32} />
                    </div><div className="inbox-contacts-connection-top-center">
                        <div className="inbox-contacts-connection-top-center-name">
                            {
                                Storage.data.session.user.consumer
                                ? (
                                    this.state.connection.roofSpecialist && this.state.connection.roofSpecialist.cocCompany.companyName
                                    ? this.state.connection.roofSpecialist.cocCompany.companyName
                                    : Translations.translations.pages.inbox.contacts.content.connection.name.anonymous
                                )
                                : (
                                    this.state.connection.consumer && this.state.connection.consumer.personalDetailsFullName
                                    ? this.state.connection.consumer.personalDetailsFullName
                                    : Translations.translations.pages.inbox.contacts.content.connection.name.anonymous
                                )
                            }
                            {
                                Storage.data.session.user.consumer
                                &&
                                this.state.connection.roofSpecialist.vcaNumber
                                &&
                                (
                                    <VCACheck />
                                )
                            }
                        </div><div className="inbox-contacts-connection-top-center-job-title">
                            {
                                Translations.translations.pages.inbox.contacts.content.connection.job
                            }: {
                                this.state.connection.job.title
                            }
                        </div>
                    </div><div className="inbox-contacts-connection-top-timestamp">
                        <div className="inbox-contacts-connection-top-timestamp-label">
                            {
                                Translations.translations.date.humanReadable(this.state.connection.lastActivity)
                            }
                        </div>
                        {
                            this.isAccepted()
                            ? (
                                <Check />
                            )
                            : (
                                this.isDeclined()
                                ? (
                                    <IconError />
                                )
                                : (
                                    this.isUnread() && !this.isActive()
                                    ? (
                                        <div className="inbox-contacts-connection-top-timestamp-dot"></div>
                                    )
                                    : undefined
                                )
                            )
                        }
                    </div>
                </div><div className="inbox-contacts-connection-preview">
                    {
                        this.getPreview()
                    }
                </div>
            </div>
        )
    }
    
    /**
     * Whenever the connection changes for a user
     */
    private onWebSocketUserConnection = (connection: InboxConnectionResponse) => {
        if (connection.id !== this.state.connection.id) {
            return
        }
        setTimeout(() => {
            this.setState({
                connection
            })
        })
    }
    
    /**
     * Get the profile picture
     */
    private getProfilePicture = (): ImageResponse|null => {
        if (Storage.data.session.user.consumer) {
            return this.state.connection.roofSpecialist
                ? this.state.connection.roofSpecialist.profilePicture
                : null
        } else {
            return this.state.connection.consumer
                ? this.state.connection.consumer.profilePicture
                : null
        }
    }

    /**
     * Get the preview text
     */
    private getPreview = (): string => {
        if (this.state.connection.messages.length > 0) {
            if (Storage.data.session.user.consumer) {
                if (this.state.connection.costsAcceptedAt && this.state.connection.costsDeclinedAt) {
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.conversationEnded
                }
            } else {
                if (this.state.connection.acceptedAt && this.state.connection.declinedAt) {
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.conversationEnded
                }
            }
            return this.state.connection.messages.sort((x, y) => {
                if (x.createdAt > y.createdAt) {
                    return -1
                } else if (y.createdAt > x.createdAt) {
                    return 1
                }
                return 0
            })[0].content
        } else {
            if (Storage.data.session.user.consumer) {
                if (this.state.connection.costsDeclinedAt) {
                    if (this.state.connection.costsAcceptedAt) {
                        return Translations.translations.pages.inbox.contacts.content.connection.preview.conversationEnded
                    }
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.costsDeclined
                } else if (this.state.connection.costsAcceptedAt) {
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.costsAccepted
                } else if (!this.state.connection.acceptedAt) {
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.newConnection
                } else {
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.costsNotAcceptedYet
                }
            } else {
                if (this.state.connection.declinedAt) {
                    if (this.state.connection.acceptedAt) {
                        return Translations.translations.pages.inbox.contacts.content.connection.preview.conversationEnded
                    }
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.declined
                }
                if (!this.state.connection.acceptedAt) {
                    return Translations.translations.pages.inbox.contacts.content.connection.preview.notAcceptedYet
                } else {
                    if (this.state.connection.declinedAt) {
                        return Translations.translations.pages.inbox.contacts.content.connection.preview.declined
                    } else {
                        return Translations.translations.pages.inbox.contacts.content.connection.preview.accepted
                    }
                }    
            }
        }
    }

    /**
     * Whether this connection is active
     */
    private isActive = (): boolean => {
        return this.props.active
    }

    /**
     * Whether this connection is unread
     */
    private isUnread = (): boolean => {
        return this.state.connection.messages
            .filter((v) =>
                v.sender.id !== Storage.data.session.user.id
                && v.readAt === null
            ).length > 0
    }

    /**
     * Whether this connection is accepted and unread
     */
    private isAccepted = (): boolean => {
        return (
            (
                !!Storage.data.session.user.roofSpecialist
                && this.state.connection.acceptedAt !== null
                && this.state.connection.costsAcceptedAt === null
            )
            ||
            (
                !!Storage.data.session.user.consumer
                && this.state.connection.acceptedAt === null
            )
        )
    }

    /**
     * Whether this connection is declined and unread
     */
    private isDeclined = (): boolean => {
        return (
            !!Storage.data.session.user.roofSpecialist
            && this.state.connection.declinedAt !== null
        )
        || (
            !!Storage.data.session.user.consumer
            && this.state.connection.costsDeclinedAt !== null
        )
    }
}