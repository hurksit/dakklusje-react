import React from 'react'
import './inbox-contacts.scss'
import InboxConnectionResponse from '../../../../backend/controller/inbox/response/InboxConnectionResponse'
import FormInput from '../../../../components/form/input/FormInput'
import Translations from '../../../../translations/Translations'
import InboxContactsConnection from './connection/InboxContactsConnection'
import Storage from '../../../../storage/Storage'
import { Subscription } from 'rxjs'
import WebSocket from '../../../../backend/WebSocket'
import Device from '../../../../shared/Device'
import Browser from '../../../../shared/Browser'
import classNames from 'classnames';

/**
 * The props
 */
interface InboxContactsProps {

    /**
     * The connections
     */
    connections: InboxConnectionResponse[]

    /**
     * The max height of the component
     */
    height: number

    /**
     * When a contact has been selected
     */
    onSelect: (selectedConnection: InboxConnectionResponse) => void

    /**
     * The selected connection
     */
    selectedConnection: InboxConnectionResponse|null
}

/**
 * The state
 */
interface InboxContactsState {

    /**
     * The string to filter the connections and messages with
     */
    filter: string|null

    /**
     * The selected connection
     */
    selectedConnection: InboxConnectionResponse|null

    /**
     * The connections
     */
    connections: InboxConnectionResponse[]
}

/**
 * The contacts section in the inbox.
 * 
 * @author Stan Hurks
 */
export default class InboxContacts extends React.Component<InboxContactsProps, InboxContactsState> {

    /**
     * The subscription for when a connection changes
     */
    private subscriptionWebSocketUserConnection!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            filter: null,

            selectedConnection: this.props.selectedConnection,

            connections: this.props.connections
        }
    }

    public componentDidMount = () => {
        this.subscriptionWebSocketUserConnection = WebSocket.userConnection.subscribe(this.onWebSocketUserConnection)
    }

    public componentWillUnmount = () => {
        this.subscriptionWebSocketUserConnection.unsubscribe()
    }

    public componentWillReceiveProps = () => {
        setTimeout(() => {
            this.setState({
                selectedConnection: this.props.selectedConnection,
                connections: this.props.connections
            }, () => {
                this.forceUpdate()
            })
        })
    }

    public render = () => {
        return (
            <div id="inbox-contacts" className={classNames({
                'scroll-hack': true,
                'pwa': Device.isPWAStandalone
            })} style={{
                height: this.props.height,
                minHeight: this.props.height,
                maxHeight: this.props.height
            }}>
                <div id="inbox-contacts-scroll-content" style={{
                    height: this.props.height,
                    minHeight: this.props.height,
                    maxHeight: this.props.height
                }}>
                    <div id="inbox-contacts-top">
                        <div id="inbox-contacts-top-title">
                            {
                                Translations.translations.pages.inbox.contacts.top.title
                            }
                        </div><div id="inbox-contacts-top-filter">
                            <FormInput
                                type="text"
                                value={this.state.filter}
                                placeholder={Translations.translations.pages.inbox.contacts.top.filter.placeholder}
                                onChange={(filter) => {
                                    this.setState({
                                        filter
                                    })
                                }}
                                />
                        </div><div id="inbox-contacts-top-hr">
                        </div>
                    </div><div id="inbox-contacts-content" style={{
                        ...window.innerWidth >= 1000 && Browser.isFirefox ? {
                            paddingBottom: 125
                        } : {
                            paddingBottom: Device.offsetBottom
                        }
                    }}>
                        {
                            this.getFilteredConnections().length === 0
                            ?
                            (
                                <div id="inbox-contacts-content-no-content"></div>
                            )
                            :
                            this.getFilteredConnections()
                                .map((connection, connectionIndex) =>
                                    <InboxContactsConnection
                                        connection={connection}
                                        key={connectionIndex}
                                        active={
                                            this.state.selectedConnection !== null
                                            && this.state.selectedConnection.id === connection.id
                                        }
                                        onSelect={(() => {
                                            this.selectConnection(connection)
                                        })} />
                                )
                        }
                    </div>
                </div>
            
                <div id="inbox-contacts-gradient"></div>
            </div>
        )
    }

    /**
     * Whenever the connection changes for a user
     */
    private onWebSocketUserConnection = () => {
        setTimeout(() => {
            this.forceUpdate()

            setTimeout(() => {
                this.forceUpdate()
            }, 100)
        }, 1000 / 30)
    }

    /**
     * Get the filtered connections
     */
    private getFilteredConnections = (): InboxConnectionResponse[] => {
        return this.state.connections
            .filter((connection) => {
                if (Storage.data.session.user.roofSpecialist && connection.costsDeclinedAt !== null) {
                    return false
                } else if (Storage.data.session.user.consumer && connection.declinedAt !== null) {
                    return false
                }

                if (this.state.filter === null || this.state.filter === '') {
                    return true
                }

                if (connection.messages.filter((message) => message.content.toLowerCase().indexOf((this.state.filter as string).toLowerCase()) !== -1).length > 0) {
                    return true
                }

                if (Storage.data.session.user.consumer) {
                    if (connection.roofSpecialist
                        && connection.roofSpecialist.cocCompany.companyName.toLowerCase().indexOf(this.state.filter.toLowerCase()) !== -1) {
                        return true
                    }
                } else {
                    if (connection.consumer && connection.consumer.personalDetailsFullName
                        && connection.consumer.personalDetailsFullName.toLowerCase().indexOf(this.state.filter.toLowerCase()) !== -1) {
                        return true
                    }
                }

                if (connection.job.title.toLowerCase().indexOf(this.state.filter.toLowerCase()) !== -1) {
                    return true
                }

                if (connection.job.description && connection.job.description.toLowerCase().indexOf(this.state.filter.toLowerCase()) !== -1) {
                    return true
                }

                return false
            })
            .sort((x, y) => {
                if (x.lastActivity > y.lastActivity) {
                    return -1
                } else if (y.lastActivity > x.lastActivity) {
                    return 1
                } else {
                    if (x.id > y.id) {
                        return -1
                    } else if (y.id > x.id) {
                        return 1
                    } else {
                        return 0
                    }
                }
            })
    }

    /**
     * Select a connection.
     */
    private selectConnection = (connection: InboxConnectionResponse) => {
        this.setState({
            selectedConnection: connection
        }, () => {
            this.props.onSelect(connection)
        })
    }
}