import React from 'react'
import { RegisterAsRoofSpecialistRoute } from '../RegisterAsRoofSpecialistPage'
import Storage from '../../../../storage/Storage'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../translations/Translations'
import WizardContentOutro from '../../../../components/wizard/content/outro/WizardContentOutro'
import FormGroup from '../../../../components/form/group/FormGroup'
import FormInput from '../../../../components/form/input/FormInput'
import CocCompanyResponse from '../../../../backend/response/entity/CocCompanyResponse'
import FormValidation from '../../../../core/FormValidation'
import WizardBottom from '../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../enumeration/Route'
import Button from '../../../../components/button/Button'
import Loader from '../../../../components/loader/Loader'
import Backend from '../../../../backend/Backend'
import Modal from '../../../../components/modal/Modal'

/**
 * The props
 */
interface RegisterAsRoofSpecialistCocProps {

    /**
     * Navigate to a route
     */
    navigate: (route: RegisterAsRoofSpecialistRoute) => void

    /**
     * Check whether a tab is disabled.
     */
    isTabDisabled: (route: RegisterAsRoofSpecialistRoute) => boolean
}

/**
 * The state
 */
export interface RegisterAsRoofSpecialistCocState {

    /**
     * The coc company name in the form input
     */
    cocNumber: string|null

    /**
     * The coc company
     */
    cocCompany: CocCompanyResponse|null

    /**
     * Whether or not the form is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The coc tab for the register as roof specialist page.
 * 
 * @author Stan Hurks
 */
export default class RegisterAsRoofSpecialistCoc extends React.Component<RegisterAsRoofSpecialistCocProps, RegisterAsRoofSpecialistCocState> {

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.registerAsRoofSpecialist.coc,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        }, () => {
            if (this.state.cocNumber) {
                this.updateCocDetails()
            }
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            })
        }
    }

    public render = () => {
        return (
            <div className="register-as-roof-specialist-coc">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.coc.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.coc.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocNumber.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocNumber.helperText}
                        validation={{
                            ...FormValidation.entity.cocCompany.cocNumber,
                            evaluateValue: () => {
                                return this.state.cocNumber
                            }
                        }}>

                        <FormInput
                            value={this.state.cocNumber}
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocNumber.placeholder}
                            type="text"
                            onChange={(cocNumber) => {
                                this.setState({
                                    cocNumber
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            onBlur={() => {
                                this.updateCocDetails()
                            }}
                            onEnter={() => {
                                this.updateCocDetails()
                            }}
                            />
                    </FormGroup>

                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocCompanyName.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocCompanyName.helperText}
                        >

                        <FormInput
                            value={this.state.cocCompany ? this.state.cocCompany.companyName : ''}
                            type="text"
                            disabled={true}
                            />
                    </FormGroup>

                    <div className="inline">
                        <FormGroup
                            label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocAddress.label}
                            helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.cocAddress.helperText}
                            >

                            <FormInput
                                value={this.state.cocCompany ? this.state.cocCompany.addressLine : ''}
                                type="text"
                                disabled={true}
                                />
                        </FormGroup>

                        <FormGroup
                            label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.postalCode.label}
                            helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.coc.postalCode.helperText}
                            >

                            <FormInput
                                value={this.state.cocCompany ? this.state.cocCompany.postalCode.postalCode : ''}
                                type="text"
                                disabled={true}
                                />
                        </FormGroup>
                    </div>

                    <WizardContentOutro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.coc.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.coc.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.ROOF_SPECIALIST_REGISTER,
                    Route.ROOF_SPECIALIST_REGISTER_COC
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.ROOF_SPECIALIST_REGISTER)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>

                    <Button color="orange" disabled={!this.state.validated} onClick={() => {
                        this.props.navigate(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Updates the coc details
     */
    private updateCocDetails = () => {
        if (!this.state.validated) {
            return
        }
        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.getCocDetails.loader)
        Backend.controllers.roofSpecialist.getCocDetails(this.state.cocNumber as string).then((response) => {
            this.setState({
                cocCompany: response.data
            }, () => {
                this.saveStateInStorage()
            })
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.roofSpecialist.getCocDetails.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
            this.setState({
                cocCompany: null
            }, () => {
                this.saveStateInStorage()
            })
        })
    }

    /**
     * Saves the state in storage
     */
    private saveStateInStorage = () => {
        Storage.data.page.registerAsRoofSpecialist.coc = this.state
        Storage.update.next()

        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }
}