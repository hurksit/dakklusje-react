import React from 'react'
import { RegisterAsRoofSpecialistRoute } from '../RegisterAsRoofSpecialistPage'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../translations/Translations'
import WizardContentOutro from '../../../../components/wizard/content/outro/WizardContentOutro'
import FormGroup from '../../../../components/form/group/FormGroup'
import WizardBottom from '../../../../components/wizard/bottom/WizardBottom'
import Button from '../../../../components/button/Button'
import { Route } from '../../../../enumeration/Route'
import Storage from '../../../../storage/Storage'
import FormValidation from '../../../../core/FormValidation'
import FormInput from '../../../../components/form/input/FormInput'

/**
 * The props
 */
interface RegisterAsRoofSpecialistContactPersonProps {

    /**
     * Navigate to a route
     */
    navigate: (route: RegisterAsRoofSpecialistRoute) => void

    /**
     * Check whether a tab is disabled.
     */
    isTabDisabled: (route: RegisterAsRoofSpecialistRoute) => boolean
}

/**
 * The state
 */
export interface RegisterAsRoofSpecialistContactPersonState {

    /**
     * The full name of contact person
     */
    fullName: string|null

    /**
     * Contact person phone number
     */
    phoneNumber: string|null

    /**
     * Whether or not the form is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The contact person tab in the `RegisterAsRoofSpecialistPage`.
 * 
 * @author Stan Hurks
 */
export default class RegisterAsRoofSpecialistContactPerson extends React.Component<RegisterAsRoofSpecialistContactPersonProps, RegisterAsRoofSpecialistContactPersonState> {

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.registerAsRoofSpecialist.contactPerson,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            })
        }
    }

    public render = () => {
        return (
            <div className="register-as-roof-specialist-contact-person">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.contactPerson.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.contactPerson.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.name.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.name.helperText}
                        validation={{
                            ...FormValidation.entity.user.fullName,
                            evaluateValue: () => {
                                return this.state.fullName
                            }
                        }}>

                        <FormInput
                            value={this.state.fullName}
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.name.placeholder}
                            type="text"
                            onChange={(fullName) => {
                                this.setState({
                                    fullName
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            />
                    </FormGroup>

                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.phoneNumber.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.phoneNumber.helperText}
                        validation={{
                            ...FormValidation.entity.roofSpecialist.phoneNumber,
                            evaluateValue: () => {
                                return this.state.phoneNumber
                            }
                        }}>

                        <FormInput
                            value={this.state.phoneNumber}
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.contactPerson.phoneNumber.placeholder}
                            type="text"
                            onChange={(phoneNumber) => {
                                this.setState({
                                    phoneNumber
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            />
                    </FormGroup>

                    <WizardContentOutro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.contactPerson.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.contactPerson.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.ROOF_SPECIALIST_REGISTER,
                    Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.ROOF_SPECIALIST_REGISTER_COC)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>

                    <Button color="orange" disabled={!this.state.validated} onClick={() => {
                        this.props.navigate(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Saves the state in storage
     */
    private saveStateInStorage = () => {
        Storage.data.page.registerAsRoofSpecialist.contactPerson = this.state
        Storage.update.next()

        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }
}