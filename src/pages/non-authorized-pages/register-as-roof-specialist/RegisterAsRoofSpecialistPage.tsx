import React from 'react'
import Hero from '../../../components/hero/Hero'
import Wizard from '../../../components/wizard/Wizard'
import WizardTop from '../../../components/wizard/top/WizardTop'
import Translations from '../../../translations/Translations'
import WizardTabs from '../../../components/wizard/tabs/WizardTabs'
import WizardTab from '../../../components/wizard/tabs/tab/WizardTab'
import { Route } from '../../../enumeration/Route'
import { Subscription } from 'rxjs'
import Storage from '../../../storage/Storage'
import App from '../../../core/app/App'
import Routes from '../../../routes/Routes'
import Window from '../../../shared/Window'
import Footer from '../../../components/footer/Footer'
import './register-as-roof-specialist-page.scss'
import RegisterAsRoofSpecialistCompanyDetails from './company-details/RegisterAsRoofSpecialistCompanyDetails'
import RegisterAsRoofSpecialistCoc from './coc/RegisterAsRoofSpecialistCoc'
import RegisterAsRoofSpecialistContactPerson from './contact-person/RegisterAsRoofSpecialistContactPerson'
import RegisterAsRoofSpecialistLoginDetails from './login-details/RegisterAsRoofSpecialistLoginDetails'
import FormValidation from '../../../core/FormValidation'
import Loader from '../../../components/loader/Loader';
import Backend from '../../../backend/Backend'
import Modal from '../../../components/modal/Modal'
import { Branche } from '../../../backend/enumeration/Branche'
import StoragePageRegisterAsRoofSpecialistDataModel from '../../../storage/data-model/register-as-roof-specialist/StoragePageRegisterAsRoofSpecialistDataModel'
import HttpResponse from '../../../shared/backend/HttpResponse'
import RoofSpecialistRegisterResponse from '../../../backend/controller/roofspecialist/response/RoofSpecialistRegisterResponse'
import RegistrationCompleteRoofSpecialistModal from '../../../components/modal/registration-complete/roof-specialist/RegistrationCompleteRoofSpecialistModal'

/**
 * The routes that belong to this page.
 */
export type RegisterAsRoofSpecialistRoute = Route.ROOF_SPECIALIST_REGISTER
    | Route.ROOF_SPECIALIST_REGISTER_COC
    | Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON
    | Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS

/**
 * The props
 */
interface RegisterAsRoofSpecialistPageProps {

    /**
     * The active tab in the profile page
     */
    route: RegisterAsRoofSpecialistRoute

    /**
     * Whether or not to navigate to the unlocked tab.
     * 
     * This is used when navigated to this page from any non related (`RegisterAsRoofSpecialistRoute`) page.
     */
    navigateToLastUnlockedTab?: boolean
}


/**
 * The state
 */
interface RegisterAsRoofSpecialistPageState {

    /**
     * The active tab in the profile page
     */
    route: RegisterAsRoofSpecialistRoute
}

/**
 * The page to register as roof specialist.
 * 
 * @author Stan Hurks
 */
export default class RegisterAsRoofSpecialistPage extends React.Component<RegisterAsRoofSpecialistPageProps, RegisterAsRoofSpecialistPageState> {
    
    /**
     * The subscription for when the storage updates
     */
    private subscriptionStorageUpdate!: Subscription

    /**
     * The subscription for when the user has logged in.
     */
    private subscriptionAppLogin!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            route: this.props.route
        }
    }

    public componentDidMount = () => {
        this.subscriptionStorageUpdate = Storage.update.subscribe(this.onStorageUpdate)
        this.subscriptionAppLogin = App.login.subscribe(this.onAppLogin)

        setTimeout(() => {
            if (this.props.navigateToLastUnlockedTab) {
                this.setState({
                    route: this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                        ? (
                            this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
                            ? (
                                this.validateTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                                ? Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS
                                : Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON
                            )
                            : Route.ROOF_SPECIALIST_REGISTER_COC
                        )
                        : Route.ROOF_SPECIALIST_REGISTER
                }, () => {
                    this.initializeRoute()
                })
            } else {
                this.initializeRoute()
            }
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionStorageUpdate.unsubscribe()
        this.subscriptionAppLogin.unsubscribe()
    }

    public render = () => {
        return (
            <div id="register-as-roof-specialist-page">
                <Hero id="register-as-roof-specialist-page-hero" absolute height={400} />

                <Wizard>
                    <WizardTop
                        percentage={this.getPercentage()}
                        title={Translations.translations.pages.registerAsRoofSpecialist.wizard.top.title}>
                    </WizardTop>

                    <WizardTabs>
                        <WizardTab
                            badge={this.getBadgeForTab(Route.ROOF_SPECIALIST_REGISTER)}
                            active={this.state.route === Route.ROOF_SPECIALIST_REGISTER}
                            onClick={() => {
                                this.navigate(Route.ROOF_SPECIALIST_REGISTER)
                            }}
                            disabled={this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER)}
                            >
                            {
                                Translations.translations.pages.registerAsRoofSpecialist.wizard.tabs.companyDetails.title
                            }
                        </WizardTab>
                        <WizardTab
                            badge={this.getBadgeForTab(Route.ROOF_SPECIALIST_REGISTER_COC)}
                            active={this.state.route === Route.ROOF_SPECIALIST_REGISTER_COC}
                            onClick={() => {
                                this.navigate(Route.ROOF_SPECIALIST_REGISTER_COC)
                            }}
                            disabled={this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_COC)}
                            >
                            {
                                Translations.translations.pages.registerAsRoofSpecialist.wizard.tabs.coc.title
                            }
                        </WizardTab>
                        <WizardTab
                            badge={this.getBadgeForTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)}
                            active={this.state.route === Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON}
                            onClick={() => {
                                this.navigate(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                            }}
                            disabled={this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)}
                            >
                            {
                                Translations.translations.pages.registerAsRoofSpecialist.wizard.tabs.contactPerson.title
                            }
                        </WizardTab>
                        <WizardTab
                            badge={this.getBadgeForTab(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)}
                            active={this.state.route === Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS}
                            onClick={() => {
                                this.navigate(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)
                            }}
                            disabled={this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)}
                            >
                            {
                                Translations.translations.pages.registerAsRoofSpecialist.wizard.tabs.loginDetails.title
                            }
                        </WizardTab>
                    </WizardTabs>

                    {
                        this.state.route === Route.ROOF_SPECIALIST_REGISTER
                        &&
                        <RegisterAsRoofSpecialistCompanyDetails isTabDisabled={this.isTabDisabled} navigate={this.navigate} />
                    }

                    {
                        this.state.route === Route.ROOF_SPECIALIST_REGISTER_COC
                        &&
                        <RegisterAsRoofSpecialistCoc isTabDisabled={this.isTabDisabled} navigate={this.navigate} />
                    }

                    {
                        this.state.route === Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON
                        &&
                        <RegisterAsRoofSpecialistContactPerson isTabDisabled={this.isTabDisabled} navigate={this.navigate} />
                    }

                    {
                        this.state.route === Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS
                        &&
                        <RegisterAsRoofSpecialistLoginDetails isTabDisabled={this.isTabDisabled} navigate={this.navigate} onFinish={this.onFinish} />
                    }
                </Wizard>

                <Footer />
            </div>
        )
    }

    /**
     * Whenever the storage is being updated
     */
    private onStorageUpdate = () => {
        setTimeout(() => {
            // Second timeout due to there also being a timeout in Storage.
            setTimeout(() => {

                this.forceUpdate()
            })
        })
    }

    /**
     * Whenever the user logs in to the app
     */
    private onAppLogin = () => {
        Routes.setCurrentRoute.next(Route.HOME)
    }

    /**
     * Initialize the route
     */
    private initializeRoute = () => {
        switch (this.state.route) {
            case Route.ROOF_SPECIALIST_REGISTER:
                Routes.setCurrentRoute.next(Route.ROOF_SPECIALIST_REGISTER)
                break
            case Route.ROOF_SPECIALIST_REGISTER_COC:
                if (!this.validateTab(Route.ROOF_SPECIALIST_REGISTER)) {
                    Routes.redirectURL.next(Route.ROOF_SPECIALIST_REGISTER)
                } else {
                    Routes.setCurrentRoute.next(Route.ROOF_SPECIALIST_REGISTER_COC)
                }
                break
            case Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON:
                if (!this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC) || !this.validateTab(Route.ROOF_SPECIALIST_REGISTER)) {
                    Routes.redirectURL.next(Route.ROOF_SPECIALIST_REGISTER_COC)
                } else {
                    Routes.setCurrentRoute.next(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                }
                break
            case Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS:
                if (!this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
                    || !this.validateTab(Route.ROOF_SPECIALIST_REGISTER) 
                    || !this.validateTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)) {

                    Routes.redirectURL.next(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                } else {
                    Routes.setCurrentRoute.next(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)
                }
                break
        }
    }

    /**
     * Navigate to a route
     */
    private navigate = (route: RegisterAsRoofSpecialistRoute) => {
        if (this.isTabDisabled(route)) {
            return
        }

        const el = document.querySelector('.wizard') as HTMLElement
        if (el) {
            Window.scrollTo({
                top: el.getBoundingClientRect().top + window.scrollY,
                behavior: 'smooth'
            }, () => {
                this.setState({
                    route
                })
                Routes.setCurrentRoute.next(route)
            })
        } else {
            this.setState({
                route
            })
            Routes.setCurrentRoute.next(route)
        }
    }

    /**
     * Get the percentage in the wizard top
     */
    private getPercentage = () => {
        if (this.validateTab(Route.ROOF_SPECIALIST_REGISTER)) {
            if (this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)) {
                if (this.validateTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)) {
                    if (this.validateTab(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)) {
                        return 100
                    } else {
                        return 75
                    }
                } else {
                    return 50
                }
            } else {
                return 25
            }
        } else {
            return 0
        }
    }

    /**
     * Get the badge for a tab
     */
    private getBadgeForTab = (route: RegisterAsRoofSpecialistRoute): 1|2|3|4|'done' => {
        switch (route) {
            case Route.ROOF_SPECIALIST_REGISTER:
                return this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                    ? 'done'
                    : 1
            case Route.ROOF_SPECIALIST_REGISTER_COC:
                return this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                    && this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
                    ? 'done'
                    : 2
            case Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON:
                return this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                    && this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
                    && this.validateTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                    ? 'done'
                    : 3
            case Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS:
                return this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                    && this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
                    && this.validateTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                    && this.validateTab(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)
                    ? 'done'
                    : 4
        }
    }

    /**
     * Checks whether a tab is disabled
     */
    private isTabDisabled = (route: RegisterAsRoofSpecialistRoute): boolean => {
        switch (route) {
            case Route.ROOF_SPECIALIST_REGISTER:
                return false
            case Route.ROOF_SPECIALIST_REGISTER_COC:
                return !this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
            case Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON:
                return !this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                    || !this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
            case Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS:
                return !this.validateTab(Route.ROOF_SPECIALIST_REGISTER)
                    || !this.validateTab(Route.ROOF_SPECIALIST_REGISTER_COC)
                    || !this.validateTab(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
        }
        return false
    }

    /**
     * Validates a tab.
     */
    private validateTab = (route: RegisterAsRoofSpecialistRoute): boolean => {
        FormValidation.stash()

        switch (route) {
            case Route.ROOF_SPECIALIST_REGISTER:
                FormValidation.addField({
                    ...FormValidation.entity.roofSpecialist.branche,
                    evaluateValue: () => {
                        return Storage.data.page.registerAsRoofSpecialist.companyDetails.branche
                    },
                    label: 'branche'
                })
                FormValidation.addField({
                    ...FormValidation.entity.roofSpecialist.companyDescription,
                    evaluateValue: () => {
                        return Storage.data.page.registerAsRoofSpecialist.companyDetails.companyDescription
                    },
                    label: 'companyDescription'
                })
                return FormValidation.validateAndUnstash()
            case Route.ROOF_SPECIALIST_REGISTER_COC:
                FormValidation.unstash()
                return Storage.data.page.registerAsRoofSpecialist.coc.cocCompany !== null
            case Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON:
                FormValidation.addField({
                    ...FormValidation.entity.user.fullName,
                    evaluateValue: () => {
                        return Storage.data.page.registerAsRoofSpecialist.contactPerson.fullName
                    },
                    label: 'fullName'
                })
                FormValidation.addField({
                    ...FormValidation.entity.roofSpecialist.phoneNumber,
                    evaluateValue: () => {
                        return Storage.data.page.registerAsRoofSpecialist.contactPerson.phoneNumber
                    },
                    label: 'phoneNumber'
                })
                return FormValidation.validateAndUnstash()
            case Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS:
                FormValidation.addField({
                    ...FormValidation.entity.user.email,
                    evaluateValue: () => {
                        return Storage.data.page.registerAsRoofSpecialist.loginDetails.emailAddress
                    },
                    label: 'email'
                })
                FormValidation.addField({
                    ...FormValidation.entity.user.password,
                    evaluateValue: () => {
                        return RegisterAsRoofSpecialistLoginDetails.lastUsedPassword
                    },
                    label: 'password'
                })
                FormValidation.addField({
                    string: {
                        matchEvaluatedValue: () => {
                            return RegisterAsRoofSpecialistLoginDetails.lastUsedPassword || ''
                        }
                    },
                    evaluateValue: () => {
                        return RegisterAsRoofSpecialistLoginDetails.lastUsedPasswordRepeat || ''
                    },
                    label: 'passwordRepeat'
                })
                FormValidation.addField({
                    mustBeTruthy: true,
                    evaluateValue: () => {
                        return Storage.data.page.registerAsRoofSpecialist.loginDetails.acceptedTermsAndConditions
                    },
                    label: 'acceptedTermsAndConditions'
                })
                return FormValidation.validateAndUnstash()
        }

        FormValidation.unstash()
        return false
    }

    /**
     * Finish the registration as roof specialist
     */
    private onFinish = () => {
        // Validate
        if (this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)) {
            return
        } else if (this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)) {
            this.navigate(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
            return
        } else if (this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_COC)) {
            this.navigate(Route.ROOF_SPECIALIST_REGISTER_COC)
            return
        } else if (this.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER)) {
            this.navigate(Route.ROOF_SPECIALIST_REGISTER)
            return
        }

        Loader.set.next(Translations.translations.backend.controllers.roofSpecialist.register.loader)
        Backend.controllers.roofSpecialist.register({
            companyDetails: {
                branche: Storage.data.page.registerAsRoofSpecialist.companyDetails.branche as Branche,
                description: Storage.data.page.registerAsRoofSpecialist.companyDetails.companyDescription === ''
                    ? null
                    : Storage.data.page.registerAsRoofSpecialist.companyDetails.companyDescription
            },
            cocNumber: Storage.data.page.registerAsRoofSpecialist.coc.cocNumber as string,
            personalDetails: {
                fullName: Storage.data.page.registerAsRoofSpecialist.contactPerson.fullName as string,
                phoneNumber: Storage.data.page.registerAsRoofSpecialist.contactPerson.phoneNumber as string
            },
            loginDetails: {
                emailAddress: Storage.data.page.registerAsRoofSpecialist.loginDetails.emailAddress as string,
                password: RegisterAsRoofSpecialistLoginDetails.lastUsedPassword as string,
                acceptedTermsAndConditions: Storage.data.page.registerAsRoofSpecialist.loginDetails.acceptedTermsAndConditions
            }
        }).then(() => {
            Storage.data.page.registerAsRoofSpecialist = new StoragePageRegisterAsRoofSpecialistDataModel()
            Storage.update.next()
            Routes.redirectURL.next(Route.HOME)
            Modal.mount.next({
                element: (
                    <RegistrationCompleteRoofSpecialistModal />
                ),
                dismissable: false
            })
        }).catch((response: HttpResponse<RoofSpecialistRegisterResponse>) => {
            const translation = Translations.translations.backend.controllers.roofSpecialist.register.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
            if (response.status === 400) {
                if (response.data.companyDetailsValidated === false) {
                    this.navigate(Route.ROOF_SPECIALIST_REGISTER)
                } else if (response.data.cocValidated === false) {
                    this.navigate(Route.ROOF_SPECIALIST_REGISTER_COC)
                } else if (response.data.contactPersonValidated === false) {
                    this.navigate(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                } else if (response.data.loginDetailsValidated === false) {
                    this.navigate(Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS)
                }
            }
        })
    }
}