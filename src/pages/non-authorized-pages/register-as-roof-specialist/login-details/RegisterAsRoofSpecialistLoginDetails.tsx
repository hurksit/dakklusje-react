import React from 'react'
import Storage from '../../../../storage/Storage'
import FormValidation from '../../../../core/FormValidation'
import { RegisterAsRoofSpecialistRoute } from '../RegisterAsRoofSpecialistPage'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../translations/Translations'
import WizardContentOutro from '../../../../components/wizard/content/outro/WizardContentOutro'
import WizardBottom from '../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../enumeration/Route'
import Button from '../../../../components/button/Button'
import FormGroup from '../../../../components/form/group/FormGroup'
import FormInput from '../../../../components/form/input/FormInput'
import FormCheckbox from '../../../../components/form/checkbox/FormCheckbox'

/**
 * The props
 */
interface RegisterAsRoofSpecialistLoginDetailsProps {

    /**
     * Navigate to a route
     */
    navigate: (route: RegisterAsRoofSpecialistRoute) => void

    /**
     * Check whether a tab is disabled.
     */
    isTabDisabled: (route: RegisterAsRoofSpecialistRoute) => boolean

    /**
     * The callback to perform upon finishing the wizards form.
     */
    onFinish: () => void
}

/**
 * The state
 */
export interface RegisterAsRoofSpecialistLoginDetailsState {

    /**
     * The emailaddress
     */
    emailAddress: string|null

    /**
     * The password
     */
    password: string|null

    /**
     * The repeated password
     */
    passwordRepeat: string|null

    /**
     * Whether or not the user accepted the terms and conditions
     */
    acceptedTermsAndConditions: boolean

    /**
     * Whether or not the form is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The login details tab in the `RegisterAsRoofSpecialistPage`.
 * 
 * @author Stan Hurks
 */
export default class RegisterAsRoofSpecialistLoginDetails extends React.Component<RegisterAsRoofSpecialistLoginDetailsProps, RegisterAsRoofSpecialistLoginDetailsState> {

    /**
     * The last used password.
     * 
     * This must be stored here instead of `Storage`, due to that being unsafe.
     * 
     * If the user navigates through the wizard it would be annoying to re-type
     * the passwords every time.
     * 
     * This will make sure though that once the tab is closed in the browser
     * that data is erased.
     */
    private static _lastUsedPassword: string|null = null

    /**
     * Get the last used password
     */
    public static get lastUsedPassword() {
        return RegisterAsRoofSpecialistLoginDetails._lastUsedPassword
    }

    /**
     * The last used passwordRepeat.
     * 
     * This must be stored here instead of `Storage`, due to that being unsafe.
     * 
     * If the user navigates through the wizard it would be annoying to re-type
     * the passwords every time.
     * 
     * This will make sure though that once the tab is closed in the browser
     * that data is erased.
     */
    private static _lastUsedPasswordRepeat: string|null = null

    /**
     * Get the last used passwordRepeat
     */
    public static get lastUsedPasswordRepeat() {
        return RegisterAsRoofSpecialistLoginDetails._lastUsedPasswordRepeat
    }

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.registerAsRoofSpecialist.loginDetails,
            password: RegisterAsRoofSpecialistLoginDetails.lastUsedPassword,
            passwordRepeat: RegisterAsRoofSpecialistLoginDetails.lastUsedPasswordRepeat,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            })
        }
    }

    public render = () => {
        return (
            <div className="register-as-roof-specialist-login-details">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.loginDetails.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.loginDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.emailAddress.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.emailAddress.helperText}
                        validation={{
                            ...FormValidation.entity.user.email,
                            evaluateValue: () => {
                                return this.state.emailAddress
                            }
                        }}>

                        <FormInput
                            value={this.state.emailAddress}
                            type="email"
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.emailAddress.placeholder}
                            onChange={(emailAddress) => {
                                this.setState({
                                    emailAddress
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            />
                    </FormGroup>

                    <div className="inline">
                        <FormGroup
                            label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.password.label}
                            helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.password.helperText}
                            validation={{
                                ...FormValidation.entity.user.password,
                                evaluateValue: () => {
                                    return this.state.password
                                }
                            }}>

                            <FormInput
                                value={this.state.password}
                                type="password"
                                placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.password.placeholder}
                                onChange={(password) => {
                                    this.setState({
                                        password
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }}
                                />
                        </FormGroup>

                        <FormGroup
                            label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.passwordRepeat.label}
                            helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.passwordRepeat.helperText}
                            validation={{
                                ...FormValidation.entity.user.password,
                                string: {
                                    allowEmpty: true,
                                    matchEvaluatedValue: () => {
                                        return this.state.password || ''
                                    }
                                },
                                evaluateValue: () => {
                                    return this.state.passwordRepeat
                                }
                            }}>

                            <FormInput
                                value={this.state.passwordRepeat}
                                type="password"
                                placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.passwordRepeat.placeholder}
                                onChange={(passwordRepeat) => {
                                    this.setState({
                                        passwordRepeat
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }}
                                />
                        </FormGroup>
                    </div>

                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.termsAndAgreements.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.termsAndAgreements.helperText}
                        validation={{
                            mustBeTruthy: true,
                            evaluateValue: () => {
                                return this.state.acceptedTermsAndConditions
                            }
                        }}>

                        <FormCheckbox
                            label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.loginDetails.termsAndAgreements.checkbox.label}
                            value={this.state.acceptedTermsAndConditions}
                            onChange={(acceptedTermsAndConditions) => {
                                this.setState({
                                    acceptedTermsAndConditions
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            />
                    </FormGroup>
                    
                    <WizardContentOutro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.loginDetails.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.loginDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part === ''
                                        ? (
                                            <br />
                                        )
                                        : part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.ROOF_SPECIALIST_REGISTER,
                    Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>

                    <Button color="orange" disabled={!this.state.validated} onClick={() => {
                        this.props.onFinish()
                    }}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.bottom.loginDetails.finish
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Saves the state in storage
     */
    private saveStateInStorage = () => {
        RegisterAsRoofSpecialistLoginDetails._lastUsedPassword = this.state.password
        RegisterAsRoofSpecialistLoginDetails._lastUsedPasswordRepeat = this.state.passwordRepeat
        Storage.data.page.registerAsRoofSpecialist.loginDetails = this.state
        Storage.update.next()

        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }
}