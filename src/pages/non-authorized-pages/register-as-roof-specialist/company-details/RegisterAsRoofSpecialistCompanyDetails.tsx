import React from 'react'
import WizardContent from '../../../../components/wizard/content/WizardContent'
import WizardContentIntro from '../../../../components/wizard/content/intro/WizardContentIntro'
import WizardContentOutro from '../../../../components/wizard/content/outro/WizardContentOutro'
import Translations from '../../../../translations/Translations'
import WizardBottom from '../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../enumeration/Route'
import Button from '../../../../components/button/Button'
import { RegisterAsRoofSpecialistRoute } from '../RegisterAsRoofSpecialistPage'
import Storage from '../../../../storage/Storage'
import FormValidation from '../../../../core/FormValidation'
import { Branche } from '../../../../backend/enumeration/Branche'
import FormGroup from '../../../../components/form/group/FormGroup'
import FormSelect from '../../../../components/form/select/FormSelect'
import FormInput from '../../../../components/form/input/FormInput'

/**
 * The props
 */
interface RegisterAsRoofSpecialistCompanyDetailsProps {

    /**
     * Navigate to a route
     */
    navigate: (route: RegisterAsRoofSpecialistRoute) => void

    /**
     * Check whether a tab is disabled.
     */
    isTabDisabled: (route: RegisterAsRoofSpecialistRoute) => boolean
}

/**
 * The state
 */
export interface RegisterAsRoofSpecialistCompanyDetailsState {
    
    /**
     * The branche
     */
    branche: Branche|null

    /**
     * The company description
     */
    companyDescription: string|null

    /**
     * Whether or not the form is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The company details tab in the `RegisterAsRoofSpecialistPage`.
 * 
 * @author Stan Hurks
 */
export default class RegisterAsRoofSpecialistCompanyDetails extends React.Component<RegisterAsRoofSpecialistCompanyDetailsProps, RegisterAsRoofSpecialistCompanyDetailsState> {

    constructor(props: any) {
        super(props)
        
        this.state = {
            ...Storage.data.page.registerAsRoofSpecialist.companyDetails,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            })
        }
    }

    public render = () => {
        return (
            <div className="register-as-roof-specialist-company-details">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.companyDetails.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.intro.companyDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {/* The branche */}
                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.branche.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.branche.helperText}
                        validation={{
                            ...FormValidation.entity.roofSpecialist.branche,
                            evaluateValue: () => {
                                return this.state.branche
                            }
                        }}
                        >

                        <FormSelect
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.branche.placeholder}
                            value={this.state.branche}
                            options={Object.keys(Branche).map((key) => ({
                                key,
                                value: Translations.translations.backend.enumerations.branche(key as Branche)
                            }))}
                            onChange={(branche) => {
                                this.setState({
                                    branche
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            />
                    </FormGroup>

                    {/* The description */}
                    <FormGroup
                        label={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.companyDescription.label}
                        helperText={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.companyDescription.helperText}
                        validation={{
                            ...FormValidation.entity.roofSpecialist.companyDescription,
                            evaluateValue: () => {
                                return this.state.companyDescription
                            }
                        }}
                        >

                        <FormInput
                            type="multiline"
                            value={this.state.companyDescription}
                            placeholder={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.form.companyDetails.companyDescription.placeholder}
                            onChange={(companyDescription) => {
                                this.setState({
                                    companyDescription
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            />
                    </FormGroup>

                    <WizardContentOutro title={Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.companyDetails.title}>
                        {
                            Translations.translations.pages.registerAsRoofSpecialist.wizard.content.outro.companyDetails.description.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>

                <WizardBottom path={[
                    Route.HOME,
                    Route.ROOF_SPECIALIST_REGISTER
                ]}>
                    <Button color="orange" disabled={this.props.isTabDisabled(Route.ROOF_SPECIALIST_REGISTER_COC) || !this.state.validated} onClick={() => {
                        this.props.navigate(Route.ROOF_SPECIALIST_REGISTER_COC)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </div>
        )
    }

    /**
     * Saves the state in storage
     */
    private saveStateInStorage = () => {
        Storage.data.page.registerAsRoofSpecialist.companyDetails = this.state
        Storage.update.next()

        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }
}