import React from 'react'
import Hero from '../../../components/hero/Hero'
import PageSection from '../../../components/page/section/PageSection'
import PageSectionTitle from '../../../components/page/section/title/PageSectionTitle'
import PageSectionContent from '../../../components/page/section/content/PageSectionContent'
import ExperienceFromOthers from '../../../components/experience-from-others/ExperienceFromOthers'
import Footer from '../../../components/footer/Footer'
import Step from '../../../components/step/Step'
import Translations from '../../../translations/Translations'
import { ArrowForward } from '@material-ui/icons'
import './about-us-page.scss'
import Routes from '../../../routes/Routes'
import { Route } from '../../../enumeration/Route'
import Breadcrumbs from '../../../components/breadcrumbs/Breadcrumbs'
import AppInfo from '../../../core/AppInfo'

/**
 * The about us page.
 * 
 * @author Stan Hurks
 */
export default class AboutUsPage extends React.Component {

    public componentDidMount = () => {
        setTimeout(() => {
            Routes.setCurrentRoute.next(Route.ABOUT_US)
        })
    }

    public render = () => {
        return (
            <div id="about-us-page">
                <Hero id="about-us-page-hero" height={230} />

                <PageSection color="light-grey" id="about-us-page-steps">
                    <Breadcrumbs path={[
                        Route.HOME,
                        Route.ABOUT_US
                    ]} />
                    <PageSectionTitle>
                        {
                            Translations.translations.document.title.route(Route.ABOUT_US)
                        }
                    </PageSectionTitle><PageSectionContent>
                        <Step title={AppInfo.designedAndBuildBy.companyName}>
                            <b>
                                {
                                    Translations.translations.components.footer.bottom.address.title
                                }
                            </b><p>
                                {
                                    Translations.translations.components.footer.bottom.address.content[0]
                                }<br/>
                                {
                                    Translations.translations.components.footer.bottom.address.content[1]
                                }<br/>
                                {
                                    Translations.translations.components.footer.bottom.address.content[2]
                                }
                            </p><b>
                                {
                                    Translations.translations.components.footer.bottom.contact.title
                                }    
                            </b><p>
                                {
                                    Translations.translations.components.footer.bottom.contact.content.email
                                }: <a href={`mailto://${AppInfo.designedAndBuildBy.emailAddress}`}>
                                    {
                                        AppInfo.designedAndBuildBy.emailAddress
                                    }
                                </a><br/>

                                {
                                    Translations.translations.components.footer.bottom.contact.content.phoneNumber
                                }: <a href={AppInfo.designedAndBuildBy.phoneNumberLink}>
                                    {AppInfo.designedAndBuildBy.phoneNumber}
                                </a>
                            </p><b>
                                {
                                    Translations.translations.components.footer.bottom.coc.title
                                }
                            </b><p>
                                {
                                    AppInfo.designedAndBuildBy.cocNumber
                                }
                            </p>
                        </Step><div className="icon">
                            <ArrowForward />
                        </div><Step title={'Stan Hurks'} index={1}>
                            {
                                Translations.translations.pages.aboutUs.steps[2]
                            }                        
                        </Step>
                    </PageSectionContent>
                </PageSection>

                <ExperienceFromOthers />

                <Footer />
            </div>
        )
    }
}