import React from 'react'
import Routes from '../../../routes/Routes'
import { Route } from '../../../enumeration/Route'
import Footer from '../../../components/footer/Footer'
import Hero from '../../../components/hero/Hero'
import Button from '../../../components/button/Button'
import './how-does-it-work-page.scss'
import Breadcrumbs from '../../../components/breadcrumbs/Breadcrumbs'
import classNames from 'classnames'
import Slider from '../../../components/slider/Slider'
import { Subject, Subscription } from 'rxjs'
import Translations from '../../../translations/Translations'

/**
 * The props
 */
interface HowDoesItWorkPageProps {
    
    /**
     * The initial view index value
     */
    initialViewIndex?: number
}

/**
 * The state
 */
interface HowDoesItWorkPageState {

    /**
     * The vertical index of the view
     */
    viewIndex: number

    /**
     * The index from which the view is changing
     */
    fromIndex: number|null

    /**
     * The index to which the view is changing
     */
    toIndex: number|null
}

/**
 * The how does it work page.
 * 
 * @author Stan Hurks
 */
export default class HowDoesItWorkPage extends React.Component<HowDoesItWorkPageProps, HowDoesItWorkPageState> {

    /**
     * Change the index of the how does it work page based on the route
     */
    public static readonly changeIndex: Subject<Route> = new Subject()

    /**
     * The amount of MS it takes to perform a full animation
     */
    private static readonly animationTimeMS: number = 500

    /**
     * The internal subscription to the changeIndex subject
     */
    private subscriptionChangeIndex!: Subscription

    /**
     * Renders the actors view
     */
    private actors: JSX.Element = (
        <div id="how-does-it-work-page-container-actors">
            <h1>
                {
                    Translations.translations.pages.howDoesItWorkPage.actors.title
                }
            </h1>
            <div id="how-does-it-work-page-container-actors-options">
                <Button color="purple" onClick={() => {
                    this.goToIndex(0)
                }}>
                    {
                        Translations.translations.pages.howDoesItWorkPage.actors.buttons.consumer
                    }
                </Button><Button color="orange" onClick={() => {
                    this.goToIndex(2)
                }}>
                    {
                        Translations.translations.pages.howDoesItWorkPage.actors.buttons.roofSpecialist
                    }
                </Button>
            </div>
        </div>
    )

    /**
     * The roof specialists view
     */
    private roofSpecialist: JSX.Element = (
        <div id="how-does-it-work-page-roof-specialist">
            <Slider slides={Translations.translations.pages.howDoesItWorkPage.roofSpecialist.slides} />
        </div>
    )

    /**
     * The consumer view
     */
    private consumer: JSX.Element = (
        <div id="how-does-it-work-page-consumer">
            <Slider slides={Translations.translations.pages.howDoesItWorkPage.consumer.slides} />
        </div>
    )

    constructor(props: any) {
        super(props)

        this.state = {
            viewIndex: this.props.initialViewIndex === undefined ? 1 : this.props.initialViewIndex,

            fromIndex: null,

            toIndex: null
        }
    }

    public componentDidMount = () => {
        this.subscriptionChangeIndex = HowDoesItWorkPage.changeIndex.subscribe(this.onChangeIndex)

        setTimeout(() => {
            Routes.setCurrentRoute.next(
                this.state.viewIndex === 0
                ? Route.HOW_DOES_IT_WORK_CONSUMER
                : (
                    this.state.viewIndex === 1
                    ? Route.HOW_DOES_IT_WORK
                    : Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST
                )
            )
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionChangeIndex.unsubscribe()
    }

    public render = () => {
        return (
            <div id="how-does-it-work-page" className={classNames({
                'white-breadcrumbs': this.state.viewIndex !== 1
            })}>
                <Hero height={100} />

                <div id="how-does-it-work-page-container" className={classNames({
                    'animation-cube': true,
                    'animation-cube-left-to-right': this.state.toIndex !== null && this.state.fromIndex !== null
                        && this.state.toIndex < this.state.fromIndex,
                    'animation-cube-right-to-left': this.state.toIndex !== null && this.state.fromIndex !== null
                        && this.state.toIndex > this.state.fromIndex
                })}>
                    <Breadcrumbs path={[
                        Route.HOME,
                        Route.HOW_DOES_IT_WORK
                    ].concat(
                        this.state.viewIndex === 0
                        ? [Route.HOW_DOES_IT_WORK_CONSUMER]
                        : (
                            this.state.viewIndex === 2
                            ? [Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST]
                            : []
                        )
                    )} />
                    <div className="cube">
                        {
                            this.renderActiveFace()
                        }
                        {
                            this.renderInactiveFace()
                        }
                    </div>
                </div>

                <Footer />
            </div>
        )
    }

    /**
     * Renders the active face of the cube
     */
    private renderActiveFace = () => {
        return (
            <div className="active" style={{
                ...this.state.viewIndex === 0
                ? {
                    background: '#4056a1'
                }
                : {
                    ...this.state.viewIndex === 1
                    ? {
                        background: '#f7f7f7'
                    }
                    : {
                        background: '#ff5741'
                    }
                }
            }}>
                {
                    this.state.viewIndex === 0
                    ?
                    this.consumer
                    :
                    (
                        this.state.viewIndex === 1
                        ?
                        this.actors
                        :
                        this.roofSpecialist
                    )
                }
            </div>
        )
    }

    /**
     * Renders the inactive face of the cube
     */
    private renderInactiveFace = () => {
        return (
            <div style={{
                ...this.state.toIndex === 0
                ? {
                    background: '#4056a1'
                }
                : {
                    ...this.state.toIndex === 1
                    ? {
                        background: '#f7f7f7'
                    }
                    : {
                        background: '#ff5741'
                    }
                }
            }}>
                {
                    this.state.toIndex === 0
                    ?
                    this.consumer
                    :
                    (
                        this.state.toIndex === 1
                        ?
                        this.actors
                        :
                        this.roofSpecialist
                    )
                }
            </div>
        )
    }

    /**
     * Go to the given index in the cube
     */
    private goToIndex = (index: number) => {
        this.setState({
            toIndex: index,

            fromIndex: this.state.viewIndex
        }, () => {
            setTimeout(() => {
                this.setState({
                    viewIndex: index,

                    toIndex: null,

                    fromIndex: null
                }, () => {
                    Routes.setCurrentRoute.next(
                        this.state.viewIndex === 0
                        ? Route.HOW_DOES_IT_WORK_CONSUMER
                        : (
                            this.state.viewIndex === 1
                            ? Route.HOW_DOES_IT_WORK
                            : Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST
                        )
                    )
                })
            }, HowDoesItWorkPage.animationTimeMS)
        })
    }

    /**
     * Whenever the changeIndex subject is called
     */
    private onChangeIndex = (route: Route) => { 
        let index: number|null = null
        switch (route) {
            case Route.HOW_DOES_IT_WORK:
                index = 1
                break
            case Route.HOW_DOES_IT_WORK_CONSUMER:
                index = 0
                break
            case Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST:
                index = 2
                break
        }

        if (index) {
            this.goToIndex(index)
        }
    }
}