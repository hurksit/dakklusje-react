import React from 'react'
import Hero from '../../../components/hero/Hero'
import Button from '../../../components/button/Button'
import PageSection from '../../../components/page/section/PageSection'
import PageSectionTitle from '../../../components/page/section/title/PageSectionTitle'
import ExperienceFromOthers from '../../../components/experience-from-others/ExperienceFromOthers'
import Step from '../../../components/step/Step'
import PageSectionContent from '../../../components/page/section/content/PageSectionContent'
import './home-page.scss'
import {
    ArrowForward
} from '@material-ui/icons'
import Footer from '../../../components/footer/Footer'
import Translations from '../../../translations/Translations'
import { Route } from '../../../enumeration/Route'
import Routes from '../../../routes/Routes'
import { RouteComponentProps } from 'react-router'
import Modal from '../../../components/modal/Modal'
import Loader from '../../../components/loader/Loader'
import RestorePasswordModal from '../../../components/modal/restore-password/RestorePasswordModal'
import Backend from '../../../backend/Backend'
import SessionStorage from '../../../storage/session-storage/SessionStorage'
import App from '../../../core/app/App'

/**
 * The home page.
 * 
 * @author Stan Hurks
 */
export default class HomePage extends React.Component<RouteComponentProps> {

    public componentDidMount = () => {
        setTimeout(() => {
            Routes.setCurrentRoute.next(Route.HOME)

            // Password restore
            if (this.props.match.params['restorePasswordUuid']) {
                const uuid = this.props.match.params['restorePasswordUuid']

                // Reset the URL
                Routes.setCurrentRoute.next(Route.HOME)

                // Do the request
                Loader.set.next(Translations.translations.backend.controllers.profile.validateRestorePassword.loader)
                Backend.controllers.profile.validateRestorePassword(uuid).then(() => {
                    Modal.mount.next({
                        element: (
                            <RestorePasswordModal uuid={uuid} />
                        ),
                        dismissable: false
                    })
                }).catch((response) => {
                    const translation: string|undefined = Translations.translations.backend.controllers.profile.validateRestorePassword.responses[response.status]
                    if (translation) {
                        Modal.mountError.next(translation)
                    }
                })
            }

            // Activate account
            else if (this.props.match.params['activateAccountUuid']) {
                const uuid = this.props.match.params['activateAccountUuid']

                // Reset the URL
                Routes.setCurrentRoute.next(Route.HOME)

                // Do the request
                Loader.set.next(Translations.translations.backend.controllers.profile.activateAccount.loader)
                Backend.controllers.profile.activateAccount(uuid).then((response) => {
                    SessionStorage.data.session.uuid = response.data.uuid
                    
                    Modal.mountSuccess.next(Translations.translations.backend.controllers.profile.activateAccount.responses[200])

                    App.login.next(response.data)
                }).catch((response) => {
                    const translation = Translations.translations.backend.controllers.profile.activateAccount.responses[response.status]
                    if (translation !== undefined) {
                        Modal.mountError.next(translation)
                    }
                })
            }

            else if (this.props.match.params['activateEmailUuid']) {
                const uuid = this.props.match.params['activateEmailUuid']

                // Reset the URL
                Routes.setCurrentRoute.next(Route.HOME)

                // Do the request
                Loader.set.next(Translations.translations.backend.controllers.profile.activateEmail.loader)
                Backend.controllers.profile.activateEmail(uuid).then(() => {
                    Modal.mountSuccess.next(Translations.translations.backend.controllers.profile.activateEmail.responses[200])
                    App.update.next()
                }).catch((response) => {
                    const translation = Translations.translations.backend.controllers.profile.activateEmail.responses[response.status]
                    if (translation !== undefined) {
                        Modal.mountError.next(translation)
                    }
                })
            }

            // Toggle intercom
            App.toggleIntercom.next(true)
        })
    }

    public componentWillUnmount = () => {

        // Untoggle intercom
        App.toggleIntercom.next(false)
    }

    public render = () => {
        return (
            <div id="home-page-content">
                <Hero id="home-page-content-hero">
                    <h1 id="home-page-content-hero-title">
                        {
                            Translations.translations.pages.home.hero.title[0]
                        }<br/>{
                            Translations.translations.pages.home.hero.title[1]
                        }
                    </h1><h2 id="home-page-content-hero-subtitle">
                        {
                            Translations.translations.pages.home.hero.subtitle
                        }
                    </h2><div id="home-page-content-hero-buttons">
                        <Button color='white' href={Route.CONSUMER_PLACE_YOUR_JOB}>
                            {
                                Translations.translations.pages.home.hero.buttons.addJob
                            }
                        </Button><Button color='orange' href={Route.ROOF_SPECIALIST_REGISTER}>
                            {
                                Translations.translations.pages.home.hero.buttons.registerAsRoofSpecialist
                            }
                        </Button>
                    </div>
                </Hero>
                <PageSection color='light-grey' id="home-page-content-steps">
                    <PageSectionTitle>
                        {
                            Translations.translations.pages.home.steps.title
                        }
                    </PageSectionTitle><PageSectionContent>
                        <Step title={Translations.translations.pages.home.steps[1].title} index={1}>
                            {
                                Translations.translations.pages.home.steps[1].content
                            }
                        </Step><div className="icon">
                            <ArrowForward />
                        </div><Step title={Translations.translations.pages.home.steps[2].title} index={2}>
                            {
                                Translations.translations.pages.home.steps[2].content
                            }
                        </Step><div className="icon">
                            <ArrowForward />
                        </div><Step title={Translations.translations.pages.home.steps[3].title} index={3}>
                            {
                                Translations.translations.pages.home.steps[3].content
                            }
                        </Step>
                    </PageSectionContent>
                </PageSection>
                <ExperienceFromOthers />
                <Footer />
            </div>
        )
    }
}