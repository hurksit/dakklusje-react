import React from 'react'
import Routes from '../../../routes/Routes'
import { Route } from '../../../enumeration/Route'
import Hero from '../../../components/hero/Hero'
import PageSection from '../../../components/page/section/PageSection'
import Breadcrumbs from '../../../components/breadcrumbs/Breadcrumbs'
import PageSectionTitle from '../../../components/page/section/title/PageSectionTitle'
import Translations from '../../../translations/Translations'
import PageSectionContent from '../../../components/page/section/content/PageSectionContent'
import Footer from '../../../components/footer/Footer'
import Button from '../../../components/button/Button'
import Modal from '../../../components/modal/Modal'
import CalculateCostsModal from '../../../components/modal/calculate-costs/CalculateCostsModal'
import './costs-page.scss'

/**
 * The page that describes the costs for the consumer and will show the roof specialist
 * that this is not a platform that promotes the lowest cost.
 * 
 * @author Stan Hurks
 */
export default class CostsPage extends React.Component {

    public componentDidMount = () => {
        setTimeout(() => {
            Routes.setCurrentRoute.next(Route.COSTS)
        })
    }

    public render = () => {
        return (
            <div id="costs-page">
                <Hero id="costs-page-hero" height={230} />

                <PageSection color="light-grey" id="costs-page-intro">
                    <Breadcrumbs path={[
                        Route.HOME,
                        Route.COSTS
                    ]} />
                    <PageSectionTitle>
                        {
                            Translations.translations.document.title.route(Route.COSTS)
                        }
                    </PageSectionTitle>
                    <PageSectionContent>
                        {
                            Translations.translations.pages.costs.pageSectionContent
                        }
                        <p>
                            <Button color="orange" onClick={() => {
                                Modal.mount.next({
                                    element: (
                                        <CalculateCostsModal />
                                    )
                                })
                            }}>
                                {
                                    Translations.translations.pages.costs.buttons.calculateCosts
                                }
                            </Button>
                        </p>
                    </PageSectionContent>
                </PageSection>

                <Footer />
            </div>
        )
    }
}