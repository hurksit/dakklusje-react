import React from 'react'
import './add-job-page.scss'
import Hero from '../../../components/hero/Hero'
import Wizard from '../../../components/wizard/Wizard'
import WizardTabs from '../../../components/wizard/tabs/WizardTabs'
import { Route } from '../../../enumeration/Route'
import Routes from '../../../routes/Routes'
import WizardTab from '../../../components/wizard/tabs/tab/WizardTab'
import WizardTop from '../../../components/wizard/top/WizardTop'
import Footer from '../../../components/footer/Footer'
import AddJobJob from './content/job/AddJobJob'
import AddJobPersonalDetails from './content/personal-details/AddJobPersonalDetails'
import AddJobPictures from './content/pictures/AddJobPictures'
import Translations from '../../../translations/Translations'
import AddJobLoginCredentials from './content/login-credentials/AddJobLoginCredentials'
import Window from '../../../shared/Window'
import Authorization from '../../../core/Authorization'
import { Subscription } from 'rxjs'
import Storage from '../../../storage/Storage'
import FormValidation from '../../../core/FormValidation'
import { Branche } from '../../../backend/enumeration/Branche'
import JobMatrix from '../../../core/JobMatrix'
import JobValidation from '../../../core/JobValidation'
import { RoofIsolationType } from '../../../backend/enumeration/RoofIsolationType'
import { RoofMaterialType } from '../../../backend/enumeration/RoofMaterialType'
import Loader from '../../../components/loader/Loader'
import Backend from '../../../backend/Backend'
import Modal from '../../../components/modal/Modal'
import HttpResponse from '../../../shared/backend/HttpResponse'
import RegisterJobResponse from '../../../backend/controller/consumer/job/register/response/RegisterJobResponse'
import RegistrationCompleteConsumerModal from '../../../components/modal/registration-complete/consumer/RegistrationCompleteConsumerModal'
import StoragePageAddJobDataModel from '../../../storage/data-model/page/StoragePageAddJobDataModel'
import App from '../../../core/app/App'
import { RouteComponentProps } from 'react-router'
import WizardContent from '../../../components/wizard/content/WizardContent'
import ProgressSpinner from '../../../components/progress-spinner/ProgressSpinner'
import JobResponse from '../../../backend/response/entity/JobResponse'

/**
 * All possible routes in this page.
 */
export type AddJobRoutes = Route.CONSUMER_PLACE_YOUR_JOB
    | Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS
    | Route.CONSUMER_PLACE_YOUR_JOB_PICTURES
    | Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS
    | Route.CONSUMER_EDIT_JOB
    | Route.CONSUMER_EDIT_JOB_PICTURES

/**
 * The props
 */
interface AddJobPageProps {

    /**
     * The active tab in the profile page
     */
    route: AddJobRoutes

    /**
     * Whether or not to navigate to the unlocked tab.
     * 
     * This is used when navigated to this page from any non related (`AddJobRoutes`) page.
     */
    navigateToLastUnlockedTab?: boolean
}

/**
 * The state
 */
interface AddJobPageState {
    /**
     * The active tab in the profile page
     */
    route: AddJobRoutes

    /**
     * The job (when editing a job)
     */
    job?: JobResponse

    /**
     * The initial job before any editing has been taking place
     * since retrieving it from the API.
     */
    initialJob?: JobResponse

    /**
     * Whether the page is done loading the job.
     */
    doneLoading: boolean
}

/**
 * The page to add a job as a consumer.
 * 
 * @author Stan Hurks
 */
export default class AddJobPage extends React.Component<AddJobPageProps & RouteComponentProps, AddJobPageState> {

    /**
     * The subscription for when the storage updates
     */
    private subscriptionStorageUpdate!: Subscription

    /**
     * The subscription for when the user has logged in.
     */
    private subscriptionAppLogin!: Subscription

    /**
     * The subscription for when the user has logged out.
     */
    private subscriptionAppLogout!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            route: this.props.route,

            doneLoading: this.props.match.params['jobId'] === undefined
        }
    }

    public componentDidMount = () => {
        this.subscriptionStorageUpdate = Storage.update.subscribe(this.onStorageUpdate)
        this.subscriptionAppLogin = App.login.subscribe(this.onAppLogin)
        this.subscriptionAppLogout = App.logout.subscribe(this.onAppLogout)

        setTimeout(() => {
            const authorized = Authorization.isAuthorized()
            const jobId = this.props.match.params['jobId']
            if (jobId) {
                if (!authorized) {
                    Routes.redirectURL.next(Route.HOME)
                    return
                }
                Loader.set.next(Translations.translations.backend.controllers.consumer.job.getJob.loader)
                Backend.controllers.consumerJob.getJob(jobId).then((response) => {
                    this.setState({
                        job: response.data,
                        initialJob: response.data,
                        doneLoading: true
                    })
                }).catch((response) => {
                    const translation = Translations.translations.backend.controllers.consumer.job.getJob.responses[response.status]
                    if (translation !== undefined) {
                        Modal.mountErrorHistory.next(translation)
                    }
                    Routes.redirectURL.next(Route.HOME)
                })
            } else {
                this.initializeRoute()
            }
        })
    }

    public componentWillUnmount = () => {
        this.subscriptionStorageUpdate.unsubscribe()
        this.subscriptionAppLogin.unsubscribe()
        this.subscriptionAppLogout.unsubscribe()
    }

    public render = () => {
        return (
            <div id="add-job-page">
                <Hero id="add-job-page-hero" absolute height={400} />

                <Wizard>
                    <WizardTop
                        percentage={this.getPercentage()}
                        title={
                            this.props.match.params['jobId']
                            ? Translations.translations.pages.addJob.wizard.title.editJob
                            : Translations.translations.pages.addJob.wizard.title.addJob
                        }
                        />
                    <WizardTabs>
                        <WizardTab 
                            badge={this.getBadgeForTab((this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB : Route.CONSUMER_PLACE_YOUR_JOB))}
                            active={this.state.route === (this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB : Route.CONSUMER_PLACE_YOUR_JOB)} 
                            onClick={() => {this.navigate((this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB : Route.CONSUMER_PLACE_YOUR_JOB))}}
                            disabled={this.isTabDisabled((this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB : Route.CONSUMER_PLACE_YOUR_JOB))}>

                            {
                                Translations.translations.pages.addJob.wizard.tabs.job
                            }
                        </WizardTab>
                        {
                            !Authorization.isAuthorized()
                            &&
                            <WizardTab 
                                badge={this.getBadgeForTab(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)}
                                active={this.state.route === Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS} 
                                onClick={() => {this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)}}
                                disabled={this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)}>

                                {
                                    Translations.translations.pages.addJob.wizard.tabs.personalDetails
                                }
                            </WizardTab>
                        }
                        <WizardTab 
                            badge={this.getBadgeForTab((this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB_PICTURES : Route.CONSUMER_PLACE_YOUR_JOB_PICTURES))}
                            active={this.state.route === (this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB_PICTURES : Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)} 
                            onClick={() => {this.navigate((this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB_PICTURES : Route.CONSUMER_PLACE_YOUR_JOB_PICTURES))}}
                            disabled={this.isTabDisabled((this.props.match.params['jobId'] ? Route.CONSUMER_EDIT_JOB_PICTURES : Route.CONSUMER_PLACE_YOUR_JOB_PICTURES))}>

                            {
                                Translations.translations.pages.addJob.wizard.tabs.pictures
                            }
                        </WizardTab>
                        {
                            !Authorization.isAuthorized()
                            &&
                            <WizardTab 
                                badge={this.getBadgeForTab(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)}
                                active={this.state.route === Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS} 
                                onClick={() => {this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)}}
                                disabled={this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)}>

                                {
                                    Translations.translations.pages.addJob.wizard.tabs.loginCredentials
                                }
                            </WizardTab>
                        }
                    </WizardTabs>
                    {
                        !this.state.doneLoading
                        &&
                        <div>
                            <WizardContent>
                                <ProgressSpinner />
                            </WizardContent>
                        </div>
                    }
                    {
                        this.state.route === Route.CONSUMER_PLACE_YOUR_JOB
                        &&
                        <AddJobJob navigate={this.navigate} isTabDisabled={this.isTabDisabled} />
                    }
                    {
                        this.state.route === Route.CONSUMER_EDIT_JOB && this.state.doneLoading
                        &&
                        <AddJobJob placeJob={this.placeJob} navigate={this.navigate} isTabDisabled={this.isTabDisabled} job={this.state.job} initialJob={this.state.initialJob} onChangeJob={this.onChangeJob} />
                    }
                    {
                        this.state.route === Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS
                        &&
                        <AddJobPersonalDetails navigate={this.navigate} isTabDisabled={this.isTabDisabled} />
                    }
                    {
                        this.state.route === Route.CONSUMER_PLACE_YOUR_JOB_PICTURES
                        &&
                        <AddJobPictures placeJob={this.placeJob} navigate={this.navigate} isTabDisabled={this.isTabDisabled} />
                    }
                    {
                        this.state.route === Route.CONSUMER_EDIT_JOB_PICTURES && this.state.doneLoading
                        &&
                        <AddJobPictures placeJob={this.placeJob} navigate={this.navigate} isTabDisabled={this.isTabDisabled} jobId={this.props.match.params['jobId']} />
                    }
                    {
                        this.state.route === Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS
                        &&
                        <AddJobLoginCredentials placeJob={this.placeJob} navigate={this.navigate} isTabDisabled={this.isTabDisabled} />
                    }
                </Wizard>

                <Footer />
            </div>
        )
    }

    /**
     * Whenever the storage is being updated
     */
    private onStorageUpdate = () => {
        setTimeout(() => {
            // Second timeout due to there also being a timeout in Storage.
            setTimeout(() => {

                this.forceUpdate()
            })
        })
    }

    /**
     * Whenever the user logs in to the app
     */
    private onAppLogin = () => {
        if (this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)) {
            this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
        } else {
            this.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
        }
    }

    /**
     * Whenever the user logs out of the app
     */
    private onAppLogout = () => {
        this.forceUpdate()
    }

    /**
     * Whenever an existing job changes in the tab child components.
     */
    private onChangeJob = (job: JobResponse) => {
        this.setState({
            job
        })
    }

    /**
     * Initializes the route
     */
    private initializeRoute = () => {
        const authorized = Authorization.isAuthorized()
        if (this.props.navigateToLastUnlockedTab) {
            if (authorized) {
                if (this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)) {
                    Routes.setCurrentRoute.next(this.state.route)
                } else {
                    Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                }
            } else {
                if (this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)) {
                    Routes.setCurrentRoute.next(this.state.route)
                } else if (this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)) {
                    Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                } else if (this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)) {
                    Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                } else {
                    Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)
                }
            }
        }
        else if (this.isTabDisabled(this.state.route)) {
            switch (this.state.route) {
                case Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS:
                    Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB)
                    break
                case Route.CONSUMER_PLACE_YOUR_JOB_PICTURES:
                    if (authorized) {
                        Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB)
                    } else {
                        Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                    }
                    break
                case Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS:
                    Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                    break
            }
        } else {
            Routes.setCurrentRoute.next(this.state.route)
        }
    }

    /**
     * Get the percentage
     */
    private getPercentage = (): number => {
        const authorized = Authorization.isAuthorized()
        if (authorized) {
            return (this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB) ? 100 : 0)
        } else {
            return (this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB) ? 25 : 0)
                + (this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS) ? 25 : 0)
                + (
                    this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                        ? (
                            this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                            ? 0
                            : 25
                        )
                        : 0
                )
                + (this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS) ? 25 : 0)
        }
    }

    /**
     * Get the badge for an index
     */
    private getBadgeForTab = (route: AddJobRoutes): 1|2|3|4|'done' => {
        const authorized = Authorization.isAuthorized()
        switch (route) {
            case Route.CONSUMER_PLACE_YOUR_JOB:
                return this.validateTab(route) ? 'done' : 1
            case Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS:
                return this.validateTab(route) ? 'done' : 2
            case Route.CONSUMER_PLACE_YOUR_JOB_PICTURES:
                return authorized
                    ? (
                        this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)
                        ? 'done'
                        : 2
                    )
                    : (
                        this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)
                        && this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                        ? 'done'
                        : 3
                    )
            case Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS:
                return this.validateTab(route) ? 'done' : 4
        }
        return 'done'
    }

    /**
     * Check whether the tab is disabled
     */
    private isTabDisabled = (route: AddJobRoutes): boolean => {
        const authorized = Authorization.isAuthorized()
        switch (route) {
            case Route.CONSUMER_EDIT_JOB:
                return this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB)
            case Route.CONSUMER_EDIT_JOB_PICTURES:
                return this.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
            case Route.CONSUMER_PLACE_YOUR_JOB:
                return false
            case Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS:
                return authorized || !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)
            case Route.CONSUMER_PLACE_YOUR_JOB_PICTURES:
                if (authorized) {
                    return !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)
                }
                return !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)
                    || !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
            case Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS:
                return authorized
                    || !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)
                    || !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                    || !this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
        }
        return false
    }

    /**
     * Place the job
     */
    private placeJob = () => {
        if (Authorization.isAuthorized()) {
            // First validate
            if (!this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)) {
                this.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
                return
            }

            // Modify an existing job
            if (this.state.job) {
                // The request
                Loader.set.next(Translations.translations.backend.controllers.consumer.job.editJob.loader)
                Backend.controllers.consumerJob.editJob(this.state.job.id, {
                    title: this.state.job.title as string,
                    description: this.state.job.description,
                    branche: this.state.job.branche as Branche,
                    roofType: this.state.job.roofType,
                    jobCategory: this.state.job.jobCategory,
                    surfaceMeasurements: this.state.job.surfaceMeasurements,
                    measurementUnit: this.state.job.surfaceMeasurementsUnit,
                    houseType: this.state.job.houseType,
                    roofMaterialType: this.state.job.roofMaterialType,
                    roofIsolationType: this.state.job.roofIsolationType
                }).then(() => {
                    Modal.mountSuccess.next(Translations.translations.backend.controllers.consumer.job.editJob.responses[200])

                    this.setState({
                        initialJob: this.state.job
                    })
                }).catch((response) => {
                    const translation = Translations.translations.backend.controllers.consumer.job.register.register.responses[response.status]
                    if (translation !== undefined) {
                        Modal.mountError.next(translation)
                    }
                })
            }
            
            // Add a new job
            else {
                // The request
                Loader.set.next(Translations.translations.backend.controllers.consumer.job.register.register.loader)
                Backend.controllers.consumerJob.addJob({
                    title: Storage.data.page.addJob.job.title as string,
                    description: Storage.data.page.addJob.job.description,
                    branche: Storage.data.page.addJob.job.branche as Branche,
                    roofType: Storage.data.page.addJob.job.roofType,
                    jobCategory: Storage.data.page.addJob.job.jobCategory,
                    surfaceMeasurements: Storage.data.page.addJob.job.surfaceMeasurements,
                    measurementUnit: Storage.data.page.addJob.job.measurementUnit,
                    houseType: Storage.data.page.addJob.job.houseType,
                    roofMaterialType: Storage.data.page.addJob.job.roofMaterialType,
                    roofIsolationType: Storage.data.page.addJob.job.roofIsolationType
                }).then(() => {
                    Storage.data.page.addJob = new StoragePageAddJobDataModel()
                    Storage.update.next()
                    Routes.redirectURL.next(Route.HOME)
                    Modal.mountSuccess.next(Translations.translations.backend.controllers.consumer.job.addJob.responses[200])
                }).catch((response) => {
                    const translation = Translations.translations.backend.controllers.consumer.job.register.register.responses[response.status]
                    if (translation !== undefined) {
                        Modal.mountError.next(translation)
                    }
                    if (response.status === 400) {
                        this.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
                    }
                })
            }
        } else {
            // First validate
            if (!this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB)) {
                this.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
                return
            }

            if (!this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)) {
                this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                return
            }

            if (!this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)) {
                this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                return
            }

            if (!this.validateTab(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)) {
                this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)
                return
            }

            // The request
            Loader.set.next(Translations.translations.backend.controllers.consumer.job.register.register.loader)
            Backend.controllers.consumerJobRegister.register({
                job: {
                    title: Storage.data.page.addJob.job.title as string,
                    description: Storage.data.page.addJob.job.description,
                    branche: Storage.data.page.addJob.job.branche as Branche,
                    roofType: Storage.data.page.addJob.job.roofType,
                    jobCategory: Storage.data.page.addJob.job.jobCategory,
                    surfaceMeasurements: Storage.data.page.addJob.job.surfaceMeasurements,
                    measurementUnit: Storage.data.page.addJob.job.measurementUnit,
                    houseType: Storage.data.page.addJob.job.houseType,
                    roofMaterialType: Storage.data.page.addJob.job.roofMaterialType,
                    roofIsolationType: Storage.data.page.addJob.job.roofIsolationType
                },
                personalDetails: {
                    name: Storage.data.page.addJob.personalDetails.fullName as string,
                    postalCode: Storage.data.page.addJob.personalDetails.postalCode as string
                },
                loginCredentials: {
                    emailAddress: Storage.data.page.addJob.loginDetails.emailAddress as string,
                    password: AddJobLoginCredentials.lastUsedPassword as string,
                    acceptedTermsAndConditions: Storage.data.page.addJob.loginDetails.acceptedTermsAndConditions
                }
            }).then(() => {
                Storage.data.page.addJob = new StoragePageAddJobDataModel()
                Storage.update.next()
                Routes.redirectURL.next(Route.HOME)
                Modal.mount.next({
                    element: (
                        <RegistrationCompleteConsumerModal />
                    ),
                    dismissable: false
                })
            }).catch((response: HttpResponse<RegisterJobResponse>) => {
                const translation = Translations.translations.backend.controllers.consumer.job.register.register.responses[response.status]
                if (translation !== undefined) {
                    Modal.mountError.next(translation)
                }
                if (response.status === 400) {
                    if (response.data.jobValidated === false) {
                        this.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
                    } else if (response.data.personalDetailsValidated === false) {
                        this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                    } else if (response.data.loginCredentialsValidated === false) {
                        this.navigate(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)
                    }
                }
            })
        }
    }

    /**
     * Validates a tab
     */
    private validateTab = (route: AddJobRoutes): boolean => {
        FormValidation.stash()

        const storageData = this.state.job
            ? this.state.job
            : Storage.data.page.addJob.job

        switch (route) {
            case Route.CONSUMER_PLACE_YOUR_JOB:
                FormValidation.addField({
                    ...FormValidation.entity.job.title,
                    evaluateValue: () => {
                        return storageData.title
                    },
                    label: 'title'
                })
                FormValidation.addField({
                    ...FormValidation.entity.job.description,
                    evaluateValue: () => {
                        return storageData.description
                    },
                    label: 'description'
                })
                FormValidation.addField({
                    ...FormValidation.entity.job.branche,
                    evaluateValue: () => {
                        return storageData.branche
                    },
                    label: 'branche'
                })

                if (storageData.branche === Branche.ROOF_GENERAL) {
                    const roofType = storageData.roofType
                    if (roofType === null) {
                        break
                    }
                    FormValidation.addField({
                        ...FormValidation.entity.job.roofType,
                        evaluateValue: () => {
                            return roofType
                        },
                        label: 'roofType'
                    })

                    const jobCategory = storageData.jobCategory
                    const jobCategories = JobMatrix[roofType]
                    if (jobCategories === null) {
                        return FormValidation.validateAndUnstash()
                    } else if (jobCategory === null || Object.keys(jobCategories).indexOf(jobCategory) === -1) {
                        break
                    }

                    // Determine whether surface measurements are required
                    const measurementUnit = JobValidation.getSurfaceMeasurementUnit(jobCategory)
                    if (measurementUnit !== null) {
                        FormValidation.addField({
                            ...FormValidation.entity.job.surfaceMeasurements,
                            evaluateValue: () => {
                                return storageData.surfaceMeasurements
                            },
                            label: 'surfaceMeasurements'
                        })
                    }

                    // house type
                    const houseType = storageData.houseType
                    const houseTypes = JobMatrix[roofType][jobCategory]
                    if (houseTypes === null) {
                        return FormValidation.validateAndUnstash()
                    } else if (houseType === null || Object.keys(houseTypes).indexOf(houseType) === -1) {
                        break
                    }

                    // check whether roof material type or roof isolation type is relevant
                    const roofMaterialOrRoofIsolationTypeMatrix = JobMatrix[roofType][jobCategory][houseType]
                    if (roofMaterialOrRoofIsolationTypeMatrix === null) {
                        return FormValidation.validateAndUnstash()
                    }

                    let isRoofMaterialType = true
                    let isRoofIsolationType = true
                    for (const key of Object.keys(roofMaterialOrRoofIsolationTypeMatrix)) {
                        if (RoofIsolationType[key] === undefined) {
                            isRoofIsolationType = false
                        }

                        if (RoofMaterialType[key] === undefined) {
                            isRoofMaterialType = false
                        }
                    }

                    // Roof material type
                    if (isRoofMaterialType) {
                        const roofMaterialType = storageData.roofMaterialType
                        const roofMaterialTypes = roofMaterialOrRoofIsolationTypeMatrix
                        if (roofMaterialType === null || Object.keys(roofMaterialTypes).indexOf(roofMaterialType) === -1) {
                            break
                        }
                    }
                    
                    // Roof isolation type
                    else if (isRoofIsolationType) {
                        const roofIsolationType = storageData.roofIsolationType
                        const roofIsolationTypes = roofMaterialOrRoofIsolationTypeMatrix
                        if (roofIsolationType === null || Object.keys(roofIsolationTypes).indexOf(roofIsolationType) === -1) {
                            break
                        }
                    }

                    // No
                    else {
                        console.error('Not roof material and also not roof isolation type, probably not a bad error, though might be proof of a future bug.')
                        return FormValidation.validateAndUnstash()
                    }
                }
                return FormValidation.validateAndUnstash()
            case Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS:
                FormValidation.addField({
                    ...FormValidation.entity.user.fullName,
                    evaluateValue: () => {
                        return Storage.data.page.addJob.personalDetails.fullName
                    },
                    label: 'fullName'
                })
                FormValidation.addField({
                    ...FormValidation.entity.user.postalCode,
                    evaluateValue: () => {
                        return Storage.data.page.addJob.personalDetails.postalCode
                    },
                    label: 'postalCode'
                })
                return FormValidation.validateAndUnstash()
            case Route.CONSUMER_PLACE_YOUR_JOB_PICTURES:
                FormValidation.unstash()
                return true
            case Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS:
                FormValidation.addField({
                    ...FormValidation.entity.user.email,
                    evaluateValue: () => {
                        return Storage.data.page.addJob.loginDetails.emailAddress
                    },
                    label: 'email'
                })
                FormValidation.addField({
                    ...FormValidation.entity.user.password,
                    evaluateValue: () => {
                        return AddJobLoginCredentials.lastUsedPassword
                    },
                    label: 'password'
                })
                FormValidation.addField({
                    string: {
                        matchEvaluatedValue: () => {
                            return AddJobLoginCredentials.lastUsedPassword || ''
                        }
                    },
                    evaluateValue: () => {
                        return AddJobLoginCredentials.lastUsedPasswordRepeat || ''
                    },
                    label: 'passwordRepeat'
                })
                FormValidation.addField({
                    mustBeTruthy: true,
                    evaluateValue: () => {
                        return Storage.data.page.addJob.loginDetails.acceptedTermsAndConditions
                    },
                    label: 'acceptedTermsAndConditions'
                })
                return FormValidation.validateAndUnstash()
        }

        FormValidation.unstash()
        return false
    }

    /**
     * Navigate to a route
     */
    private navigate = (route: AddJobRoutes) => {
        if (this.isTabDisabled(route)) {
            return
        }

        if (this.props.match.params['jobId']) {
            Routes.setCurrentRoute.next(route.replace(':jobId', String(this.props.match.params['jobId'])) as Route)
        } else {
            Routes.setCurrentRoute.next(route)
        }

        const el = document.querySelector('.wizard') as HTMLElement
        if (el) {
            Window.scrollTo({
                top: el.getBoundingClientRect().top + window.scrollY,
                behavior: 'smooth'
            }, () => {
                this.setState({
                    route
                })    
            })
        } else {
            this.setState({
                route
            })
        }
    }
}