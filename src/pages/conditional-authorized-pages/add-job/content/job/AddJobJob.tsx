import React from 'react'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import Translations from '../../../../../translations/Translations'
import Button from '../../../../../components/button/Button'
import { AddJobRoutes } from '../../AddJobPage'
import FormSelect from '../../../../../components/form/select/FormSelect'
import FormGroup from '../../../../../components/form/group/FormGroup'
import FormValidation from '../../../../../core/FormValidation'
import Storage from '../../../../../storage/Storage'
import { Branche } from '../../../../../backend/enumeration/Branche'
import { RoofType } from '../../../../../backend/enumeration/RoofType'
import { JobCategory } from '../../../../../backend/enumeration/JobCategory'
import { RoofMaterialType } from '../../../../../backend/enumeration/RoofMaterialType'
import { HouseType } from '../../../../../backend/enumeration/HouseType'
import FormInput from '../../../../../components/form/input/FormInput'
import { RoofIsolationType } from '../../../../../backend/enumeration/RoofIsolationType'
import JobMatrix from '../../../../../core/JobMatrix'
import { MeasurementUnit } from '../../../../../backend/enumeration/MeasurementUnit'
import JobValidation from '../../../../../core/JobValidation'
import Authorization from '../../../../../core/Authorization'
import JobResponse from '../../../../../backend/response/entity/JobResponse'
import Routes from '../../../../../routes/Routes'

/**
 * The props
 */
interface AddJobJobProps {
    
    /**
     * Checks whether a tab is disabled
     */
    isTabDisabled: (route: AddJobRoutes) => boolean

    /**
     * Navigate to the tab index
     */
    navigate: (route: AddJobRoutes) => void

    /**
     * The job if this is the job edit page
     */
    job?: JobResponse

    /**
     * The initial job data before last save
     */
    initialJob?: JobResponse

    /**
     * Whenever an existing job changes in this page, change it in the parent
     */
    onChangeJob?: (job: JobResponse) => void

    /**
     * Place the job
     */
    placeJob?: () => void
}

/**
 * The state
 */
export interface AddJobJobState {

    /**
     * The title
     */
    title: string|null

    /**
     * The branche
     */
    branche: Branche|null

    /**
     * The type of roof (optional)
     */
    roofType: RoofType|null

    /**
     * The category for the job (optional)
     */
    jobCategory: JobCategory|null

    /**
     * The type of house (optional)
     */
    houseType: HouseType|null

    /**
     * The type of roof material
     */
    roofMaterialType: RoofMaterialType|null

    /**
     * The type of roof isolation
     */
    roofIsolationType: RoofIsolationType|null

    /**
     * The description
     */
    description: string|null

    /**
     * The surface measurements
     */
    surfaceMeasurements: number|null

    /**
     * The unit of measurement
     */
    measurementUnit: MeasurementUnit|null

    /**
     * Whether or not the component is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The job WizardContent within the AddJobPage.
 * 
 * @author Stan Hurks
 */
export default class AddJobJob extends React.Component<AddJobJobProps, AddJobJobState> {

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.addJob.job,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        }, () => {
            if (this.props.job) {
                this.setState({
                    title: this.props.job.title,
                    branche: this.props.job.branche,
                    roofType: this.props.job.roofType,
                    jobCategory: this.props.job.jobCategory,
                    houseType: this.props.job.houseType,
                    roofMaterialType: this.props.job.roofMaterialType,
                    roofIsolationType: this.props.job.roofIsolationType,
                    description: this.props.job.description,
                    surfaceMeasurements: this.props.job.surfaceMeasurements,
                    measurementUnit: this.props.job.surfaceMeasurementsUnit,
                    resetValidationUponUpdate: true
                })

                Routes.setCurrentRoute.next({
                    route: Route.CONSUMER_EDIT_JOB,
                    params: {
                        jobId: String(this.props.job.id)
                    }
                })
            }
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            }, () => {
                if (this.props.job) {
                    this.setState({
                        title: this.props.job.title,
                        branche: this.props.job.branche,
                        roofType: this.props.job.roofType,
                        jobCategory: this.props.job.jobCategory,
                        houseType: this.props.job.houseType,
                        roofMaterialType: this.props.job.roofMaterialType,
                        roofIsolationType: this.props.job.roofIsolationType,
                        description: this.props.job.description,
                        surfaceMeasurements: this.props.job.surfaceMeasurements,
                        measurementUnit: this.props.job.surfaceMeasurementsUnit,
                    })
                }
            })
        }
    }

    public componentWillReceiveProps = () => {
        if (this.props.job) {
            this.setState({
                title: this.props.job.title,
                branche: this.props.job.branche,
                roofType: this.props.job.roofType,
                jobCategory: this.props.job.jobCategory,
                houseType: this.props.job.houseType,
                roofMaterialType: this.props.job.roofMaterialType,
                roofIsolationType: this.props.job.roofIsolationType,
                description: this.props.job.description,
                surfaceMeasurements: this.props.job.surfaceMeasurements,
                measurementUnit: this.props.job.surfaceMeasurementsUnit,
                resetValidationUponUpdate: true
            })
        }
    }

    public render = () => {
        return (
            <form className="add-job-job" onSubmit={() => {
                this.saveJob()
            }}>
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.addJob.wizard.content.job.intro.title}>
                        {
                            Translations.translations.pages.addJob.wizard.content.job.intro[
                                this.props.job
                                ? 'contentEdit'
                                : 'content'
                            ].map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {/* The title */}
                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.job.form.title.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.job.form.title.helperText}
                        validation={{
                            ...FormValidation.entity.job.title,
                            evaluateValue: () => {
                                return this.state.title
                            }
                        }}>

                        <FormInput
                            value={this.state.title}
                            placeholder={Translations.translations.pages.addJob.wizard.content.job.form.title.placeholder}
                            type="text"
                            onChange={(title) => {
                                this.setState({
                                    title
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }} />
                    </FormGroup>

                    {/* The branche */}
                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.job.form.branche.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.job.form.branche.helperText}
                        validation={{
                            ...FormValidation.entity.job.branche,
                            evaluateValue: () => {
                                return this.state.branche
                            }
                        }}>

                        <FormSelect
                            placeholder={Translations.translations.pages.addJob.wizard.content.job.form.branche.placeholder}
                            value={this.state.branche}
                            options={Object.keys(Branche).map((key) => ({
                                key,
                                value: Translations.translations.backend.enumerations.branche(key as Branche)
                            }))}
                            onChange={(branche) => {
                                this.setState({
                                    branche
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }} />
                    </FormGroup>

                    {/* Roof type */}
                    {
                        this.getRoofTypes().length > 0
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.job.form.roofType.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.job.form.roofType.helperText}
                            validation={{
                                ...FormValidation.entity.job.roofType,
                                evaluateValue: () => {
                                    return this.state.roofType
                                },
                                allowedValues: this.getRoofTypes()
                            }}>

                            <FormSelect
                                placeholder={Translations.translations.pages.addJob.wizard.content.job.form.roofType.placeholder}
                                value={this.state.roofType}
                                options={this.getRoofTypes().map((roofType) => ({
                                    key: roofType,
                                    value: Translations.translations.backend.enumerations.roofType(roofType as RoofType)
                                }))}
                                onChange={(roofType) => {
                                    this.setState({
                                        roofType
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }} />
                        </FormGroup>
                    }

                    {/* Job category */}
                    {
                        this.getJobCategories().length > 0
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.job.form.jobCategory.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.job.form.jobCategory.helperText}
                            validation={{
                                ...FormValidation.entity.job.jobCategory,
                                evaluateValue: () => {
                                    return this.state.jobCategory
                                },
                                allowedValues: this.getJobCategories()
                            }}>

                            <FormSelect
                                placeholder={Translations.translations.pages.addJob.wizard.content.job.form.jobCategory.placeholder}
                                value={this.state.jobCategory}
                                options={this.getJobCategories().map((key) => ({
                                    key,
                                    value: Translations.translations.backend.enumerations.jobCategory(key as JobCategory)
                                }))}
                                onChange={(jobCategory) => {
                                    this.setState({
                                        jobCategory
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }} />
                        </FormGroup>
                    }

                    {/* Surface measurements */}
                    {
                        this.state.branche === Branche.ROOF_GENERAL
                        &&
                        JobValidation.getSurfaceMeasurementUnit(this.state.jobCategory) !== null
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.job.form.surfaceMeasurements[JobValidation.getSurfaceMeasurementUnit(this.state.jobCategory) as MeasurementUnit].label}
                            helperText={Translations.translations.pages.addJob.wizard.content.job.form.surfaceMeasurements[JobValidation.getSurfaceMeasurementUnit(this.state.jobCategory) as MeasurementUnit].helperText}
                            validation={{
                                ...FormValidation.entity.job.surfaceMeasurements,
                                evaluateValue: () => {
                                    return this.state.surfaceMeasurements
                                }
                            }}>

                            <FormInput
                                value={this.state.surfaceMeasurements}
                                placeholder={Translations.translations.pages.addJob.wizard.content.job.form.surfaceMeasurements[JobValidation.getSurfaceMeasurementUnit(this.state.jobCategory) as MeasurementUnit].placeholder}
                                type="number"
                                onChange={(surfaceMeasurements) => {
                                    this.setState({
                                        surfaceMeasurements
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }} />
                        </FormGroup>
                    }

                    {/* House type */}
                    {
                        this.getHouseTypes().length > 0
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.job.form.houseType.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.job.form.houseType.helperText}
                            validation={{
                                ...FormValidation.entity.job.houseType,
                                evaluateValue: () => {
                                    return this.state.houseType
                                },
                                allowedValues: this.getHouseTypes()
                            }}>

                            <FormSelect
                                placeholder={Translations.translations.pages.addJob.wizard.content.job.form.houseType.placeholder}
                                value={this.state.houseType}
                                options={this.getHouseTypes().map((key) => ({
                                    key,
                                    value: Translations.translations.backend.enumerations.houseType(key as HouseType)
                                }))}
                                onChange={(houseType) => {
                                    this.setState({
                                        houseType
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }} />
                        </FormGroup>
                    }
                    
                    {/* Roof material type */}
                    {
                        this.getRoofMaterialTypes().length > 0
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.job.form.roofMaterialType.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.job.form.roofMaterialType.helperText}
                            validation={{
                                ...FormValidation.entity.job.roofMaterialType,
                                evaluateValue: () => {
                                    return this.state.roofMaterialType
                                },
                                allowedValues: this.getRoofMaterialTypes()
                            }}>

                            <FormSelect
                                placeholder={Translations.translations.pages.addJob.wizard.content.job.form.roofMaterialType.placeholder}
                                value={this.state.roofMaterialType}
                                options={this.getRoofMaterialTypes().map((key) => ({
                                    key,
                                    value: Translations.translations.backend.enumerations.roofMaterialType(key as RoofMaterialType)
                                }))}
                                onChange={(roofMaterialType) => {
                                    this.setState({
                                        roofMaterialType
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }} />
                        </FormGroup>
                    }

                    {/* Roof isolation type */}
                    {
                        this.getRoofIsolationTypes().length > 0
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.job.form.roofIsolationType.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.job.form.roofIsolationType.helperText}
                            validation={{
                                ...FormValidation.entity.job.roofIsolationType,
                                evaluateValue: () => {
                                    return this.state.roofIsolationType
                                },
                                allowedValues: this.getRoofIsolationTypes()
                            }}>

                            <FormSelect
                                placeholder={Translations.translations.pages.addJob.wizard.content.job.form.roofIsolationType.placeholder}
                                value={this.state.roofIsolationType}
                                options={this.getRoofIsolationTypes().map((key) => ({
                                    key,
                                    value: Translations.translations.backend.enumerations.roofIsolationType(key as RoofIsolationType)
                                }))}
                                onChange={(roofIsolationType) => {
                                    this.setState({
                                        roofIsolationType
                                    }, () => {
                                        this.saveStateInStorage()
                                    })
                                }} />
                        </FormGroup>
                    }

                    {/* Description */}
                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.job.form.description.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.job.form.description.helperText}
                        validation={{
                            ...FormValidation.entity.job.description,
                            evaluateValue: () => {
                                return this.state.description
                            }
                        }}>

                        <FormInput
                            value={this.state.description}
                            placeholder={Translations.translations.pages.addJob.wizard.content.job.form.description.placeholder}
                            type="multiline"
                            onChange={(description) => {
                                this.setState({
                                    description
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }} />
                    </FormGroup>

                    {
                        Authorization.isAuthorized() && this.props.job
                        &&
                        <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.job.outro.titleEdit}>
                            {
                                Translations.translations.pages.addJob.wizard.content.job.outro.contentEdit.map((part, partIndex) =>
                                    <div key={partIndex}>
                                        {
                                            part
                                        }
                                    </div>
                                )
                            }
                        </WizardContentOutro>
                    }
                    {
                        Authorization.isAuthorized() && !this.props.job
                        &&
                        <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.personalDetails.outro.title}>
                            {
                                Translations.translations.pages.addJob.wizard.content.personalDetails.outro.content.map((part, partIndex) =>
                                    <div key={partIndex}>
                                        {
                                            part
                                        }
                                    </div>
                                )
                            }
                        </WizardContentOutro>
                    }
                    {
                        !Authorization.isAuthorized()
                        &&
                        <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.job.outro.title}>
                            {
                                Translations.translations.pages.addJob.wizard.content.job.outro.content.map((part, partIndex) =>
                                    <div key={partIndex}>
                                        {
                                            part
                                        }
                                    </div>
                                )
                            }
                        </WizardContentOutro>
                    }
                </WizardContent>
                <WizardBottom path={[
                    Route.HOME,

                    ...this.props.job
                    ? [
                        Route.CONSUMER_EDIT_JOB
                    ]
                    : [
                        Route.CONSUMER_PLACE_YOUR_JOB
                    ]
                ]} params={{
                    ...this.props.job
                    ? {
                        jobId: String(this.props.job.id)
                    }
                    : {}
                }}>
                    {
                        Authorization.isAuthorized() && this.props.job
                        &&
                        <Button color="orange" disabled={!this.hasChanges() || !this.state.validated} onClick={() => {
                            if (this.props.placeJob) {
                                this.props.placeJob()
                            }
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.save
                            }
                        </Button>
                    }
                    {
                        Authorization.isAuthorized() && !this.props.job
                        &&
                        <Button color="orange" type="submit" disabled={this.props.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES) || !this.state.validated}>
                            {
                                Translations.translations.modals.defaultButtons.next
                            }
                        </Button>
                    }
                    {
                        !Authorization.isAuthorized()
                        &&
                        <Button color="orange" type="submit" disabled={this.props.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS) || !this.state.validated}>
                            {
                                Translations.translations.modals.defaultButtons.next
                            }
                        </Button>
                    }
                </WizardBottom>
            </form>
        )
    }

    /**
     * Checks whether there are changes between the form inputs
     * and the initialState.
     */
    private hasChanges = (): boolean => {
        if (!this.props.initialJob || !this.props.job) {
            return false
        }

        const props = [
            'title',
            'branche',
            'roofType',
            'jobCategory',
            'houseType',
            'roofMaterialType',
            'roofIsolationType',
            'description',
            'surfaceMeasurements',
            'measurementUnit'
        ]
        for (const prop of props) {
            if (this.props.job[prop] !== this.props.initialJob[prop]) {
                return true
            }
        }

        return false
    }

    /**
     * Save the state in storage
     */
    private saveStateInStorage = () => {
        if (this.props.job) {
            if (this.props.onChangeJob) {
                this.props.onChangeJob({
                    ...this.props.job,
                    title: this.state.title || '',
                    branche: this.state.branche,
                    roofType: this.state.roofType,
                    jobCategory: this.state.jobCategory,
                    houseType: this.state.houseType,
                    roofMaterialType: this.state.roofMaterialType,
                    roofIsolationType: this.state.roofIsolationType,
                    description: this.state.description,
                    surfaceMeasurements: this.state.surfaceMeasurements,
                    surfaceMeasurementsUnit: this.state.measurementUnit
                })
            }
        } else {
            Storage.data.page.addJob.job = {
                ...this.state,
                measurementUnit: JobValidation.getSurfaceMeasurementUnit(this.state.jobCategory)
            }
            Storage.update.next()
        }
        
        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }

    /**
     * Saves the job
     */
    private saveJob = () => {
        if (!this.state.validated) {
            return
        }
        if (Authorization.isAuthorized()) {
            this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
        } else {
            this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
        }
    }

    /**
     * Get all the current roof types
     */
    private getRoofTypes = (): RoofType[] => {
        if (!this.state.branche || this.state.branche !== Branche.ROOF_GENERAL) {
            return []
        }
        return Object.keys(RoofType) as any
    }

    /**
     * Get all the current job categories
     */
    private getJobCategories = (): JobCategory[] => {
        if (this.getRoofTypes().length === 0) {
            return []
        }
        if (!this.state.roofType) {
            return []
        }
        const roofTypeMatrix = JobMatrix[this.state.roofType]
        if (!roofTypeMatrix) {
            return []
        }
        const keys = Object.keys(roofTypeMatrix)
        for (const key of keys) {
            if (JobCategory[key] === undefined) {
                return []
            }
        }
        return keys as any
    }

    /**
     * Get all the current house types
     */
    private getHouseTypes = (): HouseType[] => {
        if (this.getJobCategories().length === 0) {
            return []
        }
        if (!this.state.jobCategory) {
            return []
        }
        const houseTypeMatrix = JobMatrix[this.state.roofType as any][this.state.jobCategory]
        if (!houseTypeMatrix) {
            return []
        }
        const keys = Object.keys(houseTypeMatrix)
        for (const key of keys) {
            if (HouseType[key] === undefined) {
                return []
            }
        }
        return keys as any
    }

    /**
     * Get all the current roof material types
     */
    private getRoofMaterialTypes = (): RoofMaterialType[] => {
        if (this.getHouseTypes().length === 0) {
            return []
        }
        if (!this.state.houseType) {
            return []
        }
        const roofMaterialTypeMatrix = JobMatrix[this.state.roofType as any][this.state.jobCategory as any][this.state.houseType]
        if (!roofMaterialTypeMatrix) {
            return []
        }
        const keys = Object.keys(roofMaterialTypeMatrix)
        for (const key of keys) {
            if (RoofMaterialType[key] === undefined) {
                return []
            }
        }
        return keys as any
    }

    /**
     * Get all the curent isolation types
     */
    private getRoofIsolationTypes = (): RoofIsolationType[] => {
        if (this.getHouseTypes().length === 0) {
            return []
        }
        if (!this.state.houseType) {
            return []
        }
        const roofIsolationTypeMatrix = JobMatrix[this.state.roofType as any][this.state.jobCategory as any][this.state.houseType]
        if (!roofIsolationTypeMatrix) {
            return []
        }
        const keys = Object.keys(roofIsolationTypeMatrix)
        for (const key of keys) {
            if (RoofIsolationType[key] === undefined) {
                return []
            }
        }
        return keys as any
    }
}