import React from 'react'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import { AddJobRoutes } from '../../AddJobPage'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import FormInput from '../../../../../components/form/input/FormInput'
import FormGroup from '../../../../../components/form/group/FormGroup'
import Storage from '../../../../../storage/Storage'
import FormValidation from '../../../../../core/FormValidation'
import Button from '../../../../../components/button/Button'
import Authorization from '../../../../../core/Authorization'
import Modal from '../../../../../components/modal/Modal'
import NavigationMenuModal from '../../../../../components/modal/navigation/menu/NavigationMenuModal'
import Routes from '../../../../../routes/Routes';

/**
 * The props
 */
interface AddJobPersonalDetailsProps {
    
    /**
     * Checks whether a tab is disabled
     */
    isTabDisabled: (route: AddJobRoutes) => boolean

    /**
     * Navigate to the tab index
     */
    navigate: (route: AddJobRoutes) => void
}

/**
 * The state
 */
export interface AddJobPersonalDetailsState {

    /**
     * The full name of the consumer
     */
    fullName: string|null

    /**
     * The postalcode of the consumer
     */
    postalCode: string|null

    /**
     * Whether or not the component is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The personal details WizardContent within the AddJobPage.
 * 
 * @author Stan Hurks
 */
export default class AddJobPersonalDetails extends React.Component<AddJobPersonalDetailsProps, AddJobPersonalDetailsState> {

    /**
     * The reference for the element to connect to the account
     */
    private connectAccountRef: React.RefObject<HTMLDivElement> = React.createRef()

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.addJob.personalDetails,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            })
        }
    }

    public render = () => {
        return (
            <form className="add-job-personal-detail" onSubmit={() => {
                this.savePersonalDetails()
            }}>
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.addJob.wizard.content.personalDetails.intro.title}>
                        {
                            Translations.translations.pages.addJob.wizard.content.personalDetails.intro.content.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {
                        !Authorization.isAuthorized()
                        &&
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.personalDetails.form.connectAccount.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.personalDetails.form.connectAccount.helperText}>
                                
                            <div ref={this.connectAccountRef}>
                                <Button fullWidth color="orange" size="small" onClick={() => {
                                    if (!this.connectAccountRef.current) {
                                        return
                                    }
                                    Modal.mount.next({
                                        element: (
                                            <NavigationMenuModal onAuthorized={() => {
                                                Routes.redirectURL.next(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                                                Modal.dismiss.next('all')
                                            }} />
                                        ),
                                        target: this.connectAccountRef.current,
                                        position: 'bottom-left',
                                        offsetY: 120,
                                        dismissable: true
                                    })
                                }}>
                                    {
                                        Translations.translations.pages.addJob.wizard.content.personalDetails.form.connectAccount.button
                                    }    
                                </Button> 
                            </div>
                        </FormGroup>
                    }

                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.personalDetails.form.name.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.personalDetails.form.name.helperText}
                        validation={{
                            ...FormValidation.entity.user.fullName,
                            evaluateValue: () => {
                                return this.state.fullName
                            }
                        }}>

                        <FormInput
                            placeholder={Translations.translations.pages.addJob.wizard.content.personalDetails.form.name.placeholder}
                            type="text"
                            value={this.state.fullName}
                            onChange={(fullName) => {
                                this.setState({
                                    fullName
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            ></FormInput>
                    </FormGroup>

                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.personalDetails.form.postalCode.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.personalDetails.form.postalCode.helperText}
                        validation={{
                            ...FormValidation.entity.user.postalCode,
                            evaluateValue: () => {
                                return this.state.postalCode
                            }
                        }}>

                        <FormInput
                            placeholder={Translations.translations.pages.addJob.wizard.content.personalDetails.form.postalCode.placeholder}
                            type="text"
                            value={this.state.postalCode}
                            onChange={(postalCode) => {
                                this.setState({
                                    postalCode
                                }, () => {
                                    this.saveStateInStorage()
                                })
                            }}
                            ></FormInput>
                    </FormGroup>

                    <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.personalDetails.outro.title}>
                        {
                            Translations.translations.pages.addJob.wizard.content.personalDetails.outro.content.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>
                <WizardBottom path={[
                    Route.HOME,
                    Route.CONSUMER_PLACE_YOUR_JOB,
                    Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button><Button color="orange" type="submit" disabled={!this.state.validated}>
                        {
                            Translations.translations.modals.defaultButtons.next
                        }
                    </Button>
                </WizardBottom>
            </form>
        )
    }

    /**
     * Save the state in storage
     */
    private saveStateInStorage = () => {
        Storage.data.page.addJob.personalDetails = this.state
        Storage.update.next()
        
        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }

    /**
     * Save the personal details
     */
    private savePersonalDetails = () => {
        if (!this.state.validated) {
            return
        }
        this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
    }
}