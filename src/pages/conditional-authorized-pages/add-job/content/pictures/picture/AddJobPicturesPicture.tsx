import React from 'react'
import './add-job-pictures-picture.scss'
import Picture from '../../../../../../components/picture/Picture'
import { CameraAlt } from '@material-ui/icons'
import Translations from '../../../../../../translations/Translations'
import TempImageResponse from '../../../../../../backend/response/entity/TempImageResponse'
import Modal from '../../../../../../components/modal/Modal'
import EditTempImageModal from '../../../../../../components/modal/edit-temp-image/EditTempImageModal'
import ImageUpload from '../../../../../../components/image-upload/ImageUpload'
import { Subscription } from 'rxjs'
import Backend from '../../../../../../backend/Backend'
import Loader from '../../../../../../components/loader/Loader'
import JobImageResponse from '../../../../../../backend/response/entity/JobImageResponse'
import EditJobImageModal from '../../../../../../components/modal/edit-job-image/EditJobImageModal'

/**
 * The props
 */
interface AddJobPicturesPictureProps {

    /**
     * The type of component
     */
    type: 'job-image' | 'temp-image'

    /**
     * The temp image
     */
    tempImage?: TempImageResponse

    /**
     * The job image (editing existing job)
     */
    jobImage?: JobImageResponse

    /**
     * The job id
     */
    jobId?: string

    /**
     * Whenever an update occurs in the temp image
     */
    onChangeTempImage?: () => void

    /**
     * Whenever a new image is available
     */
    onNewTempImage?: (tempImage: TempImageResponse) => void

    /**
     * Whenever an update occurs in the job image
     */
    onChangeJobImage?: () => void

    /**
     * Whenever a new job image is available
     */
    onNewJobImage?: (jobImage: JobImageResponse) => void
}

/**
 * The state
 */
interface AddJobPicturesPictureState {

    /**
     * Whether or not a new image has been requested from this picture.
     */
    newImageRequestedFromThisPicture: boolean
}

/**
 * A picture in the pictures section of the 'AddJob' page.
 * 
 * @author Stan Hurks
 */
export default class AddJobPicturesPicture extends React.Component<AddJobPicturesPictureProps, AddJobPicturesPictureState> {

    /**
     * The reference to the component
     */
    private ref: React.RefObject<HTMLDivElement> = React.createRef()

    /**
     * Whenever a new image upload has been completed.
     */
    private subscriptionNewImageBase64!: Subscription

    /**
     * Whenever a new image upload has been cancelled or errored.
     */
    private subscriptionUploadFailed!: Subscription

    constructor(props: any) {
        super(props)

        this.state = {
            newImageRequestedFromThisPicture: false
        }
    }

    public componentDidMount = () => {
        this.subscriptionNewImageBase64 = ImageUpload.newImageBase64.subscribe(this.onNewImageBase64)
        this.subscriptionUploadFailed = ImageUpload.uploadFailed.subscribe(this.onUploadFailed)
    }

    public componentWillUnmount = () => {
        this.subscriptionNewImageBase64.unsubscribe()
        this.subscriptionUploadFailed.unsubscribe()
    }

    public render = () => {
        return (
            <div className="add-job-pictures-picture" ref={this.ref} onClick={() => {
                if (!this.ref.current) {
                    return
                }
                
                if (this.props.jobImage) {
                    Modal.mount.next({
                        element: (
                            <EditJobImageModal
                                jobImage={this.props.jobImage}
                                onNewImageRequest={() => {
                                    this.setState({
                                        newImageRequestedFromThisPicture: true
                                    })
                                }}
                                onChangeJobImage={() => {
                                    if (!this.props.onChangeJobImage) {
                                        return
                                    }
                                    this.props.onChangeJobImage()

                                    Modal.dismiss.next()
                                }}
                                />
                        ),
                        target: this.ref.current,
                        position: this.ref.current.getBoundingClientRect().right > window.innerWidth / 2 ? 'left' : 'right',
                        offsetY: 120,
                        dismissable: true
                    })
                } else if (this.props.tempImage) {
                    Modal.mount.next({
                        element: (
                            <EditTempImageModal
                                tempImage={this.props.tempImage} 
                                onNewImageRequest={() => {
                                    this.setState({
                                        newImageRequestedFromThisPicture: true
                                    })
                                }}
                                onChangeTempImage={() => {
                                    if (!this.props.onChangeTempImage) {
                                        return
                                    }
                                    this.props.onChangeTempImage()

                                    Modal.dismiss.next()
                                }}
                                />
                        ),
                        target: this.ref.current,
                        position: this.ref.current.getBoundingClientRect().right > window.innerWidth / 2 ? 'left' : 'right',
                        offsetY: 120,
                        dismissable: true
                    })
                } else if (!this.props.tempImage) {
                    this.setState({
                        newImageRequestedFromThisPicture: true
                    })
                    ImageUpload.prompt.next()
                }
            }}>
                <div className="add-job-pictures-picture-container">
                    {
                        !this.props.tempImage && !this.props.jobImage
                        ?
                        <div className="add-job-pictures-picture-no-picture">
                            <div className="add-job-pictures-picture-no-picture-content">
                                <div className="add-job-pictures-picture-no-picture-content-top">
                                    <CameraAlt />
                                </div><div className="add-job-pictures-picture-no-picture-content-label">
                                    {
                                        Translations.translations.pages.addJob.wizard.content.pictures.pictures.newPicture.label
                                    }
                                </div>
                            </div>
                        </div>
                        : (
                            this.props.tempImage
                            ?
                            <Picture image={this.props.tempImage.image} size={180} />
                            :
                            <Picture image={(this.props.jobImage as JobImageResponse).image} size={180} />
                        )
                    }
                </div>
            </div>
        )
    }

    /**
     * Whenever theres a new image upload available.
     */
    private onNewImageBase64 = (base64Content: string) => {
        if (!this.state.newImageRequestedFromThisPicture) {
            return
        }

        if (this.props.type === 'temp-image') {
            if (this.props.tempImage) {
                this.editTempImageContent(base64Content, this.props.tempImage)
            } else {
                this.addTempImage(base64Content)
            }
        } else if (this.props.type === 'job-image') {
            if (this.props.jobImage) {
                this.editJobImageContent(base64Content, this.props.jobImage)
            } else {
                this.addJobImage(base64Content)
            }
        }

        this.setState({
            newImageRequestedFromThisPicture: false
        })
    }

    /**
     * Whenever an image upload has failed.
     */
    private onUploadFailed = () => {
        if (this.state.newImageRequestedFromThisPicture) {
            this.setState({
                newImageRequestedFromThisPicture: false
            })
        }
    }

    /**
     * Add a new temp image
     */
    private addTempImage = (base64Content: string) => {
        Loader.set.next(Translations.translations.backend.controllers.consumer.job.register.addTempImage.loader)
        Backend.controllers.consumerJobRegister.addTempImage({
            content: base64Content
        }).then((response) => {
            if (this.props.onNewTempImage) {
                this.props.onNewTempImage(response.data)
            }
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.consumer.job.register.addTempImage.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }

    /**
     * Add a new job image
     */
    private addJobImage = (base64Content: string) => {
        if (!this.props.jobId) {
            return
        }
        Loader.set.next(Translations.translations.backend.controllers.consumer.job.addJobImage.loader)
        Backend.controllers.consumerJob.addJobImage(this.props.jobId, {
            content: base64Content
        }).then((response) => {
            if (this.props.onNewJobImage) {
                this.props.onNewJobImage(response.data)
            }
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.consumer.job.addJobImage.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }

    /**
     * Edit the content of an existing temp image.
     */
    private editTempImageContent = (base64Content: string, tempImage: TempImageResponse) => {
        Loader.set.next(Translations.translations.backend.controllers.consumer.job.register.editTempImageContent.loader)
        Backend.controllers.consumerJobRegister.editTempImageContent(tempImage.id, {
            content: base64Content
        }).then((response) => {
            if (this.props.onNewTempImage) {
                this.props.onNewTempImage(response.data)
            }

            Modal.dismiss.next('all')
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.consumer.job.register.editTempImageContent.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }

    /**
     * Edit the content of an existing job image.
     */
    private editJobImageContent = (base64Content: string, jobImage: JobImageResponse) => {
        Loader.set.next(Translations.translations.backend.controllers.consumer.job.editJobImageContent.loader)
        Backend.controllers.consumerJob.editJobImageContent(jobImage.id, {
            content: base64Content
        }).then((response) => {
            if (this.props.onNewJobImage) {
                this.props.onNewJobImage(response.data)
            }

            Modal.dismiss.next('all')
        }).catch((response) => {
            const translation = Translations.translations.backend.controllers.consumer.job.editJobImageContent.responses[response.status]
            if (translation !== undefined) {
                Modal.mountErrorHistory.next(translation)
            }
        })
    }
}