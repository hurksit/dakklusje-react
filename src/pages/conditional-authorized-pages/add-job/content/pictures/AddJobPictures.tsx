import React from 'react'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import { AddJobRoutes } from '../../AddJobPage'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import TempImageResponse from '../../../../../backend/response/entity/TempImageResponse'
import Storage from '../../../../../storage/Storage'
import AddJobPicturesPicture from './picture/AddJobPicturesPicture'
import ProgressSpinner from '../../../../../components/progress-spinner/ProgressSpinner'
import Backend from '../../../../../backend/Backend'
import './add-job-pictures.scss'
import Button from '../../../../../components/button/Button'
import Authorization from '../../../../../core/Authorization'
import JobImageResponse from '../../../../../backend/response/entity/JobImageResponse'
import Routes from '../../../../../routes/Routes'

/**
 * The props
 */
interface AddJobPicturesProps {
    
    /**
     * Checks whether a tab is disabled
     */
    isTabDisabled: (route: AddJobRoutes) => boolean

    /**
     * Navigate to the tab index
     */
    navigate: (route: AddJobRoutes) => void

    /**
     * Place the job
     */
    placeJob: () => void

    /**
     * The job (when editing a job this will be filled)
     */
    jobId?: string
}

/**
 * The state
 */
export interface AddJobPicturesState {
    /**
     * The list of temp images
     */
    tempImages: TempImageResponse[]

    /**
     * The job images, in case of editing an existing job.
     */
    jobImages: JobImageResponse[]

    /**
     * Whether or not the temp images are loaded
     */
    imagesLoaded?: boolean
}

/**
 * The tempImages WizardContent within the AddJobPage.
 * 
 * @author Stan Hurks
 */
export default class AddJobPictures extends React.Component<AddJobPicturesProps, AddJobPicturesState> {

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.addJob.pictures,
            imagesLoaded: false
        }
    }

    public componentDidMount = () => {
        if (this.props.jobId) {
            this.updateJobImages()

            Routes.setCurrentRoute.next({
                route: Route.CONSUMER_EDIT_JOB_PICTURES,
                params: {
                    jobId: String(this.props.jobId)
                }
            })
        } else {
            this.updateTempImages()
        }
    }

    public render = () => {
        return (
            <div className="add-job-pictures">
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.addJob.wizard.content.pictures.intro.title}>
                        {
                            Translations.translations.pages.addJob.wizard.content.pictures.intro.content(
                                this.props.jobId
                                ?
                                this.state.jobImages.length
                                :
                                this.state.tempImages.length
                            )
                            .map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    {
                        this.state.imagesLoaded
                        ?
                        (
                            this.props.jobId
                            ?
                            this.renderJobImages()
                            :
                            this.renderTempImages()
                        )
                        :
                        <ProgressSpinner />
                    }

                    {
                        Authorization.isAuthorized() && this.props.jobId === undefined
                        &&
                        <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.loginDetails.outro.title}>
                            {
                                Translations.translations.pages.addJob.wizard.content.pictures.outro.authorizedContent.map((part, partIndex) =>
                                    <div key={partIndex}>
                                        {
                                            part
                                        }
                                    </div>
                                )
                            }
                        </WizardContentOutro>
                    }

                    {
                        !Authorization.isAuthorized() && this.props.jobId === undefined
                        &&
                        <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.pictures.outro.title}>
                            {
                                Translations.translations.pages.addJob.wizard.content.pictures.outro.content.map((part, partIndex) =>
                                    <div key={partIndex}>
                                        {
                                            part
                                        }
                                    </div>
                                )
                            }
                        </WizardContentOutro>
                    }
                </WizardContent>
                <WizardBottom path={[
                    Route.HOME,

                    ...this.props.jobId
                    ? [
                        Route.CONSUMER_EDIT_JOB,
                        Route.CONSUMER_EDIT_JOB_PICTURES
                    ]
                    : [
                        Route.CONSUMER_PLACE_YOUR_JOB,
                        Route.CONSUMER_PLACE_YOUR_JOB_PICTURES
                    ]
                ]} params={{
                    ...this.props.jobId
                    ? {
                        jobId: String(this.props.jobId)
                    }
                    : {}
                }}>
                    {
                        this.props.jobId === undefined
                        &&
                        <Button color="white" onClick={() => {
                            if (Authorization.isAuthorized()) {
                                this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB)
                            } else {
                                this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS)
                            }
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.back
                            }
                        </Button>
                    }

                    {
                        Authorization.isAuthorized() && this.props.jobId === undefined
                        &&
                        <Button color="orange" type="submit" onClick={() => {
                            this.props.placeJob()
                        }}>
                            {
                                Translations.translations.pages.addJob.wizard.content.loginDetails.form.buttons.addJob
                            }
                        </Button>
                    }

                    {
                        !Authorization.isAuthorized() && this.props.jobId === undefined
                        &&
                        <Button color="orange" type="submit" disabled={this.props.isTabDisabled(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)} onClick={() => {
                            this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS)
                        }}>
                            {
                                Translations.translations.modals.defaultButtons.next
                            }
                        </Button>
                    }
                </WizardBottom>
            </div>
        )
    }

    /**
     * Render the job images
     */
    private renderJobImages = () => {
        return (
            <div className="add-job-pictures-pictures">
                {
                    this.state.jobImages.map((jobImage, jobImageIndex) => 
                        <AddJobPicturesPicture jobImage={jobImage} key={jobImageIndex} onNewJobImage={() => {
                            this.updateJobImages()
                        }} onChangeJobImage={() => {
                            this.updateJobImages()
                        }} type="job-image" />
                    )
                }
                {
                    this.state.jobImages.length < 5
                    &&
                    <AddJobPicturesPicture onNewJobImage={(jobImage) => {
                        this.setState({
                            jobImages: [
                                ...this.state.jobImages,
                                jobImage
                            ]
                        })
                    }} onChangeJobImage={() => {
                        this.updateJobImages()
                    }} type="job-image" jobId={this.props.jobId} />
                }
            </div>
        )
    }

    /**a
     * Render the tempImages
     */
    private renderTempImages = () => {
        return (
            <div className="add-job-pictures-pictures">
                {
                    this.state.tempImages.map((tempImage, tempImageIndex) => 
                        <AddJobPicturesPicture tempImage={tempImage} key={tempImageIndex} onNewTempImage={() => {
                            this.updateTempImages()
                        }} onChangeTempImage={() => {
                            this.updateTempImages()
                        }} type="temp-image" />
                    )
                }
                {
                    this.state.tempImages.length < 5
                    &&
                    <AddJobPicturesPicture onNewTempImage={(tempImage) => {
                        this.setState({
                            tempImages: [
                                ...this.state.tempImages,
                                tempImage
                            ]
                        })
                    }} onChangeTempImage={() => {
                        this.updateTempImages()
                    }} type="temp-image" />
                }
            </div>
        )
    }

    /**
     * Update the temp images
     */
    private updateTempImages = () => {
        Backend.controllers.consumerJobRegister.listTempImages().then((response) => {
            this.setState({
                tempImages: response.data,
                imagesLoaded: true
            }, () => {
                // Force update is necessary due to when the image gets replaced
                // the link for the image in amazon web services remains the same
                this.forceUpdate()
            })
        })
    }

    /**
     * Update the job images
     */
    private updateJobImages = () => {
        if (!this.props.jobId) {
            return
        }
        Backend.controllers.consumerJob.getJob(this.props.jobId).then((response) => {
            this.setState({
                jobImages: response.data.images,
                imagesLoaded: true
            }, () => {
                // Force update is necessary due to when the image gets replaced
                // the link for the image in amazon web services remains the same
                this.forceUpdate()
            })
        })
    }
}