import React from 'react'
import WizardContent from '../../../../../components/wizard/content/WizardContent'
import WizardBottom from '../../../../../components/wizard/bottom/WizardBottom'
import { Route } from '../../../../../enumeration/Route'
import { AddJobRoutes } from '../../AddJobPage'
import WizardContentOutro from '../../../../../components/wizard/content/outro/WizardContentOutro'
import WizardContentIntro from '../../../../../components/wizard/content/intro/WizardContentIntro'
import Translations from '../../../../../translations/Translations'
import FormGroup from '../../../../../components/form/group/FormGroup'
import FormValidation from '../../../../../core/FormValidation'
import FormInput from '../../../../../components/form/input/FormInput'
import Storage from '../../../../../storage/Storage'
import Button from '../../../../../components/button/Button'
import FormCheckbox from '../../../../../components/form/checkbox/FormCheckbox';

/**
 * The props
 */
interface AddJobLoginCredentialsProps {
    
    /**
     * Checks whether a tab is disabled
     */
    isTabDisabled: (route: AddJobRoutes) => boolean

    /**
     * Navigate to the tab index
     */
    navigate: (route: AddJobRoutes) => void

    /**
     * Place the job
     */
    placeJob: () => void
}

/**
 * The state
 */
export interface AddJobLoginCredentialsState {

    /**
     * The emailaddress
     */
    emailAddress: string|null

    /**
     * The password
     */
    password: string|null

    /**
     * The repeated password
     */
    passwordRepeat: string|null

    /**
     * Whether or not the user accepted the terms and conditions
     */
    acceptedTermsAndConditions: boolean

    /**
     * Whether or not the form is validated
     */
    validated?: boolean

    /**
     * Whether or not to reset validation upon update
     */
    resetValidationUponUpdate?: boolean
}

/**
 * The login credentials WizardContent within the AddJobPage.
 * 
 * @author Stan Hurks
 */
export default class AddJobLoginCredentials extends React.Component<AddJobLoginCredentialsProps, AddJobLoginCredentialsState> {

    /**
     * The last used password.
     * 
     * This must be stored here instead of `Storage`, due to that being unsafe.
     * 
     * If the user navigates through the wizard it would be annoying to re-type
     * the passwords every time.
     * 
     * This will make sure though that once the tab is closed in the browser
     * that data is erased.
     */
    private static _lastUsedPassword: string|null = null

    /**
     * Get the last used password
     */
    public static get lastUsedPassword() {
        return AddJobLoginCredentials._lastUsedPassword
    }

    /**
     * The last used passwordRepeat.
     * 
     * This must be stored here instead of `Storage`, due to that being unsafe.
     * 
     * If the user navigates through the wizard it would be annoying to re-type
     * the passwords every time.
     * 
     * This will make sure though that once the tab is closed in the browser
     * that data is erased.
     */
    private static _lastUsedPasswordRepeat: string|null = null

    /**
     * Get the last used passwordRepeat
     */
    public static get lastUsedPasswordRepeat() {
        return AddJobLoginCredentials._lastUsedPasswordRepeat
    }

    constructor(props: any) {
        super(props)

        this.state = {
            ...Storage.data.page.addJob.loginDetails,
            password: AddJobLoginCredentials.lastUsedPassword,
            passwordRepeat: AddJobLoginCredentials.lastUsedPasswordRepeat,
            validated: false,
            resetValidationUponUpdate: false
        }

        FormValidation.reset()
        setTimeout(() => {
            this.forceUpdate()
        })
    }

    public componentDidMount = () => {
        this.setState({
            validated: FormValidation.validate()
        })
    }

    public componentDidUpdate = () => {
        if (this.state.resetValidationUponUpdate) {
            this.setState({
                validated: FormValidation.validate(),
                resetValidationUponUpdate: false
            })
        }
    }

    public render = () => {
        return (
            <form className="add-job-login-credentials" onSubmit={() => {
                this.props.placeJob()
            }}>
                <WizardContent>
                    <WizardContentIntro title={Translations.translations.pages.addJob.wizard.content.loginDetails.intro.title}>
                        {
                            Translations.translations.pages.addJob.wizard.content.loginDetails.intro.content.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentIntro>

                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.loginDetails.form.emailAddress.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.loginDetails.form.emailAddress.helperText}
                        validation={{
                            ...FormValidation.entity.user.email,
                            evaluateValue: () => {
                                return this.state.emailAddress
                            }
                        }}>

                        <FormInput
                            value={this.state.emailAddress}
                            type="email"
                            placeholder={Translations.translations.pages.addJob.wizard.content.loginDetails.form.emailAddress.placeholder}
                            onChange={(emailAddress) => {
                                this.setState({
                                    emailAddress
                                }, () => {
                                    this.saveState()
                                })
                            }}
                            />
                    </FormGroup>

                    <div className="inline">
                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.loginDetails.form.password.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.loginDetails.form.password.helperText}
                            validation={{
                                ...FormValidation.entity.user.password,
                                evaluateValue: () => {
                                    return this.state.password
                                }
                            }}>

                            <FormInput
                                value={this.state.password}
                                type="password"
                                placeholder={Translations.translations.pages.addJob.wizard.content.loginDetails.form.password.placeholder}
                                onChange={(password) => {
                                    this.setState({
                                        password
                                    }, () => {
                                        this.saveState()
                                    })
                                }}
                                />
                        </FormGroup>

                        <FormGroup
                            label={Translations.translations.pages.addJob.wizard.content.loginDetails.form.passwordRepeat.label}
                            helperText={Translations.translations.pages.addJob.wizard.content.loginDetails.form.passwordRepeat.helperText}
                            validation={{
                                ...FormValidation.entity.user.password,
                                string: {
                                    allowEmpty: true,
                                    matchEvaluatedValue: () => {
                                        return this.state.password || ''
                                    }
                                },
                                evaluateValue: () => {
                                    return this.state.passwordRepeat
                                }
                            }}>

                            <FormInput
                                value={this.state.passwordRepeat}
                                type="password"
                                placeholder={Translations.translations.pages.addJob.wizard.content.loginDetails.form.passwordRepeat.placeholder}
                                onChange={(passwordRepeat) => {
                                    this.setState({
                                        passwordRepeat
                                    }, () => {
                                        this.saveState()
                                    })
                                }}
                                />
                        </FormGroup>
                    </div>

                    <FormGroup
                        label={Translations.translations.pages.addJob.wizard.content.loginDetails.form.termsAndAgreements.label}
                        helperText={Translations.translations.pages.addJob.wizard.content.loginDetails.form.termsAndAgreements.helperText}
                        validation={{
                            mustBeTruthy: true,
                            evaluateValue: () => {
                                return this.state.acceptedTermsAndConditions
                            }
                        }}>

                        <FormCheckbox
                            label={Translations.translations.pages.addJob.wizard.content.loginDetails.form.termsAndAgreements.checkbox.label}
                            value={this.state.acceptedTermsAndConditions}
                            onChange={(acceptedTermsAndConditions) => {
                                this.setState({
                                    acceptedTermsAndConditions
                                }, () => {
                                    this.saveState()
                                })
                            }}
                            />
                    </FormGroup>

                    <WizardContentOutro title={Translations.translations.pages.addJob.wizard.content.loginDetails.outro.title}>
                        {
                            Translations.translations.pages.addJob.wizard.content.loginDetails.outro.content.map((part, partIndex) =>
                                <div key={partIndex}>
                                    {
                                        part
                                    }
                                </div>
                            )
                        }
                    </WizardContentOutro>
                </WizardContent>
                <WizardBottom path={[
                    Route.HOME,
                    Route.CONSUMER_PLACE_YOUR_JOB,
                    Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS
                ]}>
                    <Button color="white" onClick={() => {
                        this.props.navigate(Route.CONSUMER_PLACE_YOUR_JOB_PICTURES)
                    }}>
                        {
                            Translations.translations.modals.defaultButtons.back
                        }
                    </Button>

                    <Button color="orange" type="submit" disabled={!this.state.validated}>
                        {
                            Translations.translations.pages.addJob.wizard.content.loginDetails.form.buttons.addJob
                        }
                    </Button>
                </WizardBottom>
            </form>
        )
    }

    /**
     * Process state changes including writing to storage and to static members.
     */
    private saveState = () => {
        Storage.data.page.addJob.loginDetails = {
            ...this.state,

            // Make sure not to write the password to storage.
            password: null,
            passwordRepeat: null
        }
        Storage.update.next()
        
        AddJobLoginCredentials._lastUsedPassword = this.state.password
        AddJobLoginCredentials._lastUsedPasswordRepeat = this.state.passwordRepeat

        this.setState({
            validated: FormValidation.validate(),
            resetValidationUponUpdate: true
        })
    }
}