import AuthenticationController from './controller/authentication/AuthenticationController'
import ConsumerController from './controller/consumer/ConsumerController'
import ConsumerJobRegisterController from './controller/consumer/job/register/ConsumerJobRegisterController'
import ConsumerJobController from './controller/consumer/job/ConsumerJobController'
import ForgotPasswordController from './controller/forgotpassword/ForgotPasswordController'
import InboxController from './controller/inbox/InboxController'
import InboxConnectionController from './controller/inbox/connection/InboxConnectionController'
import InboxConnectionConsumerController from './controller/inbox/connection/consumer/InboxConnectionConsumerController'
import InboxConnectionRoofSpecialistController from './controller/inbox/connection/roofspecialist/InboxConnectionRoofSpecialistController'
import PaymentController from './controller/payment/PaymentController'
import ProfileController from './controller/profile/ProfileController'
import ReportController from './controller/report/ReportController'
import ReviewController from './controller/review/ReviewController'
import RoofSpecialistController from './controller/roofspecialist/RoofSpecialistController'
import RoofSpecialistJobController from './controller/roofspecialist/job/RoofSpecialistJobController'
import RoofSpecialistPortfolioController from './controller/roofspecialist/portfolio/RoofSpecialistPortfolioController'
import SocialMediaController from './controller/socialmedia/SocialMediaController'
import FirebaseController from './controller/firebase/FirebaseController'
import OriginController from './controller/origin/OriginController';

/**
 * This class manages everything backend related.
 *  
 * All entities have the same name as the corresponding Controller in the Spring MVC backend.
 * 
 * All entities can use the same interface for standard CRUD operations.
 * 
 * All classes that extend the `APIManager` class should be put as an instance in the static `Backend.communication` member.
 * The convention for this is `Backend.communication.${controllerName}`.
 * 
 * @author Stan Hurks
 */
export default class Backend {

    /**
     * All the controllers, based on the controllers used in the API.
     */
    public static controllers = {
        authentication: new AuthenticationController(),

        consumer: new ConsumerController(),

        consumerJob: new ConsumerJobController(),
        
        consumerJobRegister: new ConsumerJobRegisterController(),

        firebase: new FirebaseController(),

        forgotPassword: new ForgotPasswordController(),
        
        inbox: new InboxController(),

        inboxConnection: new InboxConnectionController(),
        
        inboxConnectionConsumer: new InboxConnectionConsumerController(),

        inboxConnectionRoofSpecialist: new InboxConnectionRoofSpecialistController(),

        origin: new OriginController(),

        payment: new PaymentController(),

        profile: new ProfileController(),

        report: new ReportController(),

        review: new ReviewController(),

        roofSpecialist: new RoofSpecialistController(),

        roofSpecialistJob: new RoofSpecialistJobController(),

        roofSpecialistPortfolio: new RoofSpecialistPortfolioController(),

        socialMedia: new SocialMediaController()
    }
}