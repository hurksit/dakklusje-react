import RoofSpecialistResponse from './RoofSpecialistResponse'
import { PaymentType } from '../../enumeration/PaymentType'
import RoofSpecialistFilterResponse from './RoofSpecialistFilterResponse'

/**
 * The roof specialist response for the own user information.
 * 
 * @author Stan Hurks
 */
export default interface AuthorizedRoofSpecialistResponse extends RoofSpecialistResponse {

    isValidatedByAdmin: boolean

    paymentType: PaymentType|null

    credits: number

    filters: RoofSpecialistFilterResponse[]

}