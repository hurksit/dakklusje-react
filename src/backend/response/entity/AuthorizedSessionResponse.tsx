import AuthorizedUserResponse from './AuthorizedUserResponse'
import { LoginType } from '../../enumeration/LoginType'

/**
 * The response for a session entity for an authorized user.
 * 
 * @author Stan Hurks
 */
export default interface AuthorizedSessionResponse {

    id: string

    user: AuthorizedUserResponse

    loginType: LoginType

    uuid: string

    createdAt: number
    
}