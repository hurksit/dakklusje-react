import ImageResponse from './ImageResponse'

/**
 * The response object for a JobImage entity.
 * 
 * @author Stan Hurks
 */
export default interface JobImageResponse {

    id: string

    image: ImageResponse
}