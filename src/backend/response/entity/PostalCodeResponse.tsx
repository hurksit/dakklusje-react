/**
 * The response for a postal code entity.
 * 
 * @author Stan Hurks
 */
export default interface PostalCodeResponse {

    postalCode: string
    
    streetName: string

    cityName: string

    latitude: number

    longitude: number
    
}