import AnonymousReviewResponse from './AnonymousReviewResponse'

/**
 * The response for a review entity.
 * 
 * @author Stan Hurks
 */
export default interface ReviewResponse extends AnonymousReviewResponse {

    id: string

    consumerUserId: string

    consumerPersonalDetailsFullName: string|null

    createdAt: number

}