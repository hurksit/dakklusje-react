/**
 * The response for the file entity.
 * 
 * @author Stan Hurks
 */
export default interface FileResponse {

    id: string

    content: string

    createdAt: number

    title: string
    
}