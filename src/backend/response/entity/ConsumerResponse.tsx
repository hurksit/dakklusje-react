import PostalCodeResponse from './PostalCodeResponse'
import ImageResponse from './ImageResponse'

/**
 * The response of a consumer entity.
 * 
 * @author Stan Hurks
 */
export default interface ConsumerResponse {

    id: string

    personalDetailsFullName: string|null

    personalDetailsPostalCode: PostalCodeResponse

    accountDeactivated: boolean

    profilePicture: ImageResponse|null
    
}