import UserResponse from './UserResponse'
import { JobCategory } from '../../enumeration/JobCategory'
import { Branche } from '../../enumeration/Branche'
import { RoofType } from '../../enumeration/RoofType'
import { HouseType } from '../../enumeration/HouseType'
import { RoofIsolationType } from '../../enumeration/RoofIsolationType'
import { RoofMaterialType } from '../../enumeration/RoofMaterialType'
import { MeasurementUnit } from '../../enumeration/MeasurementUnit'
import JobImageResponse from './JobImageResponse'

/**
 * The response for a job entity.
 * 
 * @author Stan Hurks
 */
export default interface JobResponse {

    id: string

    user: UserResponse
        
    title: string
    
    description: string|null
    
    branche: Branche|null
    
    roofType: RoofType|null
    
    jobCategory: JobCategory|null

    houseType: HouseType|null

    roofIsolationType: RoofIsolationType|null

    roofMaterialType: RoofMaterialType|null

    isDeleted: boolean

    surfaceMeasurements: number|null

    surfaceMeasurementsUnit: MeasurementUnit|null

    images: JobImageResponse[]

    createdAt: number

    updatedAt: number
}