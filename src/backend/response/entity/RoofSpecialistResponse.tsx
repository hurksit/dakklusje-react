import { Branche } from '../../enumeration/Branche'
import CocCompanyResponse from './CocCompanyResponse'
import PortfolioImageResponse from './PortfolioImageResponse'
import ImageResponse from './ImageResponse'

/**
 * The response of a roof specialist entity.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistResponse {

    id: string

    branche: Branche

    offersWarranty: boolean

    cocCompany: CocCompanyResponse

    companyDescription: string

    contactPersonName: string

    contactPersonPhoneNumber: string
 
    portfolioImages: PortfolioImageResponse[]

    profilePicture: ImageResponse|null

    vcaNumber: string|null

}