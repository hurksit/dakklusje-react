import UserResponse from './UserResponse'

/**
 * The response for the message entity.
 * 
 * @author Stan Hurks
 */
export default interface MessageResponse {

    id: string

    connectionId: string

    sender: UserResponse

    receiver: UserResponse

    createdAt: number

    content: string

    readAt: number|null
    
}