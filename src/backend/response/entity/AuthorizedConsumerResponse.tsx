import ConsumerResponse from './ConsumerResponse'

/**
 * The response for a customer entity when it's the users own information.
 *
 * @author Stan Hurks
 */
export default interface AuthorizedConsumerResponse extends ConsumerResponse {
}