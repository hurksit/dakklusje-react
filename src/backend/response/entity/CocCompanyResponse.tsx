import PostalCodeResponse from './PostalCodeResponse'

/**
 * The response for the coc company entity.
 * 
 * @author Stan Hurks
 */
export default interface CocCompanyResponse {
    
    id: string

    cocNumber: string

    companyName: string

    postalCode: PostalCodeResponse

    addressLine: string
    
}