import UserResponse from './UserResponse'
import AuthorizedConsumerResponse from './AuthorizedConsumerResponse'
import AuthorizedRoofSpecialistResponse from './AuthorizedRoofSpecialistResponse'

/**
 * The response for own user information (ME).
 * 
 * @author Stan Hurks
 */
export default interface AuthorizedUserResponse extends UserResponse {

    emailAddress: string

    userSettings: string|null

    locale: string

    consumer: AuthorizedConsumerResponse|null

    roofSpecialist: AuthorizedRoofSpecialistResponse|null
}