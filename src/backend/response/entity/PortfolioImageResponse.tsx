import ImageResponse from './ImageResponse'

/**
 * The response for the portfolio image entity.
 * 
 * @author Stan Hurks
 */
export default interface PortfolioImageResponse {

    id: string

    image: ImageResponse
}