import FileResponse from './FileResponse'

/**
 * The response for an image entity.
 *
 * @author Stan Hurks
 */
export default interface ImageResponse {

    id: string

    file: FileResponse

    backgroundPosition: string

    zoom: number
}