import RoofSpecialistResponse from './RoofSpecialistResponse'
import ConsumerResponse from './ConsumerResponse'
import ImageResponse from './ImageResponse'

/**
 * The response for a user entity.
 * 
 * @author Stan Hurks
 */
export default interface UserResponse {
    
    id: string

    roofSpecialist: RoofSpecialistResponse|null

    consumer: ConsumerResponse|null

    profilePicture: ImageResponse|null

}