import RoofSpecialistResponse from './RoofSpecialistResponse'
import ConsumerResponse from './ConsumerResponse'
import JobResponse from './JobResponse'
import MessageResponse from './MessageResponse'

/**
 * The response for the connection entity.
 * 
 * @author Stan Hurks
 */
export default interface ConnectionResponse {

    id: string

    roofSpecialist: RoofSpecialistResponse

    consumer: ConsumerResponse

    job: JobResponse

    messages: MessageResponse[]

    costsAcceptedAt: number|null

    costsDeclinedAt: number|null

    acceptedAt: number|null

    declinedAt: number|null

    createdAt: number

    lastActivity: number

}