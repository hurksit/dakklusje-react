import { RoofSpecialistFilterType } from '../../enumeration/RoofSpecialistFilterType'

/**
 * The response for a roof specialist filter.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistFilterResponse {

    filterType: RoofSpecialistFilterType

    value: string
}