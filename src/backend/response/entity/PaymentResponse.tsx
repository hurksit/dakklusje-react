import { PaymentType } from '../../enumeration/PaymentType'

/**
 * The response model for a payment entity.
 * 
 * @author Stan Hurks
 */
export default interface PaymentResponse {

    id: string

    userId: string

    molliePaymentId: string

    paymentType: PaymentType

    createdAt: number

    isReversed: boolean

    isReversalPaid: boolean
    
}