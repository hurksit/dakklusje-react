import ImageResponse from './ImageResponse'
import { JobCategory } from '../../enumeration/JobCategory'

/**
 * The response for a review entity without the users personal information and creation date.
 *
 * @author Stan Hurks
 */
export default interface AnonymousReviewResponse {

    title: string

    rating: number

    jobCategory: JobCategory

    description: string

    image: ImageResponse|null

}