import ImageResponse from './ImageResponse'

/**
 * The API response for a temp image entity.
 * 
 * @author Stan Hurks
 */
export default interface TempImageResponse {

    id: string

    image: ImageResponse

}