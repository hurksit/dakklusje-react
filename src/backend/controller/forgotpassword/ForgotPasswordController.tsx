import Controller from '../../../shared/backend/Controller'
import RequestPasswordResetRequest from './request/RequestPasswordResetRequest'
import Http from '../../Http'
import PerformPasswordResetRequest from './request/PerformPasswordResetRequest'
import HttpResponse from '../../../shared/backend/HttpResponse'

/**
 * All endpoints related to resetting the password.
 * 
 * @author Stan Hurks
 */
export default class ForgotPasswordController extends Controller {
    
    public apiBaseURL: string = 'forgot-password'

    /**
     * Request a password reset
     */
    public readonly requestPasswordReset = (request: RequestPasswordResetRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/reset/request`, request, Http.generateHeaders())
    }

    /**
     * Validate a password reset
     * @param uuid the uuid
     */
    public readonly validatePasswordReset = (uuid: string): Promise<HttpResponse<void>> => {
        return Http.get(`${this.apiBaseURL}/reset/validate/${uuid}`, Http.generateHeaders())
    }

    /**
     * Perform a password reset
     */
    public readonly performPasswordReset = (request: PerformPasswordResetRequest): Promise<HttpResponse<void>> => { 
        return Http.put(`${this.apiBaseURL}/reset/perform`, request, Http.generateHeaders())
    }
}