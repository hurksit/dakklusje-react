/**
 * The request body to perform a password reset.
 * 
 * @author Stan Hurks
 */
export default interface PerformPasswordResetRequest {

    uuid: string

    password: string

}