/**
 * The request for a password reset.
 * 
 * @author Stan Hurks
 */
export default interface RequestPasswordResetRequest {

    emailAddress: string
    
}