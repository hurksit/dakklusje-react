import Controller from '../../../../../shared/backend/Controller'
import HttpResponse from '../../../../../shared/backend/HttpResponse'
import Http from '../../../../Http'
import InboxConnectionResponse from '../../response/InboxConnectionResponse'

/**
 * All endpoints regarding the inbox connection functionality for the roof specialists.
 * 
 * @author Stan Hurks
 */
export default class InboxConnectionRoofSpecialistController extends Controller {

    public apiBaseURL: string = 'inbox/connection/roof-specialist'

    /**
     * Validates the costs for the connection
     */
    public readonly validateCosts = (connectionId: string): Promise<HttpResponse<InboxConnectionResponse>> => {
        return Http.get(`${this.apiBaseURL}/validate/${connectionId}`, Http.generateHeaders())
    }

    /**
     * Accept the costs for the connection
     */
    public readonly acceptCosts = (connectionId: string): Promise<HttpResponse<string>> => {
        return Http.post(`${this.apiBaseURL}/accept/${connectionId}`, {}, Http.generateHeaders())
    }

    /**
     * Decline the costs for the connection
     */
    public readonly declineCosts = (connectionId: string): Promise<HttpResponse<string>> => {
        return Http.post(`${this.apiBaseURL}/decline/${connectionId}`, {}, Http.generateHeaders())
    }
}