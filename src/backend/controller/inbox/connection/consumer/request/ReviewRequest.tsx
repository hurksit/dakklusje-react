/**
 * The request for giving a review
 * 
 * @author Stan Hurks
 */
export default interface ReviewRequest {

    content: string

    rating: number
}