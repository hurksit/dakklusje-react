import ImageResponse from '../../../../response/entity/ImageResponse'
import Http from '../../../../Http'
import Controller from '../../../../../shared/backend/Controller'
import ReviewResponse from '../../../../response/entity/ReviewResponse'
import ConnectionResponse from '../../../../response/entity/ConnectionResponse'
import ReviewRequest from './request/ReviewRequest'
import HttpResponse from '../../../../../shared/backend/HttpResponse'

/**
 * All consumer related endpoints regarding the inbox connection.
 * 
 * @author Stan Hurks
 */
export default class InboxConnectionConsumerController extends Controller {

    public apiBaseURL: string = 'inbox/connection/consumer'
    
    /**
     * Get the own review for the connection
     */
    public readonly getReviewForConnection = (connectionId: string): Promise<HttpResponse<ReviewResponse>> => {
        return Http.get(`${this.apiBaseURL}/${connectionId}/own-review`, Http.generateHeaders())
    }

    /**
     * List all portfolio images for a connection.
     */
    public readonly listPortfolioImagesForConnection = (connectionId: string): Promise<HttpResponse<ImageResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/${connectionId}/portfolio-image`, Http.generateHeaders())
    }

    /**
     * List all reviews for a connection.
     */
    public readonly listReviewsForConnection = (connectionId: string): Promise<HttpResponse<ReviewResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/${connectionId}/review`, Http.generateHeaders())
    }

    /**
     * Accept the connection with a roof specialist.
     */
    public readonly acceptConnection = (connectionId: string): Promise<HttpResponse<ConnectionResponse>> => {
        return Http.put(`${this.apiBaseURL}/${connectionId}/accept`, {}, Http.generateHeaders())
    }

    /**
     * Decline the connection with a roof specialist.
     */
    public readonly declineConnection = (connectionId: string): Promise<HttpResponse<ConnectionResponse>> => {
        return Http.put(`${this.apiBaseURL}/${connectionId}/decline`, {}, Http.generateHeaders())
    }

    /**
     * Add a new revision to a review.
     */
    public readonly addReviewRevision = (connectionId: string, request: ReviewRequest): Promise<HttpResponse<ReviewResponse>> => {
        return Http.post(`${this.apiBaseURL}/${connectionId}/review/revision`, request, Http.generateHeaders())
    }

    /**
     * Deletes a review from a connection.
     */
    public readonly deleteReview = (connectionId: string): Promise<HttpResponse<void>> => {
        return Http.delete(`${this.apiBaseURL}/${connectionId}/review`, {}, Http.generateHeaders())
    }
}