import Controller from '../../../../shared/backend/Controller'
import MessageResponse from '../../../response/entity/MessageResponse'
import Http from '../../../Http'
import SendMessageRequest from './request/SendMessageRequest'
import HttpResponse from '../../../../shared/backend/HttpResponse'
import InboxConnectionResponse from '../response/InboxConnectionResponse'
import ReportConnectionRequest from './request/ReportConnectionRequest'

/**
 * All inbox connection related endpoints.
 * 
 * @author Stan Hurks
 */
export default class InboxConnectionController extends Controller {

    public apiBaseURL: string = 'inbox/connection'

    /**
     * Report a connection
     */
    public readonly reportConnection = (request: ReportConnectionRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/report`, request, Http.generateHeaders())
    }

    /**
     * List all connections
     */
    public readonly listConnections = (): Promise<HttpResponse<InboxConnectionResponse[]>> => {
        return Http.get(`${this.apiBaseURL}`, Http.generateHeaders())
    }

    /**
     * Lists all messages for a connection.
     */
    public readonly listMessagesForConnection = (connectionId: string): Promise<HttpResponse<MessageResponse>> => {
        return Http.get(`${this.apiBaseURL}/${connectionId}`, Http.generateHeaders())
    }

    /**
     * Sends a message to the receiver.
     */
    public readonly sendMessage = (connectionId: string, request: SendMessageRequest): Promise<HttpResponse<MessageResponse>> => {
        return Http.post(`${this.apiBaseURL}/${connectionId}/message`, request, Http.generateHeaders())
    }

    /**
     * Read all messages
     */
    public readonly readMessages = (connectionId: string): Promise<HttpResponse<MessageResponse>> => {
        return Http.get(`${this.apiBaseURL}/${connectionId}/read-messages`, Http.generateHeaders())
    }
}