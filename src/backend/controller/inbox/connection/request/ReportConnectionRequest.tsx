/**
 * The request to report a connection.
 * 
 * @author Stan Hurks
 */
export default interface ReportConnectionRequest {

    description: string

    connectionId: string
}