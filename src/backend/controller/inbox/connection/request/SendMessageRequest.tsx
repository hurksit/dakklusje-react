/**
 * The request to send a message in the inbox.
 * 
 * @author Stan Hurks
 */
export default interface SendMessageRequest {
    
    message: string
    
}