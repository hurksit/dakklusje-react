import ConnectionResponse from '../../../response/entity/ConnectionResponse'

/**
 * The response for an inbox connection with filters so roof specialists wont find out the users
 * details before the connection is accepted.
 * 
 * @author Stan Hurks
 */
export default interface InboxConnectionResponse extends ConnectionResponse {
}