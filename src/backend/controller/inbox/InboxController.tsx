import Controller from '../../../shared/backend/Controller'
import Http from '../../Http'
import InboxConnectionResponse from './response/InboxConnectionResponse'
import HttpResponse from '../../../shared/backend/HttpResponse'

/**
 * All inbox related endpoints.
 * 
 * @author Stan Hurks
 */
export default class InboxController extends Controller {

    public apiBaseURL: string = 'inbox'
    
    /**
     * List all conversations
     */
    public readonly listConversations = (): Promise<HttpResponse<InboxConnectionResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/conversation`, Http.generateHeaders())
    }

    /**
     * Get the badge count
     */
    public readonly getBadgeCount = (): Promise<HttpResponse<number>> => {
        return Http.get(`${this.apiBaseURL}/badge-count`, Http.generateHeaders())
    }
}