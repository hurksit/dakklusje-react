import Controller from '../../../shared/backend/Controller'
import Http from '../../Http'
import AuthorizedSessionResponse from '../../response/entity/AuthorizedSessionResponse'
import ReactivateAccountRequest from './request/ReactivateAccountRequest'
import ChangePersonalDetailsRequest from './request/ChangePersonalDetailsRequest'
import HttpResponse from '../../../shared/backend/HttpResponse'
import ChangePersonalDetailsResponse from './response/ChangePersonalDetailsResponse'

/**
 * The controller for the consumer entity.
 * 
 * @author Stan Hurks
 */
export default class ConsumerController extends Controller {

    public apiBaseURL: string = 'consumer'

    /**
     * Deactivates the account for a consumer
     */
    public readonly deactivateAccount = (): Promise<HttpResponse<void>> => {
        return Http.put(`${this.apiBaseURL}/deactivate-account`, {}, Http.generateHeaders())
    }

    /**
     * Reactivates the account for a consumer after being deactivated.
     */
    public readonly reactivateAccount = (request: ReactivateAccountRequest): Promise<HttpResponse<AuthorizedSessionResponse>> => {
        return Http.put(`${this.apiBaseURL}/reactivate-account`, request, Http.generateHeaders())
    }

    /**
     * Changes the personal details for a user.
     */
    public readonly changePersonalDetails = (request: ChangePersonalDetailsRequest): Promise<HttpResponse<ChangePersonalDetailsResponse>> => {
        return Http.put(`${this.apiBaseURL}/personal-details`, request, Http.generateHeaders())
    }
    
}