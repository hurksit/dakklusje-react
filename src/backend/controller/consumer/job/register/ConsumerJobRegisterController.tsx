import Controller from '../../../../../shared/backend/Controller'
import TempImageResponse from '../../../../response/entity/TempImageResponse'
import Http from '../../../../Http'
import ImageRequest from '../../../../request/entity/image/ImageRequest'
import ImageBackgroundStyleRequest from '../../../../request/entity/image/ImageBackgroundStyleRequest'
import HttpResponse from '../../../../../shared/backend/HttpResponse'
import RegisterJobRequest from './request/RegisterJobRequest'
import RegisterJobResponse from './response/RegisterJobResponse'

/**
 * The endpoints for adding the first job as a consumer.
 * 
 * @author Stan Hurks
 */
export default class ConsumerJobRegisterController extends Controller {

    public apiBaseURL: string = 'consumer/job/register'

    /**
     * Get the list of temp images for the user
     */
    public readonly listTempImages = (): Promise<HttpResponse<TempImageResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/temp-image`, Http.generateHeaders())
    }

    /**
     * Add a new temp image based on IP address and user agent and remove archived images
     */
    public readonly addTempImage = (request: ImageRequest): Promise<HttpResponse<TempImageResponse>> => {
        return Http.post(`${this.apiBaseURL}/temp-image`, request, Http.generateHeaders())
    }

    /**
     * Modify the content of a temp image
     */
    public readonly editTempImageContent = (tempImageId: string, request: ImageRequest): Promise<HttpResponse<TempImageResponse>> => {
        return Http.put(`${this.apiBaseURL}/temp-image/${tempImageId}`, request, Http.generateHeaders())
    }

    /**
     * Modify the background styles for an existing temp image.
     */
    public readonly editTempImageBackgroundStyle = (tempImageId: string, request: ImageBackgroundStyleRequest): Promise<HttpResponse<void>> => {
        return Http.put(`${this.apiBaseURL}/temp-image/${tempImageId}/background-style`, request, Http.generateHeaders())
    }

    /**
     * Deletes a temp image by id.
     */
    public readonly deleteTempImage = (tempImageId: string): Promise<HttpResponse<void>> => {
        return Http.delete(`${this.apiBaseURL}/temp-image/${tempImageId}`, {}, Http.generateHeaders())
    }

    /**
     * Register a consumer and place the first job.
     */
    public readonly register = (request: RegisterJobRequest): Promise<HttpResponse<RegisterJobResponse>> => {
        return Http.post(`${this.apiBaseURL}`, request, Http.generateHeaders())
    }
}