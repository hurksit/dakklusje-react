/**
 * The login credentials in the job register request.
 * 
 * @author Stan Hurks
 */
export default interface RegisterJobLoginCredentialsRequest {

    emailAddress: string

    password: string

    acceptedTermsAndConditions: boolean
}