/**
 * The personal details asked when registering a job.
 * 
 * @author Stan Hurks
 */
export default interface RegisterJobPersonalDetailsRequest {

    /**
     * The consumers full name
     */
    name: string

    postalCode: string
}