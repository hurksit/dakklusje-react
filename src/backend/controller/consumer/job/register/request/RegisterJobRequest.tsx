import JobJobRequest from '../../request/JobJobRequest'
import RegisterJobPersonalDetailsRequest from './RegisterJobPersonalDetailsRequest'
import RegisterJobLoginCredentialsRequest from './RegisterJobLoginCredentialsRequest'

/**
 * The request to register a job and creating the account for the consumer.
 * 
 * @author Stan Hurks
 */
export default interface RegisterJobRequest {

    job: JobJobRequest

    personalDetails: RegisterJobPersonalDetailsRequest

    loginCredentials: RegisterJobLoginCredentialsRequest
}