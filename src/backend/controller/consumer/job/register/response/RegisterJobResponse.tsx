/**
 * The response for registering a first job.
 * 
 * @author Stan Hurks
 */
export default interface RegisterJobResponse {

    jobValidated: boolean

    personalDetailsValidated: boolean

    loginCredentialsValidated: boolean
}