import { Branche } from '../../../../enumeration/Branche'
import { RoofType } from '../../../../enumeration/RoofType'
import { JobCategory } from '../../../../enumeration/JobCategory'
import { MeasurementUnit } from '../../../../enumeration/MeasurementUnit'
import { HouseType } from '../../../../enumeration/HouseType'
import { RoofMaterialType } from '../../../../enumeration/RoofMaterialType'
import { RoofIsolationType } from '../../../../enumeration/RoofIsolationType'

/**
 * The part of the request of adding a new/updating an existing job, containing the job information.
 * 
 * @author Stan Hurks
 */
export default interface JobJobRequest {

    title: string

    description: string|null

    branche: Branche

    roofType: RoofType|null

    jobCategory: JobCategory|null

    surfaceMeasurements: number|null

    measurementUnit: MeasurementUnit|null

    houseType: HouseType|null

    roofMaterialType: RoofMaterialType|null

    roofIsolationType: RoofIsolationType|null
}