import JobResponse from '../../../../response/entity/JobResponse'

/**
 * The job response for a consumer in the jobs page.
 * 
 * @author Stan Hurks
 */
export default interface ConsumerJobResponse extends JobResponse {

    /**
     * The amount of connections associated with the job
     */
    connectionCount: number
}