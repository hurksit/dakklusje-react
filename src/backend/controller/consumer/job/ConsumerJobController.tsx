import Controller from '../../../../shared/backend/Controller'
import HttpResponse from '../../../../shared/backend/HttpResponse'
import JobResponse from '../../../response/entity/JobResponse'
import Http from '../../../Http'
import JobJobRequest from './request/JobJobRequest'
import ImageRequest from '../../../request/entity/image/ImageRequest'
import JobImageResponse from '../../../response/entity/JobImageResponse'
import ImageBackgroundStyleRequest from '../../../request/entity/image/ImageBackgroundStyleRequest'
import Pagination from '../../../pagination/Pagination'
import ConsumerJobResponse from './response/ConsumerJobResponse'

/**
 * The controller for the endpoints related to jobs for consumers.
 * 
 * @author Stan Hurks
 */
export default class ConsumerJobController extends Controller {

    public apiBaseURL: string = 'consumer/job'

    /**
     * Get a page of the paginated jobs.
     */
    public getPaginatedJobs = (pageNumber: number, filter: string): Promise<HttpResponse<Pagination<ConsumerJobResponse>>> => {
        return Http.get(`${this.apiBaseURL}/paginated-jobs/${pageNumber}`, {
            ...Http.generateHeaders(),
            filter
        })
    }

    /**
     * Get a job by id
     */
    public readonly getJob = (jobId: string): Promise<HttpResponse<JobResponse>> => {
        return Http.get(`${this.apiBaseURL}/${jobId}`, Http.generateHeaders())
    }

    /**
     * Adds a new job for an existing consumer.
     */
    public readonly addJob = (request: JobJobRequest): Promise<HttpResponse<JobResponse>> => {
        return Http.post(`${this.apiBaseURL}`, request, Http.generateHeaders())
    }

    /**
     * Edit an existing job for the consumer.
     */
    public readonly editJob = (jobId: string, request: JobJobRequest): Promise<HttpResponse<JobResponse>> => {
        return Http.put(`${this.apiBaseURL}/${jobId}`, request, Http.generateHeaders())
    }

    /**
     * Delete a job
     */
    public readonly deleteJob = (jobId: string): Promise<HttpResponse<void>> => {
        return Http.delete(`${this.apiBaseURL}/${jobId}`, {}, Http.generateHeaders())
    }

    /**
     * Add a new job image.
     */
    public readonly addJobImage = (jobId: string, request: ImageRequest): Promise<HttpResponse<JobImageResponse>> => {
        return Http.post(`${this.apiBaseURL}/${jobId}/image`, request, Http.generateHeaders())
    }

    /**
     * Change the background style for a job image.
     */
    public readonly editJobImageBackgroundStyle = (jobImageId: string, request: ImageBackgroundStyleRequest): Promise<HttpResponse<JobImageResponse>> => {
        return Http.put(`${this.apiBaseURL}/image/${jobImageId}/background-style`, request, Http.generateHeaders())
    }

    /**
     * Change the content for a job image.
     */
    public readonly editJobImageContent = (jobImageId: string, request: ImageRequest): Promise<HttpResponse<JobImageResponse>> => {
        return Http.put(`${this.apiBaseURL}/image/${jobImageId}`, request, Http.generateHeaders())
    }

    /**
     * Delets a job image
     */
    public readonly deleteJobImage = (jobImageId: string): Promise<HttpResponse<void>> => {
        return Http.delete(`${this.apiBaseURL}/image/${jobImageId}`, {}, Http.generateHeaders())
    }
}