import LoginRequest from '../../authentication/request/LoginRequest'
import ChangePersonalDetailsRequest from './ChangePersonalDetailsRequest'

/**
 * The request to reactivate the account of a consumer
 * 
 * @author Stan Hurks
 */
export default interface ReactivateAccountRequest {

    loginRequest: LoginRequest

    changePersonalDetailsRequest: ChangePersonalDetailsRequest
    
}