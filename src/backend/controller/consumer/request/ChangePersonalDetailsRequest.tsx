/**
 * The request to change the personal details of a consumer.
 * 
 * @author Stan Hurks
 */
export default interface ChangePersonalDetailsRequest {

    /**
     * The full name
     */
    fullName: string|null

    /**
     * The postal code
     */
    postalCode: string|null

    /**
     * The e-mailaddress
     */
    emailAddress: string|null

    /**
     * The password
     */
    password: string|null

    /**
     * The current password
     */
    currentPassword: string|null
}