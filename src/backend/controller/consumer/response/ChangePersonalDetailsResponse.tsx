import AuthorizedSessionResponse from '../../../response/entity/AuthorizedSessionResponse'

/**
 * The response for the change personal details endpoint for the consumer.
 * 
 * @author Stan Hurks
 */
export default interface ChangePersonalDetailsResponse {

    session: AuthorizedSessionResponse

    fullNameSuccess: boolean

    fullNameFail: boolean

    passwordSuccess: boolean

    passwordFail: boolean

    emailSuccess: boolean

    emailFail: boolean

    emailFailInUse: boolean

    postalCodeSuccess: boolean

    postalCodeFail: boolean

    postalCodeFailNotFound: boolean

    postalCodeFailGoogleGeocodingDown: boolean

}