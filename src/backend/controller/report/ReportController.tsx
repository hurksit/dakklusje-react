import Controller from '../../../shared/backend/Controller'
import Http from '../../Http'
import ReportUserRequest from './request/ReportUserRequest'
import HttpResponse from '../../../shared/backend/HttpResponse'

/**
 * All user report related endpoints.
 * 
 * @author Stan Hurks
 */
export default class ReportController extends Controller {

    public apiBaseURL: string = 'report'

    /**
     * Report a user
     */
    public readonly reportUser = (request: ReportUserRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/user`, request, Http.generateHeaders())
    }
}