/**
 * The request bodyt for reporting a user through a connection.
 * 
 * @author Stan Hurks
 */
export default interface ReportUserRequest {

    description: string

    userId: string

}