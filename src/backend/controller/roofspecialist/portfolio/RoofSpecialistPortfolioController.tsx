import Controller from '../../../../shared/backend/Controller'
import HttpResponse from '../../../../shared/backend/HttpResponse'
import PortfolioImageResponse from '../../../response/entity/PortfolioImageResponse'
import Http from '../../../Http'
import ImageRequest from '../../../request/entity/image/ImageRequest'
import ImageBackgroundStyleRequest from '../../../request/entity/image/ImageBackgroundStyleRequest'

/**
 * The controller for all portfolio related endpoints for a roof specialist.
 * 
 * @author Stan Hurks
 */
export default class RoofSpecialistPortfolioController extends Controller {

    public apiBaseURL: string = 'roof-specialist/portfolio'

    /**
     * Add a new portfolio image.
     */
    public readonly addPortfolioImage = (request: ImageRequest): Promise<HttpResponse<PortfolioImageResponse>> => {
        return Http.post(`${this.apiBaseURL}/image`, request, Http.generateHeaders())
    }

    /**
     * Edit the content for a portfolio image.
     */
    public readonly editPortfolioImageContent = (portfolioImageId: string, request: ImageRequest): Promise<HttpResponse<PortfolioImageResponse>> => {
        return Http.put(`${this.apiBaseURL}/image/${portfolioImageId}`, request, Http.generateHeaders())
    }

    /**
     * Edit the background style for a portfolio image.
     */
    public readonly editPortfolioImageBackgroundStyle = (portfolioImageId: string, request: ImageBackgroundStyleRequest): Promise<HttpResponse<PortfolioImageResponse>> => {
        return Http.put(`${this.apiBaseURL}/image/${portfolioImageId}/background-style`, request, Http.generateHeaders())
    }

    /**
     * Delete a portfolio image.
     */
    public readonly deletePortfolioImage = (portfolioImageId: string): Promise<HttpResponse<void>> => {
        return Http.delete(`${this.apiBaseURL}/image/${portfolioImageId}`, {}, Http.generateHeaders())
    }
}