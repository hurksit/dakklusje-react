/**
 * The request for changing the login details
 * 
 * @author Stan Hurks
 */
export default interface ChangeLoginDetailsRequest {

    emailAddress: string|null

    password: string|null

    currentPassword: string|null
}