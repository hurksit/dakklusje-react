/**
 * The request for changing the contact person data for a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistContactPersonRequest {

    fullName: string

    phoneNumber: string

}