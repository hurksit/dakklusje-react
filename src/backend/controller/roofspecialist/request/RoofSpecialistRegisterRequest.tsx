import RoofSpecialistCompanyDetailsRequest from './RoofSpecialistCompanyDetailsRequest'
import RoofSpecialistContactPersonRequest from './RoofSpecialistContactPersonRequest'
import RoofSpecialistLoginDetailsRequest from './RoofSpecialistLoginDetailsRequest'

/**
 * The request for registering as a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistRegisterRequest {

    companyDetails: RoofSpecialistCompanyDetailsRequest

    cocNumber: string

    personalDetails: RoofSpecialistContactPersonRequest

    loginDetails: RoofSpecialistLoginDetailsRequest

}