import { PaymentType } from '../../../enumeration/PaymentType'

/**
 * The request used to save the payment details for a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface SavePaymentDetailsRequest {

    paymentType: PaymentType

}