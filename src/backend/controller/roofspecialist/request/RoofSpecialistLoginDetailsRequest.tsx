/**
 * The request for changing the login details for a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistLoginDetailsRequest {

    emailAddress: string

    password: string

    acceptedTermsAndConditions: boolean
    
}