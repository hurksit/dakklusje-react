import { Branche } from '../../../enumeration/Branche'

/**
 * The request to change the company details for a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistCompanyDetailsRequest {
    
    branche: Branche

    description: string|null
    
}