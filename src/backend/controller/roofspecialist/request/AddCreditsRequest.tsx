/**
 * The request to add credits.
 * 
 * @author Stan Hurks
 */
export default interface AddCreditsRequest {

    /**
     * The amount to add to the credits
     */
    amount: number
}