import Controller from '../../../shared/backend/Controller'
import AuthorizedRoofSpecialistResponse from '../../response/entity/AuthorizedRoofSpecialistResponse'
import Http from '../../Http'
import RoofSpecialistCompanyDetailsRequest from './request/RoofSpecialistCompanyDetailsRequest'
import RoofSpecialistContactPersonRequest from './request/RoofSpecialistContactPersonRequest'
import SavePaymentDetailsRequest from './request/SavePaymentDetailsRequest'
import HttpResponse from '../../../shared/backend/HttpResponse'
import CocCompanyResponse from '../../response/entity/CocCompanyResponse'
import RoofSpecialistRegisterResponse from './response/RoofSpecialistRegisterResponse'
import RoofSpecialistRegisterRequest from './request/RoofSpecialistRegisterRequest'
import ChangeLoginDetailsResponse from './response/ChangeLoginDetailsResponse'
import ChangeLoginDetailsRequest from './request/ChangeLoginDetailsRequest'
import AddCreditsRequest from './request/AddCreditsRequest'
import AddCreditsResponse from './response/AddCreditsResponse'

/**
 * All roof specialist related endpoints.
 * 
 * @author Stan Hurks
 */
export default class RoofSpecialistController extends Controller {

    public apiBaseURL: string = 'roof-specialist'

    /**
     * Saves the company details data in the roof specialist profile.
     */
    public readonly saveCompanyDetails = (request: RoofSpecialistCompanyDetailsRequest): Promise<HttpResponse<AuthorizedRoofSpecialistResponse>> => {
        return Http.put(`${this.apiBaseURL}/company-details`, request, Http.generateHeaders())
    }

    /**
     * Save the contact person data in the roof specialist profile.
     */
    public readonly saveContactPerson = (request: RoofSpecialistContactPersonRequest): Promise<HttpResponse<AuthorizedRoofSpecialistResponse>> => {
        return Http.put(`${this.apiBaseURL}/contact-person`, request, Http.generateHeaders())
    }

    /**
     * Save the login details for a roof specialist.
     */
    public readonly saveLoginDetails = (request: ChangeLoginDetailsRequest): Promise<HttpResponse<ChangeLoginDetailsResponse>> => {
        return Http.put(`${this.apiBaseURL}/login-details`, request, Http.generateHeaders())
    }

    /**
     * Save the payment details
     */
    public readonly savePaymentDetails = (request: SavePaymentDetailsRequest): Promise<HttpResponse<AuthorizedRoofSpecialistResponse>> => {
        return Http.put(`${this.apiBaseURL}/payment-details`, request, Http.generateHeaders())
    }

    /**
     * Add new credits to the account
     */
    public readonly addCredits = (request: AddCreditsRequest): Promise<HttpResponse<AddCreditsResponse>> => {
        return Http.post(`${this.apiBaseURL}/credits`, request, Http.generateHeaders())
    }

    /**
     * Validate the credit request after checkout
     */
    public readonly validateCreditRequest = (id: string): Promise<HttpResponse<void>> => {
        return Http.get(`${this.apiBaseURL}/credits/validate/${id}`, Http.generateHeaders())
    }

    /**
     * The request for an invoice token
     */
    public readonly requestInvoiceToken = (yearMonth: string): Promise<HttpResponse<string>> => {
        return Http.post(`${this.apiBaseURL}/invoice-token/${yearMonth}`, {}, Http.generateHeaders())
    }

    /**
     * List all the months (yyyy-MM) that contain payments for invoice generation.
     */
    public readonly getInvoiceMonths = (): Promise<HttpResponse<string[]>> => {
        return Http.get(`${this.apiBaseURL}/invoice-months`, Http.generateHeaders())
    }

    /**
     * Get the coc details
     */
    public readonly getCocDetails = (cocNumber: string): Promise<HttpResponse<CocCompanyResponse>> => {
        return Http.get(`${this.apiBaseURL}/coc/${cocNumber}`, Http.generateHeaders())
    }

    /**
     * Register as a roof specialist.
     */
    public readonly register = (request: RoofSpecialistRegisterRequest): Promise<HttpResponse<RoofSpecialistRegisterResponse>> => {
        return Http.post(`${this.apiBaseURL}/register`, request, Http.generateHeaders())
    }
}