/**
 * The response when registering as a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistRegisterResponse {

    companyDetailsValidated: boolean

    cocValidated: boolean
    
    contactPersonValidated: boolean

    loginDetailsValidated: boolean
    
}