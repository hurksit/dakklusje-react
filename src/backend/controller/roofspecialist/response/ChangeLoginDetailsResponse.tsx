import AuthorizedSessionResponse from '../../../response/entity/AuthorizedSessionResponse'

/**
 * The response for changing the login details.
 * 
 * @author Stan Hurks
 */
export default interface ChangeLoginDetailsResponse {

    session: AuthorizedSessionResponse

    emailSuccess: boolean

    emailFail: boolean

    emailFailInUse: boolean

    passwordSuccess: boolean

    passwordFail: boolean

    passwordFailUnauthorized: boolean

}