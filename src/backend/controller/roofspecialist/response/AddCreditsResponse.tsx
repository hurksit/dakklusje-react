/**
 * The response for the request to add credits.
 * 
 * @author Stan Hurks
 */
export default interface AddCreditsResponse {

    /**
     * The id of the credit request
     */
    creditRequestId: string

    /**
     * The href to checkout
     */
    checkoutHref: string
}