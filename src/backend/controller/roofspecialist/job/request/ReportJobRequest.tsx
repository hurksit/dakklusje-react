/**
 * The request to report a job.
 * 
 * @author Stan Hurks
 */
export default interface ReportJobRequest {

    description: string
}