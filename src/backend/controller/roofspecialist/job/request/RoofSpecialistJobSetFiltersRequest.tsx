import RoofSpecialistFilterResponse from '../../../../response/entity/RoofSpecialistFilterResponse'

/**
 * The request to change the filters for a roof specialist.
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistJobSetFiltersRequest {

    filters: RoofSpecialistFilterResponse[]
}