import JobResponse from '../../../../response/entity/JobResponse'

/**
 * The job response for a roof specialist.
 * 
 * This does not contain the personal details full name of consumer
 * 
 * @author Stan Hurks
 */
export default interface RoofSpecialistJobResponse extends JobResponse {

    connectionCount: number
}