import Controller from '../../../../shared/backend/Controller'
import AuthorizedRoofSpecialistResponse from '../../../response/entity/AuthorizedRoofSpecialistResponse'
import Http from '../../../Http'
import HttpResponse from '../../../../shared/backend/HttpResponse'
import RoofSpecialistJobResponse from './response/RoofSpecialistJobResponse'
import RoofSpecialistJobSetFiltersRequest from './request/RoofSpecialistJobSetFiltersRequest'
import Pagination from '../../../pagination/Pagination'
import ReportJobRequest from './request/ReportJobRequest'

/**
 * All job endpoints for the roof specialist.
 * 
 * @author Stan Hurks
 */
export default class RoofSpecialistJobController extends Controller {

    public apiBaseURL: string = 'roof-specialist/job'

    /**
     * Set the filters for jobs for the roof specialist.
     */
    public readonly setFilters = (request: RoofSpecialistJobSetFiltersRequest): Promise<HttpResponse<AuthorizedRoofSpecialistResponse>> => {
        return Http.post(`${this.apiBaseURL}/filters`, request, Http.generateHeaders())
    }

    /**
     * Get the paginated jobs for the roof specialist based on the filters
     */
    public readonly getPaginatedJobs = (pageNumber: number): Promise<HttpResponse<Pagination<RoofSpecialistJobResponse>>> => {
        return Http.get(`${this.apiBaseURL}/paginated-jobs/${pageNumber}`, Http.generateHeaders())
    }

    /**
     * Report the job of a consumer.
     */
    public readonly reportJob = (jobId: string, request: ReportJobRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/${jobId}/report`, request, Http.generateHeaders())
    }

    /**
     * Send a connection request to the consumer
     */
    public readonly sendConnectionRequestToConsumer = (jobId: string): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/connection/${jobId}`, {}, Http.generateHeaders())
    }
}