import Controller from '../../../shared/backend/Controller'
import LoginRequest from './request/LoginRequest'
import AuthorizedSessionResponse from '../../response/entity/AuthorizedSessionResponse'
import Http from '../../Http'
import HttpResponse from '../../../shared/backend/HttpResponse'
import AuthorizedUserResponse from '../../response/entity/AuthorizedUserResponse'

/**
 * The controller used for all authentication related functionality.
 * 
 * @author Stan Hurks
 */
export default class AuthenticationController extends Controller {

    public apiBaseURL = 'authentication'

    /**
     * Log in to the platform
     */
    public readonly login = (request: LoginRequest): Promise<HttpResponse<AuthorizedSessionResponse>> => {
        return Http.post(`${this.apiBaseURL}/login`, request, Http.generateHeaders())
    }

    /**
     * Log out of the platform
     */
    public readonly logout = (): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/logout`, {}, Http.generateHeaders())
    }

    /**
     * Get the user information
     */
    public readonly me = (): Promise<HttpResponse<AuthorizedUserResponse>> => {
        return Http.get(`${this.apiBaseURL}/me`, Http.generateHeaders())
    }
}