/**
 * The request for resending the activation link.
 * 
 * @author Stan Hurks
 */
export default interface ResendActivationLinkRequest {

    /**
     * The email address
     */
    emailAddress: string
}