/**
 * The request body for the login endpoint.
 * 
 * @author Stan Hurks
 */
export default interface LoginRequest {

    emailAddress: string|null

    password: string|null

    facebookAccessToken: string|null

    googleIdToken: string|null
}