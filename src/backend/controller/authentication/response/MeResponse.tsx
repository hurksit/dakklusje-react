import ImageResponse from '../../../response/entity/ImageResponse'

/**
 * The response for the me endpoint in the authorization controller.
 * 
 * @author Stan Hurks
 */
export default interface MeResponse {

    profilePicture: ImageResponse|null

    userSettings: string

    inboxBadgeCount: number

}