/**
 * The request to send an origin.
 * 
 * @author Stan Hurks
 */
export default interface SendOriginRequest {

    queryString: string
}