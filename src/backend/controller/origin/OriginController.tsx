import Controller from '../../../shared/backend/Controller'
import HttpResponse from '../../../shared/backend/HttpResponse'
import Http from '../../Http'
import SendOriginRequest from './request/SendOriginRequest'

/**
 * The controller for the origin/analytics functionality.
 * 
 * @author Stan Hurks
 */
export default class OriginController extends Controller {

    public apiBaseURL: string = 'origin'

    /**
     * Send an origin to the database
     */
    public readonly sendOrigin = (request: SendOriginRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}`, request, Http.generateHeaders())
    }
}