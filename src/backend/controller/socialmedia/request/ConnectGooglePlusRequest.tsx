/**
 * The request body for the endpoint to connect with google plus.
 * 
 * @author Stan Hurks
 */
export default interface ConnectGooglePlusRequest {

    emailAddress: string

    password: string

    /**
     * The google id token (gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token)
     */
    googleIdToken: string
}