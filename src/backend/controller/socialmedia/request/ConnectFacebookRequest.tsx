/**
 * The request to connect to facebook.
 * 
 * @author Stan Hurks
 */
export default interface ConnectFacebookRequest {

    emailAddress: string

    password: string

    /**
     * The access token received in FB.authResponse.accessToken
     */
    facebookAccessToken: string

}