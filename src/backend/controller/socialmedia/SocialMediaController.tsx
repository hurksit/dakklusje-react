import Controller from '../../../shared/backend/Controller'
import ConnectFacebookRequest from './request/ConnectFacebookRequest'
import AuthorizedSessionResponse from '../../response/entity/AuthorizedSessionResponse'
import Http from '../../Http'
import ConnectGooglePlusRequest from './request/ConnectGooglePlusRequest'
import HttpResponse from '../../../shared/backend/HttpResponse'

/**
 * All social media related endpoints.
 * 
 * @author Stan Hurks
 */
export default class SocialMediaController extends Controller {

    public apiBaseURL: string = 'social-media'

    /**
     * Connect facebook with a user for fast-pass login.
     */
    public readonly connectWithFacebook = (request: ConnectFacebookRequest): Promise<HttpResponse<AuthorizedSessionResponse>> => {
        return Http.post(`${this.apiBaseURL}/connect-facebook`, request, Http.generateHeaders())
    }

    /**
     * Connect google plus with a user for fast-pass login.
     */
    public readonly connectWithGooglePlus = (request: ConnectGooglePlusRequest): Promise<HttpResponse<AuthorizedSessionResponse>> => {
        return Http.post(`${this.apiBaseURL}/connect-google-plus`, request, Http.generateHeaders())
    }
}