import Controller from '../../../shared/backend/Controller'
import Http from '../../Http'
import HttpResponse from '../../../shared/backend/HttpResponse'
import GetPaymentLinkForReversedPaymentsResponse from './response/GetPaymentLinkForReversedPaymentsResponse'

/**
 * All payment related functionality.
 */
export default class PaymentController extends Controller {

    public apiBaseURL: string = 'payment'

    /**
     * List all reversed payments for the user.
     */
    public readonly listReversedPayments = (): Promise<HttpResponse<PaymentResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/reversed`, Http.generateHeaders())
    }

    /**
     * Get the paymehnt link in mollie for the reversed payments.
     */
    public readonly getPaymentLinkForReversedPayments = (): Promise<HttpResponse<GetPaymentLinkForReversedPaymentsResponse>> => {
        return Http.get(`${this.apiBaseURL}/reversed/request-payment-link`, Http.generateHeaders())
    }
}