/**
 * The response for getting the payment link for the reversed payments of the user.
 * 
 * @author Stan Hurks
 */
export default interface GetPaymentLinkForReversedPaymentsResponse {

    /**
     * The link in mollie
     */
    mollieLink: string

    /**
     * The amount including VAT
     */
    amount: number
}