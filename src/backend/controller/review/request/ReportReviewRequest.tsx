/**
 * The request to report a review.
 * 
 * @author Stan Hurks
 */
export default interface ReportReviewRequest {

    description: string

    reviewId: string
}