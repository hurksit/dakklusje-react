import Controller from '../../../shared/backend/Controller'
import AnonymousReviewResponse from '../../response/entity/AnonymousReviewResponse'
import Http from '../../Http'
import HttpResponse from '../../../shared/backend/HttpResponse'
import ReviewResponse from '../../response/entity/ReviewResponse'
import Pagination from '../../pagination/Pagination'
import ReportReviewRequest from './request/ReportReviewRequest'

/**
 * All review related endpoints.
 * 
 * @author Stan Hurks
 */
export default class ReviewController extends Controller {

    public apiBaseURL: string = 'review'

    /**
     * Lists the reviews for a connection
     */
    public readonly listReviewsForConnection = (connectionId: string): Promise<HttpResponse<ReviewResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/connection/${connectionId}`, Http.generateHeaders())
    }

    /**
     * Report a review
     */
    public readonly reportReview = (request: ReportReviewRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/report`, request, Http.generateHeaders())
    }

    /**
     * Get the paginated reviews
     */
    public readonly getPaginatedReviews = (pageNumber: number): Promise<HttpResponse<Pagination<ReviewResponse>>> => {
        return Http.get(`${this.apiBaseURL}/paginated-reviews/${pageNumber}`, Http.generateHeaders())
    }

    /**
     * List the last 2 reviews from consumers to a roof specialist company for the `ExperienceFromOthers` component.
     */
    public readonly listReviewsExperienceFromOthers = (): Promise<HttpResponse<AnonymousReviewResponse[]>> => {
        return Http.get(`${this.apiBaseURL}/experience-from-others`, Http.generateHeaders())
    }
}