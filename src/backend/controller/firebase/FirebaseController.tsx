import Controller from '../../../shared/backend/Controller'
import HttpResponse from '../../../shared/backend/HttpResponse'
import Http from '../../Http'

/**
 * All firebase related endpoints
 * 
 * @author Stan Hurks
 */
export default class FirebaseController extends Controller {

    public apiBaseURL: string = 'firebase'

    /**
     * Initialize a firebase token
     */
    public readonly initializeFirebaseToken = (token: string): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/initialize/${token}`, {}, Http.generateHeaders())
    }
}