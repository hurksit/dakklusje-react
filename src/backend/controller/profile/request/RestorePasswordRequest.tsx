/**
 * The request to restore a password.
 * 
 * @author Stan Hurks
 */
export default interface RestorePasswordRequest {

    password: string
}