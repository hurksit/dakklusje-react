import Controller from '../../../shared/backend/Controller'
import AuthorizedUserResponse from '../../response/entity/AuthorizedUserResponse'
import ImageRequest from '../../request/entity/image/ImageRequest'
import Http from '../../Http'
import ImageBackgroundStyleRequest from '../../request/entity/image/ImageBackgroundStyleRequest'
import HttpResponse from '../../../shared/backend/HttpResponse'
import RestorePasswordRequest from './request/RestorePasswordRequest'
import ResendActivationLinkRequest from '../authentication/request/ResendActivationLinkRequest'
import AuthorizedSessionResponse from '../../response/entity/AuthorizedSessionResponse'

/**
 * All base profile related endpoints.
 * 
 * @author Stan Hurks
 */
export default class ProfileController extends Controller {

    public apiBaseURL: string = 'profile'

    /**
     * Set the profile picture for the user in AWS S3 and delete the old one if found.
     */
    public readonly setProfilePicture = (request: ImageRequest): Promise<HttpResponse<AuthorizedUserResponse>> => {
        return Http.put(`${this.apiBaseURL}/profile-picture`, request, Http.generateHeaders())
    }

    /**
     * Set the background style of the profile picture for the user.
     */
    public readonly setProfilePictureBackgroundStyle = (request: ImageBackgroundStyleRequest): Promise<HttpResponse<AuthorizedUserResponse>> => {
        return Http.put(`${this.apiBaseURL}/profile-picture/background-style`, request, Http.generateHeaders())
    }

    /**
     * Delete the profile picture.
     */
    public readonly deleteProfilePicture = (): Promise<HttpResponse<void>> => {
        return Http.delete(`${this.apiBaseURL}/profile-picture`, {}, Http.generateHeaders())
    }

    /**
     * Validates a restore password request
     */
    public readonly validateRestorePassword = (uuid: string): Promise<HttpResponse<void>> => {
        return Http.get(`${this.apiBaseURL}/restore-password/validate/${uuid}`, Http.generateHeaders())
    }

    /**
     * Restore the password.
     */
    public readonly restorePassword = (uuid: string, request: RestorePasswordRequest): Promise<HttpResponse<void>> => {
        return Http.put(`${this.apiBaseURL}/restore-password/${uuid}`, request, Http.generateHeaders())
    }

    /**
     * Resends the activation link
     */
    public readonly resendActivationLink = (request: ResendActivationLinkRequest): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/resend-activation-link`, request, Http.generateHeaders())
    }

    /**
     * Activates the account for the user.
     */
    public readonly activateAccount = (uuid: string): Promise<HttpResponse<AuthorizedSessionResponse>> => {
        return Http.post(`${this.apiBaseURL}/activate-account/` + uuid, {}, Http.generateHeaders())
    }

    /**
     * Activates the email for the user.
     */
    public readonly activateEmail = (uuid: string): Promise<HttpResponse<void>> => {
        return Http.post(`${this.apiBaseURL}/activate-email/` + uuid, {}, Http.generateHeaders())
    }
}