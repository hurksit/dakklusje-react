import SockJS from 'sockjs-client'
import * as Stomp from 'stompjs'
import MessageResponse from './response/entity/MessageResponse'
import { Subject } from 'rxjs'
import ReviewResponse from './response/entity/ReviewResponse'
import AuthorizedUserResponse from './response/entity/AuthorizedUserResponse'
import AuthorizedRoofSpecialistResponse from './response/entity/AuthorizedRoofSpecialistResponse'
import Http from './Http'
import Storage from '../storage/Storage'
import App from '../core/app/App'
import JobResponse from './response/entity/JobResponse'
import InboxConnectionResponse from './controller/inbox/response/InboxConnectionResponse'

/**
 * The web socket manager
 * @author Stan Hurks
 */
export default class WebSocket {
    public static readonly userInboxBadgeCount: Subject<number> = new Subject()

    public static readonly userInboxMessage: Subject<MessageResponse> = new Subject()

    public static readonly userInboxMessageRead: Subject<InboxConnectionResponse> = new Subject()

    public static readonly userMe: Subject<AuthorizedUserResponse> = new Subject()

    public static readonly userJob: Subject<JobResponse> = new Subject()

    public static readonly userJobDelete: Subject<string> = new Subject()

    public static readonly userConnection: Subject<InboxConnectionResponse> = new Subject()

    public static readonly userConnectionPaymentSuccess: Subject<InboxConnectionResponse> = new Subject()

    public static readonly userConnectionPaymentFail: Subject<InboxConnectionResponse> = new Subject()

    public static readonly roofSpecialist: Subject<AuthorizedRoofSpecialistResponse> = new Subject()

    public static readonly roofSpecialistConnectionAccepted: Subject<InboxConnectionResponse> = new Subject()

    public static readonly roofSpecialistConnectionDeclined: Subject<InboxConnectionResponse> = new Subject()

    public static readonly consumerConnectionDeclined: Subject<InboxConnectionResponse> = new Subject()
    
    public static readonly roofSpecialistReview: Subject<ReviewResponse> = new Subject()

    public static readonly roofSpecialistReviewDelete: Subject<string> = new Subject()

    public static instance: WebSocket|null = null

    private socket!: any|null
    
    private stompClient!: Stomp.Client|null

    constructor() {
        this.initializeWebSocket()
        this.connect(undefined, () => {
            const reconnect = () => {
                setTimeout(() => {
                    console.info('attempting to reconnect to web socket')
                    
                    this.socket = null
                    this.initializeWebSocket()
                    this.connect(undefined, reconnect)
                }, 3000)
            }
            reconnect()
        })
    }

    /**
     * Initializes the web socket
     */
    public initializeWebSocket = (): void => {
        console.info('initializing web socket')

        this.socket = new SockJS(Http.options.socketBase)
        this.stompClient = Stomp.over(this.socket)
        this.stompClient.debug = () => {
            // Empty
        }
    }

    /**
     * Initialize a connection to the web socket
     * @param {void} onSuccess the callback on success
     * @param {void} onError the callback on error
     */
    public connect = (onSuccess?: any, onError?: any) => {
        console.info('Connecting to web socket server')

        if (this.stompClient) {
            this.stompClient.connect({}, onSuccess || this.onConnected, onError || this.onErrorConnecting)
        } else {
            console.error('Stompclient is null')
        }
    }

    /**
     * Disconnect the socket server
     */
    public disconnect = () => {
        if (!this.stompClient || !this.stompClient.connected) {
            console.error('Stompclient is null')
            return
        }
        this.stompClient.disconnect(() => {
            this.stompClient = null
            this.socket = null
        })
    }

    /**
     * When the connection is successful.
     */
    private onConnected = () => {
        console.info('successfully connected to web socket server')

        if (!this.socket || !this.stompClient) {
            console.error('No stompclient or socket')
            return
        }

        this.socket.onclose = () => {
            const reconnect = () => {
                setTimeout(() => {
                    console.info('attempting to reconnect to web socket')
                    
                    this.socket = null
                    this.initializeWebSocket()
                    this.connect(undefined, reconnect)
                }, 3000)
            }
            reconnect()
        }

        // User inbox badge count
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/inbox-badge-count', (payload) => {
            WebSocket.userInboxBadgeCount.next(JSON.parse(payload.body) || 0)
        })

        // User inbox message
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/inbox/message', (payload) => {
            WebSocket.userInboxMessage.next(JSON.parse(payload.body))
        })

        // User inbox message read
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/inbox/message/read', (payload) => {
            WebSocket.userInboxMessageRead.next(JSON.parse(payload.body))
        })

        // User me
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/me', (payload) => {
            WebSocket.userMe.next(JSON.parse(payload.body))
            App.updateUser.next(JSON.parse(payload.body))
        })

        // User job
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/job', (payload) => {
            WebSocket.userJob.next(JSON.parse(payload.body))
        })

        // User job delete
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/job/delete', (payload) => {
            WebSocket.userJobDelete.next(JSON.parse(payload.body))
        })

        // User connection
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/connection', (payload) => {
            WebSocket.userConnection.next(JSON.parse(payload.body))
        })

        // User connection payment fail
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/connection/payment/fail', (payload) => {
            WebSocket.userConnectionPaymentFail.next(JSON.parse(payload.body))
        })

        // User connection payment success
        this.stompClient.subscribe('/user/' + Storage.data.session.user.id + '/connection/payment/success', (payload) => {
            WebSocket.userConnectionPaymentSuccess.next(JSON.parse(payload.body))
        })

        // roof specialist specific
        if (Storage.data.session.user.roofSpecialist) {
            this.stompClient.subscribe('/roof-specialist/' + Storage.data.session.user.roofSpecialist.id, (payload) => {
                WebSocket.roofSpecialist.next(JSON.parse(payload.body))
            })
    
            // roof specialist connection accepted
            this.stompClient.subscribe('/roof-specialist/' + Storage.data.session.user.roofSpecialist.id + '/connection/accepted', (payload) => {
                WebSocket.roofSpecialistConnectionAccepted.next(JSON.parse(payload.body))
            })
    
            // roof specialist connection declined
            this.stompClient.subscribe('/roof-specialist/' + Storage.data.session.user.roofSpecialist.id + '/connection/declined', (payload) => {
                WebSocket.roofSpecialistConnectionDeclined.next(JSON.parse(payload.body))
            })
    
            // roof specialist review
            this.stompClient.subscribe('/roof-specialist/' + Storage.data.session.user.roofSpecialist.id + '/review', (payload) => {
                WebSocket.roofSpecialistReview.next(JSON.parse(payload.body))
            })
    
            // roof specialist review delete
            this.stompClient.subscribe('/roof-specialist/' + Storage.data.session.user.roofSpecialist.id + '/review/delete', (payload) => {
                WebSocket.roofSpecialistReviewDelete.next(JSON.parse(payload.body))
            })
        }
        
        // Consumer specific
        else if (Storage.data.session.user.consumer) {
    
            // consumer connection declined (by roof specialist)
            this.stompClient.subscribe('/consumer/' + Storage.data.session.user.consumer.id + '/connection/declined', (payload) => {
                WebSocket.consumerConnectionDeclined.next(JSON.parse(payload.body))
            })
        }
    }

    /**
     * When the connection fails.
     * @param {Error|string} error the error
     */
    private onErrorConnecting = (error: Error|string) => {
        console.error('could not connect to websocket server', error)
    }
}
