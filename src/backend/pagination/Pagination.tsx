/**
 * A paginated set of entries.
 * 
 * @author Stan Hurks
 */
export default interface Pagination<T> {

    /**
     * The entries
     */
    entries: T[]

    /**
     * The current page available
     */
    currentPage: number

    /**
     * The last page available
     */
    lastPage: number

    /**
     * The total amount of records
     */
    total: number
}