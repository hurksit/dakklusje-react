import { Subject } from 'rxjs'
import HttpResponse from '../shared/backend/HttpResponse'
import Storage from '../storage/Storage'
import Loader from '../components/loader/Loader'

/**
 * All HTTP related functionality.
 * 
 * @author Stan Hurks
 */
export default abstract class Http {
    /**
     * Whenever the API is not reachable
     */
    public static readonly onAPINotReachable: Subject<void> = new Subject()

    /**
     * Whenever the user is unauthorized
     */
    public static readonly onHttpStatusUnauthorized: Subject<void> = new Subject()

    /**
     * Whenever the server has an error
     */
    public static readonly onHttpStatusInternalServerError: Subject<void> = new Subject()

    /**
     * The HTTP options
     */
    public static options: { apiBase: string, socketBase: string } = {
        apiBase: window.location.protocol + '//' + window.location.hostname + ':'
            + (window.location.protocol.toLowerCase().indexOf('s') === -1
                ? 8080 : 8443) + '/dakklusje/',
        socketBase: window.location.protocol + '//' + window.location.hostname + ':'
            + (window.location.protocol.toLowerCase().indexOf('s') === -1
                ? 8080 : 8443) + '/dakklusje/web-socket/'
    }

    /**
     * Makes a get request
     * @param path the path to the resource
     * @param headers the object of request headers
     */
    public static get = (path: string, headers: { [key: string]: string }): Promise<HttpResponse<any>> => {
        return Http.request('GET', Http.options.apiBase + path, {}, headers || {})
    }

    /**
     * Makes a post request
     * @param path the path to the resource
     * @param body the request body
     * @param headers the object of request headers
     */
    public static post = (
        path: string, body: { [key: string]: any },
        headers: { [key: string]: string }): Promise<HttpResponse<any>> => {

        return Http.request('POST', Http.options.apiBase + path, body || {}, headers || {})
    }

    /**
     * Makes a put request
     * @param path the path to the resource
     * @param body the request body
     * @param headers the object of request headers
     */
    public static put = (
        path: string, body: { [key: string]: any },
        headers: { [key: string]: string }): Promise<HttpResponse<any>> => {

        return Http.request('PUT', Http.options.apiBase + path, body || {}, headers || {})
    }

    /**
     * Makes a delete request
     * @param path the path to the resource
     * @param body the request body
     * @param headers the object of request headers
     */
    public static delete = (
        path: string, body: { [key: string]: any },
        headers: { [key: string]: string }): Promise<HttpResponse<any>> => {

        return Http.request('DELETE', Http.options.apiBase + path, body || {}, headers || {})
    }

    /**
     * Makes a XMLHttpRequest and return the response as a promise.
     * @param method the request method
     * @param path the path to the resource
     * @param body the request body
     * @param headers the object of request headers
     */
    public static request = (
        method: string, path: string, body: { [key: string]: any },
        headers: { [key: string]: string }): Promise<HttpResponse<any>> => {

        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest()
            request.open(method, path)
            request.setRequestHeader('Content-Type', 'application/json')
            if (headers != null) {
                Object.keys(headers).forEach((key) => {
                    request.setRequestHeader(key, headers[key])
                })
            }
            request.onload = () => {
                const headersArray = request.getAllResponseHeaders()
                    .split(/[\r\n]+/)
                    .filter((v) => v.length)
                
                const headers = {}
                for (const headersArrayPart of headersArray) { 
                    const headersArrayParts = headersArrayPart.split(':').map((v) => v.trim())
                    headers[headersArrayParts[0].toLowerCase()] = headersArrayParts[1]
                }

                if (request.status < 400) {

                    let response: any
                    try {
                        response = JSON.parse(request.response)
                    } catch (e) {
                        response = request.response
                    }
                    
                    // Disable the loader
                    Loader.set.next(null)
                    
                    resolve({
                        data: response,
                        status: request.status,
                        headers
                    })
                } else {
                    if (request.status === 401) {
                        Http.onHttpStatusUnauthorized.next()
                    } else if (request.status === 500) {
                        Http.onHttpStatusInternalServerError.next()
                    }

                    let response: any
                    try {
                        response = JSON.parse(request.response)
                    } catch (e) {
                        response = request.response
                    }

                    // Disable the loader
                    Loader.set.next(null)

                    // eslint-disable-next-line
                    reject({
                        data: response,
                        status: request.status,
                        headers
                    })
                }
            }
            request.onerror = () => {
                Http.onAPINotReachable.next()

                // Disable the loader
                Loader.set.next(null)

                reject(request.statusText)
            }
            request.send(JSON.stringify(body))
        })
    }

    /**
     * Generate the headers for a http request
     */
    public static generateHeaders = (): any => {
        return {
            Authorization: Storage.data.session.uuid
        }
    }
}
