/**
 * The type of roof.
 * 
 * @author Stan Hurks
 */
export enum RoofType {

    /**
     * Plat dak
     */
    FLAT_ROOF = "FLAT_ROOF",

    /**
     * Hellend dak
     */
    PITCHED_ROOF = "PITCHED_ROOF"
}