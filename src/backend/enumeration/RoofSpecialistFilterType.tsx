/**
 * The filter type for the filter of a roof specialist.
 * 
 * @author Stan Hurks
 */
export enum RoofSpecialistFilterType {
    
    SURFACE_MEASUREMENTS = 'SURFACE_MEASUREMENTS',
    ROOF_TYPE = 'ROOF_TYPE',
    JOB_CATEGORY = 'JOB_CATEGORY',
    HOUSE_TYPE = 'HOUSE_TYPE',
    ROOF_MATERIAL_TYPE = 'ROOF_MATERIAL_TYPE',
    ROOF_ISOLATION_TYPE = 'ROOF_ISOLATION_TYPE',
    DISTANCE = 'DISTANCE'
}