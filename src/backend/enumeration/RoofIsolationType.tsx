/**
 * The roof isolation type.
 * 
 * @author Stan Hurks
 */
export enum RoofIsolationType {

    /**
     * Zolder isolatie
     */
    ATTIC_ISOLATION = "ATTIC_ISOLATION",

    /**
     * Vanuit buiten isoleren
     */
    ISOLATE_FROM_OUTSIDE = "ISOLATE_FROM_OUTSIDE",

    /**
     * Vanuit binnen isoleren
     */
    ISOLATE_FROM_INSIDE = "ISOLATE_FROM_INSIDE",
    
    /**
     * Overig
     */
    OTHER = "OTHER"
}