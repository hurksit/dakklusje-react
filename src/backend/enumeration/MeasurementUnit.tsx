/**
 * A unit of measurement.
 * 
 * @author Stan Hurks
 */
export enum MeasurementUnit {

    METER = "METER",

    SQUARE_METER = "SQUARE_METER"
}