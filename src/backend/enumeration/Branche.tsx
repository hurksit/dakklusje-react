/**
 * The branch of work enumeration
 *
 * @author Stan Hurks
 */
export enum Branche {

    ROOF_GENERAL = 'ROOF_GENERAL',
    SOLAR_PANELS = 'SOLAR_PANELS',
    HEAT_PUMP = 'HEAT_PUMP'

}
