/**
 * All login types
 *
 * @author Stan Hurks
 */
export enum LoginType {

    CREDENTIALS = 'CREDENTIALS',
    FACEBOOK = 'FACEBOOK',
    GOOGLE_PLUS = 'GOOGLE_PLUS'

}
