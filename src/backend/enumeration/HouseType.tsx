/**
 * The type of house.
 * 
 * @author Stan Hurks
 */
export enum HouseType {

    /**
     * Rijtjes huis
     */
    TERRACED_HOUSE = "TERRACED_HOUSE",

    /**
     * Halfopen bebouwing
     */
    SEMI_OPEN_BUILDINGS = "SEMI_OPEN_BUILDINGS",

    /**
     * Vrijstaand of alleenstaand
     */
    DETACHED_OR_SINGLE = "DETACHED_OR_SINGLE",

    /**
     * Appartement
     */
    APARTMENT = "APARTMENT",

    /**
     * Kantoor/bedrijfspand
     */
    OFFICE_OR_BUSINESS_PREMISES = "OFFICE_OR_BUSINESS_PREMISES",

    /**
     * School
     */
    SCHOOL = "SCHOOL",

    /**
     * Overig
     */
    OTHER = "OTHER"

}