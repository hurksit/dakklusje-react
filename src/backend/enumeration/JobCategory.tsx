/**
 * A category for a job.
 *
 * @author Stan Hurks
 */
export enum JobCategory {
    
    /**
     * Aanleg nieuw dak
     * m2
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    CONSTRUCTION_NEW_ROOF = 'CONSTRUCTION_NEW_ROOF',

    /**
     * Renovatie/vervangen dak
     * m2
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    RENOVATION_AND_REPLACE_ROOF = 'RENOVATION_AND_REPLACE_ROOF',

    /**
     * Dakreparatie
     * m2
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    ROOF_REPAIR = 'ROOF_REPAIR',

    /**
     * Dak isolatie
     * m2
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    ROOF_ISOLATION = 'ROOF_ISOLATION',

    /**
     * Dakkoepel, lichtstraat
     * Geen m2
     * 30eu
     */
    ROOF_DOME_AND_LIGHT_STREET = 'ROOF_DOME_AND_LIGHT_STREET',

    /**
     * Dakraam
     * Geen m2
     * 30eu
     */
    SKYLIGHT = 'SKYLIGHT',

    /**
     * Dakkapel
     * Geen m2
     * 30 eu
     */
    DORMER = 'DORMER',

    /**
     * Zolderinrichting
     * gemiddeld 11k kosten
     * m2
     * 30eu
     */
    ATTIC_DEVICE = 'ATTIC_DEVICE',

    /**
     * Dak coating
     * 30eu/m2 gemiddeld
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    ROOF_COATING = 'ROOF_COATING',

    /**
     * Dak goot
     * meter ipv m2 (wel opslaan in surfaceMeasurements)
     * gemiddeld 50 euro per meter
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    ROOF_GUTTER = 'ROOF_GUTTER',

    /**
     * Boeidelen, windveer of overstek
     * meter ipv m2 (wel opslaan in surfaceMeasurements)
     * gemiddeld 60-100 euro per meter
     * <20m2: 20eu
     * >=20m2: 30eu
     */
    BUOYINGS_AND_WIND_SPRING_AND_CROSS = 'BUOYINGS_AND_WIND_SPRING_AND_CROSS',

    /**
     * Reiniging dak
     * m2
     * gemiddeld 5-15 eu/m2
     * <50m2: 20eu
     * >=50m2: 30eu
     */
    ROOF_CLEANING = 'ROOF_CLEANING',

    /**
     * Zonnepanelen
     * 30 euro
     */
    SOLAR_PANELS = 'SOLAR_PANELS',

    /**
     * Warmtepompen
     * 30 euro
     */
    HEAT_POMPS = 'HEAT_POMPS',

    /**
     * Overig
     * 30 euro
     */
    OTHER = 'OTHER'
}