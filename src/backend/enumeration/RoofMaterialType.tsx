/**
 * The type of roof material.
 * 
 * @author Stan Hurks
 */
export enum RoofMaterialType {

    /**
     * Leien
     */
    SLATE = "SLATE",

    /**
     * Dakpannen
     */
    ROOF_TILES = "ROOF_TILES",

    /**
     * Riet
     */
    REEDS = "REEDS",

    /**
     * Bitumen dakbedekking
     */
    BITUMEN_ROOFING_MATERIAL = "BITUMEN_ROOFING_MATERIAL",

    /**
     * Pvc dakbedekking
     */
    PVC_ROOFING = "PVC_ROOFING",

    /**
     * Epdm dakbedekking
     */
    EPDM_ROOFING = "EPDM_ROOFING",

    /**
     * Sedumdak/groendak
     */
    SEDUM_ROOF_OR_GREEN_ROOF = "SEDUM_ROOF_OR_GREEN_ROOF",

    /**
     * Golfplaten
     */
    CORRUGATED_SHEETS = "CORRUGATED_SHEETS",

    /**
     * Overig
     */
    OTHER = "OTHER"
}