/**
 * All reasons to report someone.
 *
 * @author Stan Hurks
 */
export enum ReportReason {

    INAPPROPRIATE_BEHAVIOUR = 'INAPPROPRIATE_BEHAVIOUR',
    OTHER = 'OTHER'

}
