/**
 * All possible payment types.
 *
 * @author Stan Hurks
 */
export enum PaymentType {

    IDEAL = 'IDEAL',
    CREDIT_CARD = 'CREDIT_CARD',
    AUTOMATIC_COLLECTION = 'AUTOMATIC_COLLECTION'

}