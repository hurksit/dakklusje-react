/**
 * The request to change the background style for an image entity.
 * 
 * @author Stan Hurks
 */
export default interface ImageBackgroundStyleRequest {
    
    backgroundPosition: string

    zoom: number

}