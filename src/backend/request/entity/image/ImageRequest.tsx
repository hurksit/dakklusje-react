/**
 * The request for an image entity.
 * 
 * @author Stan Hurks
 */
export default interface ImageRequest {

    content: string
    
}