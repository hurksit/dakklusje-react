/**
 * All location related functionality.
 * 
 * @author Stan Hurks
 */
export default class Location {

    /**
     * Calculate the distance between two geo coordinates in meters.
     */
    public static calculateDistanceMeters = (latitude1: number, longitude1: number, latitude2: number, longitude2: number) => {
        /**
         * Converts the degrees to radians
         * @param degrees the degrees
         */
        const toRadians = (degrees: number) => {
            return degrees * Math.PI / 180
        }
        
        const radiusEarthMeters = 6371e3
        const φ1 = toRadians(latitude1)
        const φ2 = toRadians(latitude2)
        var Δφ = toRadians(latitude2 - latitude1)
        var Δλ = toRadians(longitude2 - longitude1)

        const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2)

        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))

        return radiusEarthMeters * c
    }
}