/**
 * All regular expressions (RegEx) used throughout the application.
 * 
 * @author Stan Hurks
 */
export default class Expressions {

    /**
     * Validate an email address
     */
    // eslint-disable-next-line
    public static readonly email = /^[\w-+]+(\.[\w]+)*@[\w-]+(\.[\w]+)*(\.[a-zA-Z]{2,})$/

    /**
     * Validate a dutch phone number
     */
    // eslint-disable-next-line
    public static readonly dutchPhoneNumber = /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/

    /**
     * Validate a full name
     */
    // eslint-disable-next-line
    public static readonly fullName = /^(\S+ )+\S+$/

    /**
     * Validate a duch postal code
     */
    // eslint-disable-next-line
    public static readonly dutchPostalCode = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i

    /**
     * Validate a numeric value
     */
    public static readonly numeric = /^[0-9]*$/
}