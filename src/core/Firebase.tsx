import messaging from '../firebase-messaging'
import Storage from '../storage/Storage'
import Backend from '../backend/Backend'

/**
 * The class to manage push notifications through firebase with.
 * 
 * @author Stan Hurks
 */
export default class Firebase {

    /**
     * Enable push notifications
     */
    public static enable = () => {
        if (!Storage.data.session.user.id 
            || !Storage.data.session.uuid 
            || (!Storage.data.session.user.consumer && !Storage.data.session.user.roofSpecialist)) {
            return
        }
        if (messaging === null) {
            return
        }
        messaging.requestPermission()
            .then(() => {
                if (messaging === null) {
                    return
                }
                messaging.getToken().then((token) => {
                    Storage.data.firebaseToken = token

                    Firebase.initializeTopics()
                }).catch((e) => {
                    console.error(e)
                })
            })
            .catch((e) => {
                console.error(e)
            })
    }

    /**
     * Disable push notifications
     */
    public static disable = () => {
        if (Storage.data.firebaseToken && messaging !== null) {
            messaging.deleteToken(Storage.data.firebaseToken).catch((response) => {
                console.error('Could not delete firebase token: ', response)
            })
        }
    }

    /**
     * Initialize the firebase topics applicable for this application
     */
    private static initializeTopics = () => {
        Backend.controllers.firebase.initializeFirebaseToken(Storage.data.firebaseToken as string).then(() => {
            console.log('Topics initialized')
        }).catch((response) => {
            console.error('Could not initialize topics: ', response)
        })
    }
}