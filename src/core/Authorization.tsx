import Storage from '../storage/Storage'

/**
 * This class will contain all authorization related functionality.
 * 
 * @author Stan Hurks
 */
export default class Authorization {

    /**
     * Checks whether or not the user is authorized
     */
    public static isAuthorized(): boolean {
        return !!Storage.data.session.uuid
    }
}