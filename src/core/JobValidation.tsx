import { JobCategory } from '../backend/enumeration/JobCategory'
import { MeasurementUnit } from '../backend/enumeration/MeasurementUnit'
 
/**
 * All generic job validation functionality.
 * 
 * @author Stan Hurks
 */
export default class JobValidation {

    /**
     * Get the surface measurement unit based on job category.
     */
    public static getSurfaceMeasurementUnit = (jobCategory: JobCategory|null): MeasurementUnit|null => {
        switch (jobCategory) {
            case JobCategory.CONSTRUCTION_NEW_ROOF:
            case JobCategory.RENOVATION_AND_REPLACE_ROOF:
            case JobCategory.ROOF_REPAIR:
            case JobCategory.ROOF_ISOLATION:
            case JobCategory.ROOF_COATING:
            case JobCategory.BUOYINGS_AND_WIND_SPRING_AND_CROSS:
            case JobCategory.ROOF_CLEANING:
                return MeasurementUnit.SQUARE_METER
            case JobCategory.ATTIC_DEVICE:
            case JobCategory.ROOF_GUTTER:
                return MeasurementUnit.METER
            case JobCategory.OTHER:
            case JobCategory.ROOF_DOME_AND_LIGHT_STREET:
            case JobCategory.SOLAR_PANELS:
            case JobCategory.HEAT_POMPS:
            case JobCategory.SKYLIGHT:
            case JobCategory.DORMER:
            case null:
                return null
        }
    }
}