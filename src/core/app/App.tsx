import React from 'react'
import './app.scss'
import { Subject, Subscription } from 'rxjs'
import '../../animations/cube.scss'
import AuthorizedSessionResponse from '../../backend/response/entity/AuthorizedSessionResponse'
import Events from '../../shared/Events'
import Routes from '../../routes/Routes'
import Translations from '../../translations/Translations'
import Browser from '../../shared/Browser'
import CSSVariables from '../../shared/CSSVariables'
import Navigation from '../../components/navigation/Navigation'
import WebSocket from '../../backend/WebSocket'
import Loader from '../../components/loader/Loader'
import Storage from '../../storage/Storage'
import { Route } from '../../enumeration/Route'
import HowDoesItWorkPage from '../../pages/non-authorized-pages/how-does-it-work/HowDoesItWorkPage'
import Modal from '../../components/modal/Modal'
import ImageUpload from '../../components/image-upload/ImageUpload'
import Backend from '../../backend/Backend'
import Http from '../../backend/Http'
import AuthorizedUserResponse from '../../backend/response/entity/AuthorizedUserResponse'
import Authorization from '../Authorization'
import Window from '../../shared/Window'
import StorageUserDataModal from '../../storage/data-model/user/StorageUserDataModal'
import StorageImageDataModel from '../../storage/data-model/image/StorageImageDataModel'
import StorageSessionDataModel from '../../storage/data-model/session/StorageSessionDataModel'
import MobileScrolling from '../../shared/MobileScrolling'
import NonModernBrowserModal from '../../components/modal/non-modern-browser/NonModernBrowserModal'
import Device from '../../shared/Device'
import Firebase from '../../core/Firebase'
import ReactGA from 'react-ga'

/**
 * The state
 */
interface AppState {

    /**
     * The current route of the `Routes` class.
     */
    routesCurrentRoute?: Route

    /**
     * The scrollY property of window before the body scroll has been disabled.
     */
    windowScrollYBeforeBodyScroll: number

    /**
     * Whether or not intercom is enabled
     */
    intercomEnabled: boolean
}

/**
 * The main app entry point.
 * 
 * @author Stan Hurks
 */
export default class App extends React.Component<any, AppState> {

    /**
     * Whether intercom is toggled
     */
    public static intercomToggled: boolean = false

    /**
     * The subject to toggle intercom
     */
    public static toggleIntercom: Subject<boolean> = new Subject()

    /**
     * The inbox badge count
     */
    public static inboxBadgeCount: number = 0

    /**
     * Whenever information is passed to this subject the App will force update.
     * 
     * This is required for certain functionality, such as authorization.
     */
    public static readonly update: Subject<void> = new Subject()

    /**
     * When the app is updated through the `update` subject.
     */
    public static readonly updated: Subject<void> = new Subject()

    /**
     * Whenever the user information changes push it to here.
     */
    public static readonly updateUser: Subject<AuthorizedUserResponse> = new Subject()

    /**
     * Whenever the user logs in to the application.
     */
    public static readonly login: Subject<AuthorizedSessionResponse> = new Subject()

    /**
     * Whenever the user logs out of the application.
     */
    public static readonly logout: Subject<void> = new Subject()

    /**
     * Toggle body scroll
     * 
     * This should be called when a modal opens on mobile
     */
    public static readonly bodyScroll: Subject<boolean> = new Subject()

    /**
     * The context to which the ImageUpload component is uploading the image for
     */
    public static imageUploadContext: 'profile-picture' | null = null

    /**
     * Whenever a new profile picture has been set by App.
     */
    public static readonly profilePictureChange: Subject<void> = new Subject()
    
    /**
     * The internal subscription to the update subject
     */
    private subscriptionUpdate!: Subscription

    /**
     * The subscription to when the user changes
     */
    private subscriptionUpdateUser!: Subscription

    /**
     * The internal subscription to the login subject
     */
    private subscriptionLogin!: Subscription

    /**
     * The internal subscription to the logout subject
     */
    private subscriptionLogout!: Subscription

    /**
     * The subscription for when the API is not reachable
     */
    private subscriptionHttpAPINotReachable!: Subscription

    /**
     * The subscription for when the user is not authorized
     */
    private subscriptionHttpUnauthorized!: Subscription

    /**
     * The subscription for an internal server error
     */
    private subscriptionHttpInternalServerError!: Subscription

    /**
     * The subscription for clicking the document
     */
    private subscriptionDocumentClick!: Subscription

    /**
     * The subscription for submitting a form in the document
     */
    private subscriptionDocumentSubmit!: Subscription

    /**
     * The subscription for when the route changes
     */
    private subscriptionRoutesSetCurrentRoute!: Subscription

    /**
     * The internal subscription for when the body scroll should be toggled
     */
    private subscriptionBodyScroll!: Subscription

    /**
     * The subscription for when the ImageUpload has a new base64 image available
     */
    private subscriptionImageUploadNewImageBase64!: Subscription

    /**
     * The subscription for when an upload failed in the ImageUpload component
     */
    private subscriptionImageUploadUploadFailed!: Subscription

    /**
     * The subscription to the inbox badge count
     */
    private subscriptionInboxBadgeCount!: Subscription

    /**
     * The subscription to toggle intercom
     */
    private subscriptionToggleIntercom!: Subscription

    constructor(props: any) {
        super(props)

        this.initializeCore()

        this.state = {
            routesCurrentRoute: undefined,

            windowScrollYBeforeBodyScroll: 0,

            intercomEnabled: false
        }
    }

    public componentDidMount() {
        this.subscriptionUpdate = App.update.subscribe(this.onUpdate)
        this.subscriptionLogin = App.login.subscribe(this.onLogIn)
        this.subscriptionLogout = App.logout.subscribe(this.onLogOut)
        this.subscriptionDocumentClick = Events.document.click.subscribe(this.onDocumentClick)
        this.subscriptionRoutesSetCurrentRoute = Routes.setCurrentRoute.subscribe(this.onRoutesSetCurrentRoute)
        this.subscriptionBodyScroll = App.bodyScroll.subscribe(this.onBodyScroll)
        this.subscriptionDocumentSubmit = Events.document.submit.subscribe(this.onDocumentFormSubmit)
        this.subscriptionImageUploadNewImageBase64 = ImageUpload.newImageBase64.subscribe(this.onImageUploadNewImageBase64)
        this.subscriptionImageUploadUploadFailed = ImageUpload.uploadFailed.subscribe(this.onImageUploadUploadFailed)
        this.subscriptionHttpAPINotReachable = Http.onAPINotReachable.subscribe(this.onHttpAPINotReachable)
        this.subscriptionHttpUnauthorized = Http.onHttpStatusUnauthorized.subscribe(this.onHttpUnauthorized)
        this.subscriptionHttpInternalServerError = Http.onHttpStatusInternalServerError.subscribe(this.onHttpInternalServerError)
        this.subscriptionUpdateUser = App.updateUser.subscribe(this.onUpdateUser)
        this.subscriptionInboxBadgeCount = WebSocket.userInboxBadgeCount.subscribe(this.onUserInboxBadgeCount)
        this.subscriptionToggleIntercom = App.toggleIntercom.subscribe(this.onToggleIntercom)

        // Add firefox class to body for different style rendering
        // due to it being a horrible browser with its own wishes and demands.
        if (navigator.userAgent.toLowerCase().indexOf('firefox') !== -1) {
            document.body.classList.add('firefox')
        }

        if (Authorization.isAuthorized()) {
            Backend.controllers.authentication.me().then((response) => {
                App.updateUser.next(response.data)
            })

            Backend.controllers.inbox.getBadgeCount().then((response) => {
                WebSocket.userInboxBadgeCount.next(response.data)
            })
        }

        // Force update due to the locale changing, possibly.
        if (Translations.currentLanguage !== Translations.defaultLanguage) {
            setTimeout(() => {
                App.update.next()
            })
        }

        // Initialize firebase push notifications
        setTimeout(() => {
            Firebase.enable()
        })
    }

    public componentWillUnmount() {
        this.subscriptionUpdate.unsubscribe()
        this.subscriptionLogin.unsubscribe()
        this.subscriptionLogout.unsubscribe()
        this.subscriptionDocumentClick.unsubscribe()
        this.subscriptionRoutesSetCurrentRoute.unsubscribe()
        this.subscriptionBodyScroll.unsubscribe()
        this.subscriptionDocumentSubmit.unsubscribe()
        this.subscriptionImageUploadNewImageBase64.unsubscribe()
        this.subscriptionImageUploadUploadFailed.unsubscribe()
        this.subscriptionHttpAPINotReachable.unsubscribe()
        this.subscriptionHttpUnauthorized.unsubscribe()
        this.subscriptionHttpInternalServerError.unsubscribe()
        this.subscriptionUpdateUser.unsubscribe()
        this.subscriptionInboxBadgeCount.unsubscribe()
        this.subscriptionToggleIntercom.unsubscribe()

        this.cleanUpCore()
    }

    public render() {
        return (
            <div id="app">
                <ImageUpload />

                <Modal />

                <div id="navigation" style={{
                    ...this.state.windowScrollYBeforeBodyScroll === 0 ? {} : {
                        position: 'absolute',
                        left: 0,
                        top: Math.min(0, -this.state.windowScrollYBeforeBodyScroll)
                    }
                }}>
                    <Navigation variant={
                        window.location.pathname.startsWith(Route.INBOX)
                        ? 'inbox'
                        : 'default'
                    } activeTab={this.state.routesCurrentRoute} />
                </div>

                <div id="page" style={{
                    ...this.state.windowScrollYBeforeBodyScroll === 0 ? {} : {
                        position: 'absolute',
                        left: 0,
                        top: Math.min(0, -this.state.windowScrollYBeforeBodyScroll)
                    }
                }}>
                    <Routes />
                </div>

                <Loader />
            </div>
        )
    }

    /**
     * Initialize all classes in need of initialization here.
     */
    private initializeCore() {

        // Storage MUST be initialized first due to other types being dependent on it.
        Storage.initialize()

        Events.initialize()

        // Web-socket integration
        WebSocket.instance = new WebSocket()

        Translations.initialize()
        
        Browser.initialize()
        
        CSSVariables.initialize()

        MobileScrolling.instance = new MobileScrolling()

        // Trigger the user to use a modern browser
        // Or atleast warn them for potential lacks of functionality in non-chrome browsers.
        if (!Storage.data.browserNotificationSent && !Device.isMobile && !Browser.isChrome) {
            setTimeout(() => {
                Modal.mount.next({
                    element: (
                        <NonModernBrowserModal />
                    ),
                    dismissable: false
                })
            })
        }

        // Initialize intercom
        this.updateIntercom()

        // Send analytics to the database if any
        if (window.location.search.length !== 0) {
            Backend.controllers.origin.sendOrigin({
                queryString: window.location.search
            })

            window.history.pushState('', '', window.location.href.substring(0, window.location.href.indexOf(window.location.search)))
        }

        // Initialize google analytics
        ReactGA.initialize('UA-145090874-1', {
            debug: false,
            titleCase: false,
            gaOptions: {
                userId: Storage.data.session.user.id
            }
        } as any)
    }


    /**
     * Whenever the app should update
     */
    private onUpdate = () => {
        setTimeout(() => {
            this.forceUpdate()
    
            App.updated.next()
        })
    }

    /**
     * Whenever the user is updated
     */
    private onUpdateUser = (user: AuthorizedUserResponse) => {
        Storage.data.session.user = new StorageUserDataModal(user)

        // Re-initialize google analytics
        ReactGA.initialize('UA-145090874-1', {
            debug: false,
            titleCase: false,
            gaOptions: {
                userId: Storage.data.session.user.id
            }
        } as any)

        setTimeout(() => {
            App.update.next()
        })
    }

    /**
     * When the user logs in to the application
     */
    private onLogIn = (payload: AuthorizedSessionResponse) => {
        WebSocket.instance = new WebSocket()
        App.updateUser.next(payload.user)
        
        if (App.intercomToggled) {
            setTimeout(() => {
                this.updateIntercom()

                Firebase.enable()
            })
        }
    }

    /**
     * When the user out of the application.
     */
    private onLogOut = () => {
        (WebSocket.instance as WebSocket).disconnect()
        
        // Clear the session from storage.
        Storage.data.session = new StorageSessionDataModel()
        Storage.update.next()

        Routes.redirectURL.next(Route.HOME)
        App.update.next()

        if (App.intercomToggled) {
            setTimeout(() => {
                this.updateIntercom()

                Firebase.disable()
            })
        }
    }

    /**
     * Whenever the current route changes in the Routes
     */
    private onRoutesSetCurrentRoute = (payload: Route|{route: Route, params: {[key: string]: string}}) => {
        this.setState({
            routesCurrentRoute: typeof payload === 'string' ? payload : payload.route
        })
    }

    /**
     * Toggles the body scroll
     */
    private onBodyScroll = (enabled: boolean) => {
        if (enabled) {
            document.body.classList.remove('disable-scroll')
            
            // Save the original scroll top position
            const top = this.state.windowScrollYBeforeBodyScroll

            // Revert scroll position
            this.setState({
                windowScrollYBeforeBodyScroll: 0
            }, () => {

                // Scroll to original position after the state changes have been processed
                Window.scrollTo({
                    top
                })
            })
        } else {
            // Save the scroll position of window
            this.setState({
                windowScrollYBeforeBodyScroll: window.scrollY
            })

            document.body.classList.add('disable-scroll')
        }
    }

    /**
     * Whenever a form has been submitted
     */
    private onDocumentFormSubmit = (event: Event) => {
        event.preventDefault()
        event.stopPropagation()
    }

    /**
     * Whenever the document is clicked
     */
    private onDocumentClick = (event: Event) => {
        this.preventAnchorClick(event)
    }

    /**
     * Whenever the API is not reachable
     */
    private onHttpAPINotReachable = () => {
        Modal.mountErrorHistory.next(Translations.translations.backend.generic.responses.offline)
    }

    /**
     * Whenever the user is unauthorized
     */
    private onHttpUnauthorized = () => {
        setTimeout(() => {
            if (Authorization.isAuthorized()) {
                Modal.mountErrorHistory.next(Translations.translations.backend.generic.responses[401])
                Backend.controllers.authentication.logout()
                App.logout.next()
            }
        })
    }

    /**
     * Whenever the server has an error
     */
    private onHttpInternalServerError = () => {
        Modal.mountErrorHistory.next(Translations.translations.backend.generic.responses[500])
    }


    /**
     * Whenever the ImageUpload component has a new base64 image available
     */
    private onImageUploadNewImageBase64 = (content: string) => {
        switch (App.imageUploadContext) {
            case 'profile-picture':
                Loader.set.next(Translations.translations.backend.controllers.profile.setProfilePicture.loader)

                Backend.controllers.profile.setProfilePicture({
                    content
                }).then((response) => {
                    if (response.data.profilePicture !== null) {
                        Storage.data.session.user.profilePicture = new StorageImageDataModel(response.data.profilePicture)
                    }
                    setTimeout(() => {
                        App.update.next()
                        App.profilePictureChange.next()
                    })
                }).catch((response) => {
                    const translation: string|undefined = Translations.translations.backend.controllers.profile.setProfilePicture.responses[response.status]
                    if (translation) {
                        Modal.mountErrorHistory.next(translation)
                    }
                })
            break
        }
    }

    /**
     * Whenever the ImageUpload component failed to upload an image
     */
    private onImageUploadUploadFailed = () => {
        App.imageUploadContext = null
        Modal.mountErrorHistory.next(Translations.translations.components.imageUpload.uploadFailed)
    }

    /**
     * Update the inbox badge count
     */
    private onUserInboxBadgeCount = (badgeCount: number) => {
        App.inboxBadgeCount = badgeCount
    }

    /**
     * When intercom is toggled
     */
    private onToggleIntercom = (toggled: boolean) => {
        App.intercomToggled = toggled

        // Hide/show the intercom logo
        if (toggled) {
            document.body.classList.remove('hide-intercom')
        } else {
            document.body.classList.add('hide-intercom')
        }
    }

    /**
     * Updates intercom
     */
    private updateIntercom = () => {
        let node = document.getElementById('intercom-script')
        if (node) {
            document.body.removeChild(node)
        }

        node = document.createElement('script')
        node.id = 'intercom-script'

        window['intercomSettings'] = {
            ...Storage.data.session.user ? {
                name: Storage.data.session.user.name,
                email: Storage.data.session.user.email
            } : {},
            app_id: 'r3db5mnw',
            language_override: Translations.translations.document.intercomLang
        }
        node.innerHTML = `(function () {var w = window; var ic = w.Intercom; if (typeof ic === "function") { ic('reattach_activator'); ic('update', intercomSettings); } else { var d = document; var i = function () { i.c(arguments) }; i.q = []; i.c = function (args) { i.q.push(args) }; w.Intercom = i; function l() { var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/r3db5mnw'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); } if (w.attachEvent) { w.attachEvent('onload', l); } else { w.addEventListener('load', l, false); } }})()`

        document.body.appendChild(node)
    }

    /**
     * Prevents that clicking an a[href] changes the window location.
     * 
     * First check if the link starts with '/'.
     * 
     * Then if so go to that route in the same window.
     * 
     * If not set the target to _blank and open the URL in a new window
     */
    private preventAnchorClick = (event: Event) => {
        if (event.target === null) {
            return false
        }

        let element = (event.target as Element)
        if (!element) {
            return false
        }
        
        const loopThroughElementAndParentsUntilHrefIsFound = (element: Element) => {
            if (element.hasAttribute('href')) {

                // Get the url
                const url = element.getAttribute('href')
    
                if (url == null) {
                    event.preventDefault()
                    event.stopImmediatePropagation()
                    event.stopPropagation()
                    return false
                }
    
                if (url.startsWith('/') && !url.endsWith('.pdf')) {
                    
                    // Check if from and to are both in HowDoesItWorkPage
                    if (url.startsWith(Route.HOW_DOES_IT_WORK) && window.location.pathname.startsWith(Route.HOW_DOES_IT_WORK)) {

                        // Change the index of how does it work page
                        HowDoesItWorkPage.changeIndex.next(url as Route)

                    } else {
                        
                        // Local
                        Routes.redirectURL.next(url)

                        // Dismiss all modals
                        Modal.dismiss.next('all')

                        // Close the navigation
                        if (Navigation.isMenuOpen && window.innerWidth < 1000) {
                            Navigation.close.next()
                            
                            setTimeout(() => {
        
                                // Scroll to top
                                Window.scrollTo({
                                    top: 0,
                                    left: 0,
                                    behavior: 'smooth'
                                })
                            }, Navigation.animationTimeMs)
                        } else {
                            // Scroll to top
                            Window.scrollTo({
                                top: 0,
                                left: 0,
                                behavior: 'smooth'
                            })
                        }
                    }
    
                    // Prevent from going to the link
                    event.preventDefault()
                    event.stopImmediatePropagation()
                    event.stopPropagation()
                    return false
                } else if (url.startsWith('#')) {

                    // Scroll to element if found
                    const el = document.querySelector(url)
                    if (el !== null) {
                        Window.scrollTo({
                            top: el.clientTop,
                            left: el.clientLeft
                        })
                    }

                    // Prevent from going to the link
                    event.preventDefault()
                    event.stopImmediatePropagation()
                    event.stopPropagation()
                    return false
                } else if (!url.startsWith('#') && !url.startsWith('mailto://') && !url.startsWith('tel://')) {

                    // Open in a new window
                    window.open(url)

                    // Prevent from going to the link
                    event.preventDefault()
                    event.stopImmediatePropagation()
                    event.stopPropagation()
                    return false
                }
            } else if (element.parentElement !== null) {
                loopThroughElementAndParentsUntilHrefIsFound(element.parentElement)
            }
        }
        loopThroughElementAndParentsUntilHrefIsFound(element)
    }

    /**
     * Clean up all classes in need of clean up here.
     */
    private cleanUpCore() {
        CSSVariables.cleanUp()

        Storage.cleanUp()

        Events.cleanUp()
    }
}