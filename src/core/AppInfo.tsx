/**
 * Information about the application
 * 
 * @author Stan Hurks
 */
export default class AppInfo {
    /**
     * Version of the app
     */
    public static readonly version: string = '1.0.0'

    /**
     * The VAT percentage
     */
    public static readonly vatPercentage: number = 21

    /**
     * By who the app is build and designed
     */
    public static readonly designedAndBuildBy = {
        href: 'https://hurks.it',
        companyName: 'Hurks Digital B.V.',
        cocNumber: 74762818,

        emailAddress: 'info@dakklusje.nl',
        phoneNumber: '+31(6) 1939 9203',
        phoneNumberLink: 'tel://0031619399203',
        
        websiteHurksDigital: 'https://hurks.it',
        termsAndAgreementsLink: '/algemene-voorwaarden.pdf',

        linkedIn: 'https://linkedin.com/company/dakklusje-nl',
        facebook: 'https://facebook.com/dakklusje.nl',
        instagram: 'https://instagram.com/dakklusje.nl',
        address: {
            line1: 'Docterskampstraat 2A unit 2735',
            line2: '5222AM \'s-Hertogenbosch',
            line3: 'Nederland'
        }
    }

    public static readonly appURL: string = 'https://dakklusje.nl'
}