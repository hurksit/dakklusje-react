/**
 * All theme related settings.
 * 
 * @author Stan Hurks
 */
export default class Theme {

    /**
     * The theme colors
     */
    public static readonly colors = {
        red: '#ff1744',

        white: 'white',

        grey: {
            veryDark: '#222',

            dark: '#333',

            regular: '#999',

            light: '#ddd',

            veryLight: '#eee',

            nearWhite: '#f7f7f7'
        }
    }
}