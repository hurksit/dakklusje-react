import { RoofType } from '../backend/enumeration/RoofType'
import { JobCategory } from '../backend/enumeration/JobCategory'
import { HouseType } from '../backend/enumeration/HouseType'
import { RoofMaterialType } from '../backend/enumeration/RoofMaterialType'
import { RoofIsolationType } from '../backend/enumeration/RoofIsolationType'

/**
 * All options to generate 
 */
export default {
    [RoofType.FLAT_ROOF]: {
        [JobCategory.CONSTRUCTION_NEW_ROOF]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.RENOVATION_AND_REPLACE_ROOF]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_REPAIR]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_ISOLATION]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.APARTMENT]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.SCHOOL]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.OTHER]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            }
        },
        [JobCategory.ROOF_DOME_AND_LIGHT_STREET]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.OTHER]: null
    },

    [RoofType.PITCHED_ROOF]: {
        [JobCategory.CONSTRUCTION_NEW_ROOF]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.RENOVATION_AND_REPLACE_ROOF]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_REPAIR]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.APARTMENT]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.SCHOOL]: {
                [RoofMaterialType.SLATE]: null,
                [RoofMaterialType.ROOF_TILES]: null,
                [RoofMaterialType.REEDS]: null,
                [RoofMaterialType.BITUMEN_ROOFING_MATERIAL]: null,
                [RoofMaterialType.PVC_ROOFING]: null,
                [RoofMaterialType.EPDM_ROOFING]: null,
                [RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF]: null,
                [RoofMaterialType.CORRUGATED_SHEETS]: null,
                [RoofMaterialType.OTHER]: null,
            },
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_ISOLATION]: {
            [HouseType.TERRACED_HOUSE]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.SEMI_OPEN_BUILDINGS]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.DETACHED_OR_SINGLE]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.APARTMENT]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.SCHOOL]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            },
            [HouseType.OTHER]: {
                [RoofIsolationType.ATTIC_ISOLATION]: null,
                [RoofIsolationType.ISOLATE_FROM_OUTSIDE]: null,
                [RoofIsolationType.ISOLATE_FROM_INSIDE]: null,
                [RoofIsolationType.OTHER]: null
            }
        },
        [JobCategory.SKYLIGHT]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.DORMER]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.ATTIC_DEVICE]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_COATING]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_GUTTER]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.BUOYINGS_AND_WIND_SPRING_AND_CROSS]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.ROOF_CLEANING]: {
            [HouseType.TERRACED_HOUSE]: null,
            [HouseType.SEMI_OPEN_BUILDINGS]: null,
            [HouseType.DETACHED_OR_SINGLE]: null,
            [HouseType.APARTMENT]: null,
            [HouseType.OFFICE_OR_BUSINESS_PREMISES]: null,
            [HouseType.SCHOOL]: null,
            [HouseType.OTHER]: null
        },
        [JobCategory.OTHER]: null
    }
}