import { FormInvalidation } from '../enumeration/FormInvalidation'
import { Subject } from 'rxjs'
import { HouseType } from '../backend/enumeration/HouseType'
import { JobCategory } from '../backend/enumeration/JobCategory'
import { RoofType } from '../backend/enumeration/RoofType'
import { Branche } from '../backend/enumeration/Branche'
import { RoofMaterialType } from '../backend/enumeration/RoofMaterialType'
import { RoofIsolationType } from '../backend/enumeration/RoofIsolationType'
import Expressions from './Expressions'
import { PaymentType } from '../backend/enumeration/PaymentType'

/**
 * The base for the form validation options.
 */
export interface FormValidationOptionsBase {
    /**
     * Whether or not to allow null as a value
     */
    allowNull?: boolean

    /**
     * Whether or not the value must be `true`
     */
    mustBeTruthy?: boolean
    
    /**
     * Whether or not the value must be `false`
     */
    mustBeFalsy?: boolean

    /**
     * String
     */
    string?: {

        /**
         * Whether or not to allow empty
         */
        allowEmpty?: boolean

        /**
         * The minimum amount of chars
         */
        minLength?: number

        /**
         * The maximum amount of chars
         */
        maxLength?: number

        /**
         * The pattern to use
         */
        pattern?: RegExp

        /**
         * Whether or not the string should match an evaluated value
         */
        matchEvaluatedValue?: () => string
    },

    /**
     * Number
     */
    number?: {
        /**
         * The min value
         */
        minValue?: number

        /**
         * The max value
         */
        maxValue?: number
    },

    /**
     * The array options
     */
    array?: {
        /**
         * Whether or not to allow the user to use an empty array as a value
         */
        allowEmpty?: boolean

        /**
         * The min length of the array
         */
        minLength?: number

        /**
         * The max length of the array
         */
        maxLength?: number

        /**
         * Check whether the values are in another array with allowed values
         */
        allowedValues?: any[]
    }

    /**
     * Whether or not to validate if the value is an enum
     */
    enum?: {[key: string]: string},

    /**
     * Check whether the value is in an array with allowed values
     */
    allowedValues?: any[]
}

/**
 * The options for a form validation when used as a property of the FormGroup component
 */
export interface FormValidationOptionsAsFormGroupProperty extends FormValidationOptionsBase {

    /**
     * Evaluates the value
     */
    evaluateValue: () => any
}

/**
 * The options for a form validation
 */
export interface FormValidationOptions extends FormValidationOptionsAsFormGroupProperty {

    /**
     * The label to locate the field record with. Note that this should be an unique value.
     */
    label: string
}

/**
 * The interface for the subject when a field in a form has been invalidated
 */
export interface FormValidationFieldInvalidated {
        
    /**
     * The invalidation
     */
    invalidation: FormInvalidation

    /**
     * The label of the field
     */
    label: string
}

/**
 * This keeps track of everything related to form validation.
 * 
 * @author Stan Hurks
 */
export default class FormValidation {
    
    /**
     * All validation rules per entity
     */
    public static readonly entity = {
        user: {
            email: {
                string: {
                    pattern: Expressions.email
                }
            },
            fullName: {
                string: {
                    pattern: Expressions.fullName
                }
            },
            password: {
                string: {
                    minLength: 8,
                    maxLength: 64
                }
            },
            postalCode: {
                string: {
                    pattern: Expressions.dutchPostalCode
                }
            }
        },
        job: {
            title: {
                string: {
                    minLength: 10,
                    maxLength: 100
                }
            },
            branche: {
                allowedValues: Object.keys(Branche)
            },
            roofType: {
                allowedValues: Object.keys(RoofType)
            },
            jobCategory: {
                allowedValues: Object.keys(JobCategory)
            },
            houseType: {
                allowedValues: Object.keys(HouseType)  
            },
            roofMaterialType: {
                allowedValues: Object.keys(RoofMaterialType)
            },
            roofIsolationType: {
                allowedValues: Object.keys(RoofIsolationType)
            },
            description: {
                allowNull: true,
                string: {
                    allowEmpty: true,
                    maxLength: 1000
                }
            },
            surfaceMeasurements: {
                number: {
                    minValue: 1,
                    maxValue: 10000
                }
            }
        },
        message: {
            content: {
                allowNull: false,
                string: {
                    allowEmpty: false,
                    minLength: 1,
                    maxLength: 1000
                }
            }
        },
        review: {
            description:{
                string: {
                    minLength: 100,
                    maxLength: 500
                }
            }
        },
        report: {
            description: {
                string: {
                    minLength: 50,
                    maxLength: 250
                }
            }
        },
        roofSpecialist: {
            branche: {
                allowedValues: Object.keys(Branche)
            },
            companyDescription: {
                allowNull: true,
                string: {
                    allowEmpty: true,
                    minLength: 100,
                    maxLength: 1000
                }
            },
            paymentType: {
                allowedValues: Object.keys(PaymentType)
            },
            phoneNumber: {
                string: {
                    pattern: Expressions.dutchPhoneNumber                    
                }
            }
        },
        cocCompany: {
            cocNumber: {
                string: {
                    minLength: 8,
                    maxLength: 8,
                    pattern: Expressions.numeric
                }
            }
        }
    }

    /**
     * Whenever a field is invalidated it will be pushed to here.
     */
    public static fieldInvalidated: Subject<FormValidationFieldInvalidated> = new Subject()

    /**
     * Whenever a field is validated it will be pushed to here.
     */
    public static fieldValidated: Subject<string> = new Subject()

    /**
     * The fields to validate
     */
    private static fields: FormValidationOptions[] = []

    /**
     * Stashed fields for when Form Validation is used in functions as well as the JSX
     */
    private static stashedFields: FormValidationOptions[] = []

    /**
     * Stash the current fields
     */
    public static readonly stash = () => {
        FormValidation.stashedFields = FormValidation.fields
        FormValidation.fields = []
    }

    /**
     * Unstash the fields
     */
    public static readonly unstash = () => {
        FormValidation.fields = FormValidation.stashedFields
        FormValidation.stashedFields = []
    }

    /**
     * Resets the form validation
     */
    public static readonly reset = () => {
        for (const field of FormValidation.fields) {
            FormValidation.fieldValidated.next(field.label)
        }
        FormValidation.fields = []
    }

    /**
     * Validates all fields and calls the callback on them
     * to provide the right helper texts for the user
     */
    public static readonly validate = (label?: string): boolean => {
        let invalidatedFieldCount: number = 0
        for (const field of FormValidation.fields.filter((field) => label ? label === field.label : true)) {
            const value = field.evaluateValue()

            if (value === undefined) {
                console.error('Field: ' + field.label + ' is undefined.')
                return false
            }

            if (!field.allowNull && value === null) {
                FormValidation.fieldInvalidated.next({
                    invalidation: FormInvalidation.VALUE_IS_NULL,
                    label: field.label
                })
                invalidatedFieldCount ++
            }

            else if (field.mustBeTruthy && value !== true) {
                FormValidation.fieldInvalidated.next({
                    invalidation: FormInvalidation.BOOLEAN_NOT_TRUE,
                    label: field.label
                })
                invalidatedFieldCount ++
            }
            
            else if (field.mustBeFalsy && value !== false) {
                FormValidation.fieldInvalidated.next({
                    invalidation: FormInvalidation.BOOLEAN_NOT_FALSE,
                    label: field.label
                })
                invalidatedFieldCount ++
            }

            else if (field.allowNull && value == null) {
                FormValidation.fieldValidated.next(field.label)
            }

            else if (field.string) {
                if (typeof value !== 'string') {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.STRING_NO_STRING,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.string.allowEmpty === false && value.length === 0) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.STRING_EMPTY,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.string.minLength && value.length < field.string.minLength && !(value.length === 0 && field.string.allowEmpty)) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.STRING_TOO_SHORT,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.string.maxLength && value.length > field.string.maxLength) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.STRING_TOO_LONG,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (!(field.string.allowEmpty && value.length === 0) && field.string.pattern && !field.string.pattern.test(value)) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.STRING_PATTERN_NO_MATCH,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.string.matchEvaluatedValue && field.string.matchEvaluatedValue() !== value) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.STRING_EVALUATED_VALUE_NOT_MATCHED,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else {
                    FormValidation.fieldValidated.next(field.label)
                }
            }

            else if (field.number) {
                if (isNaN(value) || typeof value !== 'number') {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.NUMBER_NAN,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.number.minValue !== undefined && value < field.number.minValue) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.NUMBER_MIN_VALUE,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.number.maxValue !== undefined && value > field.number.maxValue) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.NUMBER_MAX_VALUE,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else {
                    FormValidation.fieldValidated.next(field.label)
                }
            }

            else if (field.array) {
                if (!Array.isArray(value)) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.ARRAY_NO_ARRAY,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (!field.array.allowEmpty && value.length === 0) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.ARRAY_EMPTY,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.array.allowedValues && value.filter((v) => (field.array as any).allowedValues.indexOf(v) === -1).length > 0) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.ARRAY_INVALID_VALUE,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.array.minLength !== undefined && value.length < field.array.minLength) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.ARRAY_TOO_FEW_ENTRIES,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else if (field.array.maxLength !== undefined && value.length > field.array.maxLength) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.ARRAY_TOO_MANY_ENTRIES,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else {
                    FormValidation.fieldValidated.next(field.label)
                }
            }

            else if (field.enum) {
                if (Object.keys(field.enum).filter((v) => v === value).length === 0) {
                    FormValidation.fieldInvalidated.next({
                        invalidation: FormInvalidation.ENUM_INVALID,
                        label: field.label
                    })
                    invalidatedFieldCount ++
                }

                else {
                    FormValidation.fieldValidated.next(field.label)
                }
            }

            else if (field.allowedValues && field.allowedValues.indexOf(value) === -1) {
                FormValidation.fieldInvalidated.next({
                    invalidation: FormInvalidation.VALUE_NOT_ALLOWED,
                    label: field.label
                })
                invalidatedFieldCount ++
            }
            
            else {
                FormValidation.fieldValidated.next(field.label)
            }
        }
        return invalidatedFieldCount === 0
    }

    /**
     * Validates all fields and calls the callback on them
     * to provide the right helper texts for the user
     * 
     * Additional to the validate function this will also unstash the fields.
     */
    public static readonly validateAndUnstash = (label?: string): boolean => {
        const validated = FormValidation.validate(label)
        FormValidation.unstash()
        return validated
    }

    /**
     * Adds a field for validation
     */
    public static readonly addField = (options: FormValidationOptions) => {
        FormValidation.fields = FormValidation.fields.filter((v) => v.label !== options.label).concat(options)
    }

    /**
     * Removes a field from validation
     */
    public static readonly removeField = (label: string) => {
        FormValidation.fields = FormValidation.fields.filter((v) => v.label !== label)
    }
}