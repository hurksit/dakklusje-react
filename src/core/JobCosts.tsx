import JobResponse from '../backend/response/entity/JobResponse'
import { JobCategory } from '../backend/enumeration/JobCategory'
import { Branche } from '../backend/enumeration/Branche'

/**
 * This class is used to calculate the job costs.
 * 
 * @author Stan Hurks
 */
export default class JobCosts {

    /**
     * Calculate the costs for a job.
     */
    public static readonly calculateCosts = (job: JobResponse): number => {
        switch (job.branche) {
            case Branche.HEAT_PUMP:
                return 30
            case Branche.SOLAR_PANELS:
                return 30
            case Branche.ROOF_GENERAL:
                switch (job.jobCategory) {
                    case JobCategory.CONSTRUCTION_NEW_ROOF:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.RENOVATION_AND_REPLACE_ROOF:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.ROOF_REPAIR:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.ROOF_ISOLATION:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.ROOF_DOME_AND_LIGHT_STREET:
                        return 30
                    case JobCategory.SKYLIGHT:
                        return 30
                    case JobCategory.DORMER:
                        return 30
                    case JobCategory.ATTIC_DEVICE:
                        return 30
                    case JobCategory.ROOF_COATING:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.ROOF_GUTTER:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.BUOYINGS_AND_WIND_SPRING_AND_CROSS:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 20 ? 20 : 30
                    case JobCategory.ROOF_CLEANING:
                        return job.surfaceMeasurements !== null && job.surfaceMeasurements < 50 ? 20 : 30
                    case JobCategory.SOLAR_PANELS:
                        return 30
                    case JobCategory.HEAT_POMPS:
                        return 30
                    case JobCategory.OTHER:
                        return 30
                    case null:
                        return 30
                }
                // eslint-disable-next-line
            case null:
                return 30
        }
    }
}