import app from 'firebase/app'
import 'firebase/messaging'

export const config = {
    apiKey: "AIzaSyDdiUu53TGbGu-vtHhXpM4dfbU5k6rpN8Y",
    authDomain: "dakklusje-1561087697874.firebaseapp.com",
    databaseURL: "https://dakklusje-1561087697874.firebaseio.com",
    projectId: "dakklusje-1561087697874",
    storageBucket: "",
    messagingSenderId: "670673782706"
}

let messaging: app.messaging.Messaging|null = null
try {
    messaging = app.initializeApp(config).messaging()
} catch (e) {}

export default messaging