import React from 'react'
import { Branche } from '../../backend/enumeration/Branche'
import { JobCategory } from '../../backend/enumeration/JobCategory'
import { PaymentType } from '../../backend/enumeration/PaymentType'
import { ReportReason } from '../../backend/enumeration/ReportReason'
import { Route } from '../../enumeration/Route'
import AppInfo from '../../core/AppInfo'
import { FormInvalidation } from '../../enumeration/FormInvalidation'
import { FormValidationOptionsAsFormGroupProperty } from '../../core/FormValidation'
import { HouseType } from '../../backend/enumeration/HouseType'
import { RoofType } from '../../backend/enumeration/RoofType'
import { RoofMaterialType } from '../../backend/enumeration/RoofMaterialType'
import { RoofIsolationType } from '../../backend/enumeration/RoofIsolationType'
import { MeasurementUnit } from '../../backend/enumeration/MeasurementUnit'
import moment from 'moment'
import InboxConnectionResponse from '../../backend/controller/inbox/response/InboxConnectionResponse'
import JobCosts from '../../core/JobCosts'
import ReviewResponse from '../../backend/response/entity/ReviewResponse'

/**
 * The translations for locale: 'nl_NL'
 * 
 * @author Stan Hurks
 */
const TranslationsNLNL = {
    /**
     * The description for the average costs for a job based on a job category
     */
    averageCostsForJob: (jobCategory: JobCategory): JSX.Element => {
        switch (jobCategory) {
            case JobCategory.CONSTRUCTION_NEW_ROOF:
                return (
                    <div>
                        <p>
                            Afhankelijk van uw type dak en de werkzaamheden kunnen de prijzen voor dit type klus verschillen. Hieronder ziet u een gemiddelde prijs op basis van uw dak.
                        </p><p>
                            Het verwijderen en vernieuwen van een plat dak bedraagt gemiddeld 70 tot 90 euro per vierkante meter.
                        </p><p>
                            Het verwijderen en vernieuwen van een hellend dak bedraagt gemiddeld 70 tot 110 euro per vierkante meter.
                        </p>
                    </div>
                )

            case JobCategory.RENOVATION_AND_REPLACE_ROOF:
                return (
                    <div>
                        <p>
                            Afhankelijk van uw type dak en de werkzaamheden kunnen de prijzen voor dit type klus verschillen. Hieronder ziet u een gemiddelde prijs op basis van uw dak.
                        </p><p>
                            Het renoveren van een plat dak bedraagt gemiddeld 75 tot 100 euro per vierkante meter.
                        </p><p>
                            Het renoveren van een hellend dak bedraagt gemiddeld 75 tot 130 euro per vierkante meter.
                        </p>
                    </div>
                )
            case JobCategory.ROOF_REPAIR:
                return (
                    <div>
                        <p>
                            Afhankelijk van het type klus kan de prijs verschillen. Hieronder staan de meest gebruikelijke scenarios beschreven.
                        </p><p>
                            Dakbedekking hellend dak: sommige dakpannen vervangen: gemiddeld 125 tot 250 euro per vierkante meter.
                        </p><p>
                            Dakbedekking plat dak: lekkende gedeelte vervangen: gemiddeld 150 tot 240 euro per vierkante meter.
                        </p><p>
                            Nokvorsten en pannen vervangen: gemiddeld 40 tot 95 euro per meter
                        </p><p>
                            Dakgoten vervangen en folie aanbrengen: gemiddeld 55 tot 95 euro per meter
                        </p>
                    </div>
                )
            case JobCategory.ROOF_ISOLATION:
                return (
                    <div>
                        <p>
                            Afhankelijk van het type klus kan de prijs verschillen. Hieronder staan de meest gebruikelijke scenarios beschreven.
                        </p><p>
                            Glaswolisolatie: vanaf 10 euro per vierkante meter exclusief plaatsing. Vanaf 20 euro per vierkante meter inclusief plaatsing.
                        </p><p>
                            Isolatieplaten / isolatiepanelen: vanaf 12 euro per vierkante meter exclusief plaatsing. Vanaf 25 euro per vierkante meter inclusief plaatsing.
                        </p><p>
                            Rotswol: vanaf 20 euro per vierkante meter exclusief plaatsing. Vanaf 35 euro per vierkante inclusief plaatsing.
                        </p>
                    </div>
                )
            case JobCategory.ROOF_DOME_AND_LIGHT_STREET:
                return (
                    <div>
                        <p>
                            Afhankelijk van hoe makkelijk de dakkoepel geplaatst kan worden kan er een goede prijsschatting gemaakt worden. Hieronder staan een aantal scenarios beschreven.
                        </p><p>
                            Lichtkoepel plaatsen houten dak: gemiddelde prijs 240 euro
                        </p><p>
                            Lichtkoepel plaatsen betonnen dak: gemiddelde prijs 450 euro
                        </p>
                    </div>
                )
            case JobCategory.SKYLIGHT:
                return (
                    <div>
                        <p>
                            De prijs voor het plaatsen van een dakraam is totaal afhankelijk van de afmetingen, hieronder staan een aantal scenarios beschreven.
                        </p><p>
                            55 x 78 cm: gemiddeld 400 euro
                        </p><p>
                            78 x 140 cm: gemiddeld 555 euro
                        </p><p>
                            134 x 140 cm: gemiddeld 750 euro
                        </p>
                    </div>
                )
            case JobCategory.DORMER:
                return (
                    <div>
                        <p>
                            De prijs voor het plaatsen van een dakkapel is afhankelijk van de afmeting en van het materiaal. Hieronder ziet u een aantal scenarios gebaseerd op een afmeting van 150cm hoog x 250cm breed.
                        </p><p>
                            Houten dakkapel: Prijs prefab (incl. plaatsing): Vanaf 3.000 euro. Prijs traditioneel (inclusief. plaatsing): Vanaf 4.000 euro
                        </p><p>
                            Kunststof dakkapel: Prijs prefab (incl. plaatsing): Vanaf 3.750 euro. Prijs traditioneel (inclusief. plaatsing): Vanaf 4.700 euro
                        </p><p>
                            Polyester dakkapel: Prijs prefab (incl. plaatsing): Vanaf 4.250 euro. 
                        </p>
                    </div>
                )
            case JobCategory.ATTIC_DEVICE:
                return (
                    <div>
                        <p>
                            Voor het laten verbouwen van een standaard zolder wordt een gemiddelde kostprijs in rekening gebracht van tussen de 10.500 en 11.000 euro.
                        </p>
                    </div>
                )
            case JobCategory.ROOF_COATING:
                return (
                    <div>
                        <p>
                            Voor het reinigen en coaten van een hellend dak met pannen betaald u gemiddeld tussen de 30 en 45 euro per vierkante meter (inclusief BTW).
                        </p>
                    </div>
                )
            case JobCategory.ROOF_GUTTER:
                return (
                    <div>
                        <p>
                            Voor dit type klus zijn de kosten afhankelijk van het type materiaal en de afmeting. Hieronder staan een aantal scenarios beschreven op basis van een dakgoot van 12 meter, inclusief beugels en 2 regenpijpen.
                        </p><p>
                            Reparatie dakgoot: gemiddeld 100 tot 150 euro
                        </p><p>
                            Zelf verwijderen en laten plaatsen kunststof dakgoot: gemiddeld 600 tot 900 euro
                        </p><p>
                            Verwijderen en laten plaatsen zinken dakgoot: gemiddeld 800 tot 1.300 euro
                        </p>
                    </div>
                )
            case JobCategory.BUOYINGS_AND_WIND_SPRING_AND_CROSS:
                return (
                    <div>
                        <p>
                            Voor dit type klus zijn de kosten afhankelijk van het materiaal en van de toevoegingen. Hieronder staan een aantal scenarios beschreven.
                        </p><p>
                            Zinken windveren: ca. 140 euro per meter inclusief BTW en montage.
                        </p><p>
                            Kunststof windveren: ca. 95 tot 135 euro per meter inclusief BTW en montage.
                        </p><p>
                            Houten windveren: ca. 50 tot 75 euro per meter inclusief BTW en montage.
                        </p>
                    </div>
                )
            case JobCategory.ROOF_CLEANING:
                return (
                    <div>
                        <p>
                            Afhankelijk van het type klus betaald u tusen de 5 en 20 euro per vierkante meter. Hieronder staan een aantal scenarios beschreven.
                        </p><p>
                            Dak reinigen: Tussen de 5 en 15 euro per vierkante meter
                        </p><p>
                            Dak coaten: Tussen de 10 en 20 euro per vierkante meter
                        </p><p>
                            Dak reinigen: Ongeveer 5 euro per vierkante meter gemiddeld
                        </p>
                    </div>
                )
            case JobCategory.SOLAR_PANELS:
                return (
                    <div>
                        <p>
                            Voor het plaatsen van zonnepanelen betaald u afhankelijk van het aantal panelen en vermogen een verschillende prijs.
                        </p><p>
                            Als u bijvoorbeeld 10 zonnepanelen laat plaatsen met een totaal vermogen van 2.500 Wattpiek (Wp) betaalt u nu gemiddeld 4.700 euro.
                        </p><p>
                            Voor meer informatie kunt u ook contact opnemen met onze partner <a href="https://www.greenstarsolutions.nl/">Greenstar Solutions</a>.
                        </p>
                    </div>
                )
            case JobCategory.HEAT_POMPS:
                return (
                    <div>
                        <p>
                            Voor het plaatsen van warmtepompen betaald u afhankelijk van het type warmtepomp een verschillende prijs. Hieronder staan aantal scenarios beschreven.
                        </p><p>
                            Lucht-water warmtepomp: gemiddeld 10.000 tot 15.000 euro inclusief installatiekosten
                        </p><p>
                            Lucht-lucht warmtepomp: gemiddeld 4.000 tot 8.000 euro inclusief installatiekosten
                        </p><p>
                            Grond-water warmtepomp: gemiddeld 15.000 tot 25.000 euro inclusief installatiekosten
                        </p><p>
                            Water-water warmtepomp: gemiddeld 15.000 tot 20.000 euro inclusief installatiekosten
                        </p><p>
                            Hybride warmtepomp: gemiddeld 5.000 tot 7.000 euro inclusief installatiekosten
                        </p>
                    </div>
                )
            case JobCategory.OTHER:
                return (
                    <div>
                        <p>
                            Voor deze categorie is het helaas niet mogelijk om een indicatie voor de kosten weer te geven.
                        </p>
                    </div>
                )
        }
    },

    backend: {
        controllers: {
            authentication: {
                login: {
                    loader: 'Inloggen...',
                    responses: {
                        400: 'Gelieve alle velden in te vullen',
                        401: 'U heeft het verkeerde wachtwoord ingevuld.',
                        404: 'Het opgegeven e-mailadres kon niet worden gevonden.'
                    }
                },
                logout: {
                    loader: 'Uitloggen...'
                },
                me: {
                    loader: 'Gebruikersgegevens ophalen...'
                }
            },

            consumer: {
                job: {
                    register: {
                        addTempImage: {
                            loader: 'Afbeelding toevoegen...',
                            responses: {
                                503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                            }
                        },
                        editTempImageContent: {
                            loader: 'Afbeelding vervangen...',
                            responses: {
                                400: 'De afbeelding is al verwijderd.',
                                401: 'De actie die u probeert uit te voeren behoort niet tot uw sessie.',
                                503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                            }
                        },
                        editTempImageBackgroundStyle: {
                            loader: 'Afbeelding aanpassen...',
                            responses: {
                                400: 'De afbeelding kon niet worden gevonden.',
                                401: 'Deze afbeelding hoort niet bij uw sessie.'
                            }
                        },
                        deleteTempImage: {
                            loader: 'Afbeelding verwijderen...',
                            responses: {
                                401: 'De afbeelding die u probeert te verwijderen bestaat niet.',
                                404: 'De afbeelding die u probeert te verwijderen bestaat niet.',
                                503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                            }
                        },
                        register: {
                            loader: 'Klus aanmaken...',
                            responses: {
                                400: 'U heeft het formulier nog niet volledig ingevuld.',
                                403: 'Om u bij ons aan te melden dient u akkoord te gaan met de algemene voorwaarden.',
                                404: 'De postcode kon niet gevonden worden in google, probeer een andere te proberen in de buurt.',
                                409: 'Het e-mailadres is al bij ons in gebruik. Bij de tab persoonlijke gegevens kunt u deze klus koppelen aan uw account.',
                                429: 'U kunt bij dakklusje.nl maximaal 1x per dag registreren, probeert u het morgen nogmaals.',
                                503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                            }
                        }
                    },
                    getPaginatedJobs: {
                        loader: 'Klussen ophalen...'
                    },
                    addJob: {
                        responses: {
                            200: 'Uw klus is geplaatst!'
                        }
                    },
                    deleteJob: {
                        loader: 'Klus verwijderen',
                        responses: {
                            200: 'De klus is verwijderd.',
                            400: 'De klus is al verwijderd.',
                            404: 'De klus kon niet worden gevonden.'
                        }
                    },
                    editJob: {
                        loader: 'Klus aanpassen...',
                        responses: {
                            200: 'De klus is aangepast!',
                            400: 'U heeft nog niet alle velden goed ingevuld.',
                            404: 'De klus kon niet worden gevonden.'
                        }
                    },
                    getJob: {
                        loader: 'Klus ophalen...',
                        responses: {
                            404: 'De klus kon niet worden gevonden.'
                        }
                    },
                    addJobImage: {
                        loader: 'Afbeelding toevoegen aan klus...',
                        responses: {
                            404: 'De klus kon niet worden gevonden.',
                            503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                        }
                    },
                    editJobImageBackgroundStyle: {
                        loader: 'Afbeelding aanpassen...',
                        responses: {
                            404: 'De afbeelding kon niet worden gevonden.'
                        }
                    },
                    editJobImageContent: {
                        loader: 'Afbeelding vervangen...',
                        responses: {
                            404: 'De afbeelding kon niet worden gevonden.',
                            503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                        }
                    },
                    deleteJobImage: {
                        loader: 'Afbeelding verwijderen...',
                        responses: {
                            404: 'De afbeelding die u probeert te verwijderen bestaat niet.',
                            503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                        }
                    }
                },
                deactivateAccount: {
                    loader: 'Account deactiveren...',
                    responses: {
                        200: 'Uw account is gedeactiveerd. U bent nu uitgelogd.'
                    }
                },
                reactivateAccount: {
                    loader: 'Account heractiveren...',
                    responses: {
                        200: {
                            title: 'Account geactiveerd',
                            content: 'Uw account is opnieuw geactiveerd. U bent nu ingelogd.'
                        },
                        400: 'U heeft geen geldige naam opgegeven.',
                        401: 'U heeft het verkeerde wachtwoord ingevuld.',
                        404: 'Het opgegeven e-mailadres kon niet worden gevonden.'
                    }
                },
                changePersonalDetails: {
                    loader: 'Persoonlijke gegevens aanpassen...',
                    responses: {
                        fullNameSuccess: 'Uw volledige naam is aangepast.',
                        fullNameFail: 'Uw volledige naam kon niet worden aangepast door een server fout. Wij zijn hiervan op de hoogte en doen ons best om dit zo spoedig mogelijk te verhelpen. Gelieve het later nogmaals te proberen.',
                        passwordSuccess: 'Uw wachtwoord is aangepast. Daarnaast zijn al uw sessies nu verlopen en dient u weer opnieuw in te loggen op alle andere apparaten.',
                        passwordFail: 'Uw wachtwoord kon niet worden aangepast door een server fout. Wij zijn hiervan op de hoogte en doen ons best om dit zo spoedig mogelijk te verhelpen. Gelieve het later nogmaals te proberen.',
                        passwordFailUnauthorized: 'Het ingevoerde huidige wachtwoord is onjuist.',
                        emailSuccess: 'Er is een activatiemail verzonden naar uw nieuwe e-mailadres. Zodra u deze heeft geactiveerd wordt uw nieuwe e-mailadres geregistreerd.',
                        emailFail: 'Uw e-mailadres kon niet worden aangepast door een server fout.',
                        emailFailInUse: 'Het opgegeven e-mailadres is al in gebruik.',
                        postalCodeSuccess: 'Uw postcode is aangepast.',
                        postalCodeFail: 'Uw postcode kon niet worden aangepast door een server fout. Wij zijn hiervan op de hoogte en doen ons best om dit zo spoedig mogelijk te verhelpen. Gelieve het later nogmaals te proberen.',
                        postalCodeFailNotFound: 'De postcode kon niet worden gevonden. Probeer een andere postcode of neem contact op met ons via de knop rechtsonder in het scherm.',
                        postalCodeFailGoogleGeocodingDown: 'De postcode kon niet worden aangepast, aangezien er iets misgaat in de geocoding API. Gelieve deze actie over een aantal minuten nogmaals te proberen.',
                        400: 'Gelieve alle velden correct in te vullen.'
                    }
                }
            },

            inbox: {
                connection: {
                    consumer: {
                        declineConnection: {
                            loader: {
                                declineConnection: 'Gesprek weigeren...',
                                delete: 'Verwijderen...'
                            },
                            responses: {
                                declineConnection: {
                                    200: 'U heeft het gesprek geweigerd.',
                                    400: 'Het gesprek is al geweigerd.',
                                    403: 'Deze connectie is niet van u.',
                                    404: 'De connectie kon niet worden gevonden.'
                                },
                                delete: {
                                    400: 'De connectie is al verwijderd.',
                                    403: 'Deze connectie is niet van u.',
                                    404: 'De connectie kon niet worden gevonden.'
                                }
                            },
                            destructiveAction: 'Weet u zeker dat u het gesprek wilt weigeren? Als u dit doet kan deze dakspecialist niet meer op deze klus reageren.',
                            destructiveActionDeleteContact: 'Weet u zeker dat u het gesprek wilt beëindigen? Als u dit doet kan deze dakspecialist niks meer naar u sturen.'
                        },
                        deleteReview: {
                            loader: 'Recensie verwijderen...',
                            responses: {
                                200: {
                                    title: 'Recensie verwijderd',
                                    content: 'U heeft uw recensie verwijderd.'
                                },
                                400: 'U heeft nog geen recensie geschreven.',
                                403: 'Deze connectie is niet van u.',
                                404: 'De connectie kon niet worden gevonden.'
                            }
                        },
                        acceptConnection: {
                            loader: 'Gesprek accepteren...',
                            responses: {
                                200: 'U heeft het gesprek geaccepteerd.',
                                403: 'Deze connectie is niet van u.',
                                404: 'De connectie kon niet worden gevonden.'
                            }
                        },
                        addReviewRevision: {
                            newReview: {
                                loader: 'Recensie plaatsen...',
                                responses: {
                                    200: {
                                        title: 'Recensie geplaatst',
                                        content: 'Dankuwel voor de recensie!'
                                    },
                                    400: 'Gelieve het formulier volledig in te vullen.',
                                    403: 'Dit is niet uw review.',
                                    404: 'De connectie kon niet worden gevonden.',
                                    409: 'De connectie is nog niet geaccepteerd.'
                                }
                            },
                            existingReview: {
                                loader: 'Recensie aanpassen...',
                                responses: {
                                    200: {
                                        title: 'Recensie aangepast',
                                        content: 'U heeft uw recensie aangepast.'
                                    },
                                    400: 'Het aantal sterren dient tussen de 1 en 5 te liggen.',
                                    403: 'Dit is niet uw review.',
                                    404: 'De connectie kon niet worden gevonden.',
                                    409: 'De connectie is nog niet geaccepteerd.'
                                }
                            }
                        },
                        getReviewForConnection: {
                            loader: 'Review ophalen...',
                            responses: {
                                404: 'De connectie kon niet worden gevonden.'
                            }
                        }
                    },

                    roofSpecialist: {
                        declineCosts: {
                            loader: {
                                rejectOffer: 'Aanbod intrekken...',
                                declineCosts: 'Kosten weigeren...',
                                delete: 'Verwijderen...'
                            },
                            responses: {
                                rejectOffer: {
                                    200: 'U heeft de kosten geweigerd.',
                                    400: 'U heeft de kosten al geweigerd.',
                                    404: 'De connectie kon niet worden gevonden.'
                                },
                                declineCosts: {
                                    200: 'U heeft de kosten geweigerd.',
                                    400: 'U heeft de kosten al geweigerd.',
                                    404: 'De connectie kon niet worden gevonden.'
                                },
                                delete: {
                                    400: 'U heeft de connectie al verwijderd.',
                                    404: 'De connectie kon niet worden gevonden.'
                                }
                            },
                            destructiveAction: {
                                rejectOffer: 'Weet u zeker dat u uw aanbod wilt intrekken? Als u dit doet kunt u deze klus niet meer opnieuw aannemen.',
                                declineCosts: 'Weet u zeker dat u de kosten wilt weigeren? Als u dit doet kunt u deze klus niet meer opnieuw aannemen.',
                                removeContact: 'Weet u zeker dat u het gesprek wilt beëindigen? Als u dit doet kunt u deze connectie niet opnieuw aangaan.'
                            }
                        },

                        acceptCosts: {
                            loader: 'Kosten accepteren...',
                            responses: {
                                102: 'Betaling verwerken...',
                                201: 'Betaling verwerken...',
                                400: 'U heeft de kosten al betaald.',
                                403: 'De betaling is mislukt.',
                                404: 'De connectie kon niet worden gevonden.',
                                503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                            }
                        },

                        validateCosts: {
                            loader: 'Kosten valideren...',
                            responses: {
                                200: 'De betaling is geslaagd.',
                                404: 'De connectie kon niet worden gevonden.',
                                409: 'De betaling is mislukt.'
                            }
                        }
                    },

                    reportConnection: {
                        loader: 'Gesprek rapporteren...',
                        responses: {
                            200: 'U heeft het gesprek gerapporteerd.',
                            404: 'De connectie kon niet worden gevonden.',
                            429: 'U kunt een gesprek niet vaker dan 1x per dag rapporteren.'
                        }
                    },

                    listConnections: {
                        loader: 'Connecties ophalen...'
                    },

                    sendMessage: {
                        responses: {
                            400: 'De gebruiker kon niet worden gevonden.',
                            403: 'Dit is niet uw connectie.',
                            404: 'De connectie kon niet worden gevonden.'
                        }
                    }
                }
            },

            forgotPassword: {
                requestPasswordReset: {
                    loader: 'Wachtwoord herstellen...',
                    responses: {
                        200: {
                            title: 'Wachtwoord vergeten?',
                            content: 'Wij hebben u een e-mail gestuurd met een herstel link.'
                        },
                        404: 'Het opgegeven e-mailadres is niet in gebruik.'
                    }
                }
            },

            payment: {
                getPaymentLinkForReversedPayments: {
                    loader: 'Betaalgegevens ophalen...',
                    responses: {
                        400: 'Alle betalingen zijn reeds voldaan, gelieve de actie nogmaals uit te voeren.',
                        503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                    }
                }
            },

            profile: {
                deleteProfilePicture: {
                    loader: 'Profielfoto verwijderen...'
                },
                restorePassword: {
                    loader: 'Wachtwoord herstellen...',
                    responses: {
                        200: 'Uw wachtwoord is opnieuw ingesteld.',
                        400: 'De gegeven code is ongeldig.'
                    }
                },
                setProfilePicture: {
                    loader: 'Profielfoto uploaden...',
                    responses: {
                        413: 'Afbeeldingen mogen niet meer dan 5MB in grootte zijn.',
                        503: 'Er is een fout opgetreden, gelieve het later opnieuw te proberen.'
                    }
                },
                setProfilePictureBackgroundStyle: {
                    loader: 'Profielfoto aanpassen...',
                    responses: {
                        200: {
                            title: 'Profielfoto aanpassen',
                            content: 'U heeft uw profielfoto aangepast.'
                        }
                    }
                },
                validateRestorePassword: {
                    loader: 'Wachtwoordherstel aanvraag ophalen...',
                    responses: {
                        400: 'De gegeven code is ongeldig.'
                    }
                },
                resendActivationLink: {
                    loader: 'Activatielink sturen...',
                    responses: {
                        200: {
                            title: 'Activatielink gestuurd',
                            content: 'Er is een nieuwe activatielink gestuurd naar uw e-mailadres.'
                        },
                        400: 'Uw account is al geactiveerd.',
                        404: 'Het e-mailadres is niet bij ons geregistreerd.'
                    }
                },
                activateAccount: {
                    loader: 'Account activeren...',
                    responses: {
                        200: 'Uw account is nu geactiveerd. U bent nu ingelogd.',
                        400: 'Uw account is al geactiveerd.',
                        404: 'De code is ongeldig.'
                    }
                },
                activateEmail: {
                    loader: 'E-mailadres activeren...',
                    responses: {
                        200: 'Uw nieuwe e-mailadres is ingesteld.',
                        404: 'De code is ongeldig.'
                    }
                }
            },

            report: {
                reportUser: {
                    loader: 'Gebruiker rapporteren...',
                    responses: {
                        200: 'U heeft de gebruiker gerapporteerd.',
                        403: 'U kunt alleen gebruikers rapporteren waarmee u een connectie heeft.',
                        404: 'De gebruiker kon niet worden gevonden.',
                        429: 'U kunt een gebruiker niet vaker dan 1x per dag rapporteren.'
                    }
                }
            },

            review: {
                listReviewsForConnection: {
                    loader: 'Recensies ophalen...',
                    responses: {
                        404: 'De connectie kon niet worden gevonden.'
                    }
                },

                reportReview: {
                    loader: 'Review rapporteren...',
                    responses: {
                        200: 'U heeft de review gerapporteerd.',
                        404: 'De review kon niet worden gevonden.',
                        429: 'U kunt een review niet vaker dan 1x per dag rapporteren.'
                    }
                },

                getPaginatedReviews: {
                    loader: 'Recensies ophalen...'
                }
            },

            roofSpecialist: {
                job: {
                    setFilters: {
                        loader: 'Filters aanpassen...',
                        responses: {
                            200: 'De filters zijn aangepast.',
                            400: 'Gelieve het formulier in te vullen met geldige waardes.'
                        }
                    },
                    getPaginatedJobs: {
                        loader: 'Klussen ophalen...'
                    },
                    reportJob: {
                        loader: 'Klus rapporteren...',
                        responses: {
                            200: 'U heeft de klus gerapporteerd.',
                            404: 'De klus kon niet worden gevonden.',
                            429: 'U kunt een klus niet vaker dan 1x per dag rapporteren.'
                        }
                    },
                    sendConnectionRequestToConsumer: {
                        loader: 'Connectieverzoek versturen...',
                        responses: {
                            400: 'U heeft al een connectie met deze klus.',
                            403: 'U kunt nog niet reageren op klussen, omdat uw account nog niet is gevalideerd door een admin.',
                            404: 'De klus kon niet worden gevonden.'
                        }
                    }
                },

                portfolio: {
                    addPortfolioImage: {
                        loader: 'Afbeelding toevoegen aan portfolio...',
                        responses: {
                            400: 'U kunt maximaal 5 afbeeldingen toevoegen aan uw portfolio.',
                            503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                        }
                    },

                    editPortfolioImageContent: {
                        loader: 'Afbeelding vervangen...',
                        responses: {
                            404: 'De afbeelding kon niet worden gevonden.',
                            503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                        }
                    },

                    editPortfolioImageBackgroundStyle: {
                        loader: 'Afbeelding aanpassen...',
                        responses: {
                            404: 'De afbeelding kon niet worden gevonden.'
                        }
                    },

                    deletePortfolioImage: {
                        loader: 'Afbeelding verwijderen...',
                        responses: {
                            404: 'De afbeelding die u probeert te verwijderen bestaat niet.',
                            503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                        }
                    }
                },

                saveCompanyDetails: {
                    loader: 'Bedrijfsgegevens opslaan...',
                    responses: {
                        200: 'Bedrijfsgegevens aangepast.',
                        400: 'Gelieve het formulier volledig in te vullen.'
                    }
                },

                saveContactPerson: {
                    loader: 'Contactpersoon opslaan...',
                    responses: {
                        200: 'Contactpersoon aangepast.',
                        400: 'Gelieve het formulier volledig in te vullen.'
                    }
                },

                saveLoginDetails: {
                    loader: 'Inloggegevens opslaan...',
                    responses: {
                        200: 'Inloggegevens aangepast.',
                        400: 'Gelieve het formulier volledig in te vullen.',
                        emailFail: 'Uw e-mailadres kon niet worden aangepast door een server fout.',
                        emailFailInUse: 'Het opgegeven e-mailadres is al in gebruik.',
                        passwordFail: 'Uw wachtwoord kon niet worden aangepast door een server fout. Wij zijn hiervan op de hoogte en doen ons best om dit zo spoedig mogelijk te verhelpen. Gelieve het later nogmaals te proberen.',
                        passwordFailUnauthorized: 'Het ingevoerde huidige wachtwoord is onjuist.',
                        passwordSuccess: 'Uw wachtwoord is aangepast. Daarnaast zijn al uw sessies nu verlopen en dient u weer opnieuw in te loggen op alle andere apparaten.',
                        emailSuccess: 'Er is een activatiemail verzonden naar uw nieuwe e-mailadres. Zodra u deze heeft geactiveerd wordt uw nieuwe e-mailadres geregistreerd.'
                    }
                },

                savePaymentDetails: {
                    loader: 'Betalingsgegevens aanpassen...',
                    responses: {
                        200: {
                            profileComplete: {
                                title: [
                                    'U heeft uw profiel nu ingesteld!',
                                    'De volgende stap is het zoeken naar een klus.'
                                ],
                                toJobs: 'Naar klussen'
                            },
                            profileAlreadyComplete: 'Uw betaalgegevens zijn aangepast.'
                        },
                        501: 'Momenteel is het alleen mogelijk om betalingen te doen via iDeal. Binnenkort zullen automatische incasso en creditcard geimplementeerd worden.',
                        503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                    }
                },

                getCocDetails: {
                    loader: 'KvK gegevens ophalen...',
                    responses: {
                        400: 'Het KvK nummer is ongeldig.',
                        404: 'Het KvK nummer kon niet worden gevonden.',
                        409: 'Het KvK nummer is al geregistreerd door een andere gebruiker.',
                        503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                    }
                },

                register: {
                    loader: 'Inschrijving voltooien...',
                    responses: {
                        400: 'U heeft het formulier nog niet volledig ingevuld.',
                        403: 'Om u bij ons aan te melden dient u akkoord te gaan met de algemene voorwaarden.',
                        404: 'Het KvK nummer kon niet worden gevonden.',
                        409: 'Het e-mailadres is al bij ons in gebruik. Bij de tab persoonlijke gegevens kunt u deze klus koppelen aan uw account.',
                        429: 'U kunt bij dakklusje.nl maximaal 1x per dag registreren, probeert u het morgen nogmaals.',
                        503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                    }
                },

                addCredits: {
                    loader: 'Credits opwaarderen...',
                    responses: {
                        400: 'Het bedrag inclusief BTW dient tussen de 30 en 50.000 euro te liggen.',
                        503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                    }
                },

                validateCreditRequest: {
                    loader: 'Credit aanvraag valideren...',
                    responses: {
                        200: 'De betaling is geslaagd.',
                        403: 'De betaling is mislukt.',
                        404: 'De credit aanvraag kon niet worden gevonden.',
                        503: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                    }
                },

                requestInvoiceToken: {
                    loader: 'Factuur token aanvragen...',
                    responses: {
                        400: 'Gelieve een geldige periode te selecteren.'
                    }
                },            

                downloadInvoiceForMonth: {
                    loader: 'Factuur genereren...',
                    responses: {
                        404: 'De token die u heeft meegegeven is ongeldig.'
                    }
                }
            },

            socialMedia: {
                connectWithFacebook: {
                    loader: 'Verbinden met facebook...',
                    responses: {
                        400: 'Het facebook account is al gekoppeld aan een andere gebruiker.',
                        401: 'U heeft het verkeerde wachtwoord ingevuld.',
                        404: 'Het gegeven e-mailadres is niet bij ons in gebruik.'
                    }
                },
                connectWithGooglePlus: {
                    loader: 'Verbinden met google+...',
                    responses: {
                        400: 'Het google+ account is al gekoppeld aan een andere gebruiker.',
                        401: 'U heeft het verkeerde wachtwoord ingevuld.',
                        404: 'Het gegeven e-mailadres is niet bij ons in gebruik.'
                    }
                }
            }
        },

        entity: {
            user: {
                emailAddress: {
                    placeholder: 'E-mailadres'
                },
                password: {
                    placeholder: 'Password'
                }
            }
        },

        enumerations: {
            branche: (branche: Branche): string => {
                switch (branche) {
                    case Branche.ROOF_GENERAL:
                        return 'Dak algemeen'
                    case Branche.SOLAR_PANELS:
                        return 'Zonnepanelen'
                    case Branche.HEAT_PUMP:
                        return 'Warmtepompen'
                }
            },

            jobCategory: (jobCategory: JobCategory): string => {
                switch (jobCategory) {
                    case JobCategory.CONSTRUCTION_NEW_ROOF:
                        return 'Aanleg nieuw dak'
                    case JobCategory.RENOVATION_AND_REPLACE_ROOF:
                        return 'Renovatie/vervangen dak'
                    case JobCategory.ROOF_REPAIR:
                        return 'Dakreparatie'
                    case JobCategory.ROOF_ISOLATION:
                        return 'Dak isolatie'
                    case JobCategory.ROOF_DOME_AND_LIGHT_STREET:
                        return 'Dakkoepel, lichtstraat'
                    case JobCategory.SKYLIGHT:
                        return 'Dakraam'
                    case JobCategory.DORMER:
                        return 'Dakkapel'
                    case JobCategory.ATTIC_DEVICE:
                        return 'Zolderinrichting'
                    case JobCategory.ROOF_COATING:
                        return 'Dak coating'
                    case JobCategory.ROOF_GUTTER:
                        return 'Dak goot'
                    case JobCategory.BUOYINGS_AND_WIND_SPRING_AND_CROSS:
                        return 'Boeidelen, windveer of overstek'
                    case JobCategory.ROOF_CLEANING:
                        return 'Reiniging dak'
                    case JobCategory.SOLAR_PANELS:
                        return 'Zonnepanelen'
                    case JobCategory.HEAT_POMPS:
                        return 'Warmtepompen'
                    case JobCategory.OTHER:
                        return 'Overig'
                }
            },

            houseType: (houseType: HouseType): string => {
                switch (houseType) {
                    case HouseType.TERRACED_HOUSE:
                        return 'Rijtjes huis'
                    case HouseType.SEMI_OPEN_BUILDINGS:
                        return 'Halfopen bebouwing'
                    case HouseType.DETACHED_OR_SINGLE:
                        return 'Vrijstaand of alleenstaand'
                    case HouseType.APARTMENT:
                        return 'Appartement'
                    case HouseType.OFFICE_OR_BUSINESS_PREMISES:
                        return 'Kantoor/bedrijfspand'
                    case HouseType.SCHOOL:
                        return 'School'
                    case HouseType.OTHER:
                        return 'Overig'
                }
            },

            paymentType: (paymentType: PaymentType): string => {
                switch (paymentType) {
                    case PaymentType.IDEAL:
                        return 'Ideal'
                    case PaymentType.CREDIT_CARD:
                        return 'Creditcard'
                    case PaymentType.AUTOMATIC_COLLECTION:
                        return 'Automatische incasso'
                }
            },

            reportReason: (reportReason: ReportReason): string => {
                switch (reportReason) {
                    case ReportReason.INAPPROPRIATE_BEHAVIOUR:
                        return 'Ongepast gedrag'
                    case ReportReason.OTHER:
                        return 'Overig'
                }
            },

            roofIsolationType: (roofIsolationType: RoofIsolationType): string => {
                switch (roofIsolationType) {
                    case RoofIsolationType.ATTIC_ISOLATION:
                        return 'Zolder isolatie'
                    case RoofIsolationType.ISOLATE_FROM_OUTSIDE:
                        return 'Vanuit buiten isoleren'
                    case RoofIsolationType.ISOLATE_FROM_INSIDE:
                        return 'Vanuit binnen isoleren'
                    case RoofIsolationType.OTHER:
                        return 'Overig'
                }
            },

            roofMaterialType: (roofMaterialType: RoofMaterialType): string => {
                switch (roofMaterialType) {
                    case RoofMaterialType.SLATE:
                        return 'Leien'
                    case RoofMaterialType.ROOF_TILES:
                        return 'Dakpannen'
                    case RoofMaterialType.REEDS:
                        return 'Riet'
                    case RoofMaterialType.BITUMEN_ROOFING_MATERIAL:
                        return 'Bitumen dakbedekking'
                    case RoofMaterialType.PVC_ROOFING:
                        return 'Pvc dakbedekking'
                    case RoofMaterialType.EPDM_ROOFING:
                        return 'Epdm dakbedekking'
                    case RoofMaterialType.SEDUM_ROOF_OR_GREEN_ROOF:
                        return 'Sedumdak/groendak'
                    case RoofMaterialType.CORRUGATED_SHEETS:
                        return 'Golfplaten'
                    case RoofMaterialType.OTHER:
                        return 'Overig'
                }
            },

            roofType: (roofType: RoofType): string => {
                switch (roofType) {
                    case RoofType.FLAT_ROOF:
                        return 'Plat dak'
                    case RoofType.PITCHED_ROOF:
                        return 'Hellend dak'
                }
            }
        },

        generic: {
            responses: {
                401: 'Uw sessie is verlopen, u bent nu uitgelogd.',
                500: 'Er is een fout opgetreden in de server. Wij zijn hiervan op de hoogte en doen er alles aan om dit zo spoedig mogelijk op te lossen. Probeert u de actie over enkele minuten nogmaals.',
                503: {
                    amazonWebServicesS3: 'Een externe server die benodigd is voor deze aanvraag is tijdelijk niet bereikbaar. Probeert u het over een aantal minuten opnieuw.'
                },
                offline: 'De server is offline. Wij zijn hiervan op de hoogte en doen er alles aan om dit zo spoedig mogelijk op te lossen. Probeert u de actie over enkele minuten nogmaals.'
            }
        }
    },

    /**
     * The breadcrumb name per route
     */
    breadcrumbs: (route: Route): string => {
        switch (route) {
            case Route.ABOUT_US:
                return 'Over ons'
            case Route.COSTS:
                return 'Kosten'
            case Route.CONSUMER_JOBS:
                return 'Klussenoverzicht'
            case Route.CONSUMER_PLACE_YOUR_JOB:
                return 'Plaats je klus'
            case Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS:
                return 'Persoonsgegevens'
            case Route.CONSUMER_PLACE_YOUR_JOB_PICTURES:
                return 'Foto\'s'
            case Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS:
                return 'Inloggegevens'
            case Route.CONSUMER_EDIT_JOB:
                return 'Klus aanpassen'
            case Route.CONSUMER_EDIT_JOB_PICTURES:
                return 'Foto\'s'
            case Route.HOME:
                return 'Home'
            case Route.HOW_DOES_IT_WORK:
                return 'Hoe werkt het?'
            case Route.HOW_DOES_IT_WORK_CONSUMER:
                return 'Consument'
            case Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST:
                return 'Dakspecialist'
            case Route.INBOX:
                return 'Inbox'
            case Route.PROFILE:
                return 'Profiel'
            case Route.PROFILE_CONSUMER_MANAGE:
                return 'Beheren'
            case Route.PROFILE_CONSUMER_PERSONAL_DETAILS:
                return 'Persoonlijke gegevens'
            case Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS:
                return 'Bedrijfsgegevens'
            case Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO:
                return 'Portfolio'
            case Route.PROFILE_ROOF_SPECIALIST_REVIEWS:
                return 'Reviews'
            case Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS:
                return 'Betaalgegevens'
            case Route.ROOF_SPECIALIST_JOBS:
                return 'Klussenoverzicht'
            case Route.ROOF_SPECIALIST_REGISTER:
                return 'Inschrijven als dakspecialist'
            case Route.ROOF_SPECIALIST_REGISTER_COC:
                return 'KvK'
            case Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON:
                return 'Contactpersoon'
            case Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS:
                return 'Inloggegevens'
        }
    },

    components: {
        experienceFromOthers: {
            title: 'Ervaring van anderen'
        },
        feedback: {
            content: {
                buttons: {
                    ok: 'OK'
                }
            }
        },
        footer: {
            bottom: {
                about: {
                    title: 'Over Dakklusje.nl',
                    content: {
                        aboutUs: 'Over ons',
                        terms: 'Algemene voorwaarden',
                        registerAsRoofSpecialist: 'Inschrijven als dakspecialist'
                    }
                },
                address: {
                    title: 'Adres',
                    content: [
                        'Docterskampstraat 2A unit 2735',
                        '5222AM \'s-Hertogenbosch',
                        'Nederland'
                    ]
                },
                contact: {
                    title: 'Contact',
                    content: {
                        email: 'E-mail',
                        phoneNumber: 'Telefoon'
                    }
                },
                coc: {
                    title: 'KvK'
                },
                followUs: {
                    title: 'Volg ons'
                }
            },
            top: {
                addJob: 'Plaats je klus',
                text: 'om in gesprek te komen met geschikte dakspecialisten'
            }
        },
        form: {
            input: {
                filter: {
                    placeholder: 'Filter'
                }
            }
        },
        imageCropper: {
            settings: {
                title: 'Versleep de foto om de positie te wijzigen',
                slider: {
                    label: 'Zoomen'
                }
            }
        },
        imageUpload: {
            uploadFailed: 'De foto kon niet worden geüpload, gelieve het later opnieuw te proberen.'
        },
        loader: {
            allRightsReserved: 'Alle rechten voorbehouden.'
        },
        navigation: {
            buttons: {
                addAJob: 'Plaats gratis een klus',
                home: 'Home',
                howDoesItWork: 'Hoe werkt het?',
                login: 'Inloggen',
                menu: 'Menu',
                costs: 'Kosten'
            },
            menu: {
                mobile: {
                    buttons: {
                        backToMainMenu: 'Terug naar hoofdmenu',
                        menu: 'Menu'
                    }
                }
            },
            inbox: {
                left: {
                    label: 'Inbox'
                },
                right: {
                    buttons: {
                        backToWebsite: 'Terug naar website'
                    }
                }
            }
        },
        picture: {
            noPhoto: 'Geen foto'
        },
        profilePicture: {
            alt: 'Profielfoto'
        },
        progressSpinner: {
            loading: 'Laden...'
        },
        qualityChecks: {
            1: 'We delen geen persoonlijke gegevens met dakspecialisten, totdat je daar zelf voor kiest.',
            2: 'Alleen dakspecialisten met een geldige KvK-inschrijving zijn aangesloten bij Dakklusje.nl.',
            3: 'Reviews op Dakklusje.nl zijn geschreven door huiseigenaren zoals jij.'
        }
    },

    date: {
        /**
         * Generates a human readable date based on the language
         */
        humanReadable: (timestamp: number): string => {
            const timestampNow = new Date().getTime()
            const delta = timestampNow - timestamp
            const nowDate = moment(timestampNow).format('DD-MM-YYYY')
            const timestampDate = moment(timestamp).format('DD-MM-YYYY')

            if (delta > 0) {
                // past 5 minutes
                if (delta <= 1000 * 60 * 5) {
                    return 'Zojuist'
                }

                // Past hour
                else if (delta <= 1000 * 60 * 60) {
                    return `${Math.round(delta / 1000 / 60)} minuten`
                }

                // Same day
                else if (nowDate === timestampDate) {
                    return `${Math.round(delta / 1000 / 60 / 60)} uur`
                }

                // Yesterday
                else if (delta < 2 * 24 * 3600 * 1000) {
                    return 'Gisteren'
                }

                // 2 days ago
                else if (delta < 3 * 24 * 3600 * 1000) {
                    return 'Eergisteren'
                }

                // days
                else if (delta < 7 * 24 * 3600 * 1000) {
                    return `${Math.round(delta / 1000 / 60 / 60 / 24)} dagen`
                }

                // 1 week
                else if (delta < 2 * 7 * 24 * 3600 * 1000) {
                    return '1 Week'
                }

                // Multiple weeks
                else {
                    return `${Math.round(delta / 1000 / 60 / 60 / 24 / 7)} weken`
                }
            } else {
                return 'Zojuist'
            }
        },

        month: [
            'Januari',
            'Februari',
            'Maart',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Augustus',
            'September',
            'Oktober',
            'November',
            'December'
        ]
    },

    document: {
        lang: 'nl',
        intercomLang: 'nl',
        meta: {
            description: 'Klusje aan het dak? Dakklusje.nl! Plaats gratis en vrijblijvend uw klus voor op of aan het dak en kom in contact met de juiste vakmannen, klusjesmannen, dakdekkers.'
        },
        title: {
            main: 'Dakklusje.nl',
            route: (route: Route): string => {
                switch (route) {
                    case Route.ABOUT_US:
                        return 'Over ons'
                    case Route.COSTS:
                        return 'Kosten'
                    case Route.CONSUMER_JOBS:
                        return 'Klussenoverzicht'
                    case Route.CONSUMER_PLACE_YOUR_JOB:
                        return 'Plaats je klus'
                    case Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS:
                        return 'Plaats je klus - Persoonsgegevens'
                    case Route.CONSUMER_PLACE_YOUR_JOB_PICTURES:
                        return 'Plaats je klus - Foto\'s'
                    case Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS:
                        return 'Plaats je klus - Inloggegevens'
                    case Route.CONSUMER_EDIT_JOB:
                        return 'Klus aanpassen'
                    case Route.CONSUMER_EDIT_JOB_PICTURES:
                        return 'Klus aanpassen - Foto\'s'
                    case Route.HOME:
                        return 'Vind de juiste dakspecialisten voor uw klus!'
                    case Route.HOW_DOES_IT_WORK:
                        return 'Hoe werkt het?'
                    case Route.HOW_DOES_IT_WORK_CONSUMER:
                        return 'Hoe werkt het?'
                    case Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST:
                        return 'Hoe werkt het?'
                    case Route.INBOX:
                        return 'Inbox'
                    case Route.PROFILE:
                        return 'Profiel'
                    case Route.PROFILE_CONSUMER_PERSONAL_DETAILS:
                        return 'Profiel - Persoonlijke gegevens'
                    case Route.PROFILE_CONSUMER_MANAGE:
                        return 'Profiel - Beheren'
                    case Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS:
                        return 'Profiel - Bedrijfsgegevens'
                    case Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO:
                        return 'Profiel - Portfolio'
                    case Route.PROFILE_ROOF_SPECIALIST_REVIEWS:
                        return 'Profiel - Reviews'
                    case Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS:
                        return 'Profiel - Betaalgegevens'
                    case Route.ROOF_SPECIALIST_JOBS:
                        return 'Klussenoverzicht'
                    case Route.ROOF_SPECIALIST_REGISTER:
                        return 'Inschrijven als dakspecialist'
                    case Route.ROOF_SPECIALIST_REGISTER_COC:
                        return 'Inschrijven als dakspecialist - KvK'
                    case Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON:
                        return 'Inschrijven als dakspecialist - Contactpersoon'
                    case Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS:
                        return 'Inschrijven als dakspecialist - Inloggegevens'
                }
            }
        }
    },

    enumerations: {
        formInvalidation: (formInvalidation: FormInvalidation, validation?: FormValidationOptionsAsFormGroupProperty): string => {
            switch (formInvalidation) {
                case FormInvalidation.VALUE_IS_NULL:
                    return 'De waarde mag niet leeg zijn.'
                case FormInvalidation.BOOLEAN_NOT_FALSE:
                    return 'U dient deze checkbox uit te vinken.'
                case FormInvalidation.BOOLEAN_NOT_TRUE:
                    return 'U dient deze checkbox aan te vinken.'
                case FormInvalidation.STRING_NO_STRING:
                    return 'De waarde mag niet leeg zijn.'
                case FormInvalidation.STRING_EMPTY:
                    return 'De waarde mag niet leeg zijn.'
                case FormInvalidation.STRING_TOO_SHORT:
                    return `De waarde moet tenminste ${(validation && validation.string && (validation.string.minLength || 0))} karakters bevatten.`
                case FormInvalidation.STRING_TOO_LONG:
                    return `De waarde mag niet meer dan ${(validation && validation.string && (validation.string.maxLength || 0))} karakters bevatten.`
                case FormInvalidation.STRING_PATTERN_NO_MATCH:
                    return 'Gelieve een geldige waarde in te vullen.'
                case FormInvalidation.STRING_EVALUATED_VALUE_NOT_MATCHED:
                    return 'De waardes komen niet overeen.'
                case FormInvalidation.ARRAY_NO_ARRAY:
                    return 'De waarde mag niet leeg zijn.'
                case FormInvalidation.ARRAY_EMPTY:
                    return 'Maak een keuze.'
                case FormInvalidation.ARRAY_INVALID_VALUE:
                    return 'De waarde mag niet leeg zijn.'
                case FormInvalidation.ARRAY_TOO_FEW_ENTRIES:
                    return `Kies ten minste ${(validation && validation.array && (validation.array.minLength || 0))} opties.`
                case FormInvalidation.ARRAY_TOO_MANY_ENTRIES:
                    return `Het is niet mogelijk om meer dan ${(validation && validation.array && (validation.array.maxLength || 0))} opties te kiezen.`
                case FormInvalidation.NUMBER_NAN:
                    return 'De waarde moet een nummer zijn.'
                case FormInvalidation.NUMBER_MIN_VALUE:
                    return `De waarde moet groter of gelijk zijn aan ${(validation && validation.number && (validation.number.minValue || 0))}.`
                case FormInvalidation.NUMBER_MAX_VALUE:
                    return `De waarde moet kleiner of gelijk zijn aan ${(validation && validation.number && (validation.number.maxValue || 0))}.`
                case FormInvalidation.ENUM_INVALID:
                    return 'De waarde mag niet leeg zijn.'
                case FormInvalidation.VALUE_NOT_ALLOWED:
                    return 'De waarde mag niet leeg zijn.'
            }
        }
    },

    integrations: {
        facebook: {
            login: {
                loader: 'Inloggen met facebook...',
                error: 'Inloggen met facebook is geannuleerd.'
            }
        },
        googlePlus: {
            login: {
                loader: 'Inloggen met google+...',
                error: 'Inloggen met google+ is geannuleerd.'
            }
        }
    },

    modals: {
        activateAccount: {
            title: 'Account activeren',
            content: 'U dient eerst uw account te activeren d.m.v. de link in de activatiemail voordat u deze actie kunt uitvoeren.',
            buttons: {
                sendNewActivationMail: 'Nieuwe mail sturen'
            }
        },

        addCredits: {
            title: 'Credits opwaarden',
            amount: {
                label: `Bedrag (exclusief ${AppInfo.vatPercentage}% BTW)`,
                helperText: 'Met welk bedrag wilt u uw credits aanvullen? Credits staan bij ons gelijk aan het aantal euros dat u ervoor betaald (exclusief BTW). Credits kunnen niet terugbetaald worden wanneer deze zijn opgewaardeerd.',
                placeholder: '30,00'
            },
            button: {
                add: 'Opwaarderen'
            }
        },

        authorize: {
            facebook: {
                title: 'Verbind uw facebook account door in te loggen.',
                form: {
                    emailAddress: {
                        placeholder: 'E-mailadres'
                    },
                    password: {
                        placeholder: 'Wachtwoord'
                    }
                }
            },
            googlePlus: {
                title: 'Verbind uw google+ account door in te loggen.',
                form: {
                    emailAddress: {
                        placeholder: 'E-mailadres'
                    },
                    password: {
                        placeholder: 'Wachtwoord'
                    }
                }
            }
        },

        calculateCosts: {
            title: 'Kosten berekenen',

            jobCategory: {
                label: 'Klus categorie',
                helperText: 'Selecteer een klus categorie',
                placeholder: 'Maak een keuze'
            }
        },

        connectionSettings: {
            title: 'Opties',
            buttons: {
                report: 'Rapporteren',
                deleteContact: 'Contact verwijderen'
            }
        },

        connectionSuccess: {
            title: 'U heeft op deze klus gereageerd.',
            content: (costs: number) => (
                <div>
                    <p className="light">
                        Wanneer de gebruiker uw connectieverzoek accepteerd zult u een melding krijgen hierover en kunt u na het betalen van de aanneemkosten voor deze klus het gesprek aangaan!
                    </p><p className="light">
                        De kosten bedragen over het algemeen 30 euro exclusief {AppInfo.vatPercentage}% BTW. Bij kleine klussen zal dit bedrag minder zijn.
                    </p><p>
                        <span className="medium">
                            Uw kosten bij het aannemen van deze klus zijn
                        </span>
                        <span className="bold">
                            {
                                costs.toFixed(2)
                            } euro exclusief {AppInfo.vatPercentage}% BTW.
                        </span>
                    </p>
                </div>
            )
        },

        creditPaymentValidate: {
            title: 'Uw betaling',
            description: {
                pwa: 'U kunt nu doorgaan naar de betaalpagina. Zodra u klaar bent kunt u uw betaling controleren d.m.v. onderstaande knop.',
                nonPwa: 'U wordt nu doorverwezen naar de betaalpagina. Zodra u klaar bent met uw betaling kunt u op de onderstaande knop drukken om verder te gaan.'
            }
        },

        defaultButtons: {
            back: 'Terug',
            cancel: 'Annuleren',
            close: 'Sluiten',
            ok: 'OK',
            save: 'Opslaan',
            continue: 'Doorgaan',
            next: 'Volgende',
            previous: 'Vorige',
            reset: 'Reset',
            delete: 'Verwijderen',
            edit: 'Aanpassen',
            open: 'Open',
            toCheckout: 'Naar betaalpagina',
            validateCosts: 'Betaling controleren'
        },

        destructiveAction: {
            title: 'Weet u zeker dat u wilt doorgaan?'
        },
        
        editJobImage: {
            title: 'Foto aanpassen',
            buttons: {
                reposition: 'Herpositioneren',
                newPicture: 'Andere foto kiezen',
                delete: 'Verwijderen'
            }
        },

        editPortfolioImage: {
            title: 'Foto aanpassen',
            buttons: {
                reposition: 'Herpositioneren',
                newPicture: 'Andere foto kiezen',
                delete: 'Verwijderen'
            }
        },

        editTempImage: {
            title: 'Foto aanpassen',
            buttons: {
                reposition: 'Herpositioneren',
                newPicture: 'Andere foto kiezen',
                delete: 'Verwijderen'
            }
        },

        forgotPassword: {
            buttons: {
                restorePassword: 'Herstellen'
            },
            form: {
                emailAddress: {
                    helperText: 'Vul hier uw e-mailadres in.',
                    label: 'Uw e-mailadres'
                }
            },
            title: 'Wachtwoord vergeten?',
        },

        generic: {
            error: {
                title: 'Actie mislukt'
            },

            success: {
                title: 'Actie gelukt'
            }
        },

        jobPaymentValidate: {
            title: 'Uw betaling',
            description: {
                pwa: 'U kunt nu doorgaan naar de betaalpagina. Zodra u klaar bent kunt u uw betaling controleren d.m.v. onderstaande knop.',
                nonPwa: 'U wordt nu doorverwezen naar de betaalpagina. Zodra u klaar bent met uw betaling kunt u op de onderstaande knop drukken om verder te gaan.'
            }
        },

        jobSettings: {
            consumer: {
                title: 'Opties',
                buttons: {
                    editJob: 'Klus aanpassen',
                    deleteJob: 'Klus verwijderen'
                },
                destructiveAction: {
                    content: 'Weet u zeker dat u door wilt gaan? Deze actie kan niet ongedaan gemaakt worden.'
                }
            },
            roofSpecialist: {
                title: 'Opties',
                consumer: {
                    fullName: 'Onbekend'
                },
                buttons: {
                    reportJob: 'Klus rapporteren'
                }
            }
        },

        navigationMenuModal: {
            authorized: {
                buttons: {
                    inbox: 'Inbox',
                    jobs: 'Klussen',
                    logout: 'Uitloggen',
                    profile: 'Profiel'
                },
                greeting: {
                    afternoon: 'Goedemiddag',
                    evening: 'Goedeavond',
                    morning: 'Goedemorgen',
                    night: 'Goedenacht'
                }
            },

            unauthorized: {
                form: {
                    forgotPassword: 'Wachtwoord vergeten?',
                    login: 'Inloggen',
                    orLoginWith: 'Of login via',
                    remainLoggedIn: 'Ingelogd blijven?'
                },
                title: 'Inloggen'
            }
        },

        nonModernBrowser: {
            title: 'Gebruikerservaring',
            content: 'Voor de beste gebruikerservaring raden wij het gebruik van Google Chrome aan. Dit is een eenmalig bericht.'
        },

        pendingPayment: {
            title: 'Betalingsachterstand',
            content: (amount: number) => (
                <div>
                    <p>
                        U heeft een betalingsachterstand en daarom heeft u beperkt toegang tot ons platform.
                    </p><p>
                        Voorkom incasso procedures en betaal direct het openstaande bedrag van {amount.toFixed(2).replace('.', ',')} euro (inclusief BTW).
                    </p>
                </div>
            ),
            buttons: {
                toPaymentPage: 'Betalen via iDeal'
            }
        },

        profilePicture: {
            settings: {
                delete: 'Verwijderen',
                title: 'Avatar instellingen',
                reposition: 'Herpositioneren'
            }
        },

        pwaDownloadInvoice: {
            title: 'Factuur downloaden',
            description: 'Uw factuur is klaar en u kunt deze nu downloaden.',
            buttons: {
                download: 'Download'
            }
        },

        reactivateAccount: {
            title: 'Account heractiveren',
            form: {
                fullName: {
                    label: 'Volledige naam',
                    helperText: 'Vul hier uw volledige naam in om uw account te heractiveren.',
                    placeholder: 'Uw naam'
                }
            },
            buttons: {
                reactivate: 'Heractiveren'
            }
        },

        registrationComplete: {
            consumer: {
                title: 'Account activeren',
                content: [
                    'Wij hebben u een activatielink gestuurd per e-mail.',
                    '',
                    'Zodra u uw account heeft geactiveerd wordt uw klus gepubliceerd en komt deze in het overzicht van de juiste dakspecialisten terecht!'
                ]
            },
            roofSpecialist: {
                title: 'Account activeren',
                content: [
                    'Wij hebben u een activatielink gestuurd per e-mail.',
                    '',
                    'Zodra u uw account heeft geactiveerd zullen wij binnen 1-3 werkdagen contact met u opnemen en kunt u gebruik maken van de klussen pagina om een passende klus te vinden voor uw branche.',
                    '',
                    'Doorgaans zult u binnen 24 uur gebruik kunnen maken van ons platform.'
                ]
            }
        },

        reportJob: {
            title: 'Klus rapporteren',
            form: {
                job: {
                    label: 'De klus',
                    helperText: 'De titel van de klus die u rapporteerd.'
                },
                description: {
                    label: 'Eigen beschrijving',
                    helperText: 'Wat is de reden voor het rapporteren?',
                    placeholder: 'Leg hier in maximaal 250 karakters uit wat er is gebeurd.'
                },
                buttons: {
                    report: 'Rapporteren'
                }
            }
        },

        reportUser: {
            title: 'Gebruiker rapporteren',
            form: {
                user: {
                    label: 'De gebruiker',
                    helperText: 'De naam van de gebruiker die u rapporteerd.'
                },
                description: {
                    label: 'Eigen beschrijving',
                    helperText: 'Wat is de reden voor het rapporteren?',
                    placeholder: 'Leg hier in maximaal 250 karakters uit wat er is gebeurd.'
                },
                buttons: {
                    report: 'Rapporteren'
                }
            }
        },

        reportConnection: {
            title: 'Gesprek rapporteren',
            form: {
                user: {
                    label: 'De gebruiker',
                    helperText: 'De naam van de gebruiker die aan het gesprek gekoppeld is.'
                },
                description: {
                    label: 'Eigen beschrijving',
                    helperText: 'Wat is de reden voor het rapporteren?',
                    placeholder: 'Leg hier in maximaal 250 karakters uit wat er is gebeurd.'
                },
                buttons: {
                    report: 'Rapporteren'
                }
            }
        },

        reportReview: {
            title: 'Review rapporteren',
            form: {
                user: {
                    label: 'De gebruiker',
                    helperText: 'De naam van de gebruiker die u rapporteerd.'
                },
                description: {
                    label: 'Eigen beschrijving',
                    helperText: 'Wat is de reden voor het rapporteren?',
                    placeholder: 'Leg hier in maximaal 250 karakters uit wat er is gebeurd.'
                },
                buttons: {
                    report: 'Rapporteren'
                }
            }
        },

        restorePassword: {
            buttons: {
                restorePassword: 'Wachtwoord herstellen'
            },
            form: {
                password: {
                    label: 'Wachtwoord',
                    helperText: 'Vul hier uw nieuwe wachtwoord in.',
                    placeholder: 'Uw wachtwoord'
                },
                passwordRepeat: {
                    label: 'Wachtwoord herhalan',
                    helperText: 'Vul hier ter controle nogmaals uw nieuwe wachtwoord in.',
                    placeholder: 'Uw wachtwoord'
                }
            },
            title: 'Wachtwoord herstellen'
        },

        review: {
            title: 'Nieuwe recensie',
            form: {
                description: {
                    label: 'Beschrijving',
                    helperText: 'De beschrijving voor uw review.',
                    placeholder: (connection: InboxConnectionResponse) => {
                        return `Beschrijf in 100-500 karakters uw ervaring met ${connection.roofSpecialist.cocCompany.companyName}`
                    }
                }
            },
            buttons: {
                place: 'Plaatsen'
            }
        },

        reviewSettings: {
            title: 'Opties',
            buttons: {
                report: 'Rapporteren'
            }
        },

        roofSpecialistFilters: {
            title: 'Filters',
            form: {
                surfaceMeasurements: {
                    label: 'Aantal (vierkante) meter',
                    options: [
                        {
                            key: '10',
                            value: 'Vanaf 10'
                        },
                        {
                            key: '25',
                            value: 'Vanaf 25'
                        },
                        {
                            key: '50',
                            value: 'Vanaf 50'
                        },
                        {
                            key: '100',
                            value: 'Vanaf 100'
                        },
                        {
                            key: '250',
                            value: 'Vanaf 250'
                        }
                    ],
                    placeholder: 'Alles weergeven'
                },
                distance: {
                    label: 'Maximale afstand',
                    options: [
                        {
                            key: '10',
                            value: '10 kilometer'
                        },
                        {
                            key: '25',
                            value: '25 kilometer'
                        },
                        {
                            key: '50',
                            value: '50 kilometer'
                        },
                        {
                            key: '100',
                            value: '100 kilometer'
                        },
                        {
                            key: '250',
                            value: '250 kilometer'
                        }
                    ],
                    placeholder: 'Alles weergeven'
                },
                roofType: {
                    label: (roofTypeCount: number) => {
                        if (roofTypeCount === 0) {
                            return 'Type dak'
                        } else {
                            return `Type dak (${roofTypeCount})`
                        }
                    },
                    placeholder: 'Alles weergeven'
                },
                jobCategory: {
                    label: (jobCategoryCount: number) => {
                        if (jobCategoryCount === 0) {
                            return 'Soort klus'
                        } else {
                            return `Soort klus (${jobCategoryCount})`
                        }
                    },
                    placeholder: 'Alles weergeven'
                },
                houseType: {
                    label: (houseTypeCount: number) => {
                        if (houseTypeCount === 0) {
                            return 'Type woning'
                        } else {
                            return `Type woning (${houseTypeCount})`
                        }
                    },
                    placeholder: 'Alles weergeven'
                },
                roofMaterialType: {
                    label: (roofMaterialTypeCount: number) => {
                        if (roofMaterialTypeCount === 0) {
                            return 'Dakmateriaal'
                        } else {
                            return `Dakmateriaal (${roofMaterialTypeCount})`
                        }
                    },
                    placeholder: 'Alles weergeven'
                },
                roofIsolationType: {
                    label: (roofIsolationTypeCount: number) => {
                        if (roofIsolationTypeCount === 0) {
                            return 'Dakisolatie'
                        } else {
                            return `Dakisolatie (${roofIsolationTypeCount})`
                        }
                    },
                    placeholder: 'Alles weergeven'
                }
            },
            buttons: {
                reset: 'Reset',
                set: 'Instellen'
            }
        }
    },

    pages: {
        aboutUs: {
            steps: {
                1: (
                    <div className="content">
                        <p>
                            Pim Aarts is de bedenker van dakklusje.nl.
                        </p><br/>
                        <p>
                            Hij is op het idee gekomen door eigen ervaring vanuit zijn besloten vennootschap <a href="https://secuurdak.nl">Secuurdak B.V.</a>
                        </p>
                    </div>
                ),
                2: (
                    <div className="content">
                        <p>
                            Stan Hurks is de designer en ontwikkelaar van de software.
                        </p><br/>
                        <p>
                            Hij is daarnaast ook de CEO alsmede enige aandeelhouder van <a href={AppInfo.designedAndBuildBy.websiteHurksDigital}>{AppInfo.designedAndBuildBy.companyName}</a>
                        </p>
                    </div>
                )
            }
        },

        addJob: {
            wizard: {
                title: {
                    addJob: 'Plaats uw klus',
                    editJob: 'Klus aanpassen'
                },
                tabs: {
                    job: 'Klus',
                    personalDetails: 'Persoons gegevens',
                    pictures: 'Foto\'s',
                    loginCredentials: 'Inlog gegevens'
                },
                content: {
                    job: {
                        intro: {
                            title: 'Klus',
                            content: [
                                'Vul de onderstaande velden in om te beginnen met het plaatsen van uw klus!',
                                'Afhankelijk van het type klus dient u tot maximaal 5 klus specifieke opties te selecteren.'
                            ],
                            contentEdit: [
                                'Door middel van de onderstaande velden kunt u uw klus aanpassen.'
                            ]
                        },
                        form: {
                            title: {
                                label: 'Titel',
                                helperText: 'Geef uw klus een pakkende titel.',
                                placeholder: 'Bijvoorbeeld: Dakreparatie 85m2 bitumen'
                            },
                            surfaceMeasurements: {
                                [MeasurementUnit.SQUARE_METER]: {
                                    label: 'Aantal vierkante meter',
                                    helperText: 'Hoeveel vierkante meter moet er gedaan worden?',
                                    placeholder: '0'
                                },
                                [MeasurementUnit.METER]: {
                                    label: 'Aantal meter',
                                    helperText: 'Hoeveel meter moet er gedaan worden?',
                                    placeholder: '0'
                                }
                            },
                            branche: {
                                label: 'Categorie',
                                helperText: 'Kies de categorie die het beste aansluit op uw klus.',
                                placeholder: 'Kies een categorie'
                            },
                            roofType: {
                                label: 'Type dak',
                                helperText: 'Wat voor soort dak heeft u?',
                                placeholder: 'Kies een dak type'
                            },
                            jobCategory: {
                                label: 'Soort klus',
                                helperText: 'Wat voor soort klus moet er worden gedaan?',
                                placeholder: 'Kies een klus categorie'
                            },
                            houseType: {
                                label: 'Type woning',
                                helperText: 'Wat voor type woning heeft u?',
                                placeholder: 'Kies een type woning'
                            },
                            roofMaterialType: {
                                label: 'Dakmateriaal',
                                helperText: 'Van welk materiaal is uw dak gemaakt?',
                                placeholder: 'Kies een type dakmateriaal'
                            },
                            roofIsolationType: {
                                label: 'Dakisolatie',
                                helperText: 'Wat voor isolatie gerelateerd werk dient er gedaan te worden?',
                                placeholder: 'Maak een keuze'
                            },
                            description: {
                                label: 'Beschrijving',
                                helperText: 'Voeg eventueel nog een beschrijving toe aan uw klus.',
                                placeholder: 'Optioneel: beschrijf hier in maximaal 1000 karakters wat er gedaan moet worden.'
                            }
                        },
                        outro: {
                            title: 'Hierna',
                            titleEdit: 'Opslaan',
                            content: [
                                'Na deze stap komt u bij de stap "Persoonsgegevens".',
                                'Hier wordt u gevraagd om uw naam en postcode.',
                            ],
                            contentEdit: [
                                'Indien u tevreden bent met de wijzigen kunt u deze hier opslaan.'
                            ]
                        }
                    },
                    loginDetails: {
                        intro: {
                            title: 'Inloggegevens',
                            content: [
                                'Vul hier uw e-mailadres en wachtwoord in.',
                                'Na de registratie is uw klus direct zichtbaar in het overzicht van de juiste dakspecialisten!'
                            ]
                        },
                        form: {
                            buttons: {
                                addJob: 'Plaats uw klus'
                            },
                            emailAddress: {
                                label: 'E-mailadres',
                                helperText: 'Uw e-mailadres',
                                placeholder: 'Uw e-mailadres'
                            },
                            password: {
                                label: 'Wachtwoord',
                                helperText: 'Vul hier uw wachtwoord in',
                                placeholder: 'Uw wachtwoord'
                            },
                            passwordRepeat: {
                                label: 'Wachtwoord herhalen',
                                helperText: 'Vul hier ter controle nogmaals uw wachtwoord in.',
                                placeholder: 'Uw wachtwoord'
                            },
                            termsAndAgreements: {
                                label: 'Algemene voorwaarden',
                                helperText: (
                                    <p>
                                        Om gebruik te maken van ons platform<br/>
                                        dient u akkoord te gaan met onze <a href={AppInfo.designedAndBuildBy.termsAndAgreementsLink}>algemene voorwaarden.</a>
                                    </p>
                                ),
                                checkbox: {
                                    label: 'Ik ga akkoord met de algemene voorwaarden.'
                                }
                            }
                        },
                        outro: {
                            title: 'Bijna klaar!',
                            content: [
                                'Na deze stap wordt uw klus geplaatst.',
                                'Hiervoor hoeft u alleen nog maar uw account te activeren!'
                            ]
                        }
                    },
                    personalDetails: {
                        intro: {
                            title: 'Persoonsgegevens',
                            content: [
                                'Vul hier uw persoongegevens in.',
                                'Uw naam is afgeschermd voor dakspecialisten tot u van hun een klusaanvraag heeft geaccepteerd.'
                            ]
                        },
                        form: {
                            connectAccount: {
                                label: 'Bestaand account koppelen aan deze klus?',
                                helperText: 'U kunt hier inloggen indien u al een account heeft.',
                                button: 'Inloggen'
                            },
                            name: {
                                label: 'Naam',
                                helperText: 'Dit is uw volledige naam.',
                                placeholder: 'Uw volledige naam'
                            },
                            postalCode: {
                                label: 'Postcode',
                                helperText: 'Dit is de postcode die weergegeven wordt bij uw klussen.',
                                placeholder: 'Uw postcode'
                            }
                        },
                        outro: {
                            title: 'Hierna',
                            content: [
                                'Na deze stap komt u bij de stap "Foto\'s".',
                                'Hier kunt u foto\'s toevoegen aan uw klus aanvraag.'
                            ]
                        }
                    },
                    pictures: {
                        intro: {
                            title: 'Foto\'s',
                            content: (imageCount: number) => [
                                'U bent bij de stap "Foto\'s".',

                                ...imageCount < 5
                                    ? [`U kunt nog ${5 - imageCount} foto's toevoegen aan uw klus.`]
                                    : [`U kunt geen foto's meer toevoegen aan uw klus.`]
                            ]
                        },
                        pictures: {
                            newPicture: {
                                label: 'Foto toevoegen'
                            }
                        },
                        outro: {
                            title: 'Hierna',
                            content: [
                                'Na deze stap komt u bij de stap "Inloggegevens".',
                                'Hier wordt u gevraagd om uw e-mailadres en wachtwoord om in te loggen op uw profiel.'
                            ],
                            authorizedContent: [
                                'Na deze stap wordt uw klus geplaatst.'
                            ]
                        }
                    }
                }
            }
        },

        costs: {
            pageSectionContent: (
                <div>
                    <p>
                        Bij dakklusje.nl begrijpen wij dat er aan kwaliteit een prijskaartje hangt. 
                    </p><p>
                        Aangezien de dakspecialisten even belangrijk voor ons zijn als de consument willen wij zo goed mogelijk proberen te waarborgen dat ook zij tevreden zijn met het platform.
                    </p><p>
                        Bij ons platform bent u daarentegen verzekerd van hoge kwaliteit dakspecialisten.
                    </p><p>
                        Klik op de onderstaande knop om de gemiddelde prijs te zien per type klus. Deze baseren wij op het gemiddelde van de prijzen die wij bij dakspecialisten op de website hebben zien staan.
                    </p>
                </div>
            ),
            buttons: {
                calculateCosts: 'Bereken de kosten voor uw klus'
            }
        },

        home: {
            hero: {
                buttons: {
                    addJob: 'Plaats je klus',
                    registerAsRoofSpecialist: 'Inschrijven als dakspecialist'
                },
                title: ['Klusje aan het dak?', 'Dakklusje.nl!'],
                subtitle: 'Plaats geheel gratis en vrijblijvend uw klus.'
            },
            steps: {
                1: {
                    title: 'Plaats uw klus',
                    content: 'Plaats uw klus in een paar eenvoudige stappen.'
                },
                2: {
                    title: 'Dakspecialisten reageren',
                    content: 'De juiste dakspecialisten worden direct op de hoogte gesteld van uw klus per e-mail. Hierdoor wordt u zo snel mogelijk geholpen.'
                },
                3: {
                    title: 'Ga het gesprek aan',
                    content: 'Uw gegevens blijven afgeschermd totdat u het connectieverzoek van een dakspecialist accepteert. Wanneer u tevreden bent met een voorstel kunt u er vervolgens voor kiezen om de klus door het bedrijf uit te laten voeren.'
                },
                title: 'Hoe werkt Dakklusje.nl?'
            }
        },

        howDoesItWorkPage: {
            actors: {
                buttons: {
                    consumer: 'Ik ben een consument',
                    roofSpecialist: 'Ik ben een dakspecialist'
                },
                title: 'Hoe werkt het?'
            },
            consumer: {
                slides: [
                    {
                        title: 'Plaats uw klus via de homepagina.',
                        imageURL: '/img/how-does-it-work/consumer/1.png'
                    },
                    {
                        title: 'Uw klus komt tussen de lijst van de dakspecialisten terecht. Zodra er iemand heeft gereageerd op uw klus krijgt u een mail binnen!',
                        imageURL: '/img/how-does-it-work/consumer/2.png'
                    },
                    {
                        title: 'Selecteer een passend bedrijf voor uw klus in de inbox en kom in gesprek met dakspecialisten.',
                        imageURL: '/img/how-does-it-work/consumer/3.png'
                    },
                    {
                        title: 'Mobiele gebruikers kunnen de app installeren door onze website toe te voegen aan het homescherm!',
                        imageURL: '/img/how-does-it-work/shared/4.png'
                    }
                ],
            },
            roofSpecialist: {
                slides: [
                    {
                        title: 'Schrijf u in als dakspecialist via onze homepage.',
                        imageURL: '/img/how-does-it-work/roof-specialist/1.png'
                    },
                    {
                        title: 'Reageer op een klus en betaal alleen een klein bedrag (20-30 EU, afhankelijk van de klus) wanneer de gebruiker uw connectieverzoek heeft geaccepteerd!',
                        imageURL: '/img/how-does-it-work/roof-specialist/2.png'
                    },
                    {
                        title: 'Ga het gesprek aan met de consument en maak afspraken over de klus waarop u heeft gereageerd via de inbox.',
                        imageURL: '/img/how-does-it-work/roof-specialist/3.png'
                    },
                    {
                        title: 'Mobiele gebruikers kunnen de app installeren door onze website toe te voegen aan het homescherm!',
                        imageURL: '/img/how-does-it-work/shared/4.png'
                    }
                ]
            }
        },

        inbox: {
            contacts: {
                top: {
                    title: 'Inbox',
                    filter: {
                        placeholder: 'Filter'
                    }
                },
                content: {
                    connection: {
                        name: {
                            anonymous: 'Anoniem'
                        },
                        job: 'Klus',
                        preview: {
                            conversationEnded: 'Het gesprek is beëindigd.',
                            notAcceptedYet: 'Uw connectieverzoek is nog niet geaccepteerd.',
                            accepted: 'Uw connectieverzoek is geaccepteerd.',
                            declined: 'Uw connectieverzoek is geweigerd.',
                            costsAccepted: 'De dakspecialist zal spoedig reageren.',
                            costsNotAcceptedYet: 'De dakspecialist zal spoedig reageren.',
                            costsDeclined: 'De dakspecialist heeft het aanbod ingetrokken.',
                            newConnection: 'Heeft u een connectieverzoek gestuurd.'
                        }
                    }
                }
            },

            content: {
                returnToInbox: 'Terug naar inbox',

                chat: {
                    messages: {
                        noMessages: {
                            title: 'Nog geen berichten',
                            description: 'U heeft nog geen bericht gestuurd naar de consument. Stuur nu het eerste bericht om het gesprek aan te gaan!'
                        }
                    },

                    top: {
                        consumer: {
                            buttons: {
                                viewProfile: 'Bekijk profiel'
                            }
                        }
                    },

                    bottom: {
                        newMessage: {
                            placeholder: 'Typ hier uw bericht'
                        }
                    }
                },

                companyProfile: {
                    top: {
                        buttons: {
                            returnToChat: 'Terug naar gesprek'
                        },
                        reviews: (reviews: ReviewResponse[]): string => {
                            if (reviews.length === 0) {
                                return 'Nog geen recensies.'
                            }

                            const averageRating = reviews.reduce((x, y) => x + y.rating, 0) / reviews.length
                            return `${averageRating.toFixed(1)} / 5.0 van de ${reviews.length} recensies.`
                        }
                    },
                    tabs: {
                        companyDetails: {
                            label: 'Bedrijfsgegevens',

                            content: {
                                branche: {
                                    label: 'Branche'
                                },
                                cocNumber: {
                                    label: 'KvK nummer'
                                },
                                description: {
                                    label: 'Beschrijving'
                                },
                                vca: {
                                    label: 'VCA',
                                    value: (vcaNumber: string) => {
                                        return `Deze dakspecialist beschikt over een VCA certificaat (diploma nummer: ${vcaNumber}).`
                                    }
                                }
                            }
                        },
                        portfolio: {
                            label: (connection: InboxConnectionResponse) => {
                                return `Portfolio (${connection.roofSpecialist.portfolioImages.length})`
                            }
                        },
                        reviews: {
                            label: (reviews: ReviewResponse[]) => {
                                return `Recensies (${reviews.length})`
                            },
                            
                            content: {
                                noReviews: {
                                    title: 'Nog geen recensies.',
                                    description: 'U kunt de eerste zijn!'
                                }
                            }
                        }
                    }
                },

                consumer: {
                    newConnection: {
                        lookAtProfile: 'Bekijk profiel',
                        title: (connection: InboxConnectionResponse): string => {
                            return `${connection.roofSpecialist.cocCompany.companyName} heeft gereageerd op uw klus: "${connection.job.title}".`
                        },
                        buttons: {
                            decline: 'Gesprek weigeren',
                            accept: 'Gesprek accepteren'
                        }
                    },

                    pending: {
                        title: (connection: InboxConnectionResponse): string => {
                            return `${connection.roofSpecialist.cocCompany.companyName} heeft nog geen bericht gestuurd voor uw klus "${connection.job.title}".`
                        },
                        description: 'De dakspecialist zal zo spoedig mogelijk iets naar u terug sturen, het bedrijf is ervan op de hoogte dat u het gesprek heeft geaccepteerd.',
                        buttons: {
                            goToProfile: 'Ga naar profiel'
                        }
                    },

                    costsDeclined: {
                        title: (connection: InboxConnectionResponse): string => {
                            if (connection.costsDeclinedAt && connection.costsAcceptedAt) {
                                return `${connection.roofSpecialist.cocCompany.companyName} heeft het gesprek beëindigd.`
                            }
                            return `${connection.roofSpecialist.cocCompany.companyName} heeft het aanbod op uw klus "${connection.job.title}" ingetrokken.`
                        },
                        buttons: {
                            delete: 'Verwijder'
                        }
                    }
                },

                roofSpecialist: {
                    pending: {
                        title: (connection: InboxConnectionResponse): string => {
                            return `${connection.consumer && connection.consumer.personalDetailsFullName ? connection.consumer.personalDetailsFullName : TranslationsNLNL.pages.inbox.contacts.content.connection.name.anonymous} heeft uw reactie op de klus "${connection.job.title}" nog niet beantwoord.`
                        },
                        description: 'Het kan even duren voordat de consument een reactie heeft gegeven. In de tussentijd kunt u zoeken naar andere klussen op de klussen pagina.',
                        buttons: {
                            rejectOffer: 'Aanbod intrekken'
                        }
                    },

                    declined: {
                        title: (connection: InboxConnectionResponse): string => {
                            if (connection.acceptedAt && connection.declinedAt) {
                                return `${connection.consumer && connection.consumer.personalDetailsFullName ? connection.consumer.personalDetailsFullName : TranslationsNLNL.pages.inbox.contacts.content.connection.name.anonymous} heeft het gesprek beëindigd.`
                            }
                            return `${connection.consumer && connection.consumer.personalDetailsFullName ? connection.consumer.personalDetailsFullName : TranslationsNLNL.pages.inbox.contacts.content.connection.name.anonymous} heeft uw reactie op de klus: "${connection.job.title}" geweigerd.`
                        },
                        buttons: {
                            delete: 'Verwijder'
                        }
                    },

                    costs: {
                        title: (connection: InboxConnectionResponse): string => {
                            return `${connection.consumer && connection.consumer.personalDetailsFullName ? connection.consumer.personalDetailsFullName : TranslationsNLNL.pages.inbox.contacts.content.connection.name.anonymous} heeft uw reactie op de klus: "${connection.job.title}" geaccepteerd.`
                        },
                        description: (connection: InboxConnectionResponse): string => {
                            return `Voor de acceptatie van de klus zullen er ${JobCosts.calculateCosts(connection.job)} euro ex. BTW (${AppInfo.vatPercentage}%) in rekening worden gebracht.`
                        },
                        buttons: {
                            declineCosts: 'Kosten weigeren',
                            acceptCosts: 'Kosten accepteren'
                        }
                    }
                },

                noSelection: {
                    roofSpecialist: {
                        title: 'Selecteer een bericht',
                        description: 'Selecteer een bericht uit de lijst om een gesprek aan te gaan met een consument.'
                    },
                    consumer: {
                        title: 'Selecteer een bericht',
                        description: 'Selecteer een bericht uit de lijst om een gesprek aan te gaan met een dakspecialist.'
                    }
                },

                noContacts: {
                    roofSpecialist: {
                        title: 'Nog geen contacten',
                        description: 'Zoek een klus via de klussen pagina. Zodra u daar een klus aanneemt zal de consument in de lijst van uw inbox terecht komen!',
                        button: 'Naar klussen pagina'
                    },
                    consumer: {
                        title: 'Nog geen berichten',
                        description: 'Zodra een dakspecialist u een verzoek stuurt vind u dit hier terug.'
                    }
                }
            }
        },

        jobs: {
            consumer: {
                job: {
                    jobsConsumerJob: {
                        connectionCount: (count: number) => {
                            if (count === 1) {
                                return `${count} reactie`
                            } else {
                                return `${count} reacties`
                            }
                        },
                        noDescription: 'Geen beschrijving'
                    }
                },
                wizard: {
                    top: {
                        title: 'Klussen',
                        description: 'In dit overzicht vind u al uw geplaatste klussen.'
                    },
                    content: {
                        entries: {
                            noEntries: {
                                title: 'Nog geen klussen',
                                description: 'Zodra u uw eerste klus heeft geplaatst zal deze hier in het overzicht verschijnen!',
                                buttons: 'Gratis een klus plaatsen'
                            }
                        }
                    }
                }
            },

            roofSpecialist: {
                job: {
                    detail: {
                        tabs: {
                            specifications: 'Specificaties',
                            pictures: (pictureCount: number) => {
                                return `Foto's (${pictureCount})`
                            },
                            comment: 'Reageer op deze klus!'
                        },
                        content: {
                            specifications: {
                                top: {
                                    left: {
                                        title: 'Beschrijving',
                                        noDescription: 'Geen beschrijving'
                                    },
                                    right: {
                                        title: 'Kosten',
                                        subtitle: (costs: number, vatPercentage: number) => {
                                            return `€${costs.toFixed(2)} exclusief ${vatPercentage}% BTW`
                                        },
                                        content: 'Wanneer u reageert op deze klus wordt er een connectieverzoek voor de inbox gestuurd naar deze consument. Wanneer hij/zij dat verzoek heeft geaccepteerd kunt u het gesprek aangaan en worden deze kosten na uw toestemming in rekening gebracht bij u. De hoogte van het bedrag is over het algemeen 30 euro, echter ligt dat bedrag lager bij kleine klusjes.'
                                    }
                                },
                                bottom: {
                                    branche: 'Branche',
                                    surfaceMeasurements: {
                                        [MeasurementUnit.SQUARE_METER]: 'Aantal vierkante meter',
                                        [MeasurementUnit.METER]: 'Aantal meter'
                                    },
                                    roofType: 'Type dak',
                                    jobCategory: 'Soort klus',
                                    houseType: 'Type woning',
                                    roofMaterialType: 'Dakmateriaal',
                                    roofIsolationType: 'Dakisolatie'
                                }
                            },
                            photos: {
                                noPhotos: {
                                    title: `Geen foto's`,
                                    description: `Aan deze klus zijn geen foto's toegevoegd door de consument.`
                                }
                            }
                        }
                    },
                    jobsRoofSpecialistJob: {
                        connectionCount: (count: number) => {
                            if (count === 1) {
                                return `${count} reactie`
                            } else {
                                return `${count} reacties`
                            }
                        },
                        noDescription: 'Geen beschrijving'
                    }
                },
                wizard: {
                    top: {
                        title: 'Klussen',
                        description: [
                            'In dit overzicht vind u alle klussen geplaatst door consumenten.',
                            'U ontvangt automatisch mails wanneer er nieuwe klussen zijn die aan uw filters voldoen.'
                        ]
                    },
                    content: {
                        content: {
                            container: {
                                jobDetail: {
                                    backToOverview: 'Terug naar overzicht'
                                }
                            }
                        },
                        entries: {
                            noEntries: {
                                title: 'Geen klussen gevonden',
                                description: 'Er zijn geen klussen gevonden die voldoen aan uw criteria.'
                            }
                        }
                    }
                }
            }
        },

        profile: {
            consumer: {
                title: 'Uw profiel',

                tabs: {
                    personalDetails: 'Persoonlijke gegevens',
                    manage: 'Account beheren'
                },

                personalDetails: {
                    wizard: {
                        content: {
                            intro: [
                                'Hier vind u al uw persoonsgegevens.',
                                'Dit zijn de gegevens die u in heeft gevoerd tijdens het plaatsen van uw eerste klus.'
                            ],
                            form: {
                                fullName: {
                                    label: 'Naam',
                                    helperText: 'Dit is uw volledige naam.',
                                    placeholder: 'Uw naam'
                                },
                                postalCode: {
                                    label: 'Postcode',
                                    helperText: 'Dit is de postcode die weergegeven wordt bij uw klussen.',
                                    placeholder: 'Uw postcode'
                                },
                                emailAddress: {
                                    label: 'E-mailadres',
                                    helperText: 'Dit is uw e-mailadres',
                                    placeholder: 'Uw e-mailadres'
                                },
                                password: {
                                    label: 'Nieuwe wachtwoord',
                                    helperText: 'Vul dit veld in als u uw wachtwoord wilt veranderen.',
                                    placeholder: 'Uw nieuwe wachtwoord'
                                },
                                passwordRepeat: {
                                    label: 'Wachtwoord herhalen',
                                    helperText: 'Typ ter controle nogmaals uw nieuwe wachtwoord in.',
                                    placeholder: 'Uw nieuwe wachtwoord'
                                },
                                currentPassword: {
                                    label: 'Huidige wachtwoord',
                                    helperText: 'Typ ter beveiliging uw huidige wachtwoord in.',
                                    placeholder: 'Uw huidige wachtwoord'
                                }
                            }
                        }
                    }
                },

                manage: {
                    wizard: {
                        content: {
                            intro: 'Wij zullen uw gegevens alleen gebruiken voor uiterst essentiële stukken functionaliteit op ons platform. Echter bieden wij u ook de mogelijkheid om uw account te deactiveren als u dat zou willen. Als u uw account deactiveerd verwijderd u alle persoonsgegevens en dient u deze weer opnieuw in te vullen wanneer u weer gebruik wilt kunnen maken van dakklusje.nl.',
                            form: {
                                buttons: {
                                    deactivateAccount: 'Account deactiveren'
                                },
                                destructiveActionModal: 'Wanneer u doorgaat met deze actie wordt uw profiel gedeactiveerd. Als u daarna weer gebruik wilt maken van dakklusje.nl dient u weer in te loggen en uw persoonlijke gegevens bij te werken.'
                            }
                        }
                    }
                }
            },

            roofSpecialist: {
                wizard: {
                    top: {
                        title: 'Uw profiel'
                    },

                    tabs: {
                        companyDetails: 'Bedrijfs gegevens',
                        portfolio: 'Portfolio',
                        reviews: 'Reviews',
                        paymentDetails: 'Betaal gegevens'
                    },

                    intro: {
                        companyDetails: {
                            title: 'Bedrijfsgegevens',
                            description: [
                                'Hier kunt u uw bedrijfsgegevens inzien en aanpassen.',
                                'Dit zijn de gegevens die u in heeft gevoerd tijdens uw inschrijving.'
                            ]
                        },
                        portfolio: {
                            title: 'Portfolio',
                            description: [
                                'Hier kunt u uw portfolio bijhouden.',
                                'U kunt hier foto\'s toevoegen die consumenten zullen zien wanneer ze uw profiel bekijken.'
                            ]
                        },
                        reviews: {
                            title: 'Reviews',
                            description: [
                                'Hier komen uw reviews te staan.',
                                'Deze zijn geschreven door consumenten waarvoor u klussen heeft gedaan.'
                            ],
                            descriptionProfileComplete: [
                                'Hier staan uw reviews.',
                                'Deze zijn geschreven door consumenten waarvoor u klussen heeft gedaan.'
                            ]
                        },
                        paymentDetails: {
                            title: 'Betaalgegevens',
                            description: [
                                'Hier kunt u uw betaalgegevens beheren.',
                                'Daarnaast kunt u hier ook uw facturen downloaden en uw credits aanvullen.',
                                'Binnenkort kunt u ook d.m.v. automatische incasso betalingen doen.'
                            ]
                        }
                    },

                    content: {
                        companyDetails: {
                            form: {
                                branche: {
                                    title: 'Branche',
                                    helperText: 'Dit is de branche waarin u zich specialiseerd.'
                                },
                                description: {
                                    title: 'Beschrijving',
                                    helperText: 'Beschrijf uw werkzaamheden voor de consument.'
                                }
                            },
                            coc: {
                                title: 'KvK',
                                description: 'Uw KvK gegevens',
                                content: {
                                    cocNumber: {
                                        label: 'KvK nummer',
                                        helperText: 'Dit is uw KvK nummer.'
                                    },
                                    address: {
                                        label: 'Adres',
                                        helperText: 'Dit is het vestigingsadres van uw bedrijf.'
                                    },
                                    companyName: {
                                        label: 'Bedrijfsnaam',
                                        helperText: 'Dit is uw bedrijfsnaam.'
                                    },
                                    postalCode: {
                                        label: 'Postcode',
                                        helperText: 'Dit is de postcode van uw bedrijf.'
                                    }
                                }
                            },
                            contactPerson: {
                                title: 'Contactpersoon',
                                description: 'Uw naam en telefoonnummer',
                                content: {
                                    name: {
                                        label: 'Naam',
                                        helperText: 'Uw volledige naam',
                                        placeholder: 'Uw volledige naam'
                                    },
                                    phoneNumber: {
                                        label: 'Telefoonnummer',
                                        helperText: 'Vul hier uw telefoonnummer in.',
                                        placeholder: 'Uw telefoonnummer'
                                    }
                                }
                            },
                            loginDetails: {
                                title: 'Inloggegevens',
                                description: 'Wijzig uw e-mailadres en/of wachtwoord',
                                content: {
                                    emailAddress: {
                                        label: 'E-mailadres',
                                        helperText: 'Uw e-mailadres',
                                        placeholder: 'Uw e-mailadres'
                                    },
                                    password: {
                                        label: 'Wachtwoord',
                                        helperText: 'Vul hier uw nieuwe wachtwoord indien u deze wilt wijzigen.',
                                        placeholder: 'Uw nieuwe wachtwoord'
                                    },
                                    passwordRepeat: {
                                        label: 'Wachtwoord herhalen',
                                        helperText: 'Vul ter controle nogmaals uw nieuwe wachtwoord in.',
                                        placeholder: 'Uw nieuwe wachtwoord'
                                    },
                                    passwordConfirmation: {
                                        label: 'Huidige wachtwoord',
                                        helperText: 'Vul ter beveiliging uw huidige wachtwoord in.',
                                        placeholder: 'Uw huidige wachtwoord'
                                    }
                                }
                            }
                        },
                        portfolio: {
                            title: 'Foto\'s',
                            description: (imageCount: number): string => {
                                if (imageCount === 0) {
                                    return 'Voeg minimaal 1 foto toe aan uw portfolio om uw profiel aantrekkelijker te maken voor de consument!'
                                }
                                else if (imageCount < 5) {
                                    return `U kunt nog ${5 - imageCount} foto's toevoegen.`
                                }
                                else {
                                    return 'U kunt geen foto\'s meer toevoegen.'
                                }
                            }
                        },
                        reviews: {
                            example: {
                                title: 'Uw aangenomen klus',
                                category: JobCategory.CONSTRUCTION_NEW_ROOF,
                                consumerName: 'Stan Hurks',
                                description: 'Prima ervaring. Ze plannen maximaal één maand vooruit, waardoor je niet lang hoeft te wachten. Komen gemaakte afspraken na. De...'
                            }
                        },
                        paymentDetails: {
                            invoices: {
                                label: 'Facturen',
                                helperText: 'Download uw facturen',
                                placeholder: 'Kies een maand',
                                buttons: {
                                    download: 'Download'
                                }
                            },
                            paymentMethod: {
                                label: 'Betaalmethode',
                                helperText: 'Selecteer uw betaalmethode',
                                placeholder: 'Maak een keuze'
                            },
                            credits: {
                                label: 'Credits',
                                helperText: 'U kunt d.m.v. credits klussen aannemen zonder direct kosten te betalen. Bij het aannemen van een klus worden in eerste instantie uw credits gebruikt voor de betaling.',
                                button: 'Credits opwaarderen'
                            }
                        }
                    },

                    outro: {
                        companyDetails: {
                            title: 'Hierna',
                            description: [
                                'Na deze stap komt u aan bij uw portfolio.',
                                'Hier kunt u foto\'s toevoegen die consumenten kunnen zien op uw profiel.'
                            ]
                        },
                        portfolio: {
                            title: 'Hierna',
                            description: [
                                'Na deze stap komt u aan bij uw reviews.',
                                'Hier kunt u zien wat consumenten vinden van uw werkzaamheden.'
                            ]
                        },
                        reviews: {
                            title: 'Hierna',
                            description: [
                                'Na deze stap komt u aan bij betaalgegevens.',
                                'Hier dient u een betaalmethode te selecteren voor wanneer u een klus aanneemt.'
                            ]
                        },
                        paymentDetails: {
                            title: 'Afronden',
                            description: [
                                'Voeg een betaalmethode toe om uw aanmelding te voltooien!'
                            ]
                        }
                    }
                }
            }
        },

        registerAsRoofSpecialist: {
            wizard: {
                top: {
                    title: 'Inschrijven als dakspecialist'
                },
                tabs: {
                    companyDetails: {
                        title: 'Bedrijfs gegevens',
                    },
                    coc: {
                        title: 'KvK'
                    },
                    contactPerson: {
                        title: 'Contact persoon'
                    },
                    loginDetails: {
                        title: 'Inlog gegevens'
                    }
                },
                content: {
                    intro: {
                        companyDetails: {
                            title: 'Bedrijfsgegevens',
                            description: [
                                'Vul in deze pagina uw basis bedrijfsgegevens in om uw inschrijving als dakspecialist te beginnen.'
                            ]
                        },
                        coc: {
                            title: 'KvK',
                            description: [
                                'Hieronder dient u uw KvK nummer in te vullen.',
                                'De overige velden worden automatisch aangevuld met uw bedrijfsgegevens.'
                            ]
                        },
                        contactPerson: {
                            title: 'Contactpersoon',
                            description: [
                                'Vul in dit formulier uw basis contactgegevens in.',
                                'Wij vragen u alleen om gegevens die essentieel zijn voor de werking van het platform.'
                            ]
                        },
                        loginDetails: {
                            title: 'Inloggegevens',
                            description: [
                                'Vul in dit formulier uw e-mailadres en wachtwoord in.',
                                'Hiermee kunt u inloggen op uw account en klussen aannemen voor uw bedrijf!'
                            ]
                        }
                    },
                    form: {
                        companyDetails: {
                            branche: {
                                label: 'Branche',
                                helperText: 'Dit is de branche waarin u zich specialiseerd.',
                                placeholder: 'Kies een branche'
                            },
                            companyDescription: {
                                label: 'Beschrijving',
                                helperText: 'Beschrijf uw werkzaamheden voor de consument.',
                                placeholder: 'Beschrijf hier in 100 - 1000 karakters wat uw bedrijf bijzonder maakt.'
                            }
                        },
                        coc: {
                            cocNumber: {
                                label: 'KvK nummer',
                                helperText: 'Vul hier uw KvK nummer in.',
                                placeholder: 'Uw KvK nummer'
                            },
                            cocCompanyName: {
                                label: 'Bedrijfsnaam',
                                helperText: 'Dit is uw bedrijfsnaam.'
                            },
                            cocAddress: {
                                label: 'Adres',
                                helperText: 'Dit is het vestigingsadres van uw bedrijf.'
                            },
                            postalCode: {
                                label: 'Postcode',
                                helperText: 'Dit is de postcode van uw bedrijf.'
                            }
                        },
                        contactPerson: {
                            name: {
                                label: 'Naam',
                                helperText: 'Vul hier uw volledige naam in.',
                                placeholder: 'Uw volledige naam'
                            },
                            phoneNumber: {
                                label: 'Telefoonnummer',
                                helperText: 'Vul hier uw telefoonnummer in.',
                                placeholder: 'Uw telefoonnummer'
                            }
                        },
                        loginDetails: {
                            emailAddress: {
                                label: 'E-mailadres',
                                helperText: 'Vul hier uw e-mailadres in.',
                                placeholder: 'Uw e-mailadres'
                            },
                            password: {
                                label: 'Wachtwoord',
                                helperText: 'Vul hier uw wachtwoord in.',
                                placeholder: 'Uw wachtwoord'
                            },
                            passwordRepeat: {
                                label: 'Wachtwoord herhalen',
                                helperText: 'Vul hier ter controle nogmaals uw wachtwoord in.',
                                placeholder: 'Uw wachtwoord'
                            },
                            termsAndAgreements: {
                                label: 'Algemene voorwaarden',
                                helperText: (
                                    <p>
                                        Om gebruik te maken van ons platform<br/>
                                        dient u akkoord te gaan met onze <a href={AppInfo.designedAndBuildBy.termsAndAgreementsLink}>algemene voorwaarden.</a>
                                    </p>
                                ),
                                checkbox: {
                                    label: 'Ik ga akkoord met de algemene voorwaarden en ik verklaar dit formulier naar waarheid ingevuld te hebben.'
                                }
                            }
                        }
                    },
                    outro: {
                        companyDetails: {
                            title: 'Hierna',
                            description: [
                                'Na deze stap komt u bij de stap "KvK".',
                                'Hier kunt u uw kvk gegevens automatisch invullen o.b.v. uw KvK nummer.'
                            ]
                        },
                        coc: {
                            title: 'Hierna',
                            description: [
                                'De volgende stap is "Contactpersoon".',
                                'Hier wordt u gevraagd om basis persoonsgegevens in te vullen.'
                            ]
                        },
                        contactPerson: {
                            title: 'Bijna klaar!',
                            description: [
                                'De volgende stap is "Inloggegevens".',
                                'Hier wordt u gevraagd om uw inloggegevens in te vullen.'
                            ]
                        },
                        loginDetails: {
                            title: 'Afronden',
                            description: [
                                'Double check uw gegevens..',
                                '..en klik op voltooien om uw inschrijving te voltooien.'
                            ]
                        }
                    },
                    bottom: {
                        loginDetails: {
                            finish: 'Voltooien'
                        }
                    }
                }
            }
        }
    }
}
export default TranslationsNLNL