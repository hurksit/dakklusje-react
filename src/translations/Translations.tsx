import TranslationsNLNL from './nl_NL/TranslationsNLNL'
import Storage from '../storage/Storage'

/**
 * The manager for all translation related functionality and functionality that depends on
 * translations. E.g.: Document meta tags and title.
 * 
 * @author Stan Hurks
 */
export default abstract class Translations {

    /**
     * All translations
     */
    public static translations: typeof TranslationsNLNL = TranslationsNLNL
    
    /**
     * The default language used when no language has been set
     */
    public static defaultLanguage: string = 'nl_NL'

    /**
     * The current language used by the application
     */
    public static currentLanguage: string = Translations.defaultLanguage

    /**
     * The available languages for the application
     */
    public static availableLanguages: Array<{
        label: string

        value: string 
    }> = [
        {
            label: 'Nederlands',
            value: 'nl_NL'
        }
    ]

    /**
     * Initialize the translation manager
     */
    public static initialize = () => {
        // Set the current language based on local storage or location GET parameter
        const langGetParameter: string|null = new URL(document.location.href).searchParams.get('lang')
        Translations.currentLanguage = Storage.data.session.user.locale
            || langGetParameter
            || 'nl_NL'

        // Set the translations
        Translations.translations = require(`./${
            Translations.currentLanguage
        }/Translations${
            Translations.currentLanguage.replace('_', '').toUpperCase()
        }.tsx`).default

        // Set the meta tags, html lang and title in the document
        if (document && document.documentElement && document.head) {
            document.documentElement.lang = Translations.translations.document.lang
            document.title = Translations.translations.document.title.main
            for (const key of Object.keys(Translations.translations.document.meta)) {
                const metaTag = document.head.querySelector(`meta[name="${key}"]`)
                if (metaTag) {
                    metaTag.setAttribute('content', Translations.translations.document.meta[key])
                }
            }
        }
    }

    /**
     * Get a translation
     * @param key the translation key
     * @param variables the variables (optional)
     */
    public static get = (key: string, variables?: {[key: string]: string}): string => {
        /**
         * Parse all variables in a value
         * @param value the value in which the variables are found
         */
        const parseVariables = (value: string): string => {
            if (variables) {
                for (const k of Object.keys(variables)) {
                    const v = variables[k]
                    value = value.replace(new RegExp(':' + k), v)
                }
                return value
            } else {
                return value
            }
        }

        if (!Translations.translations[Translations.currentLanguage][key]) {
            console.info('Key \'' + key + '\' missing in language: \'' 
                + Translations.currentLanguage + '\' falling back to language \'' 
                + Translations.defaultLanguage + '\'.')
            if (!Translations.translations[Translations.defaultLanguage][key]) {
                console.info('Key \'' + key + '\' undefined.')
                return parseVariables(key)
            }
            return parseVariables(Translations.translations[Translations.defaultLanguage][key])
        }
        return parseVariables(Translations.translations[Translations.currentLanguage][key])
    }
}

/**
 * Method to perform quick translates
 * @param key the translation key
 * @param variables the variables (optional)
 */
export const translate = (key: string, variables?: {[key: string]: string}) => Translations.get(key, variables)
