/**
 * All routes mapped with the link
 * 
 * @author Stan Hurks
 */
export enum Route {

    ABOUT_US = "/over-ons",
    
    CONSUMER_JOBS = "/klussen",

    COSTS = "/kosten",
    
    CONSUMER_PLACE_YOUR_JOB = "/klus",
    CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS = "/klus/persoonsgegevens",
    CONSUMER_PLACE_YOUR_JOB_PICTURES = "/klus/fotos",
    CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS = "/klus/inloggegevens",

    CONSUMER_EDIT_JOB = "/klus/:jobId",
    CONSUMER_EDIT_JOB_PICTURES = "/klus/:jobId/fotos",
    
    HOME = "/",

    HOW_DOES_IT_WORK = "/hoe-werkt-het",
    HOW_DOES_IT_WORK_CONSUMER = "/hoe-werkt-het/consument",
    HOW_DOES_IT_WORK_ROOF_SPECIALIST = "/hoe-werkt-het/dakspecialist",
    
    INBOX = "/inbox",
    
    PROFILE = "/profiel",

    PROFILE_CONSUMER_PERSONAL_DETAILS = "/profiel/persoonlijke-gegevens",
    PROFILE_CONSUMER_MANAGE = "/profiel/beheren",

    PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS = "/profiel/bedrijfsgegevens",
    PROFILE_ROOF_SPECIALIST_PORTFOLIO = "/profiel/portfolio",
    PROFILE_ROOF_SPECIALIST_REVIEWS = "/profiel/reviews",
    PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS = "/profiel/betaalgegevens",

    ROOF_SPECIALIST_JOBS = "/klussen",

    ROOF_SPECIALIST_REGISTER = "/inschrijven-als-dakspecialist",
    ROOF_SPECIALIST_REGISTER_COC = "/inschrijven-als-dakspecialist/kvk",
    ROOF_SPECIALIST_REGISTER_CONTACT_PERSON = "/inschrijven-als-dakspecialist/contactpersoon",
    ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS = "/inschrijven-als-dakspecialist/inloggegevens"
}