/**
 * All types of invalidation within a form.
 * 
 * @author Stan Hurks
 */
export enum FormInvalidation {

    /**
     * Whenever a null value is provided and not allowed
     */
    VALUE_IS_NULL = "VALUE_IS_NULL",

    /**
     * Whenever the value is not true
     */
    BOOLEAN_NOT_TRUE = "BOOLEAN_NOT_TRUE",

    /**
     * Whenever the value is not false
     */
    BOOLEAN_NOT_FALSE = "BOOLEAN_NOT_FALSE",

    /**
     * Whenever a string does not have the string datatype
     */
    STRING_NO_STRING = "STRING_NO_STRING",

    /**
     * Whenever a string is empty
     */
    STRING_EMPTY = "STRING_EMPTY",

    /**
     * Whenever a string has too few characters
     */
    STRING_TOO_SHORT = "STRING_TOO_SHORT",

    /**
     * Whenever a string has too many characters
     */
    STRING_TOO_LONG = "STRING_TOO_LONG",

    /**
     * Whenever a string does not match a pattern
     */
    STRING_PATTERN_NO_MATCH = "STRING_PATTERN_NO_MATCH",

    /**
     * Whenever the evaluated value does not match with the string
     */
    STRING_EVALUATED_VALUE_NOT_MATCHED = "STRING_EVALUATED_VALUE_NOT_MATCHED",

    /**
     * Whenever an array is invalidated by type
     */
    ARRAY_NO_ARRAY = "ARRAY_NO_ARRAY",

    /**
     * Whenever an array is empty
     */
    ARRAY_EMPTY = "ARRAY_EMPTY",

    /**
     * Whenever an array has an invalid value
     */
    ARRAY_INVALID_VALUE = "ARRAY_INVALID_VALUE",

    /**
     * Whenever theres not enough items in the array
     */
    ARRAY_TOO_FEW_ENTRIES = "ARRAY_TOO_FEW_ENTRIES",

    /**
     * Whenever theres too many items in the array
     */
    ARRAY_TOO_MANY_ENTRIES = "ARRAY_TOO_MANY_ENTRIES",

    /**
     * Whenever the number is not a number
     */
    NUMBER_NAN = "NUMBER_NAN",

    /**
     * Whenever the number is smaller than the minimum value
     */
    NUMBER_MIN_VALUE = "NUMBER_MIN_VALUE",

    /**
     * Whenever the number is greater than the maximum value
     */
    NUMBER_MAX_VALUE = "NUMBER_MAX_VALUE",
    
    /**
     * Whenever an enum is invalid
     */
    ENUM_INVALID = "ENUM_INVALID",

    /**
     * Whenever the value is not allowed
     */
    VALUE_NOT_ALLOWED = "VALUE_NOT_ALLOWED"
}