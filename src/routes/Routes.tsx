import * as React from 'react'
import {
    BrowserRouter as Router,
    Route as RouteComponent,
    Switch,
    Redirect,
} from 'react-router-dom'
import HomePage from '../pages/non-authorized-pages/home/HomePage'
import { Route } from '../enumeration/Route'
import AboutUsPage from '../pages/non-authorized-pages/about-us/AboutUsPage'
import { Subject, Subscription } from 'rxjs'
import Translations from '../translations/Translations'
import HowDoesItWorkPage from '../pages/non-authorized-pages/how-does-it-work/HowDoesItWorkPage'
import ProfileConsumerPage from '../pages/authorized-pages/profile/consumer/ProfileConsumerPage'
import Storage from '../storage/Storage'
import Authorization from '../core/Authorization'
import AddJobPage from '../pages/conditional-authorized-pages/add-job/AddJobPage'
import JobsConsumerPage from '../pages/authorized-pages/jobs/consumer/JobsConsumerPage'
import RegisterAsRoofSpecialistPage from '../pages/non-authorized-pages/register-as-roof-specialist/RegisterAsRoofSpecialistPage'
import ProfileRoofSpecialistPage from '../pages/authorized-pages/profile/roof-specialist/ProfileRoofSpecialistPage'
import JobsRoofSpecialistPage from '../pages/authorized-pages/jobs/roof-specialist/JobsRoofSpecialistPage'
import InboxPage from '../pages/authorized-pages/inbox/InboxPage'
import Navigation from '../components/navigation/Navigation'
import ReactGA from 'react-ga'

/**
 * The state
 */
interface RoutesState {

    /**
     * The redirect URL
     */
    redirectURL: Route | string | null
}

/**
 * All routes in the application should be put here.
 * 
 * @author Stan Hurks
 */
export default class Routes extends React.Component<any, RoutesState> {

    constructor(props: any) {
        super(props)

        this.state = {
            redirectURL: null
        }
    }

    /**
     * The RxJS subject to set the current route.
     */
    public static readonly setCurrentRoute: Subject<Route | { route: Route, params: { [key: string]: string } }> = new Subject()

    /**
     * The RxJS subject to redirect to a URL
     */
    public static readonly redirectURL: Subject<string> = new Subject()

    /**
     * The current route
     */
    public static currentRoute: Route = Route.HOME

    /**
     * The current domain.
     * 
     * The domain is used for sub-users to login to the application belonging to their parent user.
     */
    public static readonly domain: string | null = window.location.host.split('.').length > 2
        && !/^(http|https):\/\/((localhost)|((192|10|172))(.[0-9]{1,3}){3})(:([0-9]+))?$/.test(window.location.origin)
        ? window.location.host.split('.')[window.location.host.split('.').length - 3]
        : null

    /**
     * The internal subscription for changing the current route
     */
    private subscriptionSetCurrentRoute!: Subscription

    /**
     * The internal subscription for redirecting to a URL
     */
    private subscriptionRedirectURL!: Subscription

    public componentDidMount = () => {
        this.subscriptionSetCurrentRoute = Routes.setCurrentRoute.subscribe(this.onSetCurrentRoute)
        this.subscriptionRedirectURL = Routes.redirectURL.subscribe(this.onRedirectURL)
    }

    public componentWillUnmount = () => {
        this.subscriptionSetCurrentRoute.unsubscribe()
        this.subscriptionRedirectURL.unsubscribe()
    }

    public render() {
        return (
            <Router>
                <Switch>

                    {/* Redirect if requested */}
                    {
                        this.state.redirectURL !== null
                        &&
                        <Redirect to={this.state.redirectURL} push />
                    }

                    {/* Home page */}
                    <RouteComponent exact path={Route.HOME} render={(params) =>
                        <HomePage {...params} />
                    } />

                    {/* Restore password */}
                    <RouteComponent path="/wachtwoord-herstellen/:restorePasswordUuid" render={(params) =>
                        <HomePage {...params} />
                    } />

                    {/* Activate account */}
                    <RouteComponent path="/account-activeren/:activateAccountUuid" render={(params) =>
                        <HomePage {...params} />
                    } />

                    {/* Activate email */}
                    <RouteComponent path="/email-activeren/:activateEmailUuid" render={(params) =>
                        <HomePage {...params} />
                    } />

                    {/* About us */}
                    <RouteComponent exact path={Route.ABOUT_US} render={() =>
                        <AboutUsPage />
                    } />

                    {/* How does it work */}
                    <RouteComponent exact path={Route.HOW_DOES_IT_WORK} render={() =>
                        <HowDoesItWorkPage />
                    } />

                    {/* How does it work -> consumer */}
                    <RouteComponent exact path={Route.HOW_DOES_IT_WORK_CONSUMER} render={() =>
                        <HowDoesItWorkPage initialViewIndex={0} />
                    } />

                    {/* How does it work -> roof specialist */}
                    <RouteComponent exact path={Route.HOW_DOES_IT_WORK_ROOF_SPECIALIST} render={() =>
                        <HowDoesItWorkPage initialViewIndex={2} />
                    } />

                    {/* Profile */}
                    <RouteComponent exact path={Route.PROFILE} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.consumer
                                    ?
                                    <Redirect to={Route.PROFILE_CONSUMER_PERSONAL_DETAILS} />
                                    :
                                    <Redirect to={Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Profile - consumer - personal details */}
                    <RouteComponent exact path={Route.PROFILE_CONSUMER_PERSONAL_DETAILS} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.consumer
                                    ?
                                    <ProfileConsumerPage route={Route.PROFILE_CONSUMER_PERSONAL_DETAILS} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Profiel - consumer - manage */}
                    <RouteComponent exact path={Route.PROFILE_CONSUMER_MANAGE} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.consumer
                                    ?
                                    <ProfileConsumerPage route={Route.PROFILE_CONSUMER_MANAGE} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Profile - roof specialist - company details */}
                    <RouteComponent exact path={Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.roofSpecialist
                                    ?
                                    <ProfileRoofSpecialistPage route={Route.PROFILE_ROOF_SPECIALIST_COMPANY_DETAILS} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Profile - roof specialist - portfolio */}
                    <RouteComponent exact path={Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.roofSpecialist
                                    ?
                                    <ProfileRoofSpecialistPage route={Route.PROFILE_ROOF_SPECIALIST_PORTFOLIO} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Profile - roof specialist - reviews */}
                    <RouteComponent exact path={Route.PROFILE_ROOF_SPECIALIST_REVIEWS} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.roofSpecialist
                                    ?
                                    <ProfileRoofSpecialistPage route={Route.PROFILE_ROOF_SPECIALIST_REVIEWS} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Profile - roof specialist - payment details */}
                    <RouteComponent exact path={Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS} render={() =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.roofSpecialist
                                    ?
                                    <ProfileRoofSpecialistPage route={Route.PROFILE_ROOF_SPECIALIST_PAYMENT_DETAILS} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Plaats je klus */}
                    <RouteComponent exact path={Route.CONSUMER_PLACE_YOUR_JOB} render={(params) =>
                        !Authorization.isAuthorized() || Storage.data.session.user.consumer
                            ? <AddJobPage route={Route.CONSUMER_PLACE_YOUR_JOB} {...params} navigateToLastUnlockedTab={true} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Plaats je klus - persoonsgegevens */}
                    <RouteComponent exact path={Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS} render={(params) =>
                        !Authorization.isAuthorized() || Storage.data.session.user.consumer
                            ? <AddJobPage route={Route.CONSUMER_PLACE_YOUR_JOB_PERSONAL_DETAILS} {...params} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Plaats je klus - foto's */}
                    <RouteComponent exact path={Route.CONSUMER_PLACE_YOUR_JOB_PICTURES} render={(params) =>
                        !Authorization.isAuthorized() || Storage.data.session.user.consumer
                            ? <AddJobPage route={Route.CONSUMER_PLACE_YOUR_JOB_PICTURES} {...params} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Plaats je klus - inloggegevens */}
                    <RouteComponent exact path={Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS} render={(params) =>
                        !Authorization.isAuthorized() || Storage.data.session.user.consumer
                            ? <AddJobPage route={Route.CONSUMER_PLACE_YOUR_JOB_LOGIN_DETAILS} {...params} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Plaats je klus - aanpassen */}
                    <RouteComponent exact path={Route.CONSUMER_EDIT_JOB} render={(params) =>
                        !Authorization.isAuthorized() || Storage.data.session.user.consumer
                            ? <AddJobPage route={Route.CONSUMER_EDIT_JOB} navigateToLastUnlockedTab={true} {...params} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Plaats je klus - aanpassen - foto's */}
                    <RouteComponent exact path={Route.CONSUMER_EDIT_JOB_PICTURES} render={(params) =>
                        !Authorization.isAuthorized() || Storage.data.session.user.consumer
                            ? <AddJobPage route={Route.CONSUMER_EDIT_JOB_PICTURES} {...params} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Jobs page - all */}
                    <RouteComponent exact path={Route.CONSUMER_JOBS} render={(params) =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.consumer
                                    ?
                                    <JobsConsumerPage {...params} />
                                    :
                                    <JobsRoofSpecialistPage {...params} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Jobs page - all - with page number */}
                    <RouteComponent exact path={`${Route.CONSUMER_JOBS}/:pageNumber`} render={(params) =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.consumer
                                    ?
                                    <JobsConsumerPage {...params} />
                                    :
                                    <JobsRoofSpecialistPage {...params} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Jobs page - all - with filter - with page number */}
                    <RouteComponent exact path={`${Route.CONSUMER_JOBS}/:pageNumber/:filter`} render={(params) =>
                        Authorization.isAuthorized()
                            ? (
                                Storage.data.session.user.consumer
                                    ?
                                    <JobsConsumerPage {...params} />
                                    :
                                    <Redirect to={Route.HOME} />
                            )
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Register as roof specialist - company details */}
                    <RouteComponent exact path={Route.ROOF_SPECIALIST_REGISTER} render={() =>
                        !Authorization.isAuthorized()
                            ? <RegisterAsRoofSpecialistPage route={Route.ROOF_SPECIALIST_REGISTER} navigateToLastUnlockedTab={true} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Register as roof specialist - coc */}
                    <RouteComponent exact path={Route.ROOF_SPECIALIST_REGISTER_COC} render={() =>
                        !Authorization.isAuthorized()
                            ? <RegisterAsRoofSpecialistPage route={Route.ROOF_SPECIALIST_REGISTER_COC} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Register as roof specialist - contact details */}
                    <RouteComponent exact path={Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON} render={() =>
                        !Authorization.isAuthorized()
                            ? <RegisterAsRoofSpecialistPage route={Route.ROOF_SPECIALIST_REGISTER_CONTACT_PERSON} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Register as roof specialist - login details */}
                    <RouteComponent exact path={Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS} render={() =>
                        !Authorization.isAuthorized()
                            ? <RegisterAsRoofSpecialistPage route={Route.ROOF_SPECIALIST_REGISTER_LOGIN_DETAILS} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Inbox */}
                    <RouteComponent exact path={Route.INBOX} render={(params) =>
                        Authorization.isAuthorized()
                            ? <InboxPage {...params} />
                            : <Redirect to={Route.HOME} />
                    } />

                    {/* Redirect when there is no match */}
                    <RouteComponent render={() =>
                        <Redirect to={this.getFirstPageURL()} />
                    } />

                </Switch>
            </Router>
        )
    }

    /**
     * Whenever the current route has changed
     */
    private onSetCurrentRoute = (payload: Route | { route: Route, params: { [key: string]: string } }) => {
        const route: Route = typeof payload === 'string'
            ? payload
            : payload.route

        let href: string = route

        document.title = Translations.translations.document.title.main + ' - '
            + Translations.translations.document.title.route(route)

        if (typeof payload !== 'string') {
            for (const param of Object.keys(payload.params)) {
                href = href.replace(`:${param}`, payload.params[param])
            }
        }

        if (href !== window.location.pathname) {
            window.history.pushState('', '', href)
        }

        if (route === Route.INBOX) {
            document.body.classList.add('background-white')
            document.body.classList.remove('background-purple')
        } else {
            document.body.classList.add('background-purple')
            document.body.classList.remove('background-white')
        }

        // Google analytics
        setTimeout(() => {
            ReactGA.pageview(window.location.pathname + window.location.search)
        })

        Navigation.hide.next(false)
    }

    /**
     * Whenever there is a redirect request for a URL
     */
    private onRedirectURL = (redirectURL: string) => {
        this.setState({
            redirectURL
        }, () => {
            this.setState({
                redirectURL: null
            })
        })
    }

    /**
     * Get the first page url
     */
    private getFirstPageURL = () => {
        return Route.HOME
    }
}